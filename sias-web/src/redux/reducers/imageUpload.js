import * as ActionTypes from "../actions/ActionTypes";

export const ImageUpload = (state = {
    isUploading: false,
    errMess: null,
    successStatus: false,
    dataToInsert: null
}, action) => {
    // console.log(action);
    switch (action.type) {
        case ActionTypes.UPLOAD_IMAGE_SUCCESSFULLY:
            return {
                ...state,
                isUploading: false,
                errMess: null,
                successStatus: true,
                dataToInsert: action.payload
            }

        case ActionTypes.UPLOAD_IMAGE_FAILED:
            return {
                ...state,
                isUploading: false,
                errMess: action.payload,
                successStatus: false,
                dataToInsert: null
            }

        case ActionTypes.SUBMIT_FAILED:
            return {
                ...state,
                isUploading: false,
                errMess: action.payload,
                successStatus: false,
                dataToInsert: null
            }

        case ActionTypes.UPLOADING_IMAGE:
            return {
                ...state,
                isUploading: true,
                errMess: null,
                successStatus: false,
                // dataToInsert: null
            }
        default:
            return state;
    }
}