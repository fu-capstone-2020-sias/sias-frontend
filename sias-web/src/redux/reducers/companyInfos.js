import * as ActionTypes from "../actions/ActionTypes";

export const CompanyInfos = (state = {
    errMess: null,
    companyInfo: null,
    successStatus: false,
    successMessage: null
}, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_COMPANY_INFO:
            return {
                ...state,
                companyInfo: action.payload.companyInfo,
                successStatus: true
            }
        case ActionTypes.COMPANY_INFO_FAILED:
            return {
                ...state,
                errMess: action.payload,
                companyInfo: null,
                successStatus: false,
                successMessage: null
            }
        case ActionTypes.MODIFY_COMPANY_INFO_SUCCESS:
            return {
                ...state,
                errMess: null,
                successStatus: true,
                successMessage: action.payload
            }
        case ActionTypes.MODIFY_COMPANY_INFO_FAILED:
            return {
                ...state,
                errMess: action.payload, // array of error messages
                successStatus: false,
                successMessage: null
            }
        default:
            return state;
    }
}