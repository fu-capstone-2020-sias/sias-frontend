import * as ActionTypes from '../actions/ActionTypes';

export const Warehouses = (state = {
    isLoading: true,
    errMess: null,
    warehouses: null,
    currentPageNumber: 0,
    numberOfPages: 0,
    totalEntries: 0,
    searchData: null,
    successStatus: false,
    deleteSuccessStatus: false
}, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_WAREHOUSE:
            return {
                ...state,
                isLoading: false,
                errMess: null,
                warehouses: action.payload.warehouseList,
                numberOfPages: action.payload.totalPages,
                totalEntries: action.payload.count,
                currentPageNumber: action.payload.currentPageNumber
            };

        case ActionTypes.WAREHOUSE_FAILED:
            return { ...state, isLoading: false, errMess: action.payload };

        case ActionTypes.SUBMIT_FAILED:
            return { ...state, errMess: action.payload, deleteSuccessStatus: false, successStatus: false };

        case ActionTypes.DELETE_WAREHOUSE:
            return { ...state, errMess: null, deleteSuccessStatus: true };

        case ActionTypes.DELETE_WAREHOUSE_FAILED:
            return { ...state, errMess: action.payload, deleteSuccessStatus: false };

        default:
            return state;
    }
};

export const WarehouseSelections = (state = {
    isLoaded: false,
    errMess: null,
    selectionList: [],
    totalEntries: 0
}, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_WAREHOUSE_SELECTION:
            return {
                ...state,
                isLoaded: true,
                errMess: null,
                selectionList: action.payload.rows,
                totalEntries: action.payload.count,
            };

        case ActionTypes.WAREHOUSE_SELECTION_FAILED:
            return { ...state, isLoading: true, errMess: action.payload, totalEntries: 0 };

        default:
            return state;
    }
};