import * as ActionTypes from '../actions/ActionTypes';

export const Addresses = (state = {
    isLoaded: false,
    errMess: null,
    cityProvinceList: [],
    districtList: [],
    wardCommuneList: [],
    departmentList: [],
    managerList: [],
}, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_CITY_PROVINCE:
            return {
                ...state,
                errMess: null,
                cityProvinceList: action.payload,
                isLoaded: true
            };

        case ActionTypes.FETCH_DISTRICT:
            return {
                ...state,
                errMess: null,
                districtList: action.payload,
                isLoaded: true
            };

        case ActionTypes.FETCH_WARD_COMMUNE:
            return {
                ...state,
                errMess: null,
                wardCommuneList: action.payload,
                isLoaded: true
            };

        case ActionTypes.ADDRESS_FAILED:
            return { ...state, 
                isLoaded: true, 
                errMess: action.payload };
        
        case ActionTypes.FETCH_DEPARTMENT:
            return { ...state, 
                errMess: null,
                departmentList: action.payload,
                isLoaded: true 
            };
        
        case ActionTypes.FETCH_WAREHOUSE_MANAGER:
            return { ...state, 
                errMess: null,
                managerList: action.payload,
                isLoaded: true 
            };

        default:
            return state;
    }
};