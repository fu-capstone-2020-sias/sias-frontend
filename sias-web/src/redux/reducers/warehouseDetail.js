import * as ActionTypes from '../actions/ActionTypes';

export const WarehouseDetail = (state = {
    errMess: null,
    warehouse: null,
    items: null,
    successStatus: false,
    addedSuccessStatus: false,
    addedWarehouseId: null,
    isLoading: true,
    isAddWarehouseLoading: false
}, action) => {
    switch (action.type) {
        case ActionTypes.START_LOADING:
            return { ...state, isAddWarehouseLoading: true }

        case ActionTypes.FETCH_WAREHOUSE_DETAIL:
            return { ...state, isLoading: false, errMess: null, warehouse: action.payload, addedSuccessStatus: false };

        case ActionTypes.WAREHOUSE_DETAIL_FAILED:
            return { ...state, isLoading: false, errMess: action.payload, addedSuccessStatus: false };

        case ActionTypes.MODIFY_WAREHOUSE_DETAIL:
            return { ...state, errMess: null, successStatus: true, addedSuccessStatus: false }

        case ActionTypes.ADD_WAREHOUSE:
            return { ...state, isAddWarehouseLoading: false, errMess: null, addedSuccessStatus: true, addedWarehouseId: action.payload.newWarehouse.warehouseId }

        case ActionTypes.SUBMIT_FAILED:
            return { ...state, errMess: action.payload, successStatus: false, addedSuccessStatus: false, addedWarehouseId: null }

        default:
            return state;
    }
};

export const CurrentlyViewedWarehouse = (state = {
    currentWarehouseId: 0,
}, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_WAREHOUSE_DETAIL:
            return { ...state, currentWarehouseId: action.payload.warehouseId };

        case ActionTypes.WAREHOUSE_DETAIL_FAILED:
            return { ...state, currentWarehouseId: 0 };

        default:
            return state;
    }
};