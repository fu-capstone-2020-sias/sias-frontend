import * as ActionTypes from '../actions/ActionTypes';

export const OrderDetail = (state = {
    errMess: null,
    orderDetailErrMess: null,
    order: null,
    items: null,
    successStatus: false,
    addedSuccessStatus: false,
    isAddOrderLoading: false,
    addedOrderId: null,
    addItemsSuccess: {
        status: false,
        msg: null
    },
    modifyItemsSuccess: {
        status: false,
        msg: null
    },
    deleteItemsSuccess: {
        status: false,
        msg: null
    }
}, action) => {
    switch (action.type) {
        case ActionTypes.START_LOADING:
            return { ...state, isAddOrderLoading: true }

        case ActionTypes.FETCH_ORDER_DETAIL:
            return { ...state, addedSuccessStatus: false, isLoading: false, orderDetailErrMess: null, order: action.payload };

        case ActionTypes.ORDER_DETAIL_FAILED:
            return { ...state, addedSuccessStatus: false, isLoading: false, orderDetailErrMess: action.payload };

        case ActionTypes.FETCH_ORDER_ITEMS:
            return { ...state, addedSuccessStatus: false, isLoading: false, errMess: null, items: action.payload };

        case ActionTypes.ORDER_ITEMS_FAILED:
            return { ...state, addedSuccessStatus: false, isLoading: false, errMess: action.payload };

        case ActionTypes.MARK_DONE_ORDER:
            return { ...state, addedSuccessStatus: false, errMess: null, successStatus: true }

        case ActionTypes.MODIFY_ORDER_DETAIL:
            return { ...state, addedSuccessStatus: false, errMess: null, successStatus: true }

        case ActionTypes.ADD_ORDER:
            return { ...state, isAddOrderLoading: false, errMess: null, addedSuccessStatus: true, addedOrderId: action.payload.newOrder.orderId }

        case ActionTypes.ADD_ORDER_ITEMS:
            return {
                ...state,
                addItemsSuccess: {
                    status: true,
                    msg: null
                }
            };

        case ActionTypes.MODIFY_ORDER_ITEMS:
            return {
                ...state,
                modifyItemsSuccess: {
                    status: true,
                    msg: null
                }
            };

        case ActionTypes.DELETE_ORDER_ITEMS:
            return {
                ...state,
                deleteItemsSuccess: {
                    status: true,
                    msg: null
                }
            };

        case ActionTypes.SUBMIT_MODIFY_ITEMS_FAILED:
            return {
                ...state,
                errMess: action.payload,
                addItemsSuccess: {
                    status: false,
                    msg: action.payload
                },
                modifyItemsSuccess: {
                    status: false,
                    msg: action.payload
                },
                deleteItemsSuccess: {
                    status: false,
                    msg: action.payload
                }
            }

        case ActionTypes.SUBMIT_FAILED:
            return { ...state, errMess: action.payload, successStatus: false, addedSuccessStatus: false, addedOrderId: null }

        default:
            return state;
    }
};

export const CurrentlyViewedOrder = (state = {
    currentOrderId: 0,
}, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_ORDER_DETAIL:
            return { ...state, currentOrderId: action.payload.orderId };

        case ActionTypes.ORDER_DETAIL_FAILED:
            return { ...state, currentOrderId: 0 };

        default:
            return state;
    }
};