import * as ActionTypes from "../actions/ActionTypes";

export const DeleteDisabledAccount = (state = {
    errMess: null,
    successStatus: false,
    successMessage: null
}, action) => {
    // console.log(action);
    switch (action.type) {
        case ActionTypes.DELETE_DISABLED_ACCOUNT_SUCCESS:
            return {
                ...state,
                errMess: null,
                successStatus: true,
                successMessage: action.payload
            }
        case ActionTypes.DELETE_DISABLED_ACCOUNT_FAILED:
            return {
                ...state,
                errMess: action.payload, // array of error messages
                successStatus: false,
                successMessage: null
            }
        default:
            return state;
    }
}