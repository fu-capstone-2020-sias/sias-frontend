import * as ActionTypes from '../actions/ActionTypes';

export const Orders = (state = {
    isLoading: true,
    errMess: null,
    orders: null,
    currentPageNumber: 0,
    numberOfPages: 0,
    totalEntries: 0,
    searchData: null,
    successStatus: false,
    deleteSuccessStatus: false
}, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_ORDERS:
            return {
                ...state,
                isLoading: false,
                errMess: null,
                orders: action.payload.orders,
                numberOfPages: action.payload.numberOfPages,
                totalEntries: action.payload.totalEntries,
                currentPageNumber: action.payload.currentPageNumber
            };

        case ActionTypes.FETCH_ORDERS_SEARCH:
            return {
                ...state,
                isLoading: false,
                errMess: null,
                orders: action.payload.orders,
                searchData: action.payload.searchData,
                numberOfPages: action.payload.numberOfPages,
                totalEntries: action.payload.totalEntries,
                currentPageNumber: action.payload.currentPageNumber
            };

        case ActionTypes.ORDERS_FAILED:
            return { ...state, isLoading: false, errMess: action.payload };

        case ActionTypes.ADD_ORDER:
            return { ...state, errMess: null, successStatus: true }

        case ActionTypes.MARK_DONE_ORDER:
            return { ...state, errMess: null, successStatus: true }

        case ActionTypes.DELETE_ORDERS:
            return { ...state, errMess: null, deleteSuccessStatus: true }
            
        case ActionTypes.SUBMIT_FAILED:
            return { ...state, errMess: action.payload, deleteSuccessStatus: false, successStatus: false }

        default:
            return state;
    }
};