import * as ActionTypes from "../actions/ActionTypes";

export const DemoCreateCuttingInstruction = (state = {
    errMess: null,
    successStatus: false,
    successMessage: null
}, action) => {
    // console.log(action);
    switch (action.type) {
        case ActionTypes.DEMO_CREATE_INSTRUCTION_SUCCESS:
            return {
                errMess: null,
                successMessage: action.payload,
                successStatus: true
            }
        default:
            return state;
    }
}