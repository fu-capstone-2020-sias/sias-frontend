import * as ActionTypes from "../actions/ActionTypes";

export const FetchCuttingInstructions = (state = {
    isLoading: true,
    errMess: null,
    cuttingInstruction: null,
    successStatus: false,
    successMessage: null
}, action) => {
    // console.log(action);
    switch (action.type) {
        case ActionTypes.FETCH_CUTTING_INSTRUCTION_SUCCESS:
            return {
                ...state,
                isLoading: false,
                errMess: null,
                cuttingInstruction: action.payload,
                successStatus: true
            }
        case ActionTypes.FETCH_CUTTING_INSTRUCTION_FAILED:
            // console.log(action.payload);
            return {
                ...state,
                isLoading: false,
                errMess: action.payload,
                cuttingInstruction: null,
                successStatus: false,
                successMessage: null
            }
        default:
            return state;
    }
}