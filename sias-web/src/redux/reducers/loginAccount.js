import * as ActionTypes from '../actions/ActionTypes';
import Cookies from 'js-cookie';

// const fakeAccount = {
//     address: "Hoan Kiem Lake",
//     cityProvinceId: "CTY001",
//     companyInfoId: "COM003",
//     createdDate: "2020-10-10",
//     departmentId: "DEPT003",
//     disableFlag: 0,
//     districtId: "DST001",
//     dob: "1999-01-01",
//     email: "hoangchhe130830@fpt.edu.vn",
//     firstName: "Hoang",
//     gender: 0,
//     lastName: "Cao",
//     phoneNumber: "012345678",
//     roleId: "R003",
//     updatedDate: "2020-10-10",
//     userName: "test1",
//     wardCommuneId: "WRD001",
// }

// const fakeAdminAccount = {
//     address: "Hoan Kiem Lake",
//     cityProvinceId: "CTY001",
//     companyInfoId: "COM001",
//     createdDate: "2020-10-10",
//     departmentId: "DEPT001",
//     disableFlag: 0,
//     districtId: "DST001",
//     dob: "1999-01-11",
//     email: "hoangchhe130830@fpt.edu.vn",
//     firstName: "Hoàng",
//     gender: 0,
//     lastName: "Cáo",
//     phoneNumber: "0347619675",
//     roleId: "R001",
//     updatedDate: "2020-10-20",
//     userName: "test2",
//     wardCommuneId: "WRD001"
// }

const currentAccount = Cookies.get('account') == null ? null : JSON.parse(Cookies.get('account'));

export const LoginAccount = (state = {
    account: currentAccount,
    errMess: null
}, action) => {
    // console.log(currentAccount);
    switch (action.type) {
        case ActionTypes.SUBMIT_FAILED:
            return { ...state, errMess: action.payload };

        case ActionTypes.LOGIN_FAILED:
            return { ...state, errMess: action.payload };

        case ActionTypes.LOGIN_FORM_OPEN:
            return { ...state, errMess: null };

        case ActionTypes.LOGIN_SUCCESSFULLY:
            return { ...state, account: action.payload, errMess: null };

        case ActionTypes.LOGOUT_SUCCESSFULLY:
            return { ...state, account: null };

        default:
            return state;
    }

}