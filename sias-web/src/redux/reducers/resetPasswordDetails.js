import * as ActionTypes from '../actions/ActionTypes';

export const ResetPasswordDetails = (state = {
    email: null,
    errMess: null
}, action) => {
    switch (action.type) {
        case ActionTypes.SUBMIT_FAILED:
            return { ...state, email: null, errMess: action.payload };

        case ActionTypes.RESET_PASSWORD_FAILED:
            return { ...state, email: null, errMess: action.payload };

        case ActionTypes.RESET_PASSWORD_SUCCESSFULLY:
            return { ...state, email: action.payload, errMess: null };

        default:
            return state;
    }
}