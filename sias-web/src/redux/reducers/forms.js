// const usernameDefault = ''

import { ORDER_STATUS_PENDING, STOCK_PRODUCT_STATUS_GOOD } from "../../shared/constants"

export const InitialFeedback = {
    userName: '',
    userEmail: '',
    title: '',
    description: ''
}

export const InitialSearchOrderList = {
    orderId: '',
    customerName: '',
    customerPhoneNumber: '',
    customerEmail: '',
    customerCompanyName: '',
    searchByDate: 0,
    fromDate: new Date('2020-01-01').toISOString().substr(0, 10),
    toDate: new Date().toISOString().substr(0, 10)
}

//new
export const InitialSearchStockProductList = {
    barcode: '',
    searchByDate: 0,
    fromDate: new Date('2020-01-01').toISOString().substr(0, 10),
    toDate: new Date().toISOString().substr(0, 10)
}

export const InitialCompanyInfo = {
    name: "",
    cityProvinceId: "",
    districtId: "",
    wardCommuneId: "",
    address: "",
    phoneNumber: "",
    email: "",
    notes: "",
    companyWebsite: ""
}

export const InitialAccountInfo = {
    userName: "",
    password: "",
    oldPassword: "",
    isChangePassword: "0",
    roleId: "",
    departmentId: "",
    firstName: "",
    lastName: "",
    gender: "",
    dob: "",
    email: "",
    phoneNumber: "",
    cityProvinceId: "",
    districtId: "",
    wardCommuneId: "",
    address: "",
    disableFlag: "0",
    screen_id: "WEB_UC08"
}

export const InitialNewAccount = {
    userName: "",
    password: "",
    isChangePassword: "0",
    roleId: "",
    departmentId: "",
    firstName: "",
    lastName: "",
    gender: "",
    dob: "",
    email: "",
    phoneNumber: "",
    cityProvinceId: "",
    districtId: "",
    wardCommuneId: "",
    address: "",
    disableFlag: "0",
    screen_id: "WEB_UC08"
}

export const InitialSearchAccountList = {
    userName: '',
    fullName: '',
    phoneNumber: '',
    email: ''
}

export const InitialOrderDetail = {
    orderStatusId: ORDER_STATUS_PENDING,
    customerName: '',
    customerEmail: '',
    customerPhoneNumber: '',
    customerCompanyName: '',
    customerCityProvinceId: '',
    customerDistrictId: '',
    customerWardCommuneId: '',
    customerAddress: '',
    totalPrice: 0,
    approver: '',
    assignee: '',
}

export const InitialStockProductDetail = {
    statusId: STOCK_PRODUCT_STATUS_GOOD,
    sku: '',
    barcode: '',
    type: '',
    stockGroupId: '',
    standard: '',
    warehouseId: '',
    name: '',
    price: 0,
    length: 0,
    manufacturedDate: new Date('2020-01-01').toISOString().substr(0, 10),
    manufacturedDay: '',
    manufacturedMonth: '',
    manufacturedYear: ''
}

export const InitialWarehouseDetail = {
    warehouseName: '',
    cityProvinceId: '',
    districtId: '',
    communeId: '',
    address: '',
    department: '',
    warehouseManager: '',
}

export const InitialAddressIdLookup = {
    cityProvinceId: "",
    districtId: "",
    wardCommuneId: ""
}

export const InitialSteelInfosLookup = {
    type: '',
    stockGroupId: '',
    standard: '',
    warehouseId: ''
}

export const InitialDemoCuttingInstruction = {
    steelAvailable: '',
    steelToCut: '',
    bladeWidth: ''
}