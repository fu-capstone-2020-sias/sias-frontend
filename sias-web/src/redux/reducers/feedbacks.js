import * as ActionTypes from '../actions/ActionTypes';

export const Feedbacks = (state = {
    isLoading: true,
    errMess: null,
    feedbacks: null,
    currentPageNumber: 0,
    numberOfPages: 0,
    successStatus: false
}, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_FEEDBACKS:
            return {
                ...state,
                isLoading: false,
                errMess: null,
                feedbacks: action.payload.feedbacks,
                numberOfPages: action.payload.numberOfPages,
                currentPageNumber: action.payload.currentPageNumber
            };

        case ActionTypes.FEEDBACKS_FAILED:
            return { ...state, isLoading: false, errMess: action.payload, successStatus: false };

        case ActionTypes.SUBMIT_FAILED:
            return { ...state, isLoading: false, errMess: action.payload, successStatus: false };

        case ActionTypes.ADD_FEEDBACK:
            return { ...state, errMess: null, successStatus: true }

        default:
            return state;
    }
};