import * as ActionTypes from '../actions/ActionTypes';

export const StockProducts = (state = {
    isLoading: true,
    errMess: null,
    stockProducts: null,
    currentPageNumber: 0,
    numberOfPages: 0,
    totalEntries: 0,
    searchData: null,
    successStatus: false,
    deleteSuccessStatus: false
}, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_STOCK_PRODUCTS:
            return {
                ...state,
                isLoading: false,
                errMess: null,
                stockProducts: action.payload.stockproductList,
                numberOfPages: action.payload.totalPages,
                totalEntries: action.payload.totalResult,
                currentPageNumber: action.payload.currentPageNumber
            };

        case ActionTypes.FETCH_STOCK_PRODUCTS_SEARCH:
            return {
                ...state,
                isLoading: false,
                errMess: null,
                stockProducts: action.payload.stockproductList,
                searchData: action.payload.searchData,
                numberOfPages: action.payload.totalPages,
                totalEntries: action.payload.totalResult,
                currentPageNumber: action.payload.currentPageNumber
            };

        case ActionTypes.STOCK_PRODUCTS_FAILED:
            return { ...state, isLoading: false, errMess: action.payload };

        case ActionTypes.ADD_STOCK_PRODUCT:
            return { ...state, errMess: null, successStatus: true }

        case ActionTypes.DELETE_STOCK_PRODUCTS:
            return { ...state, errMess: null, deleteSuccessStatus: true }
            
        case ActionTypes.SUBMIT_FAILED:
            return { ...state, errMess: action.payload, deleteSuccessStatus: false, successStatus: false }

        default:
            return state;
    }
};