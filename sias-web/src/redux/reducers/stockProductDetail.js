import * as ActionTypes from '../actions/ActionTypes';

export const StockProductDetail = (state = {
    errMess: null,
    stockProduct: null,
    successStatus: false,
    addedSuccessStatus: false,
    addedStockProductId: null,
    isAddStockProductLoading: false
}, action) => {
    switch (action.type) {
        case ActionTypes.START_LOADING:
            return { ...state, isAddStockProductLoading: true }

        case ActionTypes.FETCH_STOCK_PRODUCT_DETAIL:
            return { ...state, addedSuccessStatus: false, isLoading: false, errMess: null, stockProduct: action.payload };

        case ActionTypes.STOCK_PRODUCT_DETAIL_FAILED:
            return { ...state, addedSuccessStatus: false, isLoading: false, errMess: action.payload };

        case ActionTypes.MODIFY_STOCK_PRODUCT_DETAIL:
            return { ...state, addedSuccessStatus: false, errMess: null, successStatus: true }

        case ActionTypes.ADD_STOCK_PRODUCT:
            return { ...state, isAddStockProductLoading: false, errMess: null, addedSuccessStatus: true, addedStockProductId: action.payload.stockProductId }

        case ActionTypes.SUBMIT_FAILED:
            return { ...state, errMess: action.payload, successStatus: false, addedSuccessStatus: false, addedStockProductId: null }

        default:
            return state;
    }
};

export const CurrentlyViewedStockProduct = (state = {
    currentStockProductId: 0,
}, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_STOCK_PRODUCT_DETAIL:
            return { ...state, currentStockProductId: action.payload.stockProductId };

        case ActionTypes.STOCK_PRODUCT_DETAIL_FAILED:
            return { ...state, currentStockProductId: 0 };

        default:
            return state;
    }
};