import * as ActionTypes from "../actions/ActionTypes";

export const CreateCuttingInstruction = (state = {
    errMess: null,
    successStatus: false,
    successMessage: null
}, action) => {
    switch (action.type) {
        case ActionTypes.CREATE_INSTRUCTION_SUCCESS:
            return {
                errMess: null,
                successMessage: action.payload,
                successStatus: true
            }
        case ActionTypes.CREATE_INSTRUCTION_FAILED:
            return {
                ...state,
                errMess: action.payload,
                successStatus: false,
                successMessage: null
            }
        default:
            return state;
    }
}