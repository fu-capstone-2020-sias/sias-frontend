import * as ActionTypes from '../actions/ActionTypes';

export const SteelInfos = (state = {
    isSteelTypeLoaded: false,
    isStockGroupLoaded: false,
    isStockProductStandardLoaded: false,
    errMess: null,
    steelTypeList: [],
    stockGroupList: [],
    stockProductStandardList: []
}, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_STEEL_TYPE:
            return {
                ...state,
                errMess: null,
                steelTypeList: action.payload,
                isSteelTypeLoaded: true
            };

        case ActionTypes.FETCH_STOCK_GROUP:
            return {
                ...state,
                errMess: null,
                stockGroupList: action.payload,
                isStockGroupLoaded: true
            };

        case ActionTypes.FETCH_STOCK_PRODUCT_STANDARD:
            return {
                ...state,
                errMess: null,
                stockProductStandardList: action.payload,
                isStockProductStandardLoaded: true
            };

        case ActionTypes.STEEL_INFO_FAILED:
            return {
                ...state,
                errMess: action.payload
            };

        default:
            return state;
    }
};