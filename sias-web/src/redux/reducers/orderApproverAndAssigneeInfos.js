import * as ActionTypes from '../actions/ActionTypes';

export const OrderApproverAndAssigneeInfos = (state = {
    isApproverListLoaded: false,
    isAssigneeListLoaded: false,
    errMess: null,
    approverList: [],
    assigneeList: []
}, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_APPROVER:
            return {
                ...state,
                errMess: null,
                approverList: action.payload,
                isApproverListLoaded: true
            };

        case ActionTypes.FETCH_ASSIGNEE:
            return {
                ...state,
                errMess: null,
                assigneeList: action.payload,
                isAssigneeListLoaded: true
            };

        case ActionTypes.APPROVER_AND_ASSIGNEE_INFO_FAILED:
            return {
                ...state,
                errMess: action.payload
            };

        default:
            return state;
    }
};