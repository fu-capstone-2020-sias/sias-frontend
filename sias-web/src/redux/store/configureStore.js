import {createStore, combineReducers, applyMiddleware} from 'redux';
import {createForms} from 'react-redux-form';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import {AccountInfos} from "../reducers/accountInfos";
import {AddNewAccounts} from "../reducers/addNewAccounts";
import {DeleteDisabledAccount} from "../reducers/deleteDisabledAccount";
import {ImportOrderBatch} from "../reducers/importOrderBatch";
import {FetchCuttingInstructions} from "../reducers/fetchCuttingInstruction";
import {CreateCuttingInstruction} from "../reducers/createInstruction";
import {ExportOrderBatch} from "../reducers/exportOrderBatch";
import {ImportStockProductBatch} from "../reducers/importStockProductBatch";
import {
    InitialAccountInfo,
    InitialCompanyInfo, InitialDemoCuttingInstruction,
    InitialFeedback,
    InitialNewAccount,
    InitialOrderDetail,
    InitialStockProductDetail,
    InitialWarehouseDetail,
    InitialAddressIdLookup,
    InitialSteelInfosLookup,
} from '../reducers/forms';
import {LoginAccount} from '../reducers/loginAccount';
import {Feedbacks} from '../reducers/feedbacks';
import {Orders} from '../reducers/orders';
import {Accounts} from '../reducers/accounts';
import {StockProducts} from '../reducers/stockProducts'
import {OrderDetail, CurrentlyViewedOrder} from '../reducers/orderDetail';
import {CompanyInfos} from '../reducers/companyInfos'
import {ResetPasswordDetails} from '../reducers/resetPasswordDetails';
import {Warehouses, WarehouseSelections} from '../reducers/warehouses'
import {WarehouseDetail, CurrentlyViewedWarehouse} from '../reducers/warehouseDetail';
import {Addresses} from '../reducers/addresses';
import {SteelInfos} from '../reducers/steelInfos';
import {StockProductDetail, CurrentlyViewedStockProduct} from '../reducers/stockProductDetail';
import {ExportStockBatch} from "../reducers/exportStockBatch";
import {ImageUpload} from "../reducers/imageUpload";
import {DemoCreateCuttingInstruction} from "../reducers/demoCuttingInstruction";
import { OrderApproverAndAssigneeInfos } from '../reducers/orderApproverAndAssigneeInfos';


export const ConfigureStore = () => {
    const store = createStore(
        combineReducers({
            loginAccount: LoginAccount,
            feedbacks: Feedbacks,
            orders: Orders,
            stockProducts: StockProducts,
            steelInfos: SteelInfos,
            accounts: Accounts,
            resetPasswordDetails: ResetPasswordDetails,
            currentlyViewedOrder: CurrentlyViewedOrder,
            orderDetail: OrderDetail,
            orderApproverAndAssigneeInfos: OrderApproverAndAssigneeInfos,
            currentlyViewedStockProduct: CurrentlyViewedStockProduct,
            stockProductDetail: StockProductDetail,
            companyInfos: CompanyInfos,
            accountInfos: AccountInfos,
            addNewAccounts: AddNewAccounts,
            deleteDisabledAccounts: DeleteDisabledAccount,
            importOrderBatch: ImportOrderBatch,
            fetchCuttingInstruction: FetchCuttingInstructions,
            createCuttingInstruction: CreateCuttingInstruction,
            exportOrderBatch: ExportOrderBatch,
            exportStockBatch: ExportStockBatch,
            importStockProductBatch: ImportStockProductBatch,
            imageUpload: ImageUpload,
            warehouses: Warehouses,
            currentlyViewedWarehouse: CurrentlyViewedWarehouse,
            warehouseDetail: WarehouseDetail,
            addresses: Addresses,
            warehouseSelections: WarehouseSelections,
            demoCuttingInstructions: DemoCreateCuttingInstruction,
            ...createForms({
                feedback: InitialFeedback,
                companyInfo: InitialCompanyInfo,
                accountInfo: InitialAccountInfo,
                addNewAccount: InitialNewAccount,
                editOrderDetail: InitialOrderDetail,
                editStockProductDetail: InitialStockProductDetail,
                editWarehouseDetail: InitialWarehouseDetail,
                addOrder: InitialOrderDetail,
                addNewStockProduct: InitialStockProductDetail,
                addWarehouse: InitialWarehouseDetail,
                addressIdLookup: InitialAddressIdLookup,
                steelInfosLookup: InitialSteelInfosLookup,
                demoCuttingInstruction: InitialDemoCuttingInstruction
            })
        }),
        applyMiddleware(thunk, logger)
    );

    return store;
};