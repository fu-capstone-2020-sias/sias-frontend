import {
    ROUTE_VIEW_WAREHOUSE_LIST,
    ROUTE_ADD_NEW_WAREHOUSE,
    ROUTE_VIEW_STOCK_PRODUCT_LIST,
    ROUTE_ADD_NEW_STOCK_PRODUCT,
    ROUTE_VIEW_ORDER_LIST,
    ROUTE_ADD_NEW_ORDER,
    ROUTE_VIEW_COMPANY,
    ROUTE_FEEDBACK
} from "./constants";

export const serviceList = [
    {
        id: 1,
        name: 'Warehouse',
        description: 'Description',
        details: [
            {
                name: 'View Warehouse List',
                description: "View a list of your company's warehouse",
                url: ROUTE_VIEW_WAREHOUSE_LIST
            },
            {
                name: 'Add a new Warehouse',
                description: 'Add a new warehouse to the database',
                url: ROUTE_ADD_NEW_WAREHOUSE
            }
        ]
    },
    {
        id: 2,
        name: 'Stock Products',
        description: 'Description',
        details: [
            {
                name: 'View Stock Product List',
                description: "View a list of your company's stock products",
                url: ROUTE_VIEW_STOCK_PRODUCT_LIST
            },
            {
                name: 'Add new Stock Products',
                description: 'Add new stock product(s) to the database',
                url: ROUTE_ADD_NEW_STOCK_PRODUCT
            }
        ]
    },
    {
        id: 3,
        name: 'Orders',
        description: 'Description',
        details: [
            {
                name: 'View Order List',
                description: "View a list of orders from customers",
                url: ROUTE_VIEW_ORDER_LIST
            },
            {
                name: 'Add new Orders',
                description: "Add new order(s) with customers' information",
                url: ROUTE_ADD_NEW_ORDER
            }
        ]
    },
    {
        id: 4,
        name: 'Company',
        description: 'Description',
        details: [
            {
                name: 'View Company Information',
                description: 'Get to know your company',
                url: ROUTE_VIEW_COMPANY
            },
            {
                name: 'Feedback',
                description: "Send feedback to company's administrators",
                url: ROUTE_FEEDBACK
            }
        ]
    }
];