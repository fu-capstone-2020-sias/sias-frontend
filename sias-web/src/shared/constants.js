export const ROLE_ADMIN = 'R001';
export const ROLE_OFFICE_MANAGER = 'R002';
export const ROLE_OFFICE_EMPLOYEE = 'R004';
export const ROLE_FACTORY_MANAGER = 'R003';

export const ORDER_STATUS_PENDING = 'OS001';
export const ORDER_STATUS_APPROVED = 'OS002';
export const ORDER_STATUS_PROCESSING = 'OS003';
export const ORDER_STATUS_COMPLETED = 'OS004';
export const ORDER_STATUS_REJECTED = 'OS005';

export const ORDER_STATUS_PENDING_TEXT = 'Pending';
export const ORDER_STATUS_APPROVED_TEXT = 'Approved';
export const ORDER_STATUS_PROCESSING_TEXT = 'Processing';
export const ORDER_STATUS_COMPLETED_TEXT = 'Completed';
export const ORDER_STATUS_REJECTED_TEXT = 'Rejected';

export const STOCK_PRODUCT_STATUS_GOOD = 'SPS001';
export const STOCK_PRODUCT_STATUS_MEDIUM = 'SPS002';
export const STOCK_PRODUCT_STATUS_BAD = 'SPS003';

export const STOCK_PRODUCT_STATUS_GOOD_TEXT = 'Good';
export const STOCK_PRODUCT_STATUS_MEDIUM_TEXT = 'Medium';
export const STOCK_PRODUCT_STATUS_BAD_TEXT = 'Bad';

export const LABEL_IMAGE_TYPE_NSMP = 'NSMP';
export const LABEL_IMAGE_TYPE_NIKKEN = 'NIKKEN';
export const LABEL_IMAGE_TYPE_NIKKEN_DOC = 'NIKKEN_DOC';
export const LABEL_IMAGE_TYPE_BCP = 'BCP';
export const LABEL_IMAGE_TYPE_EN = 'EN';

//error types
export const ERROR_PAGE_NOT_FOUND = 'ERROR_PAGE_NOT_FOUND';
export const ERROR_UNAUTHORIZED = 'ERROR_UNAUTHORIZED';

export const ROUTE_HOME = '/home';
export const ROUTE_VIEW_ORDER_LIST = '/viewOrderList';
export const ROUTE_ORDER_DETAILS = '/orderDetails';
export const ROUTE_FEEDBACK = '/feedback';
export const ROUTE_VIEW_ACCOUNT_LIST = '/viewAccountList';
export const ROUTE_FORGOT_PASSWORD = '/forgotPassword';
export const ROUTE_VIEW_ACCOUNT_INFO = "/viewAccountInfo"
export const ROUTE_VIEW_USER_INFO = "/viewUserInfo"
export const ROUTE_MODIFY_ACCOUNT_INFO = "/modifyAccountInfo"
export const ROUTE_CHANGE_PASSWORD = "/changePassword"
export const ROUTE_ADD_NEW_ACCOUNT = "/addNewAccount"
export const ROUTE_IMPORT_ORDER_BATCH = "/importOrderBatch"
export const ROUTE_IMPORT_STOCK_PRODUCT_BATCH = "/importStockProductBatch"
export const ROUTE_VIEW_CUTTING_INSTRUCTION = "/viewCuttingInstruction"
export const ROUTE_ADD_NEW_ORDER = '/addOrder';
export const ROUTE_ADD_NEW_ORDER_FORM = '/addOrderForm';
export const ROUTE_VIEW_COMPANY = '/viewCompany';
export const ROUTE_MODIFY_COMPANY = "/modifyCompany";
export const ROUTE_VIEW_STOCK_PRODUCT_LIST = '/viewStockProductlist';
export const ROUTE_STOCK_PRODUCT_DETAILS = '/stockProductDetails';
export const ROUTE_ADD_NEW_STOCK_PRODUCT = '/addStockProduct';
export const ROUTE_ADD_NEW_STOCK_PRODUCT_FORM = '/addStockProductForm';
export const ROUTE_UPLOAD_STOCK_PRODUCT_IMAGE = '/uploadStockProductImage';
export const ROUTE_VIEW_WAREHOUSE_LIST = '/viewWarehouseList';
export const ROUTE_ADD_NEW_WAREHOUSE = '/addWarehouse';
export const ROUTE_WAREHOUSE_DETAILS = '/warehouseDetails';

export const ROUTE_ADDRESS_ID_LOOKUP = '/addressId';
export const ROUTE_STEEL_INFOS_LOOKUP = '/steelInfos';

export const ROUTE_DEMO = '/demo';

export const LOCALE_VI_VN = 'vi-VN';

//Validation messages
export const VALIDATION_MSG_REQUIRED = 'This is a required field';
export const VALIDATION_MSG_NUMBERS_ONLY = 'All input should only be numbers';

export const VALIDATION_MSG_ITEMS_GENERAL = 'Invalid field(s):';
export const VALIDATION_MSG_INVALID_ITEM_LENGTH = "Item's length should be between 1 and 5 digits";
export const VALIDATION_MSG_INVALID_ITEM_QUANTITY = "Item's quantity should be between 1 and 5 digits";
export const VALIDATION_MSG_INVALID_ITEM_PRICE = "Item's price should be between 1 and 10 digits";
export const VALIDATION_MSG_INVALID_ITEM_BLADE_WIDTH = "Item's blade width should be between 0 and 99 mm";

export const VALIDATION_MSG_INVALID_SKU ='Invalid SKU';
export const VALIDATION_MSG_INVALID_BAR_CODE ='Invalid bar code';
export const VALIDATION_MSG_INVALID_PHONE_NUMBER ='Invalid phone number';
export const VALIDATION_MSG_INVALID_EMAIL = 'Invalid email';
export const VALIDATION_MSG_INVALID_WEBSITE = 'Invalid website address';
export const VALIDATION_MSG_INVALID_NAME = 'Invalid name';
export const VALIDATION_MSG_INVALID_USERNAME = 'Invalid username';
export const VALIDATION_MSG_INVALID_COMPANY_NAME = 'Invalid company name';
export const VALIDATION_MSG_INVALID_WAREHOUSE_NAME = 'Invalid warehouse name';
export const VALIDATION_MSG_INVALID_PRODUCT_NAME = 'Invalid product name';
export const VALIDATION_MSG_MAX_LENGTH = (val) => `This field must be no more than ${val} characters`;
export const VALIDATION_MSG_MIN_LENGTH = (val) => `This field must be at least ${val} characters`;
export const VALIDATION_MSG_EXACT_LENGTH = (val) => `This field must be exact ${val} characters`;

//Error messages
export const ERROR_MESSAGE_SUBMIT_GENERAL = "Some unexpected error occured. Please try again later or contact your company's administrators.";
export const ERROR_MESSAGE_EMPTY_USERNAME_OR_PASSWORD = 'You must provide both username and password!';
export const ERROR_MESSAGE_UPLOAD_IMAGE_GENERAL = 'Unable to read data from label image. Make sure you are uploading the correct image file.';

//Success messages
export const SUCCESS_MESSAGE_SUBMIT_GENERAL = "Done successfully!";
export const SUCCESS_MESSAGE_DELETE_GENERAL = 'Delete successfully.';
export const SUCCESS_MESSAGE_SEND_FEEDBACK = 'Your feedback was sent successfully.';
export const SUCCESS_MESSAGE_ADD_NEW_ORDER = 'Order was added successfully.';
export const SUCCESS_MESSAGE_ADD_NEW_WAREHOUSE = 'Warehouse was added successfully.';
export const SUCCESS_MESSAGE_ADD_NEW_STOCK_PRODUCT = 'Stock product was added successfully.';

//Unauthorized Access messages
export const UNAUTHORIZED_ACCESS_MSG_GENERAL = 'UNAUTHORIZED ACCESS';
export const UNAUTHORIZED_ACCESS_MSG_GENERAL_DESCRIPTION = 'You are not authorized to view this page. If you think this is a mistake, please send a feedback to company administrators.';

//Error display messages
export const PAGE_NOT_FOUND_MSG_GENERAL = 'PAGE NOT FOUND';
export const PAGE_NOT_FOUND_MSG_GENERAL_DESCRIPTION = 'If you think this is a mistake, please send a feedback to company administrators.';

//Order with no items messages
export const MSG_CANNOT_CHANGE_ORDER_STATUS_IF_NO_ITEMS = 'Notice: You cannot approve or reject order with no items. Please add items to change order status.';

//Numbers
export const STOCK_PRODUCT_PRICE_MIN = 0;
export const STOCK_PRODUCT_PRICE_MAX = 1e10-1;
export const STOCK_PRODUCT_LENGTH_MIN = 0;
export const STOCK_PRODUCT_LENGTH_MAX = 1e5-1;