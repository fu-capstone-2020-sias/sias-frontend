import {
    ORDER_STATUS_APPROVED,
    ORDER_STATUS_PENDING,
    ORDER_STATUS_PROCESSING,
    ORDER_STATUS_COMPLETED,
} from "./constants";

import { baseUrl } from "../shared/baseUrl";
import { ROLE_ADMIN, ROLE_OFFICE_MANAGER, ROLE_FACTORY_MANAGER, ROLE_OFFICE_EMPLOYEE, LOCALE_VI_VN } from './constants';

// export const mapCityProvinceNameToId = (name, cityProvinceList) => {
//     if (cityProvinceList.length < 1) return '';
//     const cityProvinceObj = cityProvinceList.filter(city => city.name === name)[0];
//     return cityProvinceObj.cityProvinceId;
// }

// export const mapDistrictNameToId = (name, districtList) => {
//     if (districtList.length < 1) return '';
//     const districtObj = districtList.filter(dist => dist.name === name)[0];
//     return districtObj.districtId;
// }

// export const mapWardCommuneNameToId = (name, wardCommuneList) => {
//     if (wardCommuneList.length < 1) return '';
//     const wardCommuneObj = wardCommuneList.filter(ward => ward.name === name)[0];
//     return wardCommuneObj.wardCommuneId;
// }

export const hiddenEmail = (email) => {
    const split = email.split('@');
    const domain = split[1];
    const firstPart = split[0];
    const len = firstPart.length;
    let hiddenPart = '**';
    if (len <= 2) {
        hiddenPart = '*'.repeat(len);
    } else if (len <= 5) {
        hiddenPart = firstPart.substring(0, 1) + '*'.repeat(len - 2) + firstPart.substring(len - 1, len);
    } else {
        hiddenPart = firstPart.substring(0, 2) + '*'.repeat(len - 4) + firstPart.substring(len - 2, len);
    }
    return hiddenPart + '@' + domain;
}

export const mapParamsToObject = (params) => {
    return JSON.parse('{"' + params.replace(/&/g, '","').replace(/=/g, '":"') + '"}',
        function (key, value) { return key === "" ? value : decodeURIComponent(value) });
}

export const getFullLocaleDateString = (date = new Date(), locale = 'vi-VN') => {
    const daysOfWeek = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
    ]
    return daysOfWeek[date.getDay()] + ', ' + date.toLocaleDateString(locale);
}

export const getYearList = () => {
    const currentYear = new Date().getFullYear();
    let yearList = [];
    for (let i = currentYear; i >= 1970; i--) {
        yearList.push(i);
    }
    return yearList;
}

export const getMonthList = () => {
    let monthList = [];
    for (let i = 1; i <= 12; i++) {
        monthList.push(i);
    }
    return monthList;
}

const isLeap = year => new Date(year, 1, 29).getDate() === 29;

export const getDayList = (m, year) => {
    let dayList = [];
    let daysOfMonth = 31;
    const month = 1 + parseInt(m); //In Javascript, months are from 0 to 11
    switch (month) {
        case 2:
            daysOfMonth = isLeap(year) ? 29 : 28;
            // console.log('here');
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            daysOfMonth = 30;
            break;
        default:
            daysOfMonth = 31;
            break;
    }
    for (let i = 1; i <= daysOfMonth; i++) {
        dayList.push(i);
    }
    return dayList;
}

export const formatterLengthUnits = new Intl.NumberFormat('vi-VN', {
    style: 'unit',
    unitDisplay: 'short',
    unit: 'millimeter',

    // These options are needed to round to whole numbers if that's what you want.
    minimumFractionDigits: 0,
    maximumFractionDigits: 2,
});

// formatterLengthUnits.format(1234); --> 1.234 mm

export const formatterVND = new Intl.NumberFormat('vi-VN', {
    style: 'currency',
    currency: 'VND',

    // These options are needed to round to whole numbers if that's what you want.
    minimumFractionDigits: 0,
    maximumFractionDigits: 2,
});

// formatterVND.format(50000) --> 50.000 ₫

export const dateDisplay = (date, locale) => {
    try {
        if (date != null) {
            const dateAfterConvert = new Date(date).toLocaleDateString(locale);
            return dateAfterConvert;
        } else {
            throw new Error();
        }
    } catch {
        return date;
    }
}

export const dateDisplayVietnamese = (date) => {
    return dateDisplay(date, LOCALE_VI_VN);
}

export const mapRoleIdToRole = (roleId) => {
    switch (roleId) {
        case ROLE_ADMIN:
            return 'ADMIN';
        case ROLE_OFFICE_MANAGER:
            return 'OFFICE MANAGER';
        case ROLE_FACTORY_MANAGER:
            return 'FACTORY MANAGER';
        case ROLE_OFFICE_EMPLOYEE:
            return 'OFFICE EMPLOYEE';
        default:
            return 'N/A';
    }
}

export const mapStatusIdToStatus = (statusId) => {
    switch (statusId) {
        case ORDER_STATUS_APPROVED:
            return 'Approved';
        case ORDER_STATUS_PENDING:
            return 'Pending';
        case ORDER_STATUS_PROCESSING:
            return 'Processing';
        case ORDER_STATUS_COMPLETED:
            return 'Completed';
        default:
            return 'N/A';
    }
}

export const mapStatusToStatusId = (status) => {
    switch (status) {
        case 'Approved':
            return ORDER_STATUS_APPROVED;
        case 'Pending':
            return ORDER_STATUS_PENDING;
        case 'Processing':
            return ORDER_STATUS_PROCESSING;
        case 'Completed':
            return ORDER_STATUS_COMPLETED;
        default:
            return '';
    }
}

export const getValueOf = (info) => {
    if (info != null) return info;
    return "N/A";
};

export const fetchCityProvinceIdList = async () => {
    let resp = await fetch(baseUrl + "cityProvince", {
        method: "GET",
        credentials: "include",
    });
    // console.log(resp.json());
    return resp.json();
};

export const fetchDistrictIdList = async (cityProvinceId) => {
    let resp = await fetch(baseUrl + "district/" + cityProvinceId, {
        method: "GET",
        credentials: "include",
    });

    return resp.json();
};

export const fetchWardCommuneIdList = async (districtId) => {
    let resp = await fetch(baseUrl + "wardCommune/" + districtId, {
        method: "GET",
        credentials: "include",
    });

    return resp.json();
};

export const fetchRoleList = async () => {
    let resp = await fetch(baseUrl + "role", {
        method: "GET",
        credentials: "include",
    })

    return resp.json();
}

export const fetchDepartmentList = async () => {
    let resp = await fetch(baseUrl + "department", {
        method: "GET",
        credentials: "include",
    })

    return resp.json();
}
