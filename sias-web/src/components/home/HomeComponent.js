import React from 'react';
import { Jumbotron, Media } from 'reactstrap';
import { connect } from 'react-redux';
import { Fade, Stagger } from 'react-animation-components';
import GuestContent from './GuestContentComponent'
import { Link } from 'react-router-dom';
import { serviceList } from '../../shared/serviceList';
import { ROUTE_VIEW_ACCOUNT_INFO } from '../../shared/constants';
import { getFullLocaleDateString } from '../../shared/utils';

function RenderServiceDetails(props) {
    const serviceDetails = props.serviceDetails;
    return (
        <div key={serviceDetails.name} className="col-12 mt-5">
            <Media tag="li">
                <Media body className="ml-lg-5">
                    <Media heading><Link className="service-detail-link" to={serviceDetails.url}>{serviceDetails.name}</Link></Media>
                    <p>{serviceDetails.description}</p>
                </Media>
            </Media>
        </div>
    );
}

function RenderServiceList(props) {
    const serviceList = props.serviceList;
    const displayserviceList = (list) => {
        return list.map(serviceDetails => (
            <Fade in>
                <RenderServiceDetails serviceDetails={serviceDetails} />
            </Fade>
        ));
    }

    return serviceList.map(service => (
        <div className="services-sect col-12 col-sm-6">
            <div className="services-title">
                <h3 className="services-title-text">{service.name}</h3>
            </div>
            <div className="services-list">
                <div className="row">
                    <Media list>
                        <Stagger in delay={200}>
                            {displayserviceList(service.details)}
                        </Stagger>
                    </Media>
                </div>
            </div>
        </div>
    ));
}

function Intro(props) {
    const serviceList = props.serviceList;
    return (
        <div className="container">
            <div className="intro-section row">
                <div className="col-12">
                    <h1 className="intro-title-text mb-5">Services</h1>
                </div>

                <RenderServiceList serviceList={serviceList} />

            </div>
        </div>
    );
}

function HomeContent(props) {
    const accountFullName = props.account.firstName + ' ' + props.account.lastName;
    return (
        <div className="container">
            <div className="row row-content">
                <div className="col-12">
                    <Intro serviceList={serviceList} />
                </div>

                <div className="col-12 mt-5">
                    <h2>Welcome back, <Link to={ROUTE_VIEW_ACCOUNT_INFO}>{accountFullName}</Link></h2>
                    <h4>Today is {getFullLocaleDateString()}</h4>
                </div>
            </div>
        </div>
    );
}

function Home(props) {
    const account = props.loginAccount.account;
    return (
        <div className="home">
            <Jumbotron className="d-none d-sm-block">
                <div className="container">
                    <div className="row row-header">
                        <div>
                            <h2>Steel Industry Assistant System</h2>
                            <h4>
                                With the Steel Industry Assistant System (SIAS), we aim to optimize
                                the operational structure of the steel industry system, both versions of the software are provided with high quality of algorithms and performances,
                                bringing the best result and experience to our customer.
                            </h4>
                        </div>
                    </div>
                </div>
            </Jumbotron>
            {/* <BackgroundAsImageWithCenteredContent /> */}
            {(account == null) ? <GuestContent /> : <HomeContent account={account} />}
        </div>
    )

}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
    };
}

export default connect(mapStateToProps)(Home);