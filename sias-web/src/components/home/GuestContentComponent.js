import React from 'react';

export default function GuestContent(props) {
    return (
        <div className="container">
            <div className="row row-content">
                <div>
                    <h2>You are not logged in</h2>
                    <h4>Please log in to use our services.</h4>
                </div>
            </div>
        </div>
    );
}