import React, { Component } from 'react';
import { connect } from 'react-redux';
import SendFeedback from './SendFeedbackComponent';
import ViewFeedback from './ViewFeedbackComponent';
import { ROLE_ADMIN, ERROR_PAGE_NOT_FOUND } from '../../shared/constants'
import { actions } from 'react-redux-form';
import { postFeedback, fetchFeedback } from '../../redux/actions/ActionCreators';
import { Loading } from '../common/LoadingComponent';
import ErrorDisplay from '../common/ErrorDisplay';

class Feedback extends Component {
    componentDidMount() {
        const account = this.props.loginAccount.account;
        if (null != account && account.roleId === ROLE_ADMIN) {
            const feedbackList = this.props.feedbacks.feedbacks;
            const currentPageNumber = this.props.feedbacks.currentPageNumber;
            const pageNumber = this.props.pageNumber;
            if ((null == feedbackList || pageNumber !== currentPageNumber) && null != account && account.roleId === ROLE_ADMIN) {
                this.props.fetchFeedback(pageNumber);
            }
        }
    }

    render() {
        const account = this.props.loginAccount.account;
        if (account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first</h1>
                    </div>
                </div>
            );
        } else if (account.roleId === ROLE_ADMIN) {
            const feedbackList = this.props.feedbacks.feedbacks;
            const numberOfPages = this.props.feedbacks.numberOfPages;
            const currentPageNumber = this.props.feedbacks.currentPageNumber;
            const isLoading = this.props.feedbacks.isLoading;

            if (currentPageNumber > numberOfPages) {
                return (
                    <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
                )
            }

            return (
                <React.Fragment>
                    {isLoading ?
                        <div className="container">
                            <div className="row row-content">
                                <Loading />
                            </div>
                        </div> :
                        <ViewFeedback
                            feedbackList={feedbackList}
                            numberOfPages={numberOfPages}
                            currentPageNumber={currentPageNumber}
                        />
                    }
                </React.Fragment>
            );
        } else {
            return (
                <SendFeedback
                    loginAccount={account}
                    resetFeedbackForm={this.props.resetFeedbackForm}
                    postFeedback={this.props.postFeedback}
                />
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        feedbacks: state.feedbacks
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        postFeedback: (userName, userEmail, title, description) => dispatch(postFeedback(userName, userEmail, title, description)),
        resetFeedbackForm: () => { dispatch(actions.reset('feedback')) },
        fetchFeedback: (pageNumber) => { dispatch(fetchFeedback(pageNumber)) }
    });
}

export default connect(mapStateToProps, mapDispatchToProps)(Feedback);
// export default Feedback;