import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Button, Label, Col, Row } from 'reactstrap';
import { Form, Control, Errors } from 'react-redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { VALIDATION_MSG_REQUIRED, SUCCESS_MESSAGE_SEND_FEEDBACK, ROUTE_HOME } from '../../shared/constants';
import { required } from '../../shared/fieldValidation';
import FormLabel from '../common/FormLabel';

class SendFeedback extends Component {
    constructor(props) {
        super(props);
        this.state = {
            accountUserName: this.props.loginAccount.userName,
            accountEmail: this.props.loginAccount.email
        };

        //BINDING FUNCTIONs ARE REQUIRED FOR HANDLING EVENT
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(values) {
        this.props.postFeedback(this.state.accountUserName, this.state.accountEmail, values.title, values.description);
        this.props.resetFeedbackForm();
    }

    render() {
        const successStatus = this.props.successStatus;
        const errMess = this.props.errMess;
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                        <BreadcrumbItem active>Feedback</BreadcrumbItem>
                    </Breadcrumb>
                </div>
                <div className="row row-content">
                    <div className="col-12">
                        <h3>Send us your feedback</h3>
                        <h5 className="text-success">{successStatus ? SUCCESS_MESSAGE_SEND_FEEDBACK : ''}</h5>
                        <h5 className="text-danger">{errMess != null ? errMess : ''}</h5>
                    </div>
                    <div className="col-12 col-md-9">
                        <Form model="feedback" onSubmit={(values) => this.handleSubmit(values)}>
                            <Row className="form-group">
                                <Label htmlFor="username" md={2}>Username</Label>
                                <Col md={10}>
                                    <Control.text model=".username" id="username" name="username"
                                        className="form-control" value={this.state.accountUserName} disabled />

                                </Col>
                            </Row>

                            <Row className="form-group">
                                <Label htmlFor="title" md={2}>
                                    <FormLabel text="Title" required='true' />
                                </Label>
                                <Col md={10}>
                                    <Control.text model=".title" id="title" name="title"
                                        className="form-control" validators={{ required }} />
                                    <Errors className="text-danger" model=".title" show="touched"
                                        messages={{
                                            required: VALIDATION_MSG_REQUIRED
                                        }} />
                                </Col>
                            </Row>

                            <Row className="form-group">
                                <Label htmlFor="description" md={2}>
                                    <FormLabel text="Description" required='true' />
                                </Label>
                                <Col md={10}>
                                    <Control.textarea model=".description" id="description" name="description"
                                        rows="12" className="form-control"
                                        validators={{ required }} />
                                    <Errors className="text-danger" model=".description" show="touched"
                                        messages={{
                                            required: VALIDATION_MSG_REQUIRED
                                        }} />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Col md={{ size: 10, offset: 2 }}>
                                    <Button type="submit" color="warning" className="submitButton">
                                        Send Feedback
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        successStatus: state.feedbacks.successStatus,
        errMess: state.feedbacks.errMess
    };
}

export default connect(mapStateToProps)(SendFeedback);