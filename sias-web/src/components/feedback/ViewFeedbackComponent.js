import React from 'react';
import {Breadcrumb, BreadcrumbItem} from 'reactstrap';
import {Link} from 'react-router-dom';
import {makeStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import {ROUTE_FEEDBACK, ROUTE_HOME, ROUTE_VIEW_USER_INFO} from '../../shared/constants';
import {StyledTableCell} from '../common/StyledTableCell';
import TablePaging from '../common/TablePaging';
import EnhancedTableHead from '../common/EnhancedTableHead';
import Paper from '@material-ui/core/Paper';
import {getValueOf} from "../../shared/utils";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

const headCells = [
    {id: 'id', numeric: true, disablePadding: false, label: 'ID'},
    {id: 'title', numeric: false, disablePadding: false, label: 'Title'},
    {id: 'description', numeric: false, disablePadding: false, label: 'Description'},
    {id: 'sender', numeric: false, disablePadding: false, label: 'Sender'},
    {id: 'createdDate', numeric: false, disablePadding: false, label: 'Created Date'},
];

EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    rowCount: PropTypes.number.isRequired
};

function FeedbackTable(props) {
    const classes = useStyles();
    const feedbackList = props.feedbackList;
    // console.log(props.currentPage);

    return (
        <div className="col-12">
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <EnhancedTableHead
                        headCells={headCells}
                        classes={classes}
                        rowCount={feedbackList.length}
                    />
                    <TableBody>
                        {feedbackList.map((row) => (
                            <TableRow key={row.feedbackId}>
                                <StyledTableCell width="5%" align="right">{row.feedbackId}</StyledTableCell>
                                <StyledTableCell width="25%">{row.title}</StyledTableCell>
                                <StyledTableCell width="40%">{row.description}</StyledTableCell>
                                <StyledTableCell width="20%">
                                    {getValueOf(row.senderName) !== 'N/A' ?
                                        <Link
                                            to={`${ROUTE_VIEW_USER_INFO}?userName=${getValueOf(row.senderName)}`}>
                                            {getValueOf(row.senderName)}
                                        </Link> : 'N/A'
                                    }
                                </StyledTableCell>
                                <StyledTableCell width="10%">{row.createdDate}</StyledTableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <br/>
            <TablePaging numberOfPages={props.numberOfPages}
                         currentPage={props.currentPage}
                         linkTo={ROUTE_FEEDBACK}/>
        </div>
    );
}

function ViewFeedback(props) {
    if (null == props.feedbackList) {
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                        <BreadcrumbItem>Feedback</BreadcrumbItem>
                        <BreadcrumbItem active>View Feedback List</BreadcrumbItem>
                    </Breadcrumb>
                </div>
            </div>
        );
    } else {
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                        <BreadcrumbItem>Feedback</BreadcrumbItem>
                        <BreadcrumbItem active>View Feedback List</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>View Feedback List</h3>
                        <hr/>
                    </div>
                </div>

                <div className="row">
                    <FeedbackTable
                        feedbackList={props.feedbackList}
                        numberOfPages={props.numberOfPages}
                        currentPage={props.currentPageNumber}/>
                </div>

            </div>
        );
    }


}

// export default connect(mapStateToProps)(ViewFeedback);
export default ViewFeedback;