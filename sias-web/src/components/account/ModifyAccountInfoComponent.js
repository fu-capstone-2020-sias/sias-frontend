import React, { Component } from "react";
import {
    Breadcrumb,
    BreadcrumbItem,
    Button,
    Label,
    Col,
    Row,
} from "reactstrap";
import { Toast, ToastBody, ToastHeader } from "reactstrap";
import { Form, Control, Errors } from "react-redux-form";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { fetchAccountInfo, modifyAccountInfo } from "../../redux/actions/ActionCreators";
import { ROLE_ADMIN, ROUTE_HOME, ERROR_PAGE_NOT_FOUND, VALIDATION_MSG_REQUIRED, VALIDATION_MSG_INVALID_NAME, VALIDATION_MSG_INVALID_EMAIL, VALIDATION_MSG_INVALID_PHONE_NUMBER } from "../../shared/constants";
import { actions } from "react-redux-form";
import {
    fetchCityProvinceIdList,
    fetchDistrictIdList,
    fetchWardCommuneIdList,
    fetchRoleList,
    fetchDepartmentList
} from "../../shared/utils";
import { Loading } from "../common/LoadingComponent";
import FormLabel from '../common/FormLabel';
import UnauthorizedAccessDisplay from "../common/UnauthorizedAccessDisplay";
import ErrorDisplay from "../common/ErrorDisplay";
import { required, validName, validPhoneNumber, validEmail } from '../../shared/fieldValidation';

class ModifyAccountInfo extends Component {
    handleSubmit = async (values) => {
        await this.props.modifyAccountInfo(
            this.props.accountInfos.accountInfo.userName,
            "",
            "",
            "",
            values.roleId,
            values.departmentId,
            values.firstName,
            values.lastName,
            values.gender,
            values.dob,
            values.email,
            values.phoneNumber,
            this.state.selectedCityProvinceId,
            this.state.selectedDistrictId,
            this.state.selectedWardCommuneId,
            values.address,
            values.disableFlag,
            "WEB_UC08"
        );

        this.setState({
            isToastOpen: true
        })

        setTimeout(() => {
            this.setState({
                isToastOpen: false
            })
            window.location.reload();
        }, 3000)
    };

    fetchRoleList = async () => {
        let response = await fetchRoleList();
        this.setState({
            roleList: response.roles,
        });
    };

    fetchDepartmentList = async () => {
        let response = await fetchDepartmentList();
        this.setState({
            departmentList: response.data.rows,
        });
    };

    fetchCityProvinceIdList = async () => {
        let response = await fetchCityProvinceIdList();
        this.setState({
            cityProvinceIdList: response.data.rows,
        });
    };

    fetchDistrictIdList = async (cityProvinceId) => {
        let response = await fetchDistrictIdList(cityProvinceId);
        this.setState({
            districtIdList: response.data.rows,
        });
    };

    fetchWardCommuneIdList = async (districtId) => {
        let response = await fetchWardCommuneIdList(districtId);
        this.setState({
            wardCommuneIdList: response.data.rows,
        });
    };

    mapErrMessToParagraph = (errMessArr) => {
        return errMessArr.map((errMessObj) => {
            return <p>{errMessObj}</p>
        })
    }

    mapRoleToOption = () => {
        return this.state.roleList.map((roleObj) => (
            <option
                key={roleObj.roleId}
                value={roleObj.roleId}
            >
                {roleObj.name}
            </option>
        ));
    };

    mapDepartmentToOption = () => {
        return this.state.departmentList.map((departmentObj) => (
            <option
                key={departmentObj.departmentId}
                value={departmentObj.departmentId}
            >
                {departmentObj.name}
            </option>
        ));
    };

    mapCityProvinceToOption = () => {
        return this.state.cityProvinceIdList.map((cityProvinceObj) => (
            <option
                key={cityProvinceObj.cityProvinceId}
                value={cityProvinceObj.cityProvinceId}
            >
                {cityProvinceObj.name}
            </option>
        ));
    };

    mapDistrictToOption = () => {
        return this.state.districtIdList.map((districtObj) => (
            <option key={districtObj.districtId}
                value={districtObj.districtId}
            >
                {districtObj.name}
            </option>
        ));
    };

    mapWardCommuneToOption = () => {
        return this.state.wardCommuneIdList.map((wardCommuneObj) => (
            <option
                key={wardCommuneObj.wardCommuneId}
                value={wardCommuneObj.wardCommuneId}
            >
                {wardCommuneObj.name}
            </option>
        ));
    };

    state = {
        roleList: [],
        departmentList: [],
        cityProvinceIdList: [],
        districtIdList: [],
        wardCommuneIdList: [],
        selectedRoleId: "",
        selectedDepartmentId: "",
        selectedCityProvinceId: "",
        selectedDistrictId: "",
        selectedWardCommuneId: "",
        isToastOpen: false,
        firstLoad: false,
        isDisabled: null,
        userName: null,
        validParam: true
    };

    async componentDidMount() {
        const account = this.props.loginAccount.account;
        this.props.resetAccountInfoForm();
        this.setState({
            account: account,
        });
        const search = window.location.search;
        const params = new URLSearchParams(search);
        const foo = params.get('userName');
        this.setState({
            userName: foo
        })

        if (!foo) {
            this.setState({
                validParam: false
            })
        }

        if (null != account) {
            await this.props.fetchAccountInfo(foo); // Pass userName từ màn View Account List
        }
    }

    render() {
        const errMess = this.props.accountInfos.errMess;
        if (!this.state.validParam) {
            return (
                <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
            )
        }

        if (this.state.account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first.</h1>
                    </div>
                </div>
            );
        } else if (this.state.account.roleId !== ROLE_ADMIN) {
            return (
                <UnauthorizedAccessDisplay />
            );
        } else if (this.props.accountInfos.accountInfo !== null) {
            // Stop Admin chỉnh sửa thông tin của nhau
            if (this.state.account.roleId === ROLE_ADMIN
                && this.props.accountInfos.accountInfo.roleId === ROLE_ADMIN
                && this.state.account.userName !== this.props.accountInfos.accountInfo.userName) {
                return (
                    <UnauthorizedAccessDisplay />
                );
            }

            if (this.state.firstLoad === false) {
                this.fetchRoleList();
                this.fetchDepartmentList();
                this.fetchCityProvinceIdList();
                this.fetchDistrictIdList(this.props.accountInfos.accountInfo.cityProvinceId);
                this.fetchWardCommuneIdList(this.props.accountInfos.accountInfo.districtId);
                this.setState({
                    firstLoad: true,
                    selectedCityProvinceId: this.props.accountInfos.accountInfo.cityProvinceId,
                    selectedDistrictId: this.props.accountInfos.accountInfo.districtId,
                    selectedWardCommuneId: this.props.accountInfos.accountInfo.wardCommuneId,
                    selectedRoleId: this.props.accountInfos.accountInfo.roleId,
                    selectedDepartmentId: this.props.accountInfos.accountInfo.departmentId,
                    isDisabled: this.props.accountInfos.accountInfo.disableFlag === 1
                });
            }

            if (this.state.isDisabled !== null) {
                return (
                    <div className="container" style={{ position: "relative" }}>
                        <div className="row">
                            <Breadcrumb>
                                <BreadcrumbItem>
                                    <Link to={ROUTE_HOME}>Home</Link>
                                </BreadcrumbItem>
                                <BreadcrumbItem>Account Profile</BreadcrumbItem>
                                <BreadcrumbItem active>Modify Account Information</BreadcrumbItem>
                            </Breadcrumb>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <h3>Modify Account Information</h3>
                                <hr />
                            </div>
                            <div className="col-12 col-md-10 offset-md-1 col-xl-8 offset-xl-2">
                                {this.state.isDisabled ?
                                    <Label style={{ color: 'red' }}>
                                        Note: This account is DISABLED. Change the status of this account to
                                        "ACTIVE" in
                                        order to make fields editable.
                                        </Label> : ""
                                }
                                <Form
                                    model="accountInfo"
                                    onSubmit={(values) => this.handleSubmit(values)}
                                >
                                    <Row className="form-group">
                                        <Label htmlFor="userName" md={3}>
                                            Username
                                            </Label>
                                        <Col md={9}>
                                            <Control.text
                                                model=".userName"
                                                id="userName"
                                                name="userName"
                                                className="form-control"
                                                disabled={true}
                                                defaultValue={this.props.accountInfos.accountInfo.userName}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="roleId" md={3}>
                                            <FormLabel text="Role" required={false} />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select
                                                className="form-control"
                                                defaultValue={this.props.accountInfos.accountInfo.roleId}
                                                model=".roleId"
                                                id=".roleId"
                                                disabled={true}
                                                onClick={async (event) => {
                                                    if (
                                                        event.target.value !==
                                                        this.state.selectedRoleId
                                                    ) {
                                                        // console.log("Check event mapRoleToOption");
                                                        // console.log(event.target.value);
                                                        this.setState({
                                                            selectedRoleId: event.target.value,
                                                        });
                                                    }
                                                }}
                                            >
                                                {/* Using method to fetch */}
                                                {this.state.roleList.length !== 0 ? (
                                                    this.mapRoleToOption()
                                                ) : (
                                                        <option key={"1"} value={"1"}>
                                                            {"Loading..."}
                                                        </option>
                                                    )}
                                            </Control.select>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="departmentId" md={3}>
                                            <FormLabel text="Department" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select
                                                className="form-control"
                                                disabled={this.state.isDisabled}
                                                defaultValue={this.props.accountInfos.accountInfo.departmentId}
                                                model=".departmentId"
                                                id=".departmentId"
                                                onClick={async (event) => {
                                                    if (event.target.value !== this.state.selectedDepartmentId) {
                                                        this.setState({
                                                            selectedDepartmentId: event.target.value,
                                                        });
                                                    }
                                                }}
                                            >
                                                {/* Using method to fetch */}
                                                {this.state.departmentList.length !== 0 ? (
                                                    this.mapDepartmentToOption()
                                                ) : (
                                                        <option key={"1"} value={"1"}>
                                                            {"Loading..."}
                                                        </option>
                                                    )}
                                            </Control.select>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="firstName" md={3}>
                                            <FormLabel text="First Name" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text
                                                disabled={this.state.isDisabled}
                                                defaultValue={this.props.accountInfos.accountInfo.firstName}
                                                model=".firstName"
                                                id="firstName"
                                                name="firstName"
                                                className="form-control"
                                                validators={{ required, validName }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".firstName"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".firstName"
                                                show="touched"
                                                messages={{
                                                    validName: VALIDATION_MSG_INVALID_NAME
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="lastName" md={3}>
                                            <FormLabel text="Last Name" required={true} />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text
                                                disabled={this.state.isDisabled}
                                                defaultValue={this.props.accountInfos.accountInfo.lastName}
                                                model=".lastName"
                                                id="lastName"
                                                name="lastName"
                                                className="form-control"
                                                validators={{ required, validName }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".lastName"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".lastName"
                                                show="touched"
                                                messages={{
                                                    validName: VALIDATION_MSG_INVALID_NAME
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="gender" md={3}>
                                            <FormLabel text="Gender" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select
                                                className="form-control"
                                                disabled={this.state.isDisabled}
                                                defaultValue={this.props.accountInfos.accountInfo.gender}
                                                model=".gender"
                                                id=".gender"
                                            >
                                                <option key={"0"} value="0">
                                                    Male
                                                    </option>
                                                <option key={"1"} value="1">
                                                    Female
                                                    </option>
                                            </Control.select>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="dob" md={3}>
                                            <FormLabel text="Date of birth" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control
                                                disabled={this.state.isDisabled}
                                                type="date"
                                                model=".dob"
                                                id="dob"
                                                name="dob"
                                                className="form-control"
                                                defaultValue={this.props.accountInfos.accountInfo.dob}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="phoneNumber" md={3}>
                                            <FormLabel text="Phone Number" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text
                                                disabled={this.state.isDisabled}
                                                model=".phoneNumber"
                                                id="phoneNumber"
                                                name="phoneNumber"
                                                className="form-control"
                                                validators={{ required, validPhoneNumber }}
                                                defaultValue={this.props.accountInfos.accountInfo.phoneNumber}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".phoneNumber"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED,
                                                }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".phoneNumber"
                                                show="touched"
                                                messages={{
                                                    validPhoneNumber: VALIDATION_MSG_INVALID_PHONE_NUMBER,
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="email" md={3}>
                                            <FormLabel text="Email" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text
                                                disabled={this.state.isDisabled}
                                                model=".email"
                                                id="email"
                                                name="email"
                                                className="form-control"
                                                validators={{ required, validEmail }}
                                                defaultValue={this.props.accountInfos.accountInfo.email}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".email"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".email"
                                                show="touched"
                                                messages={{
                                                    validEmail: VALIDATION_MSG_INVALID_EMAIL
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="address" md={3}>
                                            <FormLabel text="Address" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text
                                                disabled={this.state.isDisabled}
                                                model=".address"
                                                id="address"
                                                name="address"
                                                className="form-control"
                                                validators={{ required }}
                                                defaultValue={this.props.accountInfos.accountInfo.address}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".address"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="cityProvinceId" md={3}>
                                            <FormLabel text="City/Province" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select
                                                className="form-control"
                                                disabled={this.state.isDisabled}
                                                defaultValue={this.props.accountInfos.accountInfo.cityProvinceId}
                                                model=".cityProvinceId"
                                                id=".cityProvinceId"
                                                onClick={async (event) => {
                                                    if (
                                                        event.target.value !==
                                                        this.state.selectedCityProvinceId
                                                    ) {
                                                        // console.log("Check event mapCityProvinceToOption");
                                                        // console.log(event.target.value);
                                                        this.setState({
                                                            selectedCityProvinceId: event.target.value,
                                                        });

                                                        await this.fetchDistrictIdList(event.target.value);

                                                        this.setState({
                                                            selectedDistrictId: this.state.districtIdList[0]
                                                                .districtId,
                                                        });

                                                        await this.fetchWardCommuneIdList(
                                                            this.state.districtIdList[0].districtId // Default
                                                        );

                                                        this.setState({
                                                            selectedWardCommuneId: this.state
                                                                .wardCommuneIdList[0].wardCommuneId,
                                                        });
                                                    }
                                                }}
                                            >
                                                {this.state.cityProvinceIdList.length !== 0 ? (
                                                    this.mapCityProvinceToOption()
                                                ) : (
                                                        <option key={"1"} value={"1"}>
                                                            {"Loading..."}
                                                        </option>
                                                    )}
                                            </Control.select>
                                            <Errors
                                                className="text-danger"
                                                model=".cityProvinceId"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="district" md={3}>
                                            <FormLabel text="District" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select
                                                className="form-control"
                                                disabled={this.state.isDisabled}
                                                defaultValue={this.props.accountInfos.accountInfo.districtId}
                                                model=".districtId"
                                                id=".districtId"
                                                onClick={async (event) => {
                                                    if (event.target.value !== this.state.selectedDistrictId) {
                                                        this.setState({
                                                            selectedDistrictId: event.target.value,
                                                        });

                                                        await this.fetchWardCommuneIdList(event.target.value);

                                                        this.setState({
                                                            selectedWardCommuneId: this.state
                                                                .wardCommuneIdList[0].wardCommuneId,
                                                        });
                                                    }
                                                }}
                                            >
                                                {/* Using method to fetch */}
                                                {this.state.districtIdList.length !== 0 ?
                                                    (this.mapDistrictToOption()) :
                                                    (<option key={"1"} value={"1"}>Loading...</option>)
                                                }
                                            </Control.select>
                                            <Errors
                                                className="text-danger"
                                                model=".districtId"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="wardCommuneId" md={3}>
                                            <FormLabel text="Ward/Commune" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select
                                                className="form-control"
                                                disabled={this.state.isDisabled}
                                                defaultValue={this.props.accountInfos.accountInfo.wardCommuneId}
                                                model=".wardCommuneId"
                                                id=".wardCommuneId"
                                                onClick={async (event) => {
                                                    if (event.target.value !== this.state.selectedWardCommuneId) {
                                                        this.setState({
                                                            selectedWardCommuneId: event.target.value,
                                                        });
                                                    }
                                                }}
                                            >
                                                {/* Using method to fetch */}
                                                {this.state.wardCommuneIdList.length !== 0 ? (
                                                    this.mapWardCommuneToOption()
                                                ) : (
                                                        <option key={"1"} value={"1"}>
                                                            {"Loading..."}
                                                        </option>
                                                    )}
                                            </Control.select>
                                            <Errors
                                                className="text-danger"
                                                model=".wardCommuneId"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    {this.props.accountInfos.accountInfo.roleId !== ROLE_ADMIN ?
                                        <Row className="form-group">
                                            <Label htmlFor="disableFlag" md={3}>
                                                <FormLabel text="Account Status" required='true' />
                                            </Label>
                                            <Col md={9}>
                                                <Control.select
                                                    className="form-control"
                                                    defaultValue={this.props.accountInfos.accountInfo.disableFlag}
                                                    model=".disableFlag"
                                                    id=".disableFlag"
                                                >
                                                    <option key={"0"} value={"0"}
                                                        style={{ color: "green" }}>
                                                        Active
                                                        </option>
                                                    <option key={"1"} value={"1"}
                                                        style={{ color: "red" }}>
                                                        Disabled
                                                        </option>
                                                </Control.select>
                                            </Col>
                                        </Row>
                                        : ""
                                    }

                                    <Row className="form-group">
                                        <Col md={3}>
                                            <Button
                                                type="submit"
                                                color="warning"
                                                className="submitButton"
                                            >
                                                Submit
                                                </Button>
                                        </Col>
                                    </Row>
                                </Form>

                                <Toast
                                    isOpen={this.state.isToastOpen}
                                    style={{
                                        boxShadow: "0 0.05rem 0.75rem #303841",
                                        position: "fixed",
                                        bottom: "10px",
                                        right: "10px",
                                        backgroundColor: "white",
                                        zIndex: "1"
                                    }}
                                >
                                    <ToastHeader
                                        toggle={() => {
                                            this.setState({
                                                isToastOpen: !this.state.isToastOpen,
                                            });
                                        }}
                                    >
                                        Information
                                        </ToastHeader>
                                    <ToastBody>
                                        {this.props.accountInfos.successStatus
                                            ? this.props.accountInfos.successMessage
                                            :
                                            this.props.accountInfos.errMess !== null ?
                                                this.mapErrMessToParagraph(this.props.accountInfos.errMess) : ""}
                                    </ToastBody>
                                </Toast>
                            </div>
                        </div>
                    </div>
                );
            } else {
                return (
                    <div className="container">
                        <Loading />
                    </div>
                )
            }
        } else if (errMess != null) {
            return (
                // <Loading />
                <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
            );
        } else {
            return <Loading />;
        }
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        accountInfos: state.accountInfos,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchAccountInfo: (userName) => {
            dispatch(fetchAccountInfo(userName));
        },
        modifyAccountInfo: async (
            userName,
            password,
            oldPassword,
            isChangePassword,
            roleId,
            departmentId,
            firstName,
            lastName,
            gender,
            dob,
            email,
            phoneNumber,
            cityProvinceId,
            districtId,
            wardCommuneId,
            address,
            disableFlag,
            screen_id
        ) => {
            dispatch(
                modifyAccountInfo(
                    userName,
                    password,
                    oldPassword,
                    isChangePassword,
                    roleId,
                    departmentId,
                    firstName,
                    lastName,
                    gender,
                    dob,
                    email,
                    phoneNumber,
                    cityProvinceId,
                    districtId,
                    wardCommuneId,
                    address,
                    disableFlag,
                    screen_id
                )
            );
        },
        resetAccountInfoForm: () => {
            dispatch(actions.reset("accountInfo"));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModifyAccountInfo);