import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, Col } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import { ROLE_ADMIN, ROLE_OFFICE_EMPLOYEE, ROLE_FACTORY_MANAGER, ROLE_OFFICE_MANAGER } from '../../shared/constants';
import { connect } from 'react-redux';
import { InitialSearchAccountList } from '../../redux/reducers/forms';

class AccountListSearchForm extends Component {
    constructor(props) {
        super(props);
        this.handleSearch = this.handleSearch.bind(this);
    }

    handleSearch(event) {
        let searchData = {
            userName: this.userName.value,
            fullName: this.fullName.value,
            phoneNumber: this.phoneNumber.value,
            email: this.email.value,
        }
        if (this.role.value !== 'All') {
            searchData.roleId = this.role.value;
        }
        if (this.status.value !== 'All') {
            searchData.disableFlag = this.status.value;
        }

        this.props.fetchAccountSearch(1, searchData);
        event.preventDefault();
    }
    render() {
        const searchData = (this.props.accounts.searchData != null) ? this.props.accounts.searchData : InitialSearchAccountList;
        return (
            <Form
                innerRef={form => this.form = form}
                onSubmit={this.handleSearch}>
                <FormGroup row>
                    <Label sm={3} htmlFor="userName">Username</Label>
                    <Col sm={9}>
                        <Input type="text" id="userName" name="userName"
                            defaultValue={searchData.userName}
                            innerRef={(input) => this.userName = input} />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label sm={3} htmlFor="fullName">Full Name</Label>
                    <Col sm={9}>
                        <Input type="text" id="fullName" name="fullName"
                            defaultValue={searchData.fullName}
                            innerRef={(input) => this.fullName = input} />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label sm={3} htmlFor="email">Email</Label>
                    <Col sm={9}>
                        <Input type="email" id="email" name="email"
                            defaultValue={searchData.email}
                            innerRef={(input) => this.email = input} />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label sm={3} htmlFor="phoneNumber">Phone number</Label>
                    <Col sm={9}>
                        <Input type="phoneNumber" id="phoneNumber" name="phoneNumber"
                            defaultValue={searchData.phoneNumber}
                            innerRef={(input) => this.phoneNumber = input} />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="role" sm={3}>Role</Label>
                    <Col sm={6} lg={4}>
                        <Input type="select" name="role" id="role"
                            defaultValue={(searchData.roleId != null) ? searchData.roleId : 'All'}
                            innerRef={(input) => this.role = input}>
                            <option value="All">All</option>
                            <option value={ROLE_ADMIN}>Administrator</option>
                            <option value={ROLE_OFFICE_MANAGER}>Office Manager</option>
                            <option value={ROLE_OFFICE_EMPLOYEE}>Office Employee</option>
                            <option value={ROLE_FACTORY_MANAGER}>Factory Manager</option>
                        </Input>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="status" sm={3}>Status</Label>
                    <Col sm={6} lg={4}>
                        <Input type="select" name="status" id="status"
                            defaultValue={(searchData.disableFlag != null) ? searchData.disableFlag : 'All'}
                            innerRef={(input) => this.status = input}>
                            <option value="All">All</option>
                            <option value={0}>Active</option>
                            <option value={1}>Disabled</option>
                        </Input>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col xs={4} sm={2}>
                        <Button
                            type="submit"
                            color="warning">Search</Button>

                    </Col>                    
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        accounts: state.accounts
    };
}


export default withRouter(connect(mapStateToProps)(AccountListSearchForm));