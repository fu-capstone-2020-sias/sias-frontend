import React, { Component } from "react";
import {
    Breadcrumb,
    BreadcrumbItem,
    Label,
    Col,
    Row,
} from "reactstrap";
import { Form, Control } from "react-redux-form";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { fetchAccountInfo } from "../../redux/actions/ActionCreators";
import { ROUTE_HOME, ERROR_PAGE_NOT_FOUND } from "../../shared/constants";
import ErrorDisplay from "../common/ErrorDisplay";
import { Loading } from "../common/LoadingComponent";

class ViewUserInfo extends Component {
    state = {
        account: null
    }

    async componentDidMount() {
        const search = window.location.search;
        const params = new URLSearchParams(search);
        const userName = params.get('userName');
        if (userName != null)
            await this.props.fetchAccountInfo(userName);
    }

    render() {
        const search = window.location.search;
        const params = new URLSearchParams(search);
        // const userName = params.get('userName');
        const errMess = this.props.accountInfos.errMess;
        if (this.props.loginAccount.account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first.</h1>
                    </div>
                </div>
            );
        } else if (this.props.accountInfos.accountInfo !== null) {
            return (
                <div className="container" style={{ position: "relative" }}>
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem>
                                <Link to={ROUTE_HOME}>Home</Link>
                            </BreadcrumbItem>
                            <BreadcrumbItem>User Profile</BreadcrumbItem>
                            <BreadcrumbItem active>View User Profile</BreadcrumbItem>
                        </Breadcrumb>
                    </div>
                    <div className="row">
                        <div className="col-12" style={{ display: "flex", justifyContent: "space-between" }}>
                            <h3>View User Profile</h3>
                        </div>
                        <div className={"col-12"}>
                            <hr />
                        </div>
                        <div className="col-12 col-md-12">
                            <Form
                                model="accountInfo"
                                onSubmit={(values) => this.handleSubmit(values)}
                            >
                                <Row className="form-group">
                                    <Label htmlFor="userName" md={2}>
                                        Username
                                        </Label>
                                    <Col md={6}>
                                        <Control.text
                                            model=".userName"
                                            id="userName"
                                            name="userName"
                                            className="form-control"
                                            disabled={true}
                                            value={this.props.accountInfos.accountInfo.userName}
                                        />
                                    </Col>
                                </Row>

                                <Row className="form-group">
                                    <Label htmlFor="firstName" md={2}>
                                        First Name
                                        </Label>
                                    <Col md={6}>
                                        <Control.text
                                            model=".firstName"
                                            id="firstName"
                                            name="firstName"
                                            className="form-control"
                                            disabled={true}
                                            value={this.props.accountInfos.accountInfo.firstName}
                                        />
                                    </Col>
                                </Row>

                                <Row className="form-group">
                                    <Label htmlFor="lastName" md={2}>
                                        Last Name
                                        </Label>
                                    <Col md={6}>
                                        <Control.text
                                            model=".lastName"
                                            id="lastName"
                                            name="lastName"
                                            className="form-control"
                                            disabled={true}
                                            value={this.props.accountInfos.accountInfo.lastName}
                                        />
                                    </Col>
                                </Row>

                                <Row className="form-group">
                                    <Label htmlFor="gender" md={2}>
                                        Gender
                                        </Label>
                                    <Col md={6}>
                                        <Control.text
                                            model=".gender"
                                            id="gender"
                                            name="gender"
                                            className="form-control"
                                            disabled={true}
                                            value={this.props.accountInfos.accountInfo.gender === "1" ? "Female" : "Male"}
                                        />
                                    </Col>
                                </Row>


                                <Row className="form-group">
                                    <Label htmlFor="dob" md={2}>
                                        Date of Birth
                                        </Label>
                                    <Col md={6}>
                                        <Control.text
                                            model=".dob"
                                            id="dob"
                                            name="dob"
                                            className="form-control"
                                            disabled={true}
                                            value={this.props.accountInfos.accountInfo.dob}
                                        />
                                    </Col>
                                </Row>

                                <Row className="form-group">
                                    <Label htmlFor="email" md={2}>
                                        Email
                                        </Label>
                                    <Col md={6}>
                                        <Control.text
                                            model=".email"
                                            id="email"
                                            name="email"
                                            className="form-control"
                                            disabled={true}
                                            value={this.props.accountInfos.accountInfo.email}
                                        />
                                    </Col>
                                </Row>

                                <Row className="form-group">
                                    <Label htmlFor="phoneNumber" md={2}>
                                        Phone Number
                                        </Label>
                                    <Col md={6}>
                                        <Control.text
                                            model=".phoneNumber"
                                            id="phoneNumber"
                                            name="phoneNumber"
                                            className="form-control"
                                            disabled={true}
                                            value={this.props.accountInfos.accountInfo.phoneNumber}
                                        />
                                    </Col>
                                </Row>

                                <Row className="form-group">
                                    <Label htmlFor="role" md={2}>
                                        Role
                                        </Label>
                                    <Col md={6}>
                                        <Control.text
                                            model=".role"
                                            id="role"
                                            name="role"
                                            className="form-control"
                                            disabled={true}
                                            value={this.props.accountInfos.accountInfo.role}
                                        />
                                    </Col>
                                </Row>

                                <Row className="form-group">
                                    <Label htmlFor="department" md={2}>
                                        Department
                                        </Label>
                                    <Col md={6}>
                                        <Control.text
                                            model=".department"
                                            id="department"
                                            name="department"
                                            className="form-control"
                                            disabled={true}
                                            value={this.props.accountInfos.accountInfo.department}
                                        />
                                    </Col>
                                </Row>

                                <Row className="form-group">
                                    <Label htmlFor="address" md={2}>
                                        Address
                                        </Label>
                                    <Col md={6}>
                                        <Control.text
                                            model=".address"
                                            id="address"
                                            name="address"
                                            className="form-control"
                                            disabled={true}
                                            value={this.props.accountInfos.accountInfo.address}
                                        />
                                    </Col>
                                </Row>

                                <Row className="form-group">
                                    <Label htmlFor="cityProvince" md={2}>
                                        City/Province
                                        </Label>
                                    <Col md={6}>
                                        <Control.text
                                            model=".cityProvince"
                                            id="cityProvince"
                                            name="cityProvince"
                                            className="form-control"
                                            disabled={true}
                                            value={this.props.accountInfos.accountInfo.cityProvince}
                                        />
                                    </Col>
                                </Row>

                                <Row className="form-group">
                                    <Label htmlFor="district" md={2}>
                                        District
                                        </Label>
                                    <Col md={6}>
                                        <Control.text
                                            model=".district"
                                            id="district"
                                            name="district"
                                            className="form-control"
                                            disabled={true}
                                            value={this.props.accountInfos.accountInfo.district}
                                        />
                                    </Col>
                                </Row>

                                <Row className="form-group">
                                    <Label htmlFor="wardCommune" md={2}>
                                        Ward/Commune
                                        </Label>
                                    <Col md={6}>
                                        <Control.text
                                            model=".wardCommune"
                                            id="wardCommune"
                                            name="wardCommune"
                                            className="form-control"
                                            disabled={true}
                                            value={this.props.accountInfos.accountInfo.wardCommune}
                                        />
                                    </Col>
                                </Row>
                            </Form>
                        </div>
                    </div>
                </div>
            );
        } else if (errMess != null) {
            return (
                // <Loading />
                <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
            );
        } else {
            return <Loading />;
        }
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        accountInfos: state.accountInfos,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchAccountInfo: (userName) => {
            dispatch(fetchAccountInfo(userName));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewUserInfo);
