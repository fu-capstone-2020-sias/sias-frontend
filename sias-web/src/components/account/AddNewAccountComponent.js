import React, { Component } from "react";
import {
    Breadcrumb,
    BreadcrumbItem,
    Button,
    Label,
    Col,
    Row,
} from "reactstrap";
import { Toast, ToastBody, ToastHeader } from "reactstrap";
import { Form, Control, Errors } from "react-redux-form";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { addNewAccount } from "../../redux/actions/ActionCreators";
import { ROLE_ADMIN, VALIDATION_MSG_MAX_LENGTH, ROUTE_HOME, VALIDATION_MSG_REQUIRED, VALIDATION_MSG_INVALID_USERNAME, VALIDATION_MSG_INVALID_NAME, VALIDATION_MSG_INVALID_EMAIL, VALIDATION_MSG_INVALID_PHONE_NUMBER } from "../../shared/constants";
import { actions } from "react-redux-form";
import {
    fetchCityProvinceIdList,
    fetchDistrictIdList,
    fetchWardCommuneIdList,
    fetchRoleList,
    fetchDepartmentList
} from "../../shared/utils";
import { Loading } from "../common/LoadingComponent";
import FormLabel from '../common/FormLabel';
import UnauthorizedAccessDisplay from "../common/UnauthorizedAccessDisplay";
import { required, validUserName, validName, validPhoneNumber, validEmail, validPassword, maxLength } from '../../shared/fieldValidation';

class AddNewAccount extends Component {
    handleSubmit = async (values) => {
        await this.props.addNewAccount(
            values.userName,
            values.password,
            "1",
            values.roleId,
            values.departmentId,
            values.firstName,
            values.lastName,
            values.gender,
            values.dob,
            values.email,
            values.phoneNumber,
            this.state.selectedCityProvinceId,
            this.state.selectedDistrictId,
            this.state.selectedWardCommuneId,
            values.address,
            values.disableFlag,
            "WEB_UC08"
        );

        this.setState({
            isToastOpen: true
        })

        setTimeout(() => {
            this.setState({
                isToastOpen: false
            });
            this.props.resetAddNewAccountForm();
        }, 5000)
    };

    fetchRoleList = async () => {
        let response = await fetchRoleList();

        this.setState({
            roleList: response.roles,
        });
    };

    fetchDepartmentList = async () => {
        let response = await fetchDepartmentList();

        this.setState({
            departmentList: response.data.rows,
        });
    };

    fetchCityProvinceIdList = async () => {
        let response = await fetchCityProvinceIdList();

        this.setState({
            cityProvinceIdList: response.data.rows,
        });
    };

    fetchDistrictIdList = async (cityProvinceId) => {
        let response = await fetchDistrictIdList(cityProvinceId);

        this.setState({
            districtIdList: response.data.rows,
        });
    };

    fetchWardCommuneIdList = async (districtId) => {
        let response = await fetchWardCommuneIdList(districtId);

        this.setState({
            wardCommuneIdList: response.data.rows,
        });
    };

    mapErrMessToParagraph = (errMessArr) => {
        return errMessArr.map((errMessObj) => {
            return <p>{errMessObj}</p>
        })
    }

    mapRoleToOption = () => {
        return this.state.roleList.map((roleObj) => (
            <option
                key={roleObj.roleId}
                value={roleObj.roleId}
            >
                {roleObj.name}
            </option>
        ));
    };

    mapDepartmentToOption = () => {
        return this.state.departmentList.map((departmentObj) => (
            <option
                key={departmentObj.departmentId}
                value={departmentObj.departmentId}
            >
                {departmentObj.name}
            </option>
        ));
    };

    mapCityProvinceToOption = () => {
        return this.state.cityProvinceIdList.map((cityProvinceObj) => (
            <option
                key={cityProvinceObj.cityProvinceId}
                value={cityProvinceObj.cityProvinceId}
            >
                {cityProvinceObj.name}
            </option>
        ));
    };

    mapDistrictToOption = () => {
        return this.state.districtIdList.map((districtObj) => (
            <option key={districtObj.districtId}
                value={districtObj.districtId}
            >
                {districtObj.name}
            </option>
        ));
    };

    mapWardCommuneToOption = () => {
        return this.state.wardCommuneIdList.map((wardCommuneObj) => (
            <option
                key={wardCommuneObj.wardCommuneId}
                value={wardCommuneObj.wardCommuneId}
            >
                {wardCommuneObj.name}
            </option>
        ));
    };

    state = {
        roleList: [],
        departmentList: [],
        cityProvinceIdList: [],
        districtIdList: [],
        wardCommuneIdList: [],
        selectedRoleId: "",
        selectedDepartmentId: "",
        selectedCityProvinceId: "",
        selectedDistrictId: "",
        selectedWardCommuneId: "",
        isToastOpen: false,
        firstLoad: false,
        visibleNewPassword: false
    };

    async componentDidMount() {
        const account = this.props.loginAccount.account;
        this.props.resetAddNewAccountForm();
        this.setState({
            account: account,
        });


    }

    render() {
        if (this.state.account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first.</h1>
                    </div>
                </div>
            );
        } else if (this.state.account.roleId !== ROLE_ADMIN) {
            return (
                <UnauthorizedAccessDisplay />
            );
        } else {
            if (this.state.account !== null) {
                if (this.state.firstLoad === false) {
                    this.fetchRoleList();
                    this.fetchDepartmentList();
                    this.fetchCityProvinceIdList();
                    this.fetchDistrictIdList("1");
                    this.fetchWardCommuneIdList("1");
                    this.setState({
                        firstLoad: true,
                        selectedCityProvinceId: "1",
                        selectedDistrictId: "1",
                        selectedWardCommuneId: "1",
                        selectedRoleId: "R001",
                        selectedDepartmentId: "DEPT001",
                    });
                }

                return (
                    <div className="container" style={{ position: "relative" }}>
                        <div className="row">
                            <Breadcrumb>
                                <BreadcrumbItem>
                                    <Link to={ROUTE_HOME}>Home</Link>
                                </BreadcrumbItem>
                                <BreadcrumbItem>Account Management
                                </BreadcrumbItem>
                                <BreadcrumbItem active>Add New Account</BreadcrumbItem>
                            </Breadcrumb>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <h3>Add New Account</h3>
                                <hr />
                            </div>
                            <div className="col-12 col-md-10 offset-md-1 col-xl-8 offset-xl-2 mb-5">
                                <Form
                                    model="addNewAccount"
                                    onSubmit={(values) => this.handleSubmit(values)}
                                >
                                    <Row className="form-group">
                                        <Label htmlFor="userName" md={3}>
                                            <FormLabel text="Username" required='true' />
                                        </Label>
                                        <Col md={8}>
                                            <Control.text
                                                model=".userName"
                                                id="userName"
                                                name="userName"
                                                className="form-control"
                                                validators={{ required, validUserName }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".userName"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".userName"
                                                show="touched"
                                                messages={{
                                                    validUserName: VALIDATION_MSG_INVALID_USERNAME
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="password" md={3}>
                                            <FormLabel text="Password" required='true' />
                                        </Label>
                                        <Col md={8}>
                                            <Control.input
                                                type={this.state.visibleNewPassword ? "text" : "password"}
                                                model=".password"
                                                id="password"
                                                name="password"
                                                className="form-control"
                                                validators={{ required, validPassword, maxLength: maxLength(32) }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".password"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".password"
                                                show="touched"
                                                messages={{
                                                    validPassword: "Password must meet minimum " +
                                                        "eight characters, at least one uppercase letter, " +
                                                        "one lowercase letter, " +
                                                        "one number and one special character (@, $, !, %, *, ?, &)."
                                                }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".password"
                                                show="touched"
                                                messages={{
                                                    maxLength: VALIDATION_MSG_MAX_LENGTH(32)
                                                }}
                                            />
                                        </Col>
                                        <Col md={1}>
                                            <i onClick={() => {
                                                this.setState({
                                                    visibleNewPassword: !this.state.visibleNewPassword
                                                })
                                            }}
                                                className={this.state.visibleNewPassword ?
                                                    "fa fa-eye-slash fa-lg" : "fa fa-eye fa-lg"} />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="roleId" md={3}>
                                            <FormLabel text="Role" required='true' />
                                        </Label>
                                        <Col md={8}>
                                            <Control.select
                                                className="form-control"
                                                defaultValue={"R001"}
                                                model=".roleId"
                                                id=".roleId"
                                                onClick={async (event) => {
                                                    if (event.target.value !== this.state.selectedRoleId) {
                                                        this.setState({
                                                            selectedRoleId: event.target.value,
                                                        });
                                                    }
                                                }}
                                            >
                                                {/* Using method to fetch */}
                                                {this.state.roleList.length !== 0 ? (
                                                    this.mapRoleToOption()
                                                ) : (
                                                        <option key={"1"} value={"1"}>
                                                            {"Loading..."}
                                                        </option>
                                                    )}
                                            </Control.select>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="departmentId" md={3}>
                                            <FormLabel text="Department" required='true' />
                                        </Label>
                                        <Col md={8}>
                                            <Control.select
                                                className="form-control"
                                                defaultValue={"DEPT001"}
                                                model=".departmentId"
                                                id=".departmentId"
                                                onClick={async (event) => {
                                                    if (event.target.value !== this.state.selectedDepartmentId) {
                                                        this.setState({
                                                            selectedDepartmentId: event.target.value,
                                                        });
                                                    }
                                                }}
                                            >
                                                {/* Using method to fetch */}
                                                {this.state.departmentList.length !== 0 ? (
                                                    this.mapDepartmentToOption()
                                                ) : (
                                                        <option key={"1"} value={"1"}>
                                                            {"Loading..."}
                                                        </option>
                                                    )}
                                            </Control.select>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="firstName" md={3}>
                                            <FormLabel text="First Name" required='true' />
                                        </Label>
                                        <Col md={8}>
                                            <Control.text

                                                model=".firstName"
                                                id="firstName"
                                                name="firstName"
                                                className="form-control"
                                                validators={{ required, validName }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".firstName"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".firstName"
                                                show="touched"
                                                messages={{
                                                    validName: VALIDATION_MSG_INVALID_NAME
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="lastName" md={3}>
                                            <FormLabel text="Last Name" required='true' />
                                        </Label>
                                        <Col md={8}>
                                            <Control.text

                                                model=".lastName"
                                                id="lastName"
                                                name="lastName"
                                                className="form-control"
                                                validators={{ required, validName }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".lastName"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".lastName"
                                                show="touched"
                                                messages={{
                                                    validName: VALIDATION_MSG_INVALID_NAME
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="gender" md={3}>
                                            <FormLabel text="Gender" required='true' />
                                        </Label>
                                        <Col md={8}>
                                            <Control.select
                                                className="form-control"
                                                defaultValue={"0"}
                                                model=".gender"
                                                id=".gender"
                                            >
                                                <option key={"0"} value="0"
                                                >
                                                    Male
                                                </option>
                                                <option key={"1"} value="1"
                                                >
                                                    Female
                                                </option>
                                            </Control.select>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="dob" md={3}>
                                            <FormLabel text="Date of birth" required='true' />
                                        </Label>
                                        <Col md={8}>
                                            <Control
                                                defaultValue={"1970-01-01"}
                                                type="date"
                                                model=".dob"
                                                id="dob"
                                                name="dob"
                                                className="form-control"

                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="phoneNumber" md={3}>
                                            <FormLabel text="Phone Number" required='true' />
                                        </Label>
                                        <Col md={8}>
                                            <Control.text

                                                model=".phoneNumber"
                                                id="phoneNumber"
                                                name="phoneNumber"
                                                className="form-control"
                                                validators={{ required, validPhoneNumber }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".phoneNumber"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED,
                                                }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".phoneNumber"
                                                show="touched"
                                                messages={{
                                                    validPhoneNumber: VALIDATION_MSG_INVALID_PHONE_NUMBER,
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="email" md={3}>
                                            <FormLabel text="Email" required='true' />
                                        </Label>
                                        <Col md={8}>
                                            <Control.text

                                                model=".email"
                                                id="email"
                                                name="email"
                                                className="form-control"
                                                validators={{ required, validEmail }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".email"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".email"
                                                show="touched"
                                                messages={{
                                                    validEmail: VALIDATION_MSG_INVALID_EMAIL
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="address" md={3}>
                                            <FormLabel text="Address" required='true' />
                                        </Label>
                                        <Col md={8}>
                                            <Control.text
                                                model=".address"
                                                id="address"
                                                name="address"
                                                className="form-control"
                                                validators={{ required }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".address"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="cityProvinceId" md={3}>
                                            <FormLabel text="City/Province" required='true' />
                                        </Label>
                                        <Col md={8}>
                                            <Control.select
                                                className="form-control"
                                                model=".cityProvinceId"
                                                id=".cityProvinceId"
                                                onClick={async (event) => {
                                                    if (event.target.value !== this.state.selectedCityProvinceId) {
                                                        this.setState({
                                                            selectedCityProvinceId: event.target.value,
                                                        });

                                                        await this.fetchDistrictIdList(event.target.value);

                                                        this.setState({
                                                            selectedDistrictId: this.state.districtIdList[0]
                                                                .districtId,
                                                        });

                                                        await this.fetchWardCommuneIdList(
                                                            this.state.districtIdList[0].districtId // Default
                                                        );

                                                        this.setState({
                                                            selectedWardCommuneId: this.state
                                                                .wardCommuneIdList[0].wardCommuneId,
                                                        });
                                                    }
                                                }}
                                            >
                                                {this.state.cityProvinceIdList.length !== 0 ? (
                                                    this.mapCityProvinceToOption()
                                                ) : (
                                                        <option key={"1"} value={"1"}>
                                                            {"Loading..."}
                                                        </option>
                                                    )}
                                            </Control.select>
                                            <Errors
                                                className="text-danger"
                                                model=".cityProvinceId"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="district" md={3}>
                                            <FormLabel text="District" required='true' />
                                        </Label>
                                        <Col md={8}>
                                            <Control.select
                                                className="form-control"
                                                model=".districtId"
                                                id=".districtId"
                                                onClick={async (event) => {
                                                    if (event.target.value !== this.state.selectedDistrictId) {
                                                        this.setState({
                                                            selectedDistrictId: event.target.value,
                                                        });

                                                        await this.fetchWardCommuneIdList(event.target.value);

                                                        this.setState({
                                                            selectedWardCommuneId: this.state
                                                                .wardCommuneIdList[0].wardCommuneId,
                                                        });
                                                    }
                                                }}
                                            >
                                                {/* Using method to fetch */}
                                                {this.state.districtIdList.length !== 0 ? (
                                                    this.mapDistrictToOption()
                                                ) : (
                                                        <option key={"1"} value={"1"}>
                                                            {"Loading..."}
                                                        </option>
                                                    )}
                                            </Control.select>
                                            <Errors
                                                className="text-danger"
                                                model=".districtId"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="wardCommuneId" md={3}>
                                            <FormLabel text="Ward/Commune" required='true' />
                                        </Label>
                                        <Col md={8}>
                                            <Control.select
                                                className="form-control"
                                                model=".wardCommuneId"
                                                id=".wardCommuneId"
                                                onClick={async (event) => {
                                                    if (event.target.value !== this.state.selectedWardCommuneId) {
                                                        this.setState({
                                                            selectedWardCommuneId: event.target.value,
                                                        });
                                                    }
                                                }}
                                            >
                                                {/* Using method to fetch */}
                                                {this.state.wardCommuneIdList.length !== 0 ? (
                                                    this.mapWardCommuneToOption()
                                                ) : (
                                                        <option key={"1"} value={"1"}>
                                                            {"Loading..."}
                                                        </option>
                                                    )}
                                            </Control.select>
                                            <Errors
                                                className="text-danger"
                                                model=".wardCommuneId"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED,
                                                }}
                                            />
                                        </Col>
                                    </Row>


                                    <Row className="form-group">
                                        <Label htmlFor="disableFlag" md={3}>
                                            <FormLabel text="Account Status" required='true' />
                                        </Label>
                                        <Col md={8}>
                                            <Control.select
                                                className="form-control"
                                                model=".disableFlag"
                                                id=".disableFlag"
                                            >
                                                <option key={"0"} value={"0"}
                                                    style={{ color: "green" }}>
                                                    Active
                                                </option>
                                                <option key={"1"} value={"1"}
                                                    style={{ color: "red" }}>
                                                    Disabled
                                                </option>
                                            </Control.select>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Col md={3}>
                                            <Button
                                                type="submit"
                                                color="warning"
                                                className="submitButton"
                                            >
                                                Submit
                                            </Button>
                                        </Col>
                                    </Row>
                                </Form>

                                <Toast
                                    isOpen={this.state.isToastOpen}
                                    style={{
                                        boxShadow: "0 0.05rem 0.75rem #303841",
                                        position: "fixed",
                                        bottom: "10px",
                                        right: "10px",
                                        backgroundColor: "white",
                                        zIndex: "1"
                                    }}
                                >
                                    <ToastHeader
                                        toggle={() => {
                                            this.setState({
                                                isToastOpen: !this.state.isToastOpen,
                                            });
                                        }}
                                    >
                                        Information
                                    </ToastHeader>
                                    <ToastBody>
                                        {this.props.addNewAccounts.successStatus
                                            ? this.props.addNewAccounts.successMessage
                                            :
                                            this.props.addNewAccounts.errMess !== null ?
                                                this.mapErrMessToParagraph(this.props.addNewAccounts.errMess) : ""}
                                    </ToastBody>
                                </Toast>

                            </div>
                        </div>
                    </div>
                );

            } else {
                return (
                    <div className={"container"}>
                        <Loading />
                    </div>
                )
            }
        }
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        addNewAccounts: state.addNewAccounts
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addNewAccount: async (
            userName,
            password,
            isChangePassword,
            roleId,
            departmentId,
            firstName,
            lastName,
            gender,
            dob,
            email,
            phoneNumber,
            cityProvinceId,
            districtId,
            wardCommuneId,
            address,
            disableFlag,
            screen_id
        ) => {
            dispatch(addNewAccount(
                userName,
                password,
                isChangePassword,
                roleId,
                departmentId,
                firstName,
                lastName,
                gender,
                dob,
                email,
                phoneNumber,
                cityProvinceId,
                districtId,
                wardCommuneId,
                address,
                disableFlag,
                screen_id
            ));
        },
        resetAddNewAccountForm: () => {
            dispatch(actions.reset("addNewAccount"));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddNewAccount);