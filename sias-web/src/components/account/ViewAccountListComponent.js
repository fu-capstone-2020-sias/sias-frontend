import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Toast, ToastBody, ToastHeader } from 'reactstrap';
import { Link } from 'react-router-dom';
import AccountListSearchForm from './ViewAccountListSearchFormComponent';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Paper from '@material-ui/core/Paper';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

import { connect } from 'react-redux';
import { ROLE_ADMIN, ROUTE_VIEW_ACCOUNT_LIST, ROUTE_HOME, ERROR_PAGE_NOT_FOUND } from '../../shared/constants';
import { getValueOf } from '../../shared/utils';
import { StyledTableCell } from '../common/StyledTableCell';
import TablePaging from '../common/TablePaging';
import DisableFlagDisplay from '../common/DisableFlagDisplay';
import { deleteDisabledAccount, fetchAccount } from '../../redux/actions/ActionCreators';
import { Loading } from '../common/LoadingComponent';
import UnauthorizedAccessDisplay from '../common/UnauthorizedAccessDisplay';
import ErrorDisplay from '../common/ErrorDisplay';

const useStyles = makeStyles({
    table: {
        minWidth: 600,
    }
});

const headCells = [
    { id: 'row_num', numeric: true, disablePadding: false, label: 'Row' },
    { id: 'userName', numeric: false, disablePadding: false, label: 'Username' },
    { id: 'fullName', numeric: false, disablePadding: false, label: 'Full Name' },
    { id: 'gender', numeric: false, disablePadding: false, label: 'Gender' },
    { id: 'phoneNumber', numeric: false, disablePadding: false, label: 'Phone no.' },
    { id: 'email', numeric: false, disablePadding: false, label: 'Email' },
    { id: 'department', numeric: false, disablePadding: false, label: 'Department' },
    { id: 'role', numeric: false, disablePadding: false, label: 'Role' },
    { id: 'status', numeric: false, disablePadding: false, label: 'Status' }
];

function AccountListTableHead() {
    return (
        <TableHead>
            <TableRow>
                {headCells.map((headCell) => (
                    <StyledTableCell
                        key={headCell.id}
                        align={headCell.numeric ? 'right' : 'left'}
                        padding={headCell.disablePadding ? 'none' : 'default'}
                    >
                        {headCell.label}
                    </StyledTableCell>
                ))}
                <StyledTableCell></StyledTableCell>
                <StyledTableCell></StyledTableCell>
            </TableRow>
        </TableHead>
    );
}

AccountListTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    rowCount: PropTypes.number.isRequired
};

function AccountListTable(props) {
    const classes = useStyles();
    let rows = props.accountList;

    return (
        <React.Fragment>
            <div className="col-12 col-md-10 col-lg-9 col-xl-8">
                <AccountListSearchForm fetchAccountSearch={props.fetchAccountSearch} />
            </div>
            <hr />
            <div className="col-12 mt-5">
                <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="customized table">
                        <AccountListTableHead
                            classes={classes}
                            rowCount={rows.length}
                        />
                        <TableBody>
                            {rows.map((row, index) => {
                                return (
                                    <TableRow key={row.row_num}
                                        style={index % 2 ? { background: "#f2f2f2" } : { background: "white" }}>
                                        <StyledTableCell
                                            width="3%"
                                            align="right">{getValueOf(row.row_num)}</StyledTableCell>
                                        <StyledTableCell width="7%">{getValueOf(row.userName)}</StyledTableCell>
                                        <StyledTableCell
                                            width="20%">{getValueOf(row.firstName + ' ' + row.lastName)}</StyledTableCell>
                                        <StyledTableCell
                                            width="5%">{row.gender === 0 ? 'Male' : 'Female'}</StyledTableCell>
                                        <StyledTableCell width="7%">{getValueOf(row.phoneNumber)}</StyledTableCell>
                                        <StyledTableCell width="10%">{getValueOf(row.email)}</StyledTableCell>
                                        <StyledTableCell width="10%">{getValueOf(row.department)}</StyledTableCell>
                                        <StyledTableCell width="5%">{getValueOf(row.role)}</StyledTableCell>
                                        <StyledTableCell
                                            width="5%"><DisableFlagDisplay disableFlag={row.disableFlag} /></StyledTableCell>
                                        <TableCell width="5%"><Link
                                            to={`/modifyAccountInfo?userName=${row.userName}`}
                                            target="_blank">Edit</Link>
                                        </TableCell>
                                        <TableCell width="5%">
                                            {(row.disableFlag === "1" || row.disableFlag === 1) ?
                                                <Link
                                                    to="#" onClick={() => {
                                                        props.handleDeleteDisabledAccount(row.userName, props.changeState)
                                                    }
                                                    }>Delete</Link>
                                                : ""}
                                        </TableCell>
                                    </TableRow>
                                )
                            }
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
                <br />
                <TablePaging numberOfPages={props.numberOfPages}
                    currentPage={props.currentPage}
                    linkTo={ROUTE_VIEW_ACCOUNT_LIST} />
            </div>
        </React.Fragment>
    );
}

class ViewAccountList extends Component {
    state = {
        isToastOpen: false,
        messageReceived: false
    }

    // Lỗi
    changeState = async () => {
        await this.setState({
            isToastOpen: true
        })
    }

    handleDeleteDisabledAccount = (userName, methodToChangeState) => {
        confirmAlert({
            title: 'Confirm',
            message: `Are you sure to delete this account (username: ${userName})? 
            Once you chooses to perform this action, there is no way to revert it.`,
            buttons: [
                {
                    label: 'Delete',
                    onClick: async () => {
                        await this.props.deleteDisabledAccountsFunc(userName)
                        // console.log("state toast");
                        // console.log(this.state.isToastOpen);
                        methodToChangeState();
                        setTimeout(() => {
                            window.location.replace("/viewaccountlist")
                        }, 5000);
                    }
                },
                {
                    label: 'Cancel'
                }
            ]
        });
    }

    mapErrMessToParagraph = (errMessArr) => {
        return errMessArr.map((errMessObj) => {
            return <p>{errMessObj}</p>
        })
    }

    componentDidMount() {
        const account = this.props.loginAccount.account;
        if (null != account && (account.roleId === ROLE_ADMIN)) {
            const accountList = this.props.accounts.accounts;
            const currentPageNumber = this.props.accounts.currentPageNumber;
            const numberOfPages = this.props.accounts.numberOfPages;
            let pageNumber = this.props.pageNumber;
            const searchData = this.props.accounts.searchData;
            // console.log(pageNumber + '/' + numberOfPages);

            if (null == accountList || (pageNumber !== currentPageNumber && pageNumber <= numberOfPages)) {
                if (searchData != null) {
                    this.props.fetchAccountSearch(pageNumber, searchData);
                } else {
                    this.props.fetchAccount(pageNumber);
                }
            }
        }
    }

    render() {
        const account = this.props.loginAccount.account;
        if (account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first</h1>
                    </div>
                </div>
            );
        } else if (account.roleId === ROLE_ADMIN) {
            const accountList = this.props.accounts.accounts;
            const numberOfPages = this.props.accounts.numberOfPages;
            const currentPageNumber = this.props.accounts.currentPageNumber;
            const isLoading = this.props.accounts.isLoading;

            if (currentPageNumber > numberOfPages && numberOfPages > 0) {
                return (
                    <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
                )
            }

            if (!this.state.messageReceived) {
                if (this.props.deleteDisabledAccounts.successMessage !== null && this.props.deleteDisabledAccounts.successMessage !== undefined) {
                    this.setState({
                        isToastOpen: true,
                        messageReceived: true
                    })
                }
            }

            // console.log("state toast");
            // console.log(this.state.isToastOpen);

            return (
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                            <BreadcrumbItem>Account Management</BreadcrumbItem>
                            <BreadcrumbItem active>View Account List</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>View Account List</h3>
                            <hr />
                        </div>
                    </div>
                    {isLoading ?
                        <Loading /> :
                        <div className="row row-content">
                            <h4>Search</h4>
                            <div className="col-12 row">
                                <AccountListTable accountList={(accountList == null) ? [] : accountList}
                                    numberOfPages={numberOfPages}
                                    currentPage={currentPageNumber}
                                    fetchAccountSearch={this.props.fetchAccountSearch}
                                    handleDeleteDisabledAccount={this.handleDeleteDisabledAccount}
                                    changeState={this.changeState}
                                />
                            </div>
                        </div>
                    }
                    <Toast
                        isOpen={this.state.isToastOpen}
                        style={{
                            // position: "absolute",
                            // bottom: 20,
                            // left: "-11%",
                            boxShadow: "0 0.05rem 0.75rem #303841",
                            position: "fixed",
                            bottom: "10px",
                            right: "10px",
                            backgroundColor: "white",
                            zIndex: "1"
                        }}
                    >
                        <ToastHeader
                            toggle={() => {
                                this.setState({
                                    isToastOpen: !this.state.isToastOpen,
                                });
                            }}
                        >
                            Information
                        </ToastHeader>
                        <ToastBody>
                            {this.props.deleteDisabledAccounts.successStatus
                                ? this.props.deleteDisabledAccounts.successMessage
                                :
                                this.props.deleteDisabledAccounts.errMess !== null && this.props.deleteDisabledAccounts.errMess !== undefined ?
                                    this.mapErrMessToParagraph(this.props.deleteDisabledAccounts.errMess) : ""}
                        </ToastBody>
                    </Toast>
                </div>
            )
        } else {
            return (
                <UnauthorizedAccessDisplay />
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        accounts: state.accounts,
        deleteDisabledAccounts: state.deleteDisabledAccounts
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        fetchAccount: (pageNumber) => {
            dispatch(fetchAccount(pageNumber))
        },
        fetchAccountSearch: (pageNumber, searchData) => {
            dispatch(fetchAccount(pageNumber, 'POST', searchData))
        },
        deleteDisabledAccountsFunc: (arrOfUsernames) => {
            dispatch(deleteDisabledAccount(arrOfUsernames));
        }
    });
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewAccountList);