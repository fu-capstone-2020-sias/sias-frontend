import React, {Component} from "react";
import {
    Breadcrumb,
    BreadcrumbItem,
    Button,
    Label,
    Col,
    Row,
} from "reactstrap";
import {Form, Control, Errors} from "react-redux-form";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {ROLE_ADMIN, ROLE_FACTORY_MANAGER, ROUTE_HOME} from "../../shared/constants";
import {actions} from "react-redux-form";
import UnauthorizedAccessDisplay from "../common/UnauthorizedAccessDisplay";
import FormLabel from "../common/FormLabel";
import {demoCreateInstruction} from "../../redux/actions/ActionCreators";

const required = (val) => val && val.length;


class Demo extends Component {
    constructor(args) {
        super();
    }

    handleSubmit = async (values) => {
        await this.props.demoCreateInstruction(
            values.steelAvailable,
            values.steelToCut,
            values.bladeWidth,
        );
    };

    state = {};

    componentDidMount() {
        const account = this.props.loginAccount.account;
        this.props.resetDemoCuttingInstruction();
        this.setState({
            account: account,
        });
    }

    render() {
        if (this.state.account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first.</h1>
                    </div>
                </div>
            );
        } else if (this.state.account.roleId !== "R005") {
            return (
                <UnauthorizedAccessDisplay/>
            );
        } else {
            return (
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem>
                                <Link to={ROUTE_HOME}>Home</Link>
                            </BreadcrumbItem>
                            <BreadcrumbItem active>Demo Cutting Instruction</BreadcrumbItem>
                        </Breadcrumb>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <h3>Demo Cutting Instruction</h3>
                            <hr/>
                        </div>
                        <div className="col-12 col-md-10 offset-md-1 col-xl-8 offset-xl-2 mb-5">
                            <Form
                                model="demoCuttingInstruction"
                                onSubmit={(values) => this.handleSubmit(values)}
                            >
                                <Row className="form-group">
                                    <Col md={9}><p>Đơn vị: mm</p></Col>
                                    <Col md={9}><p style={{color: "red"}}>Vui lòng nhập input theo giá trị mẫu trong
                                        form</p></Col>
                                </Row>
                                <Row className="form-group">
                                    <Label htmlFor="email" md={4}>
                                        <FormLabel text="Thanh có sẵn (dùng để cắt)" required='true'/>
                                    </Label>
                                    <Col md={8}>
                                        <Control.text
                                            model=".steelAvailable"
                                            id="steelAvailable"
                                            name="steelAvailable"
                                            className="form-control"
                                            validators={{required}}
                                            defaultValue={"5000,1000,2000"}

                                        />
                                        <Errors
                                            className="text-danger"
                                            model=".steelAvailable"
                                            show="touched"
                                            messages={{
                                                required: "This is a required field. ",
                                            }}
                                        />
                                    </Col>
                                </Row>

                                <Row className="form-group">
                                    <Label htmlFor="notes" md={4}>
                                        <FormLabel text="Thanh cần cắt" required='true'/>
                                    </Label>
                                    <Col md={8}>
                                        <Control.text
                                            model=".steelToCut"
                                            id="steelToCut"
                                            name="steelToCut"
                                            className="form-control"
                                            validators={{required}}
                                            defaultValue={"1000,2000,1000"}
                                        />
                                        <Errors
                                            className="text-danger"
                                            model=".steelToCut"
                                            show="touched"
                                            messages={{
                                                required: "This is a required field. ",
                                            }}
                                        />
                                    </Col>
                                </Row>

                                <Row className="form-group">
                                    <Label htmlFor="companyWebsite" md={4}>
                                        <FormLabel text="Độ rộng lưỡi cắt" required='true'/>
                                    </Label>
                                    <Col md={8}>
                                        <Control.text
                                            model=".bladeWidth"
                                            id="bladeWidth"
                                            name="bladeWidth"
                                            className="form-control"
                                            validators={{required}}
                                            defaultValue={"5"}
                                        />
                                        <Errors
                                            className="text-danger"
                                            model=".bladeWidth"
                                            show="touched"
                                            messages={{
                                                required: "This is a required field. ",
                                            }}
                                        />
                                    </Col>
                                </Row>

                                <Row className="form-group">
                                    <Col md={3}>
                                        <Button
                                            type="submit"
                                            color="warning"
                                            className="submitButton"
                                        >
                                            Tạo chỉ dẫn cắt
                                        </Button>
                                    </Col>
                                </Row>
                            </Form>

                            <div style={{marginTop: 40}}>
                                <p>Kết quả</p>
                                <div style={{height: 200, width: '100%', backgroundColor: "#dbdbd9", padding: 20}}>
                                    {
                                        this.props.demoCuttingInstructions.successMessage != null ?
                                            this.props.demoCuttingInstructions.successMessage : ""
                                    }
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        demoCuttingInstructions: state.demoCuttingInstructions
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        demoCreateInstruction: (steelAvailable, steelToCut, bladeWidth) => {
            dispatch(demoCreateInstruction(steelAvailable, steelToCut, bladeWidth));
        },
        resetDemoCuttingInstruction: () => {
            dispatch(actions.reset("demoCuttingInstruction"));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Demo);
