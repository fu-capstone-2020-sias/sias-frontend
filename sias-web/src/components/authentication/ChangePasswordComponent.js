import React, {Component} from "react";
import {
    Breadcrumb,
    BreadcrumbItem,
    Button,
    Label,
    Col,
    Row,
} from "reactstrap";
import {Toast, ToastBody, ToastHeader} from "reactstrap";
import {Form, Control, Errors} from "react-redux-form";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {fetchAccountInfo, modifyAccountInfo, postLogout} from "../../redux/actions/ActionCreators";
import {actions} from "react-redux-form";
import {Loading} from "../common/LoadingComponent";
import Cookies from "js-cookie";
import createHistory from 'history/createBrowserHistory'
import FormLabel from "../common/FormLabel";
import {required, validPassword, maxLength} from '../../shared/fieldValidation';
import {VALIDATION_MSG_MAX_LENGTH, ROUTE_HOME} from "../../shared/constants";

class ChangePassword extends Component {
    mapErrMessToParagraph = (errMessArr) => {
        return errMessArr.map((errMessObj) => {
            return <p>{errMessObj}</p>
        })
    }

    handleSubmit = async (values) => {
        await this.props.modifyAccountInfo(
            this.props.accountInfos.accountInfo.userName,
            values.password,
            values.oldPassword,
            "1",
            this.state.account.roleId,
            this.state.account.departmentId,
            this.state.account.firstName,
            this.state.account.lastName,
            this.state.account.gender,
            this.state.account.dob,
            this.state.account.email,
            this.state.account.phoneNumber,
            this.state.account.cityProvinceId,
            this.state.account.districtId,
            this.state.account.wardCommuneId,
            this.state.account.address,
            this.state.account.disableFlag,
            "WEB_UC08"
        );

        this.setState({
            isToastOpen: true
        })

        setTimeout(() => {
            this.setState({
                isToastOpen: false
            })
            window.location.reload();
        }, 5000)
    };

    state = {
        isToastOpen: false,
        firstLoad: false,
        visibleOldPassword: false,
        visibleNewPassword: false,
        visibleConfirmPassword: false,
        newPassword: '',
        confirmPassword: '',
        passwordsMatch: true,
    };

    async componentDidMount() {
        const account = this.props.loginAccount.account;
        this.props.resetAccountInfoForm();
        this.setState({
            account: account,
        });

        if (null != account) {
            await this.props.fetchAccountInfo(account.userName); // Pass userName từ màn View Account List
        }
    }

    render() {
        if (this.props.accountInfos.errMess === "Error 401: Unauthorized") {
            const history = createHistory();
            history.replace("/home", "urlhistory"); // Come back to homepage
            Cookies.remove('account');
            window.location.reload(false);
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first.</h1>
                    </div>
                </div>
            );
        }

        if (this.state.account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first.</h1>
                    </div>
                </div>
            );
        } else {
            if (this.props.accountInfos.accountInfo !== null) {
                return (
                    <div className="container" style={{position: "relative"}}>
                        <div className="row">
                            <Breadcrumb>
                                <BreadcrumbItem>
                                    <Link to={ROUTE_HOME}>Home</Link>
                                </BreadcrumbItem>
                                <BreadcrumbItem>Account Profile</BreadcrumbItem>
                                <BreadcrumbItem active>Change Password</BreadcrumbItem>
                            </Breadcrumb>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <h3>Change Password</h3>
                                <hr/>
                            </div>
                            <div className="col-12 col-md-9">
                                <Form
                                    model="accountInfo"
                                    onSubmit={
                                        (values) => {
                                            if (this.state.passwordsMatch)
                                                this.handleSubmit(values)
                                        }
                                    }
                                    onChange={(values) => {
                                        this.setState({
                                            newPassword: values.password,
                                            confirmPassword: values.confirmPassword,
                                            passwordsMatch: values.password === values.confirmPassword
                                        })
                                    }}
                                >
                                    <Row className="form-group">
                                        <Label htmlFor="oldPassword" md={4} lg={3}>
                                            <FormLabel text="Old Password" required='true'/>
                                        </Label>
                                        <Col xs={11} md={7}>
                                            <Control.input
                                                type={this.state.visibleOldPassword ? "text" : "password"}
                                                model=".oldPassword"
                                                id="oldPassword"
                                                name="oldPassword"
                                                className="form-control"
                                                validators={{required, maxLength: maxLength(32)}}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".oldPassword"
                                                show="touched"
                                                messages={{
                                                    required: "This is a required field. "
                                                }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".oldPassword"
                                                show="touched"
                                                messages={{
                                                    maxLength: VALIDATION_MSG_MAX_LENGTH(32)
                                                }}
                                            />
                                        </Col>
                                        <Col xs={1}>
                                            <i
                                                onClick={() => {
                                                    this.setState({
                                                        visibleOldPassword: !this.state.visibleOldPassword
                                                    })
                                                }}
                                                className={this.state.visibleOldPassword ?
                                                    "fa fa-eye-slash fa-lg" : "fa fa-eye fa-lg"}/>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="password" md={4} lg={3}>
                                            <FormLabel text="New Password" required='true'/>
                                        </Label>
                                        <Col xs={11} md={7}>
                                            <Control.input
                                                type={this.state.visibleNewPassword ? "text" : "password"}
                                                model=".password"
                                                id="password"
                                                name="password"
                                                className="form-control"
                                                validators={{required, validPassword, maxLength: maxLength(32)}}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".password"
                                                show="touched"
                                                messages={{
                                                    required: "This is a required field. ",
                                                    validPassword: "Password must meet minimum " +
                                                        "eight characters, at least one uppercase letter, " +
                                                        "one lowercase letter, " +
                                                        "one number and one special character (@, $, !, %, *, ?, &)."
                                                }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".password"
                                                show="touched"
                                                messages={{
                                                    maxLength: VALIDATION_MSG_MAX_LENGTH(32)
                                                }}
                                            />
                                        </Col>
                                        <Col xs={1}>
                                            <i onClick={() => {
                                                this.setState({
                                                    visibleNewPassword: !this.state.visibleNewPassword
                                                })
                                            }}
                                               className={this.state.visibleNewPassword ?
                                                   "fa fa-eye-slash fa-lg" : "fa fa-eye fa-lg"}/>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="confirmPassword" md={4} lg={3}>
                                            <FormLabel text="Confirm Password" required='true'/>
                                        </Label>
                                        <Col xs={11} md={7}>
                                            <Control.input
                                                type={this.state.visibleConfirmPassword ? "text" : "password"}
                                                model=".confirmPassword"
                                                id="confirmPassword"
                                                name="confirmPassword"
                                                className="form-control"
                                                validators={{required}}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".confirmPassword"
                                                show="touched"
                                                messages={{
                                                    required: "This is a required field."
                                                }}
                                            />
                                            {!this.state.passwordsMatch &&
                                            <p className="text-danger">Confirm password and New password do not
                                                match.</p>
                                            }
                                        </Col>
                                        <Col xs={1}>
                                            <i onClick={() => {
                                                this.setState({
                                                    visibleConfirmPassword: !this.state.visibleConfirmPassword
                                                })
                                            }}
                                               className={this.state.visibleConfirmPassword ?
                                                   "fa fa-eye-slash fa-lg" : "fa fa-eye fa-lg"}/>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Col md={{size: 10, offset: 2}}>
                                            <Button
                                                type="submit"
                                                color="warning"
                                                className="submitButton"
                                            >
                                                Submit
                                            </Button>
                                        </Col>
                                    </Row>
                                </Form>

                                <Toast
                                    isOpen={this.state.isToastOpen}
                                    style={{
                                        boxShadow: "0 0.05rem 0.75rem #303841",
                                        position: "fixed",
                                        bottom: "10px",
                                        right: "10px",
                                        backgroundColor: "white",
                                        zIndex: "1"
                                    }}
                                >
                                    <ToastHeader
                                        toggle={() => {
                                            this.setState({
                                                isToastOpen: !this.state.isToastOpen,
                                            });
                                        }}
                                    >
                                        Information
                                    </ToastHeader>
                                    <ToastBody>
                                        {this.props.accountInfos.successStatus
                                            ? this.props.accountInfos.successMessage
                                            :
                                            this.mapErrMessToParagraph(this.props.accountInfos.errMess)}
                                    </ToastBody>
                                </Toast>
                            </div>
                        </div>
                    </div>
                );

            } else {
                return (
                    <div className={"container"}>
                        <Loading/>
                    </div>
                )
            }
        }
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        accountInfos: state.accountInfos,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchAccountInfo: (userName) => {
            dispatch(fetchAccountInfo(userName));
        },
        modifyAccountInfo: async (
            userName,
            password,
            oldPassword,
            isChangePassword,
            roleId,
            departmentId,
            firstName,
            lastName,
            gender,
            dob,
            email,
            phoneNumber,
            cityProvinceId,
            districtId,
            wardCommuneId,
            address,
            disableFlag,
            screen_id
        ) => {
            dispatch(
                modifyAccountInfo(
                    userName,
                    password,
                    oldPassword,
                    isChangePassword,
                    roleId,
                    departmentId,
                    firstName,
                    lastName,
                    gender,
                    dob,
                    email,
                    phoneNumber,
                    cityProvinceId,
                    districtId,
                    wardCommuneId,
                    address,
                    disableFlag,
                    screen_id
                )
            );
        },
        resetAccountInfoForm: () => {
            dispatch(actions.reset("accountInfo"));
        },
        postLogout: () => dispatch(postLogout())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);
