import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { ROUTE_FORGOT_PASSWORD, ERROR_MESSAGE_EMPTY_USERNAME_OR_PASSWORD } from '../../shared/constants';
import { openLoginForm } from '../../redux/actions/ActionCreators';

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false
        };
        this.toggleModal = this.toggleModal.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    toggleModal() {
        if (!this.state.isModalOpen) {
            this.props.openLoginForm();
        }
        this.setState({
            isModalOpen: !this.state.isModalOpen
        });
    }

    handleLogin(event) {
        event.preventDefault();
        const userName = this.userName.value;
        const password = this.password.value;
        if (userName === '' || password === '') {
            alert(ERROR_MESSAGE_EMPTY_USERNAME_OR_PASSWORD);
        } else {
            const loginDetails = {
                userName: this.userName.value,
                password: this.password.value,
            }
            this.props.postLogin(loginDetails);
        }
    }

    render() {
        const errMess = this.props.errMess;
        return (
            <React.Fragment>
                <Button outline onClick={this.toggleModal}>
                    <span className="fa fa-sign-in fa-lg d-none d-lg-block" /> Login
                </Button>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Login</ModalHeader>
                    <ModalBody>
                        <Form onSubmit={this.handleLogin}>
                            <FormGroup>
                                <Label htmlFor="userName">Username</Label>
                                <Input type="text" id="userName" name="userName"
                                    innerRef={(input) => this.userName = input} />
                            </FormGroup>

                            <FormGroup>
                                <Label htmlFor="password">Password</Label>
                                <Input type="password" id="password" name="password"
                                    innerRef={(input) => this.password = input} />
                            </FormGroup>

                            {errMess != null &&
                                <Label className="text-danger">{errMess}</Label>
                            }

                            <FormGroup>
                                <Link className="float-right"
                                    to={`${ROUTE_FORGOT_PASSWORD}`}
                                    onClick={this.toggleModal}>
                                    Forgot password?
                                </Link>
                            </FormGroup>
                            <br />
                            <Button
                                type="submit"
                                value="submit"
                                color="warning">
                                Login
                                </Button>
                        </Form>
                    </ModalBody>
                </Modal>
            </React.Fragment>
        )
    }
}

const mapStateToProps = () => {
    return {
        // loginAccount: state.loginAccount,
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        openLoginForm: () => dispatch(openLoginForm())
    });
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);