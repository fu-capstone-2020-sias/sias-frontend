import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, Col } from 'reactstrap';
import { connect } from 'react-redux';
import { postResetPassword } from '../../redux/actions/ActionCreators';
import { hiddenEmail } from '../../shared/utils';

class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.handleResetPassword = this.handleResetPassword.bind(this);
    }
    handleResetPassword(event) {
        event.preventDefault();

        this.props.postResetPassword(this.userName.value);
    }

    render() {
        const errMess = this.props.resetPasswordDetails.errMess;
        const email = this.props.resetPasswordDetails.email;
        return (
            <div className="container">
                <div className="row row-content">
                    <div className="col-12 row">
                        <div className="col-12">
                            <h3>Forgot Password</h3>
                            <hr />
                        </div>
                    </div>
                    <div className="col-12 row">
                        <div className="col"></div>
                        <div className="col-12 col-sm-10 col-xl-8">
                            <Form onSubmit={this.handleResetPassword}>
                                <FormGroup row>
                                    <Label htmlFor="userName" sm={3}>Username</Label>
                                    <Col sm={9}>
                                        <Input type="text" id="userName" name="userName"
                                            required={true}
                                            innerRef={(input) => this.userName = input} />
                                    </Col>
                                </FormGroup>

                                {errMess != null &&
                                    <Label className="text-danger">{errMess}</Label>
                                }

                                {email != null &&
                                    <Label className="text-success">An email has been sent to your inbox ({hiddenEmail(email)}). Please check it.</Label>
                                }

                                <br />
                                {email == null &&
                                    <Button className="float-right" type="submit" value="submit" color="warning">Reset Password</Button>
                                }
                            </Form>
                        </div>
                        <div className="col"></div>

                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        resetPasswordDetails: state.resetPasswordDetails
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        postResetPassword: (userName) => { dispatch(postResetPassword(userName)) }
    });
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);