import React, { Component } from 'react';
import {
    Navbar,
    NavbarBrand,
    Nav,
    NavItem,
    Collapse,
    NavbarToggler,
    Dropdown,
    DropdownItem,
    DropdownToggle,
    DropdownMenu
} from 'reactstrap';
import { NavLink, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { postLogin, postLogout } from '../../redux/actions/ActionCreators';
import LoginForm from '../authentication/LoginFormComponent';
import {
    ROUTE_VIEW_ACCOUNT_INFO,
    ROUTE_CHANGE_PASSWORD,
    ROUTE_ADD_NEW_ACCOUNT,
    ROLE_ADMIN,
    ROUTE_HOME,
    ROUTE_VIEW_ORDER_LIST,
    ROUTE_VIEW_ACCOUNT_LIST,
    ROUTE_FEEDBACK,
    ROUTE_VIEW_COMPANY,
    ROUTE_MODIFY_COMPANY,
    ROUTE_ADD_NEW_ORDER,
    ROUTE_VIEW_STOCK_PRODUCT_LIST,
    ROUTE_ADD_NEW_STOCK_PRODUCT,
    ROUTE_VIEW_WAREHOUSE_LIST,
    ROUTE_ADD_NEW_WAREHOUSE,
} from '../../shared/constants';

function LogoutButton(props) {
    const account = props.account;
    // const history = useHistory();

    const logout = () => {
        // history.push({ROUTE_HOME});
        props.postLogout();
    }

    return (
        // <Link to={ROUTE_HOME} onClick={logout}>{account.userName} Logout</Link>
        <Dropdown nav isOpen={props.isProfileDropdownOpen} toggle={props.toggleProfileDropdown}>
            <DropdownToggle nav caret>
                <i className="fa fa-user-circle-o fa-lg" aria-hidden="true"></i> {account.userName}
            </DropdownToggle>
            <DropdownMenu>
                <DropdownItem>
                    <Link to={`${ROUTE_VIEW_ACCOUNT_INFO}`}>View Account Profile</Link>
                    {/*<Link to={`${ROUTE_VIEW_ACCOUNT_INFO}/${account.userName}`}>View Account Profile</Link>*/}
                </DropdownItem>
                <DropdownItem>
                    <Link to={`${ROUTE_CHANGE_PASSWORD}`}>Change Password</Link>
                </DropdownItem>
                <DropdownItem>
                    <Link to={`${ROUTE_HOME}`} onClick={logout}>Logout</Link>
                </DropdownItem>
            </DropdownMenu>
        </Dropdown>
    );
}

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isNavOpen: false,
            isStockDropdownOpen: false,
            isOrderDropdownOpen: false,
            isAccountDropdownOpen: false,
            isCompanyDropdownOpen: false,
            isProfileDropdownOpen: false,
        }
        this.toggleNav = this.toggleNav.bind(this);
        this.toggleStockDropdown = this.toggleStockDropdown.bind(this);
        this.toggleOrderDropdown = this.toggleOrderDropdown.bind(this);
        this.toggleAccountDropdown = this.toggleAccountDropdown.bind(this);
        this.toggleCompanyDropdown = this.toggleCompanyDropdown.bind(this);
        this.toggleProfileDropdown = this.toggleProfileDropdown.bind(this);
        // this.logout = this.logout.bind(this);
    }

    toggleNav() {
        this.setState({
            isNavOpen: !this.state.isNavOpen
        });
    }

    toggleStockDropdown() {
        this.setState({
            isStockDropdownOpen: !this.state.isStockDropdownOpen
        });
    }

    toggleOrderDropdown() {
        this.setState({
            isOrderDropdownOpen: !this.state.isOrderDropdownOpen
        });
    }

    toggleAccountDropdown() {
        this.setState({
            isAccountDropdownOpen: !this.state.isAccountDropdownOpen
        });
    }

    toggleCompanyDropdown() {
        this.setState({
            isCompanyDropdownOpen: !this.state.isCompanyDropdownOpen
        });
    }

    toggleProfileDropdown() {
        this.setState({
            isProfileDropdownOpen: !this.state.isProfileDropdownOpen
        })
    }

    render() {
        const account = this.props.loginAccount.account;
        const errMess = this.props.loginAccount.errMess;
        const LoginFormOrName = (account == null) ?
            <NavItem><LoginForm postLogin={this.props.postLogin} errMess={errMess} /></NavItem> :

            <LogoutButton postLogout={this.props.postLogout}
                account={account}
                isProfileDropdownOpen={this.state.isProfileDropdownOpen}
                toggleProfileDropdown={this.toggleProfileDropdown}
            />


        const NavItems = (account == null) ?
            (<Nav navbar className="ml-auto"></Nav>) :
            (<Nav navbar className="ml-auto">
                <NavItem>
                    <NavLink className="nav-link mr-3" to={`${ROUTE_HOME}`}>
                        <i className="fa fa-home fa-lg d-none d-xl-inline"></i> Home
                    </NavLink>
                </NavItem>
                {(account.roleId === ROLE_ADMIN) ?
                    (
                        <Dropdown nav isOpen={this.state.isAccountDropdownOpen} toggle={this.toggleAccountDropdown}>
                            <DropdownToggle nav caret>
                                <i className="fa fa-address-book fa-lg d-none d-xl-inline" aria-hidden="true"></i> Account Management
                            </DropdownToggle>
                            <DropdownMenu>
                                {/* Office Manager, Office Employee */}
                                <DropdownItem>
                                    <Link to={`${ROUTE_VIEW_ACCOUNT_LIST}`}>View Account List</Link>

                                </DropdownItem>
                                {/* Office Manager, Office Employee */}
                                <DropdownItem>
                                    <Link to={`${ROUTE_ADD_NEW_ACCOUNT}`}>Add New Account</Link></DropdownItem>
                            </DropdownMenu>
                        </Dropdown>
                    ) : (
                        <React.Fragment>
                            <Dropdown nav isOpen={this.state.isStockDropdownOpen} toggle={this.toggleStockDropdown}>
                                <DropdownToggle nav caret>
                                    <i className="fa fa-industry fa-lg d-none d-xl-inline"></i> Stock Management
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem header>Warehouse</DropdownItem>
                                    {/* Factory Manager, Office Manager */}
                                    <DropdownItem>
                                        <Link to={`${ROUTE_VIEW_WAREHOUSE_LIST}`}>View Warehouse List</Link>
                                    </DropdownItem>
                                    {/* Office Manager */}
                                    <DropdownItem>
                                        <Link to={`${ROUTE_ADD_NEW_WAREHOUSE}`}>Create New Warehouse</Link>
                                    </DropdownItem>

                                    <DropdownItem divider />

                                    <DropdownItem header>Stock Products</DropdownItem>
                                    {/* Factory Manager, Office Manager */}
                                    <DropdownItem>
                                        <Link to={`${ROUTE_VIEW_STOCK_PRODUCT_LIST}`}>View Stock Product List</Link>
                                    </DropdownItem>
                                    {/* Factory Manager, Office Manager */}
                                    <DropdownItem>
                                        <Link to={`${ROUTE_ADD_NEW_STOCK_PRODUCT}`}>Add New Stock Product</Link>
                                    </DropdownItem>
                                    {/* Factory Manager, Office Manager */}
                                    {/* <DropdownItem>
                                        <Link to={`${ROUTE_IMPORT_STOCK_PRODUCT_BATCH}`}>Import Stock Product
                                            Batch</Link>
                                    </DropdownItem> */}
                                </DropdownMenu>
                            </Dropdown>

                            <Dropdown nav isOpen={this.state.isOrderDropdownOpen} toggle={this.toggleOrderDropdown}>
                                <DropdownToggle nav caret>
                                    <i className="fa fa-usd fa-lg d-none d-xl-inline" aria-hidden="true"></i> Order Management
                                </DropdownToggle>
                                <DropdownMenu>
                                    {/* Office Manager, Office Employee, Factory Manager */}
                                    <DropdownItem>
                                        <Link to={`${ROUTE_VIEW_ORDER_LIST}`}>View Order List</Link>

                                    </DropdownItem>
                                    {/* Office Manager, Office Employee */}
                                    <DropdownItem>
                                        <Link to={`${ROUTE_ADD_NEW_ORDER}`}>Add New Order</Link>
                                    </DropdownItem>
                                    {/* Office Manager, Office Employee */}
                                    {/* <DropdownItem>
                                        <Link to={`${ROUTE_IMPORT_ORDER_BATCH}`}>Import Order Batch</Link>
                                    </DropdownItem> */}

                                    {/* Factory Manager */}
                                    {/*<DropdownItem>View Cutting Instructions</DropdownItem>*/}
                                </DropdownMenu>
                            </Dropdown>
                        </React.Fragment>
                    )

                }
                <Dropdown nav isOpen={this.state.isCompanyDropdownOpen} toggle={this.toggleCompanyDropdown}>
                    <DropdownToggle nav caret>
                        <i className="fa fa-building fa-lg d-none d-xl-inline"></i> Company Information
                    </DropdownToggle>
                    <DropdownMenu>
                        <DropdownItem>
                            <Link to={`${ROUTE_VIEW_COMPANY}`}>View Company Information</Link>

                        </DropdownItem>
                        {/* Only Admin */}
                        {(account.roleId === ROLE_ADMIN) ?
                            <DropdownItem>
                                <Link to={`${ROUTE_MODIFY_COMPANY}`}>Modify Company Information</Link>

                            </DropdownItem> : ""
                        }
                    </DropdownMenu>
                </Dropdown>
                <NavItem>
                    <NavLink className="nav-link" to={`${ROUTE_FEEDBACK}`}>
                        Feedback
                    </NavLink>
                </NavItem>
            </Nav>);

        return (
            <React.Fragment>
                <Navbar light expand="md">
                    <div className="container">
                        <NavbarToggler onClick={this.toggleNav} />
                        <NavbarBrand href="/">
                            <img src="/assets/images/logo_header.png" height="65" width="65"
                                alt="Steel Industry Assistant System" />
                        </NavbarBrand>
                        <Collapse isOpen={this.state.isNavOpen} navbar>

                            {NavItems}

                            <Nav className="ml-auto" navbar>
                                {LoginFormOrName}
                            </Nav>
                        </Collapse>
                    </div>
                </Navbar>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        postLogin: (loginDetails) => dispatch(postLogin(loginDetails)),
        postLogout: () => dispatch(postLogout())
    });
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);