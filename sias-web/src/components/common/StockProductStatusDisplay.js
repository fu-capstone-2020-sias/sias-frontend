import React from 'react';
import { STOCK_PRODUCT_STATUS_GOOD_TEXT, STOCK_PRODUCT_STATUS_BAD_TEXT, STOCK_PRODUCT_STATUS_MEDIUM_TEXT } from '../../shared/constants';

export default function StockProductStatusDisplay(props) {
    const status = props.status;
    let className = 'p-2 mb-2 ';
    switch (status) {
        case STOCK_PRODUCT_STATUS_MEDIUM_TEXT:
            className += 'bg-info text-white';
            break;
        case STOCK_PRODUCT_STATUS_GOOD_TEXT:
            className += 'bg-success text-white';
            break;
        case STOCK_PRODUCT_STATUS_BAD_TEXT:
            className += 'bg-danger';
            break;
        default:
            className += '';
            break;
    }
    return <span className={className}>{status}</span>
}