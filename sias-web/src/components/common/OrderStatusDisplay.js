import React from 'react';
import { ORDER_STATUS_APPROVED_TEXT, ORDER_STATUS_PENDING_TEXT, ORDER_STATUS_PROCESSING_TEXT, ORDER_STATUS_COMPLETED_TEXT, ORDER_STATUS_REJECTED_TEXT } from '../../shared/constants';

export default function OrderStatusDisplay(props) {
    const status = props.status;
    let className = 'p-2 mb-2 ';
    switch (status) {
        case ORDER_STATUS_APPROVED_TEXT:
            className += 'bg-info text-white';
            break;
        case ORDER_STATUS_PENDING_TEXT:
            className += 'bg-secondary text-white';
            break;
        case ORDER_STATUS_PROCESSING_TEXT:
            className += 'bg-warning';
            break;
        case ORDER_STATUS_COMPLETED_TEXT:
            className += 'bg-success text-white';
            break;
        case ORDER_STATUS_REJECTED_TEXT:
            className += 'bg-danger';
            break;
        default:
            className += '';
            break;
    }
    return <span className={className}>{status}</span>
}