import React from 'react';

export const Loading = (props) => {

    let defaultText = <h5 style={{marginTop: 10}}>Loading...</h5>;

    if (props.message !== null && props.message !== undefined) {
        defaultText = <h5 style={{marginTop: 10}}>{props.message}</h5>
    }

    return (
        <div className="col-12 loader center" style={{height: "650px"}}>
            <center style={styles.center}>
            <span
                // className="fa fa-cog fa-spin"
                className="fa fa-spinner fa-pulse fa-3x fa-fw text-warning"
            />
                {defaultText}
            </center>
        </div>
    );
};

export const Uploading = () => {
    return (
        <div className="col-12 loader center">
            <span
                // className="fa fa-cog fa-spin"
                className="fa fa-spinner fa-pulse fa-3x fa-fw text-warning"
            />
            <p>Uploading File...</p>
        </div>
    );
};

const styles = {
    center: {
        position: 'absolute',
        left: '50%',
        top: '50%',
        transform: 'translate(-50%, -50%)'
    }
}
