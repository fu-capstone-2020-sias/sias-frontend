import React from 'react';

export default function DisableFlagDisplay(props) {
    const disableFlag = props.disableFlag
    let className = 'p-2 mb-2 ';
    let text = 'Active';
    switch (disableFlag) {        
        case 1:
            className += 'bg-danger';
            text = 'Disabled';
            break;
        default:
            className += '';
            break;
    }
    return <span className={className}>{text}</span>
}