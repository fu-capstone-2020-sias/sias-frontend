import React from 'react';
import {
    ERROR_UNAUTHORIZED
} from '../../shared/constants';
import ErrorDisplay from './ErrorDisplay';

export default function UnauthorizedAccessDisplay() {
    return (
        <ErrorDisplay errorType={ERROR_UNAUTHORIZED} imgSrc={"/assets/images/access_denied.png"} />
    )
}