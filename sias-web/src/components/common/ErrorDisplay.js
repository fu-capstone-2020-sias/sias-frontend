import React from 'react';
import {
    ROUTE_HOME,
    ERROR_PAGE_NOT_FOUND,
    PAGE_NOT_FOUND_MSG_GENERAL,
    PAGE_NOT_FOUND_MSG_GENERAL_DESCRIPTION,
    ERROR_MESSAGE_SUBMIT_GENERAL,
    UNAUTHORIZED_ACCESS_MSG_GENERAL,
    UNAUTHORIZED_ACCESS_MSG_GENERAL_DESCRIPTION,
    ERROR_UNAUTHORIZED
} from '../../shared/constants';
import { Link } from 'react-router-dom';

export default function ErrorDisplay(props) {
    const imgSrc = props.imgSrc == null ? "/assets/images/404.png" : props.imgSrc;
    let errorContentHeader = '';
    let errorContentDescription = '';
    switch (props.errorType) {
        case ERROR_PAGE_NOT_FOUND:
            errorContentHeader = PAGE_NOT_FOUND_MSG_GENERAL;
            errorContentDescription = PAGE_NOT_FOUND_MSG_GENERAL_DESCRIPTION;
            break;
        case ERROR_UNAUTHORIZED:
            errorContentHeader = UNAUTHORIZED_ACCESS_MSG_GENERAL;
            errorContentDescription = UNAUTHORIZED_ACCESS_MSG_GENERAL_DESCRIPTION;
            break;
        default:
            errorContentHeader = 'ERROR';
            errorContentDescription = ERROR_MESSAGE_SUBMIT_GENERAL;
            break;
    }

    const errorContent = (
        <div className='col-6 ml-5'>
            <h1>{errorContentHeader}</h1>
            <p>{errorContentDescription}</p>
            <Link to={ROUTE_HOME}>Return to Homepage</Link>
        </div>
    );
    return (
        <div className="container">
            <div className="row row-content">
                <div className='col-12 d-flex justify-content-center align-items-center'>
                    <img src={imgSrc} height="200" width="200" alt="Error" />
                    {errorContent}
                </div>
            </div>
        </div>
    )
}