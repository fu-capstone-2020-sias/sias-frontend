import React from 'react';
import { Link } from 'react-router-dom';

export default function TablePaging(props) {
    const numberOfPages = props.numberOfPages;
    const linkTo = props.linkTo;
    if (numberOfPages < 1) return (<div></div>);
    else if (numberOfPages < 10) {
        const pageArray = [...Array(numberOfPages).keys()].map(i => i + 1);
        const currentPage = props.currentPage;
        const prevPage = (currentPage <= 1) ? null : <Link key={currentPage - 1} className="link" to={`${linkTo}/${currentPage - 1}`}>Prev</Link>
        const nextPage = (currentPage >= numberOfPages) ? null : <Link key={currentPage + 1} className="link" to={`${linkTo}/${currentPage + 1}`}>Next</Link>
        const allPages = pageArray.map(i => {
            return (
                <span key={i}>&nbsp;
                    {(i === currentPage) ? <Link key={i} className="link active" to='#'>{i}</Link> :
                        <Link key={i} className="link" to={`${linkTo}/${i}`}>{i}</Link>}
                &nbsp;</span>
            );
        });
        return (
            <div align="right" className="table-paging">
                <h5>Pages: {allPages} </h5>
                {numberOfPages > 1 &&
                    <h5>{prevPage} | {nextPage}</h5>
                }
            </div>
        )
    } else {
        const currentPage = props.currentPage;
        const first3Pages = [1, 2, 3].map(i => {
            return (
                <span key={i}>&nbsp;
                    {(i === currentPage) ? <Link className="link active" to='#'>{i}</Link> :
                        <Link key={i} className="link" to={`${linkTo}/${i}`}>{i}</Link>}
                &nbsp;</span>
            );
        });
        const last3Pages = [numberOfPages - 2, numberOfPages - 1, numberOfPages].map(i => {
            return (
                <span>&nbsp;
                    {(i === currentPage) ? <Link className="link active" to='#'>{i}</Link> :
                        <Link key={i} className="link" to={`${linkTo}/${i}`}>{i}</Link>}
                &nbsp;</span>
            );
        });
        const isCurrentPageIncluded = !(currentPage < 4 || currentPage > numberOfPages - 3);
        const currentPageIfIncluded = (
            <span>&nbsp;
                {<Link key={currentPage} className="link active" to='#'>{currentPage}</Link>}
            &nbsp;</span>
        )
        const first3Dot = (currentPage < 5) ? '' : '...';
        const last3Dot = (currentPage > numberOfPages - 4) ? '' : '...';
        const prevPage = (currentPage <= 1) ? null : <Link key={currentPage - 1} className="link" to={`${linkTo}/${currentPage - 1}`}>Prev</Link>
        const nextPage = (currentPage >= numberOfPages) ? null : <Link key={currentPage + 1} className="link" to={`${linkTo}/${currentPage + 1}`}>Next</Link>
        return (
            <div align="right" className="table-paging">
                <h5>Pages:{first3Pages}{first3Dot}{isCurrentPageIncluded ? currentPageIfIncluded : ''}{last3Dot}{last3Pages}</h5>
                <h5>{prevPage} | {nextPage}</h5>
            </div>
        );
    }
}