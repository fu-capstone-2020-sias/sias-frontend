import React from 'react';

export default function FormLabel(props) {
    const text = props.text;
    if (props.required === 'true' || props.required) {
        return <span>{text} <span className="text-danger">(*)</span></span>;
    } else {
        return <span>{text}</span>;
    }
}