import React from 'react';
import Home from '../home/HomeComponent';
import Feedback from '../feedback/FeedbackComponent';
import ViewOrderList from '../order/ViewOrderListComponent';
import ViewAccountList from '../account/ViewAccountListComponent';
import ViewCompanyInfo from "../company/ViewCompanyInfoComponent";
import ModifyCompanyInfo from "../company/ModifyCompanyInfoComponent";
import ForgotPassword from '../authentication/ForgotPasswordComponent';
import AddNewOrder from '../order/AddNewOrderComponent';
import AddNewOrderForm from '../order/AddNewOrderFormComponent';
import OrderDetail from '../order/OrderDetailComponent';
import ViewStockProductList from '../stock/ViewStockProductListComponent';
import AddNewStockProduct from '../stock/AddNewStockProductComponent';
import AddNewStockProductForm from '../stock/AddNewStockProductFormComponent';
import StockProductDetail from '../stock/StockProductDetailComponent';
import ViewWarehouseList from '../warehouse/ViewWarehouseListComponent';
import AddNewWarehouse from '../warehouse/AddNewWarehousesComponent';
import WarehouseDetail from '../warehouse/WarehouseDetailComponent';
import AddressIdLookup from '../misc/AddressIdLookupComponent';
import SteelInfosLookup from '../misc/SteelInfosLookupComponent';
import { Switch, Route, Redirect } from 'react-router-dom';
import {
    ROUTE_HOME,
    ROUTE_ORDER_DETAILS,
    ROUTE_VIEW_ORDER_LIST,
    ROUTE_VIEW_ACCOUNT_LIST,
    ROUTE_FEEDBACK,
    ROUTE_FORGOT_PASSWORD,
    ROUTE_ADD_NEW_ORDER_FORM,
    ROUTE_VIEW_STOCK_PRODUCT_LIST,
    ROUTE_VIEW_ACCOUNT_INFO,
    ROUTE_MODIFY_ACCOUNT_INFO,
    ROUTE_CHANGE_PASSWORD,
    ROUTE_ADD_NEW_ACCOUNT,
    ROUTE_VIEW_COMPANY,
    ROUTE_MODIFY_COMPANY,
    ROUTE_IMPORT_ORDER_BATCH,
    ROUTE_IMPORT_STOCK_PRODUCT_BATCH,
    ROUTE_VIEW_CUTTING_INSTRUCTION,
    ROUTE_STOCK_PRODUCT_DETAILS,
    ROUTE_ADD_NEW_STOCK_PRODUCT_FORM,
    ROUTE_ADD_NEW_ORDER,
    ROUTE_ADD_NEW_STOCK_PRODUCT,
    ROUTE_VIEW_WAREHOUSE_LIST,
    ROUTE_ADD_NEW_WAREHOUSE,
    ROUTE_WAREHOUSE_DETAILS,
    ROUTE_UPLOAD_STOCK_PRODUCT_IMAGE,
    ROUTE_DEMO,
    ROUTE_VIEW_USER_INFO,
    ROUTE_ADDRESS_ID_LOOKUP,
    ROUTE_STEEL_INFOS_LOOKUP,
    ERROR_PAGE_NOT_FOUND
} from '../../shared/constants';
import ViewAccountInfo from "../account/ViewAccountInfoComponent";
import ModifyAccountInfo from "../account/ModifyAccountInfoComponent";
import ChangePassword from "../authentication/ChangePasswordComponent";
import AddNewAccount from "../account/AddNewAccountComponent";
import ImportOrderBatch from "../order/ImportOrderBatchComponent";
import ImportStockProductBatch from "../stock/ImportStockProductBatchComponent";
import UploadStockProductImage from "../stock/UploadStockProductImageComponent";
import ViewCuttingInstruction from "../order/ViewCuttingInstructionComponent";
import Demo from "../demo/Demo";
import ViewUserInfo from "../account/ViewUserInfoComponent";
import { isNormalInteger } from '../../shared/fieldValidation';
import ErrorDisplay from './ErrorDisplay';

function SwitchRoutes(props) {
    const OrderDetailWithId = ({ match }) => {
        const orderId = parseInt(match.params.orderId, 10);
        if (isNormalInteger(match.params.orderId) && orderId > 0) {
            return (
                <OrderDetail orderId={orderId} />
            );
        } else {
            return <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
        }
    }

    const ViewOrderListWithPage = ({ match }) => {
        const pageNumber = parseInt(match.params.pageNumber, 10);
        if (isNormalInteger(match.params.pageNumber) && pageNumber > 0) {
            return (
                <ViewOrderList pageNumber={pageNumber} />
            );
        } else {
            return <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
        }
    }

    const StockProductDetailWithId = ({ match }) => {
        const stockProductId = parseInt(match.params.stockProductId, 10);
        if (isNormalInteger(match.params.stockProductId) && stockProductId > 0) {
            return (
                <StockProductDetail stockProductId={stockProductId} />
            );
        } else {
            return <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
        }
    }

    const ViewStockProductListWithPage = ({ match }) => {
        const pageNumber = parseInt(match.params.pageNumber, 10);
        if (isNormalInteger(match.params.pageNumber) && pageNumber > 0) {
            return (
                <ViewStockProductList pageNumber={pageNumber} />
            );
        } else {
            return <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
        }
    }

    const ViewWarehouseListWithPage = ({ match }) => {
        const pageNumber = parseInt(match.params.pageNumber, 10);
        if (isNormalInteger(match.params.pageNumber) && pageNumber > 0) {
            return (
                <ViewWarehouseList pageNumber={pageNumber} />
            );
        } else {
            return <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
        }
    }

    const WarehouseDetailWithId = ({ match }) => {
        const warehouseId = parseInt(match.params.warehouseId, 10);
        if (isNormalInteger(match.params.warehouseId) && warehouseId > 0) {
            return (
                <WarehouseDetail warehouseId={warehouseId} />
            );
        } else {
            return <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
        }
    }

    const ViewAccountListWithPage = ({ match }) => {
        const pageNumber = parseInt(match.params.pageNumber, 10);
        if (isNormalInteger(match.params.pageNumber) && pageNumber > 0) {
            return (
                <ViewAccountList pageNumber={pageNumber} />
            );
        } else {
            return <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
        }
    }

    const ViewFeedbackWithPage = ({ match }) => {
        const pageNumber = parseInt(match.params.pageNumber, 10);
        if (isNormalInteger(match.params.pageNumber) && pageNumber > 0) {
            return (
                <Feedback pageNumber={pageNumber} />
            );
        } else {
            return <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
        }
    }

    if (props.account != null) {
        return (
            <Switch>
                <Route path={`${ROUTE_HOME}`} component={() => <Home />} />
                <Route exact path={`${ROUTE_FEEDBACK}`} component={() => <Feedback pageNumber={1} />} />
                <Route path={`${ROUTE_FEEDBACK}/:pageNumber`} component={ViewFeedbackWithPage} />
                <Route exact path={`${ROUTE_VIEW_STOCK_PRODUCT_LIST}`}
                    component={() => <ViewStockProductList pageNumber={1} />} />
                <Route path={`${ROUTE_VIEW_STOCK_PRODUCT_LIST}/:pageNumber`} component={ViewStockProductListWithPage} />
                <Route path={`${ROUTE_STOCK_PRODUCT_DETAILS}/:stockProductId`} component={StockProductDetailWithId} />
                <Route exact path={`${ROUTE_ADD_NEW_STOCK_PRODUCT}`} component={() => <AddNewStockProduct />} />
                <Route exact path={`${ROUTE_ADD_NEW_STOCK_PRODUCT_FORM}`} component={() => <AddNewStockProductForm />} />
                <Route exact path={`${ROUTE_UPLOAD_STOCK_PRODUCT_IMAGE}`} component={() => <UploadStockProductImage />} />
                <Route exact path={`${ROUTE_VIEW_ORDER_LIST}`} component={() => <ViewOrderList pageNumber={1} />} />
                <Route path={`${ROUTE_VIEW_ORDER_LIST}/:pageNumber`} component={ViewOrderListWithPage} />
                <Route path={`${ROUTE_ORDER_DETAILS}/:orderId`} component={OrderDetailWithId} />
                <Route exact path={`${ROUTE_ADD_NEW_ORDER}`} component={() => <AddNewOrder />} />
                <Route exact path={`${ROUTE_ADD_NEW_ORDER_FORM}`} component={() => <AddNewOrderForm />} />
                <Route exact path={`${ROUTE_VIEW_ACCOUNT_LIST}`} component={() => <ViewAccountList pageNumber={1} />} />
                <Route path={`${ROUTE_VIEW_ACCOUNT_LIST}/:pageNumber`} component={ViewAccountListWithPage} />
                <Route exact path={`${ROUTE_VIEW_COMPANY}`} component={() => <ViewCompanyInfo />} />
                <Route exact path={`${ROUTE_MODIFY_COMPANY}`} component={() => <ModifyCompanyInfo />} />
                <Route exact path={`${ROUTE_VIEW_ACCOUNT_INFO}`} component={() => <ViewAccountInfo />} />
                <Route exact path={`${ROUTE_VIEW_USER_INFO}`} component={() => <ViewUserInfo />} />
                <Route exact path={`${ROUTE_MODIFY_ACCOUNT_INFO}`} component={() => <ModifyAccountInfo />} />
                <Route exact path={`${ROUTE_CHANGE_PASSWORD}`} component={() => <ChangePassword />} />
                <Route exact path={`${ROUTE_ADD_NEW_ACCOUNT}`} component={() => <AddNewAccount />} />
                <Route exact path={`${ROUTE_IMPORT_ORDER_BATCH}`} component={() => <ImportOrderBatch />} />
                <Route exact path={`${ROUTE_IMPORT_STOCK_PRODUCT_BATCH}`}
                    component={() => <ImportStockProductBatch />} />
                <Route exact path={`${ROUTE_VIEW_CUTTING_INSTRUCTION}`}
                    component={() => <ViewCuttingInstruction />} />
                {/* <Route exact path={`/test`} component={() => <AddNewOrder />} /> */}
                <Route exact path={`${ROUTE_VIEW_WAREHOUSE_LIST}`}
                    component={() => <ViewWarehouseList pageNumber={1} />} />
                <Route path={`${ROUTE_VIEW_WAREHOUSE_LIST}/:pageNumber`} component={ViewWarehouseListWithPage} />
                <Route path={`${ROUTE_WAREHOUSE_DETAILS}/:warehouseId`} component={WarehouseDetailWithId} />
                <Route exact path={`${ROUTE_ADD_NEW_WAREHOUSE}`} component={() => <AddNewWarehouse />} />
                <Route exact path={`${ROUTE_ADDRESS_ID_LOOKUP}`} component={() => <AddressIdLookup />} />
                <Route exact path={`${ROUTE_STEEL_INFOS_LOOKUP}`} component={() => <SteelInfosLookup />} />
                <Route exact path={`${ROUTE_DEMO}`} component={() => <Demo />} />
                <Redirect to={`${ROUTE_HOME}`} />
            </Switch>
        );
    } else {
        return (
            <Switch>
                <Route path={`${ROUTE_HOME}`} component={() => <Home />} />
                <Route path={`${ROUTE_FORGOT_PASSWORD}`} component={() => <ForgotPassword />} />
                <Redirect to={`${ROUTE_HOME}`} />
            </Switch>
        );
    }
}

export default SwitchRoutes;