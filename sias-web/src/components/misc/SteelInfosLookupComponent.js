import React, { Component } from 'react';
import { Label, Col, Row, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Form, Control } from 'react-redux-form';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {
    fetchSteelTypeList,
    fetchStockGroupList,
    fetchStockProductStandardList,
    fetchWarehouseSelectionList
} from '../../redux/actions/ActionCreators';
import {
    ROUTE_HOME, STOCK_PRODUCT_STATUS_GOOD, STOCK_PRODUCT_STATUS_MEDIUM, STOCK_PRODUCT_STATUS_BAD
} from '../../shared/constants';
import FormLabel from '../common/FormLabel';
import { getValueOf } from '../../shared/utils';

class SteelInfosLookup extends Component {
    constructor(props) {
        super(props);

        const steelInfos = this.props.steelInfos;
        const warehouseSelections = this.props.warehouseSelections;

        if (steelInfos.steelTypeList.length === 0 && !steelInfos.isSteelTypeLoaded) {
            this.props.fetchSteelTypeList();
        }
        if (steelInfos.stockGroupList.length === 0 && !steelInfos.isStockGroupLoaded) {
            this.props.fetchStockGroupList();
        }
        if (steelInfos.stockProductStandardList.length === 0 && !steelInfos.isStockProductStandardLoaded) {
            this.props.fetchStockProductStandardList();
        }
        if (warehouseSelections.selectionList.length === 0 && !warehouseSelections.isSteelTypeLoaded) {
            this.props.fetchWarehouseSelectionList();
        }

        this.state = {
            selectedSteelTypeId: null,
            selectedStockGroupId: null,
            selectedProductStandardId: null,
            selectedStatusId: null,
            selectedWarehouseId: null
        };
    }

    mapSteelTypeListToOption = (list) => {
        return list.map((item) => (
            <option
                key={item.steeltypeId}
                value={item.steeltypeId}
            >
                {item.name}
            </option>
        ));
    };

    mapStockGroupListToOption = (list) => {
        return list.map((item) => (
            <option
                key={item.stockProductGroupId}
                value={item.stockProductGroupId}
            >
                {item.name}
            </option>
        ));
    };

    mapStockProductStandardListToOption = (list) => {
        return list.map((item) => (
            <option
                key={item.stockproductstandardId}
                value={item.stockproductstandardId}
            >
                {item.name}
            </option>
        ));
    };

    mapWarehouseToOption = () => {
        return this.props.warehouseSelections.selectionList.map((warehouseObj) => (
            <option
                key={warehouseObj.warehouseId}
                value={warehouseObj.warehouseId}
            >
                {warehouseObj.name}
            </option>
        ));
    };

    handleSteelTypeChange = (event) => {
        this.setState({
            selectedSteelTypeId: event.target.value
        });
    }

    handleStockGroupChange = (event) => {
        this.setState({
            selectedStockGroupId: event.target.value,
        });
    }

    handleProductStandardChange = (event) => {
        this.setState({
            selectedProductStandardId: event.target.value
        });
    }

    handleStatusChange = (event) => {
        this.setState({
            selectedStatusId: event.target.value
        });
    }

    handleWarehouseChange = (event) => {
        this.setState({
            selectedWarehouseId: event.target.value
        });
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                        <BreadcrumbItem active>Steel Infos Lookup</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>Steel Infos Lookup</h3>
                        <hr />
                    </div>
                </div>
                <div className="row row-content">
                    <div className="col-12 col-md-9 col-xl-8">
                        <Form model="steelInfosLookup">
                            <Row className="form-group">
                                <Label htmlFor="type" md={3}>
                                    <FormLabel text="Type" required='true' />
                                </Label>
                                <Col md={9}>
                                    <Control.select model=".type" name="type" id="type"
                                        onChange={this.handleSteelTypeChange}
                                        className="form-control">
                                        <option key='' value=''>
                                            -Type-
                                        </option>
                                        {this.mapSteelTypeListToOption(this.props.steelInfos.steelTypeList)}
                                    </Control.select>
                                </Col>
                            </Row>

                            <Row className="form-group">
                                <Label htmlFor="stockGroupId" md={3}>
                                    <FormLabel text="Stock Group" required='true' />
                                </Label>
                                <Col md={9}>
                                    <Control.select model=".stockGroupId" name="stockGroupId" id="stockGroupId"
                                        onChange={this.handleStockGroupChange}
                                        className="form-control">
                                        <option key='' value=''>
                                            -Stock Group-
                                        </option>
                                        {this.mapStockGroupListToOption(this.props.steelInfos.stockGroupList)}
                                    </Control.select>
                                </Col>
                            </Row>

                            <Row className="form-group">
                                <Label htmlFor="standard" md={3}>
                                    <FormLabel text="Standard" required='true' />
                                </Label>
                                <Col md={9}>
                                    <Control.select model=".standard" name="standard" id="standard"
                                        onChange={this.handleProductStandardChange}
                                        className="form-control">
                                        <option key='' value=''>
                                            -Standard-
                                        </option>
                                        {this.mapStockProductStandardListToOption(this.props.steelInfos.stockProductStandardList)}
                                    </Control.select>
                                </Col>
                            </Row>

                            <Row className="form-group">
                                <Label htmlFor="statusId" md={3}>
                                    <FormLabel text="Status" required='true' />
                                </Label>
                                <Col md={9}>
                                    <Control.select model=".statusId" name="statusId" id="statusId"
                                        onChange={this.handleStatusChange}
                                        className="form-control">
                                        <option key='' value=''>
                                            -Status-
                                        </option>
                                        <option key={STOCK_PRODUCT_STATUS_GOOD} value={STOCK_PRODUCT_STATUS_GOOD}>Good</option>
                                        <option key={STOCK_PRODUCT_STATUS_MEDIUM} value={STOCK_PRODUCT_STATUS_MEDIUM}>Medium</option>
                                        <option key={STOCK_PRODUCT_STATUS_BAD} value={STOCK_PRODUCT_STATUS_BAD}>Bad</option>
                                    </Control.select>
                                </Col>
                            </Row>

                            <Row className="form-group">
                                <Label htmlFor="warehouseId" md={3}>
                                    <FormLabel text="Warehouse" required='true' />
                                </Label>
                                <Col md={9}>
                                    <Control.select model=".warehouseId" name="warehouseId" id="warehouseId"
                                        onChange={this.handleWarehouseChange}
                                        className="form-control">
                                        <option key='' value=''>
                                            -Warehouse-
                                        </option>
                                        {this.mapWarehouseToOption(this.props.warehouseSelections.selectionList)}
                                    </Control.select>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                    <div className="col-12">
                        <h5>Steel Type ID: <span className="text-danger">{getValueOf(this.state.selectedSteelTypeId)}</span></h5>
                        <h5>Stock Group ID: <span className="text-danger">{getValueOf(this.state.selectedStockGroupId)}</span></h5>
                        <h5>Product Standard ID: <span className="text-danger">{getValueOf(this.state.selectedProductStandardId)}</span></h5>
                        <h5>Status ID: <span className="text-danger">{getValueOf(this.state.selectedStatusId)}</span></h5>
                        <h5>Warehouse ID: <span className="text-danger">{getValueOf(this.state.selectedWarehouseId)}</span></h5>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        steelInfos: state.steelInfos,
        warehouseSelections: state.warehouseSelections
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        fetchSteelTypeList: () => { dispatch(fetchSteelTypeList()) },
        fetchStockGroupList: () => { dispatch(fetchStockGroupList()) },
        fetchStockProductStandardList: () => { dispatch(fetchStockProductStandardList()) },
        fetchWarehouseSelectionList: () => { dispatch(fetchWarehouseSelectionList()) }
    });
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SteelInfosLookup));