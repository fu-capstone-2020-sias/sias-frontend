import React, { Component } from 'react';
import { Label, Col, Row, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Form, Control } from 'react-redux-form';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchCityProvinceIdList, fetchDistrictIdList, fetchWardCommuneIdList } from '../../redux/actions/ActionCreators';
import {
    ROUTE_HOME
} from '../../shared/constants';
import FormLabel from '../common/FormLabel';
import { getValueOf } from '../../shared/utils';

class AddressIdLookup extends Component {
    constructor(props) {
        super(props);
        const addresses = this.props.addresses;

        if (addresses.cityProvinceList.length === 0 && !addresses.isLoaded) {
            this.props.fetchCityProvinceIdList();
            this.props.fetchDistrictIdList();
            this.props.fetchWardCommuneIdList();
        }

        this.state = {
            selectedCityProvinceId: null,
            selectedDistrictId: null,
            selectedWardCommuneId: null
        };
    }

    mapCityProvinceToOption = () => {
        return this.props.addresses.cityProvinceList.map((cityProvinceObj) => (
            <option
                key={cityProvinceObj.cityProvinceId}
                value={cityProvinceObj.cityProvinceId}
            >
                {cityProvinceObj.name}
            </option>
        ));
    };

    mapDistrictToOption = () => {
        const districtList = this.props.addresses.districtList;
        let provinceId = "";

        provinceId = this.state.selectedCityProvinceId;

        if (provinceId != null && provinceId !== '') {
            return districtList
                .filter(dist => dist.cityProvinceId === parseInt(provinceId))
                .map((districtObj) => (
                    <option
                        key={districtObj.districtId}
                        value={districtObj.districtId}>
                        {districtObj.name}
                    </option>
                ));
        }
    }

    mapWardCommuneToOption = () => {
        const wardCommuneList = this.props.addresses.wardCommuneList;
        let districtId = "";

        districtId = this.state.selectedDistrictId;

        if (districtId != null && districtId !== '') {
            return wardCommuneList
                .filter(ward => ward.districtId === parseInt(districtId))
                .map((wardCommuneObj) => (
                    <option
                        key={wardCommuneObj.wardCommuneId}
                        value={wardCommuneObj.wardCommuneId}
                    >
                        {wardCommuneObj.name}
                    </option>
                ));
        }
    }

    handleCityProvinceChange = (event) => {
        this.setState({
            selectedCityProvinceId: event.target.value,
            selectedDistrictId: '',
            selectedWardCommuneId: '',
        });
    }

    handleDistrictChange = (event) => {
        this.setState({
            selectedDistrictId: event.target.value,
            selectedWardCommuneId: '',
        });
    }

    handleWardCommuneChange = (event) => {
        this.setState({
            selectedWardCommuneId: event.target.value
        });
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                        <BreadcrumbItem active>Address Id Lookup</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>Address Id Lookup</h3>
                        <hr />
                    </div>
                </div>
                <div className="row row-content">
                    <div className="col-12 col-md-9 col-xl-8">
                        <Form model="addressIdLookup">
                            <Row className="form-group">
                                <Label htmlFor="customerCityProvinceId" md={3}>
                                    <FormLabel text="City/Province" required='true' />
                                </Label>
                                <Col md={9}>
                                    <Control.select
                                        model=".customerCityProvinceId"
                                        id="customerCityProvinceId"
                                        name="customerCityProvinceId"
                                        className="form-control"
                                        onChange={this.handleCityProvinceChange}
                                    >
                                        <option key='' value=''>
                                            -City/Province-
                                        </option>
                                        {this.mapCityProvinceToOption()}
                                    </Control.select>
                                </Col>
                            </Row>

                            <Row className="form-group">
                                <Label htmlFor="customerDistrictId" md={3}>
                                    <FormLabel text="District" required='true' />
                                </Label>
                                <Col md={9}>
                                    <Control.select
                                        model=".customerDistrictId"
                                        id="customerDistrictId"
                                        name="customerDistrictId"
                                        className="form-control"
                                        onChange={this.handleDistrictChange}
                                    >
                                        <option key='' value=''>
                                            -District-
                                        </option>
                                        {this.mapDistrictToOption()}
                                    </Control.select>
                                </Col>
                            </Row>

                            <Row className="form-group">
                                <Label htmlFor="customerWardCommuneId" md={3}>
                                    <FormLabel text="Ward/Commune" required='true' />
                                </Label>
                                <Col md={9}>
                                    <Control.select
                                        model=".customerWardCommuneId"
                                        id="customerWardCommuneId"
                                        name="customerWardCommuneId"
                                        className="form-control"
                                        onChange={this.handleWardCommuneChange}
                                    >
                                        <option key='' value=''>
                                            -Ward/Commune-
                                        </option>
                                        {this.mapWardCommuneToOption()}
                                    </Control.select>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                    <div className="col-12">
                        <h5>City/Province ID: <span className="text-danger">{getValueOf(this.state.selectedCityProvinceId)}</span></h5>
                        <h5>District ID: <span className="text-danger">{getValueOf(this.state.selectedDistrictId)}</span></h5>
                        <h5>Ward/Commune ID: <span className="text-danger">{getValueOf(this.state.selectedWardCommuneId)}</span></h5>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        addresses: state.addresses
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        fetchCityProvinceIdList: () => { dispatch(fetchCityProvinceIdList()) },
        fetchDistrictIdList: () => { dispatch(fetchDistrictIdList()) },
        fetchWardCommuneIdList: () => { dispatch(fetchWardCommuneIdList()) }
    });
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddressIdLookup));