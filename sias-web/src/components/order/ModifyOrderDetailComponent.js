import React, { Component } from 'react';
import { Button, Label, Col, Row } from 'reactstrap';
import { Form, Control, Errors, actions } from 'react-redux-form';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {
    ORDER_STATUS_PENDING_TEXT,
    ORDER_STATUS_APPROVED_TEXT,
    ORDER_STATUS_REJECTED_TEXT,
    ORDER_STATUS_PENDING,
    ORDER_STATUS_APPROVED,
    ORDER_STATUS_REJECTED,
    ROLE_OFFICE_MANAGER,
    VALIDATION_MSG_INVALID_NAME,
    VALIDATION_MSG_INVALID_COMPANY_NAME,
    VALIDATION_MSG_REQUIRED,
    VALIDATION_MSG_INVALID_EMAIL,
    VALIDATION_MSG_INVALID_PHONE_NUMBER,
    MSG_CANNOT_CHANGE_ORDER_STATUS_IF_NO_ITEMS
} from '../../shared/constants';
import {
    putModifyOrderDetail,
    fetchCityProvinceIdList,
    fetchDistrictIdList,
    fetchWardCommuneIdList,
    fetchAssigneeList
} from '../../redux/actions/ActionCreators';
import { required, validCompanyName, validCustomerName, validEmail, validPhoneNumber } from '../../shared/fieldValidation';
import FormLabel from '../common/FormLabel';

class ModifyOrderDetail extends Component {
    constructor(props) {
        super(props);
        const addresses = this.props.addresses;
        const orderApproverAndAssigneeInfos = this.props.orderApproverAndAssigneeInfos;

        if (addresses.cityProvinceList.length === 0 && !addresses.isLoaded) {
            this.props.fetchCityProvinceIdList();
            this.props.fetchDistrictIdList();
            this.props.fetchWardCommuneIdList();
        }

        if (orderApproverAndAssigneeInfos.assigneeList.length === 0 && !orderApproverAndAssigneeInfos.isAssigneeListLoaded) {
            this.props.fetchAssigneeList();
        }

        this.toggleEditAddress = this.toggleEditAddress.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            selectedCityProvinceId: "",
            selectedDistrictId: "",
            selectedWardCommuneId: "",
            editAddress: false,
            firstLoad: true
        };
    }

    handleSubmit = (values) => {
        this.props.putModifyOrderDetail(values);
    }

    toggleEditAddress() {
        this.setState({
            editAddress: !this.state.editAddress
        });
    }

    mapCityProvinceToOption = () => {
        return this.props.addresses.cityProvinceList.map((cityProvinceObj) => (
            <option
                key={cityProvinceObj.cityProvinceId}
                value={cityProvinceObj.cityProvinceId}
            >
                {cityProvinceObj.name}
            </option>
        ));
    };

    mapDistrictToOption = () => {
        const districtList = this.props.addresses.districtList;
        let provinceId = "";

        provinceId = this.state.selectedCityProvinceId;

        if (provinceId != null && provinceId !== '') {
            return districtList
                .filter(dist => dist.cityProvinceId === parseInt(provinceId))
                .map((districtObj) => (
                    <option
                        key={districtObj.districtId}
                        value={districtObj.districtId}>
                        {districtObj.name}
                    </option>
                ));
        }
    }

    mapWardCommuneToOption = () => {
        const wardCommuneList = this.props.addresses.wardCommuneList;
        let districtId = "";

        districtId = this.state.selectedDistrictId;

        if (districtId != null && districtId !== '') {
            return wardCommuneList
                .filter(ward => ward.districtId === parseInt(districtId))
                .map((wardCommuneObj) => (
                    <option
                        key={wardCommuneObj.wardCommuneId}
                        value={wardCommuneObj.wardCommuneId}
                    >
                        {wardCommuneObj.name}
                    </option>
                ));
        }
    }

    mapAssigneeListToOption = (list) => {
        return list.map((item) => (
            <option
                key={item.userName}
                value={item.userName}
            >
                {item.userName}
            </option>
        ));
    };

    handleCityProvinceChange = (event) => {
        this.setState({
            selectedCityProvinceId: event.target.value,
            selectedDistrictId: '',
            selectedWardCommuneId: '',
        });
    }

    handleDistrictChange = (event) => {
        this.setState({
            selectedDistrictId: event.target.value,
            selectedWardCommuneId: '',
        });
    }

    handleWardCommuneChange = (event) => {
        this.setState({
            selectedWardCommuneId: event.target.value
        });
    }

    isSaveButtonEnabled() {
        return (
            !this.state.editAddress
            || (this.state.selectedCityProvinceId !== ''
                && this.state.selectedDistrictId !== ''
                && this.state.selectedWardCommuneId !== '')
        );
    }

    render() {
        let order = this.props.order;

        if (order == null) {
            return (<div></div>);
        }
        if (this.state.firstLoad) {
            this.props.setDefaultEditOrderDetail(order);
            this.setState({
                firstLoad: false
            })
        }

        const account = this.props.account;
        return (
            <div>
                {account.roleId === ROLE_OFFICE_MANAGER && !this.props.isStatusEditable &&
                    <h5 className="text-danger">{MSG_CANNOT_CHANGE_ORDER_STATUS_IF_NO_ITEMS}</h5>
                }
                <Form model="editOrderDetail"
                    onSubmit={(values) => this.handleSubmit(values)}
                >
                    <Row className="form-group">
                        <Label htmlFor="orderId" md={3}>Order Id</Label>
                        <Col md={9}>
                            <Control.text model=".orderId" id="orderId" name="orderId"
                                className="form-control"
                                value={order.orderId}
                                disabled />

                        </Col>
                    </Row>

                    {account.roleId === ROLE_OFFICE_MANAGER &&
                        <Row className="form-group">
                            <Label htmlFor="orderStatusId" md={3}>
                                <FormLabel text="Status" required='true' />
                            </Label>
                            <Col md={9}>
                                <Control.select model=".orderStatusId" name="orderStatusId" id="orderStatusId"
                                    disabled={!this.props.isStatusEditable}
                                    className="form-control">
                                    <option key={ORDER_STATUS_PENDING} value={ORDER_STATUS_PENDING}>{ORDER_STATUS_PENDING_TEXT}</option>
                                    <option key={ORDER_STATUS_APPROVED} value={ORDER_STATUS_APPROVED}>{ORDER_STATUS_APPROVED_TEXT}</option>
                                    <option key={ORDER_STATUS_REJECTED} value={ORDER_STATUS_REJECTED}>{ORDER_STATUS_REJECTED_TEXT}</option>
                                </Control.select>
                            </Col>
                        </Row>
                    }

                    <Row className="form-group">
                        <Label htmlFor="assignee" md={3}>
                            <FormLabel text="Assignee" required='true' />
                        </Label>
                        <Col md={9}>
                            <Control.select model=".assignee" name="assignee" id="assignee"
                                className="form-control" validators={{ required }}>
                                <option key='' value=''>
                                    -Assignee-
                                        </option>
                                {this.mapAssigneeListToOption(this.props.orderApproverAndAssigneeInfos.assigneeList)}
                            </Control.select>
                            <Errors className="text-danger" model=".assignee" show="touched"
                                messages={{
                                    required: VALIDATION_MSG_REQUIRED
                                }} />
                        </Col>
                    </Row>

                    <Row className="form-group">
                        <Label htmlFor="customerName" md={3}>
                            <FormLabel text="Customer's Name" required='true' />
                        </Label>
                        <Col md={9}>
                            <Control.text model=".customerName" id="customerName" name="customerName"
                                className="form-control" validators={{ required, validCustomerName }} />
                            <Errors className="text-danger" model=".customerName" show="touched"
                                messages={{
                                    required: VALIDATION_MSG_REQUIRED,
                                    validCustomerName: VALIDATION_MSG_INVALID_NAME
                                }} />
                        </Col>
                    </Row>

                    <Row className="form-group">
                        <Label htmlFor="customerCompanyName" md={3}>
                            <FormLabel text="Customer's Company" required='true' />
                        </Label>
                        <Col md={9}>
                            <Control.text model=".customerCompanyName" id="customerCompanyName" name="customerCompanyName"
                                className="form-control" validators={{ required, validCompanyName }} />
                            <Errors className="text-danger" model=".customerCompanyName" show="touched"
                                messages={{
                                    required: VALIDATION_MSG_REQUIRED,
                                    validCompanyName: VALIDATION_MSG_INVALID_COMPANY_NAME
                                }} />
                        </Col>
                    </Row>

                    <Row className="form-group">
                        <Label htmlFor="customerEmail" md={3}>
                            <FormLabel text="Customer's Email" required='true' />
                        </Label>
                        <Col md={9}>
                            <Control.text model=".customerEmail" id="customerEmail" name="customerEmail"
                                className="form-control" validators={{ required, validEmail }} />
                            <Errors className="text-danger" model=".customerEmail" show="touched"
                                messages={{
                                    // required: VALIDATION_MSG_REQUIRED,
                                    validEmail: VALIDATION_MSG_INVALID_EMAIL
                                }} />
                        </Col>
                    </Row>

                    <Row className="form-group">
                        <Label htmlFor="customerPhoneNumber" md={3}>
                            <FormLabel text="Customer's Phone number" required='true' />
                        </Label>
                        <Col md={9}>
                            <Control.text model=".customerPhoneNumber" id="customerPhoneNumber" name="customerPhoneNumber"
                                className="form-control" validators={{ required, validPhoneNumber }} />
                            <Errors className="text-danger" model=".customerPhoneNumber" show="touched"
                                messages={{
                                    // required: VALIDATION_MSG_REQUIRED,
                                    validPhoneNumber: VALIDATION_MSG_INVALID_PHONE_NUMBER
                                }} />
                        </Col>
                    </Row>

                    <Row className="form-group">
                        <Col md={9}>
                            <div className="form-check">
                                <Label check>
                                    <Control.checkbox model=".editAddress" name="editAddress" id="editAddress"
                                        className="form-check-input"
                                        defaultChecked={false}
                                        onClick={this.toggleEditAddress} />
                                    <strong>Edit address? &nbsp;</strong>
                                </Label>
                            </div>
                        </Col>
                    </Row>
                    {!this.state.editAddress &&
                        <React.Fragment>
                            <Row className="form-group">
                                <Label htmlFor="customerCityProvinceId" md={3}>City/Province</Label>
                                <Col md={9}>
                                    <Control.text model=".customerCityProvince" id="customerCityProvince" name="customerCityProvince"
                                        className="form-control"
                                        value={order.customerCityProvince}
                                        disabled />

                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="customerDistrictId" md={3}>District</Label>
                                <Col md={9}>
                                    <Control.text model=".customerDistrict" id="customerDistrict" name="customerDistrict"
                                        className="form-control"
                                        value={order.customerDistrict}
                                        disabled />

                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="customerWardCommuneId" md={3}>Ward/Commune</Label>
                                <Col md={9}>
                                    <Control.text model=".customerWardCommune" id="customerWardCommune" name="customerWardCommune"
                                        className="form-control"
                                        value={order.customerWardCommune}
                                        disabled />

                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="customerAddress" md={3}>Address</Label>
                                <Col md={9}>
                                    <Control.text model=".customerAddress" id="customerAddress" name="customerAddress"
                                        className="form-control"
                                        value={order.customerAddress}
                                        disabled />
                                </Col>
                            </Row>
                        </React.Fragment>
                    }

                    {this.state.editAddress &&
                        <React.Fragment>
                            <Row className="form-group">
                                <Label htmlFor="newCustomerCityProvinceId" md={3}>
                                    <FormLabel text="City/Province" required='true' />
                                </Label>
                                <Col md={9}>
                                    <Control.select
                                        model=".newCustomerCityProvinceId"
                                        id="newCustomerCityProvinceId"
                                        name="newCustomerCityProvinceId"
                                        className="form-control"
                                        validators={{ required }}
                                        onChange={this.handleCityProvinceChange}
                                    >
                                        <option key='' value=''>
                                            -City/Province-
                                    </option>
                                        {this.mapCityProvinceToOption()}
                                    </Control.select>
                                    <Errors className="text-danger" model=".newCustomerCityProvinceId" show="touched"
                                        messages={{
                                            required: VALIDATION_MSG_REQUIRED
                                        }} />
                                </Col>
                            </Row>

                            <Row className="form-group">
                                <Label htmlFor="newCustomerDistrictId" md={3}>
                                    <FormLabel text="District" required='true' />
                                </Label>
                                <Col md={9}>
                                    <Control.select
                                        model=".newCustomerDistrictId"
                                        id="newCustomerDistrictId"
                                        name="newCustomerDistrictId"
                                        className="form-control"
                                        validators={{ required }}
                                        onChange={this.handleDistrictChange}
                                    >
                                        <option key='' value=''>
                                            -District-
                                    </option>
                                        {this.mapDistrictToOption()}
                                    </Control.select>
                                    <Errors className="text-danger" model=".newCustomerDistrictId" show="touched"
                                        messages={{
                                            required: VALIDATION_MSG_REQUIRED
                                        }} />
                                </Col>
                            </Row>

                            <Row className="form-group">
                                <Label htmlFor="newCustomerWardCommuneId" md={3}>
                                    <FormLabel text="Ward/Commune" required='true' />
                                </Label>
                                <Col md={9}>
                                    <Control.select
                                        model=".newCustomerWardCommuneId"
                                        id="newCustomerWardCommuneId"
                                        name="newCustomerWardCommuneId"
                                        className="form-control"
                                        validators={{ required }}
                                        onChange={this.handleWardCommuneChange}
                                    >
                                        <option key='' value=''>
                                            -Ward/Commune-
                                    </option>
                                        {this.mapWardCommuneToOption()}
                                    </Control.select>
                                    <Errors className="text-danger" model=".newCustomerWardCommuneId" show="touched"
                                        messages={{
                                            required: VALIDATION_MSG_REQUIRED
                                        }} />
                                </Col>
                            </Row>

                            <Row className="form-group">
                                <Label htmlFor="newCustomerAddress" md={3}>
                                    <FormLabel text="Address" required='true' />
                                </Label>
                                <Col md={9}>
                                    <Control.text model=".newCustomerAddress" id="newCustomerAddress" name="newCustomerAddress"
                                        className="form-control" validators={{ required }} />
                                    <Errors className="text-danger" model=".newCustomerAddress" show="touched"
                                        messages={{
                                            required: VALIDATION_MSG_REQUIRED
                                        }} />
                                </Col>
                            </Row>
                        </React.Fragment>
                    }



                    <Row className="form-group">
                        <Col>
                            <Button disabled={!this.isSaveButtonEnabled()}
                                color="warning"
                                type="submit"
                                className="submitButton"
                            >
                                Save
                            </Button>

                        &nbsp;
                        <Button color="light"
                                className="backButton"
                                onClick={this.props.backToView}
                            >
                                Back
                        </Button>
                        </Col>
                    </Row>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        successStatus: state.orderDetail.successStatus,
        addresses: state.addresses,
        orderApproverAndAssigneeInfos: state.orderApproverAndAssigneeInfos
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        fetchCityProvinceIdList: () => { dispatch(fetchCityProvinceIdList()) },
        fetchDistrictIdList: () => { dispatch(fetchDistrictIdList()) },
        fetchWardCommuneIdList: () => { dispatch(fetchWardCommuneIdList()) },
        fetchAssigneeList: () => { dispatch(fetchAssigneeList()) },
        setDefaultEditOrderDetail: (values) => { dispatch(actions.merge("editOrderDetail", values)) },
        putModifyOrderDetail: (values) => { dispatch(putModifyOrderDetail(values, 'WEB_UC33')) }
    });
}



export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ModifyOrderDetail));