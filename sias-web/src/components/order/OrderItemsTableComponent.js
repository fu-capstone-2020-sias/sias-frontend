import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {getValueOf, dateDisplayVietnamese, formatterVND, formatterLengthUnits} from '../../shared/utils';
import {StyledTableCell} from '../common/StyledTableCell';
import EnhancedTableHead from '../common/EnhancedTableHead';
import {Link} from "react-router-dom";
import {ROUTE_VIEW_USER_INFO} from "../../shared/constants";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

const headCells = [
    {id: 'orderItemId', numeric: true, disablePadding: false, label: 'Item ID'},
    {id: 'length', numeric: true, disablePadding: false, label: 'Length'},
    {id: 'bladeWidth', numeric: true, disablePadding: false, label: 'Blade width'},
    {id: 'quantity', numeric: true, disablePadding: true, label: 'Quantity'},
    {id: 'price', numeric: true, disablePadding: false, label: 'Unit price'},
    {id: 'createdDate', numeric: false, disablePadding: false, label: 'Created date'},
    {id: 'creator', numeric: false, disablePadding: false, label: 'Creator'},
    {id: 'updatedDate', numeric: false, disablePadding: false, label: 'Updated date'},
    {id: 'updater', numeric: false, disablePadding: false, label: 'Updater'}
];

EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    rowCount: PropTypes.number.isRequired
};

function OrderItemsTable(props) {
    const items = props.items;
    const classes = useStyles();
    if (items == null || items.length < 1) return (<h5>No items in this order</h5>);
    else {
        return (
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <EnhancedTableHead
                        headCells={headCells}
                        classes={classes}
                        rowCount={items.length}
                    />
                    <TableBody>
                        {items.map((row) => (
                            <TableRow key={row.orderItemId}>
                                <StyledTableCell align="right">{getValueOf(row.orderItemId)}</StyledTableCell>
                                <StyledTableCell
                                    align="right">{getValueOf(formatterLengthUnits.format(row.length))}</StyledTableCell>
                                <StyledTableCell
                                    align="right">{getValueOf(formatterLengthUnits.format(row.bladeWidth))}</StyledTableCell>
                                <StyledTableCell align="right">{getValueOf(row.quantity)}</StyledTableCell>
                                <StyledTableCell
                                    align="right">{getValueOf(formatterVND.format(row.price))}</StyledTableCell>
                                <StyledTableCell>{getValueOf(dateDisplayVietnamese(row.createdDate))}</StyledTableCell>
                                <StyledTableCell>
                                    {getValueOf(row.creator) !== 'N/A' ?
                                        <Link
                                            to={`${ROUTE_VIEW_USER_INFO}?userName=${getValueOf(row.creator)}`}>
                                            {getValueOf(row.creator)}
                                        </Link> : 'N/A'
                                    }
                                </StyledTableCell>
                                <StyledTableCell>{getValueOf(dateDisplayVietnamese(row.updatedDate))}</StyledTableCell>
                                <StyledTableCell>
                                    {getValueOf(row.updater) !== 'N/A' ?
                                        <Link
                                            to={`${ROUTE_VIEW_USER_INFO}?userName=${getValueOf(row.updater)}`}>
                                            {getValueOf(row.updater)}
                                        </Link> : 'N/A'
                                    }
                                </StyledTableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        );
    }
}

export default OrderItemsTable;