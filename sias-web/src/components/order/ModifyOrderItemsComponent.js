import React, { useState } from 'react';
import Paper from '@material-ui/core/Paper';
import { EditingState } from '@devexpress/dx-react-grid';
import {
    Grid,
    Table,
    TableHeaderRow,
    TableEditRow,
    TableEditColumn,
} from '@devexpress/dx-react-grid-material-ui';
import { Button, Col } from 'reactstrap';
import { ROLE_OFFICE_MANAGER, ROLE_OFFICE_EMPLOYEE, SUCCESS_MESSAGE_SUBMIT_GENERAL, VALIDATION_MSG_ITEMS_GENERAL, VALIDATION_MSG_INVALID_ITEM_LENGTH, VALIDATION_MSG_INVALID_ITEM_QUANTITY, VALIDATION_MSG_INVALID_ITEM_PRICE, VALIDATION_MSG_INVALID_ITEM_BLADE_WIDTH } from '../../shared/constants';
import { connect } from 'react-redux';
import { postAddOrderItems, putModifyOrderItems, delDeleteOrderItems } from '../../redux/actions/ActionCreators';
import { validNumberField } from '../../shared/fieldValidation';

const getRowId = row => row.orderItemId;

const tempIdPrefix = "new";

const columns = [
    // { name: 'orderItemId', title: 'ID' },
    { name: 'length', title: 'Length (mm)' },
    { name: 'quantity', title: 'Quantity' },
    { name: 'price', title: 'Unit price (VND)' },
    { name: 'bladeWidth', title: 'Blade width (mm)' }
];


function ModifyOrderItems(props) {
    const account = props.account;
    const items = props.items;
    const orderId = props.orderId;
    const [tempId, setTempId] = useState(0);
    const [del, setDel] = useState([]);
    const [rows, setRows] = useState(items);
    const [editingRowIds, setEditingRowIds] = useState([]);
    const [addedRows, setAddedRows] = useState([]);
    const [rowChanges, setRowChanges] = useState({});

    const changeAddedRows = (value) => {
        const initialized = value.map(row => (Object.keys(row).length ? row : {}));
        setAddedRows(initialized);
    };

    const commitChanges = ({ added, changed, deleted }) => {
        let changedRows;
        if (added) {
            changedRows = [
                ...rows,
                ...added.map((row) => {
                    setTempId(tempId + 1);
                    return ({
                        orderItemId: tempIdPrefix + tempId,
                        orderId: orderId,
                        newItem: true,
                        ...row,
                    });
                }),
            ];
        }
        if (changed) {
            changedRows = rows.map(row => (changed[row.orderItemId] ? { ...row, ...changed[row.orderItemId] } : row));
        }
        if (deleted) {
            const deletedSet = new Set(deleted);
            changedRows = rows.filter(row => !deletedSet.has(row.orderItemId));
            let changedDel = rows.filter(row => deletedSet.has(row.orderItemId));
            // console.log(changedDel);
            setDel([...del, changedDel[0]]);
        }
        setRows(changedRows);
    };

    const submit = async () => {
        const addedRows = rows.filter(row => row.newItem === true);
        const modifiedRows = rows.filter(row => row.newItem == null);
        const deletedRows = del.filter(row => row.newItem == null);
        // alert(addedRows.length + ' ' + modifiedRows.length + ' ' + deletedRows.length);

        if (addedRows.length > 0) await props.postAddOrderItems(orderId, addedRows);
        if (modifiedRows.length > 0) await props.putModifyOrderItems(orderId, modifiedRows);
        if (deletedRows.length > 0) await props.delDeleteOrderItems(deletedRows);
        setTimeout(() => { window.location.reload(false); }, 3000);

    }

    if (account == null) {
        return (
            <div className="container">
                <div className="row row-content">
                    <h1>You must login first</h1>
                </div>
            </div>
        );
    } else if (roleAccessible(account.roleId)) {
        const addItemsSuccess = props.addItemsSuccess;
        const modifyItemsSuccess = props.modifyItemsSuccess;
        const deleteItemsSuccess = props.deleteItemsSuccess;

        return (
            <div>
                <div className="col-12">
                    <h3>Order Items</h3>
                </div>
                <div className="col-12">
                    <h5 className="text-success">{isSuccessful(addItemsSuccess, modifyItemsSuccess, deleteItemsSuccess) ? SUCCESS_MESSAGE_SUBMIT_GENERAL : ''}</h5>
                    {/* <h5 className="text-danger">{isFailed(addItemsSuccess, modifyItemsSuccess, deleteItemsSuccess) != null ? errMess : ''}</h5> */}
                </div>
                <div className="mt-3">
                    <Paper>
                        <Grid
                            rows={rows}
                            columns={columns}
                            getRowId={getRowId}
                        >
                            <EditingState
                                editingRowIds={editingRowIds}
                                onEditingRowIdsChange={setEditingRowIds}
                                rowChanges={rowChanges}
                                onRowChangesChange={setRowChanges}
                                addedRows={addedRows}
                                onAddedRowsChange={changeAddedRows}
                                onCommitChanges={commitChanges}
                            />
                            <Table
                            // columnExtensions={tableColumnExtensions}
                            />
                            <TableHeaderRow />
                            <TableEditRow />
                            <TableEditColumn
                                showAddCommand={!addedRows.length}
                                showEditCommand
                                showDeleteCommand
                            />
                        </Grid>
                    </Paper>
                </div>
                <div className="mt-3">
                    <Col>
                        <Button
                            disabled={!isValidAllFields(rows)}
                            color="warning"
                            type="submit"
                            className="submitButton"
                            onClick={submit}
                        >
                            Save
                            </Button>

                        &nbsp;
                        <Button color="light"
                            className="backButton"
                            onClick={props.backToView}
                        >
                            Back
                        </Button>
                    </Col>
                    <Col>
                        <h5 className="text-danger">
                            {isValidAllFields(rows) ? '' : VALIDATION_MSG_ITEMS_GENERAL}
                            <ul>
                                {isValidLength(rows) ? null : <li>{VALIDATION_MSG_INVALID_ITEM_LENGTH}</li>}
                                {isValidQuantity(rows) ? null : <li>{VALIDATION_MSG_INVALID_ITEM_QUANTITY}</li>}
                                {isValidPrice(rows) ? null : <li>{VALIDATION_MSG_INVALID_ITEM_PRICE}</li>}
                                {isValidBladeWidth(rows) ? null : <li>{VALIDATION_MSG_INVALID_ITEM_BLADE_WIDTH}</li>}
                            </ul>
                        </h5>
                    </Col>
                </div>
            </div>

        );
    }
}

const isValidAllFields = (items) => {
    return isValidLength(items) && isValidQuantity(items) && isValidPrice(items) && isValidBladeWidth(items);
}

const isValidLength = (items) => { //allow integers with length corresponding to fields
    for (let i = 0; i < items.length; i++) {
        const it = items[i];
        const isRowInvalid = !validNumberField(it.length, 1, 5);
        if (isRowInvalid) return false;
    }
    return true;
}

const isValidQuantity = (items) => { //allow integers with length corresponding to fields
    for (let i = 0; i < items.length; i++) {
        const it = items[i];
        const isRowInvalid = !validNumberField(it.quantity, 1, 5);
        if (isRowInvalid) return false;
    }
    return true;
}

const isValidPrice = (items) => { //allow integers with length corresponding to fields
    for (let i = 0; i < items.length; i++) {
        const it = items[i];
        const isRowInvalid = !validNumberField(it.price, 1, 10);
        if (isRowInvalid) return false;
    }
    return true;
}

const isValidBladeWidth = (items) => { //allow integers with length corresponding to fields
    for (let i = 0; i < items.length; i++) {
        const it = items[i];
        const isRowInvalid = !validNumberField(it.bladeWidth, 1, 2);
        if (isRowInvalid) return false;
    }
    return true;
}

const isSuccessful = (add, mod, del) => {
    return add.msg == null && mod.msg == null && del.msg == null
        && (add.status || mod.status || del.status);
}

// const isFailed = (add, mod, del) => {
//     return add.msg != null || mod.msg != null || del.msg != null;
// }

const roleAccessible = (roleId) => {
    return roleId === ROLE_OFFICE_MANAGER || roleId === ROLE_OFFICE_EMPLOYEE;
}

const mapStateToProps = (state) => {
    return {
        addItemsSuccess: state.orderDetail.addItemsSuccess,
        modifyItemsSuccess: state.orderDetail.modifyItemsSuccess,
        deleteItemsSuccess: state.orderDetail.deleteItemsSuccess,
        errMess: state.orderDetail.errMess
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        postAddOrderItems: (orderId, addedItems) => { dispatch(postAddOrderItems(orderId, addedItems)) },
        putModifyOrderItems: (orderId, modifiedItems) => { dispatch(putModifyOrderItems(orderId, modifiedItems)) },
        delDeleteOrderItems: (deletedItems) => { dispatch(delDeleteOrderItems(deletedItems)) },
    });
}

export default connect(mapStateToProps, mapDispatchToProps)(ModifyOrderItems);