import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { connect } from 'react-redux';
import {
    ROLE_FACTORY_MANAGER,
    ROUTE_VIEW_CUTTING_INSTRUCTION,
    ROUTE_HOME
} from '../../shared/constants';
import { getValueOf } from '../../shared/utils';
import { StyledTableCell } from '../common/StyledTableCell';
import TablePaging from '../common/TablePaging';
import OrderStatusDisplay from '../common/OrderStatusDisplay';
import { Loading } from '../common/LoadingComponent';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { fetchCuttingInstructionByOrderId } from "../../redux/actions/ActionCreators"; // Import css
import UnauthorizedAccessDisplay from '../common/UnauthorizedAccessDisplay';


const useStyles = makeStyles({
    table: {
        minWidth: 600,
    }
});

const headCells = [
    { id: 'orderItemId', numeric: true, disablePadding: false, label: 'Order Item ID' },
    { id: 'cuttingInstructionId', numeric: true, disablePadding: false, label: 'Cutting Instruction ID' },
    { id: 'steelBarNumber', numeric: true, disablePadding: false, label: 'Steel Bar #' },
    { id: 'steelLength', numeric: true, disablePadding: false, label: 'Wanted Length' },
    { id: 'bladeWidth', numeric: true, disablePadding: false, label: 'Blade Width' },
    { id: 'stockProductId', numeric: true, disablePadding: false, label: 'Stock Product ID to cut from' },
    { id: 'stockProductLength', numeric: true, disablePadding: false, label: 'After-cut Stock Product Length' },
    { id: 'assignee', numeric: false, disablePadding: false, label: 'Assignee' },
];

function ViewCuttingInstructionTableHead() {
    return (
        <TableHead>
            <TableRow>
                {headCells.map((headCell) => (
                    <StyledTableCell
                        key={headCell.id}
                        align={headCell.numeric ? 'right' : 'left'}
                        padding={headCell.disablePadding ? 'none' : 'default'}
                    >
                        {headCell.label}
                    </StyledTableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

ViewCuttingInstructionTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    // numSelected: PropTypes.number.isRequired,
    // onSelectAllClick: PropTypes.func.isRequired,
    rowCount: PropTypes.number.isRequired
};

function ViewCuttingInstructionTable(props) {
    const classes = useStyles();
    // if (props.cuttingInstruction === null || props.cuttingInstruction.errMess !== null) {
    //     if (props.cuttingInstruction === null) {
    //         return (
    //             <div className="col-12">
    //                 <Loading />
    //             </div>
    //         )
    //     }
    // }

    if (props.errMess !== null
        && props.errMess !== undefined &&
        props.errMess.length !== 0) {
        // console.log("props.cuttingInstruction[0]");
        // console.log(props.errMess[0])
        return (
            <div className="col-12">
                <h4>{props.errMess[0]}</h4>
            </div>
        )
    }

    let rows = null;
    // console.log("CUTTING INSTRUCTION");
    if (props.cuttingInstruction.cuttingInstruction !== undefined) {
        // console.log(props.cuttingInstruction.cuttingInstruction.cuttingInstruction);

        rows = props.cuttingInstruction.cuttingInstruction.cuttingInstruction;
    }

    if (rows === null) {
        return <Loading />
    }

    // console.log("rows")
    // console.log(rows);

    let orderStatus = null;
    let orderId = null;
    if (props.cuttingInstruction.cuttingInstruction !== undefined) {
        orderStatus = props.cuttingInstruction.cuttingInstruction.order[0].orderStatus;
        orderId = props.cuttingInstruction.cuttingInstruction.order[0].orderId;
    }

    // console.log(orderStatus)

    let checkExist = null;
    let skip = 0;

    return (
        <React.Fragment>
            <div className="col-12">
                <div style={{ marginBottom: "20px" }}>
                    <h5>Order ID: {orderId}</h5>
                    <h5>
                        Order Status: <OrderStatusDisplay status={getValueOf(orderStatus)} />
                    </h5>
                </div>

                <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="customized table">
                        <ViewCuttingInstructionTableHead
                            classes={classes}
                            rowCount={rows.length}
                        />
                        <TableBody>
                            {rows.map((row) => {
                                let orderItemId = row.orderItemId;
                                skip = 0;
                                if (checkExist === orderItemId) {
                                    skip = 1;
                                }

                                let orderItem = rows.map((row) => {
                                    if (row.orderItemId === orderItemId && skip === 0) {
                                        checkExist = orderItemId;
                                        return (
                                            <TableRow key={row.cuttingInstructionId}
                                            >
                                                <StyledTableCell
                                                    width="5%"
                                                    align="right"></StyledTableCell>
                                                <StyledTableCell
                                                    width="10%"
                                                    align="right">{getValueOf(row.cuttingInstructionId)}</StyledTableCell>
                                                <StyledTableCell
                                                    width="10%"
                                                    align="right">{getValueOf(row.steelBarNumber)}</StyledTableCell>
                                                <StyledTableCell
                                                    width="15%"
                                                    align="right">{getValueOf(row.steelLength)}</StyledTableCell>
                                                <StyledTableCell
                                                    width="15%"
                                                    align="right">{getValueOf(row.bladeWidth)}</StyledTableCell>
                                                <StyledTableCell
                                                    width="15%"
                                                    align="right">{getValueOf(row.stockProductId)}</StyledTableCell>
                                                <StyledTableCell
                                                    width="15%"
                                                    align="right">{getValueOf(row.stockProductLength)}</StyledTableCell>
                                                <StyledTableCell
                                                    width="15%">{getValueOf(row.assignee)}</StyledTableCell>
                                            </TableRow>
                                        )
                                    }
                                    return null;
                                })
                                if (skip === 1) {
                                    return null;
                                }
                                return (
                                    <>
                                        <TableRow key={row.cuttingInstructionId}
                                            style={{
                                                backgroundColor: "#eef1f4"
                                            }}
                                        >
                                            <StyledTableCell
                                                colSpan={8}
                                                width="5%"
                                                align="left">{getValueOf(row.orderItemId)}</StyledTableCell>
                                        </TableRow>
                                        {orderItem}
                                    </>
                                )
                            }
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
                <br />
                <TablePaging numberOfPages={props.numberOfPages}
                    currentPage={props.currentPage}
                    linkTo={ROUTE_VIEW_CUTTING_INSTRUCTION} />
            </div>
        </React.Fragment>
    );
}

class ViewCuttingInstruction extends Component {
    state = {
        isToastOpen: false,
        messageReceived: false
    }

    mapErrMessToParagraph = (errMessArr) => {
        return errMessArr.map((errMessObj) => {
            return <p>{errMessObj}</p>
        })
    }

    async componentDidMount() {
        const search = window.location.search;
        const params = new URLSearchParams(search);
        const foo = params.get('orderId');
        await this.props.fetchCuttingInstruction(foo);
    }

    render() {
        const account = this.props.loginAccount.account;
        if (account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first</h1>
                    </div>
                </div>
            );
        } else if (account.roleId === ROLE_FACTORY_MANAGER) {
            const cuttingInstructionCheck = this.props.fetchCuttingInstructions.cuttingInstruction;
            // console.log("cutitngInstructionCheck");
            // console.log(cuttingInstructionCheck);
            const cuttingInstruction = this.props.fetchCuttingInstructions;

            return (
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                            <BreadcrumbItem>Order Management</BreadcrumbItem>
                            <BreadcrumbItem active>View Cutting Instruction</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>View Cutting Instruction</h3>
                            <hr />
                        </div>
                    </div>
                    {/* {isLoading ? */}
                    {/* <Loading /> : */}
                    <div className="row">
                        <ViewCuttingInstructionTable
                            cuttingInstruction={(cuttingInstructionCheck === null || cuttingInstructionCheck === []) ? "" : cuttingInstruction}
                            numberOfPages={0}
                            currentPage={0}
                            handleDeleteDisabledAccount={this.handleDeleteDisabledAccount}
                            errMess={cuttingInstruction.errMess}
                        />
                    </div>
                    {/* } */}
                </div>
            )
        } else {
            return (
                <UnauthorizedAccessDisplay />
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        fetchCuttingInstructions: state.fetchCuttingInstruction
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        fetchCuttingInstruction: (orderId) => {
            dispatch(fetchCuttingInstructionByOrderId(orderId));
        }
    });
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewCuttingInstruction);