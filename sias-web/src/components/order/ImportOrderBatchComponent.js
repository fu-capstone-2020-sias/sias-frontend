import React, { Component } from "react";
import {
    Breadcrumb,
    BreadcrumbItem,
    Label,
} from "reactstrap";

import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { ROLE_OFFICE_EMPLOYEE, ROLE_OFFICE_MANAGER, ROUTE_ADD_NEW_ORDER, ROUTE_HOME, ROUTE_ADDRESS_ID_LOOKUP } from "../../shared/constants";
import { Loading } from "../common/LoadingComponent";
import { baseUrl } from "../../shared/baseUrl";
import { clearImportOrderBatch, importOrderBatch } from "../../redux/actions/ActionCreators";
import { confirmAlert } from "react-confirm-alert";
import UnauthorizedAccessDisplay from "../common/UnauthorizedAccessDisplay";

class ImportOrderBatch extends Component {
    state = {
        account: null,
        selectedFile: null,
        isUploading: false
    }

    onChangeHandler = (event) => {
        // console.log(event.target.files[0])
        this.setState({
            selectedFile: event.target.files[0] !== undefined ? event.target.files[0] : null,
        })
    }

    onClickHandler = async () => {
        if (this.state.selectedFile === null) {
            confirmAlert({
                title: 'Information',
                message: `Please choose 1 file to upload.`,
                buttons: [
                    {
                        label: 'Yes',
                    }
                ]
            });
            return;
        }

        this.props.clearImportOrderBatch();
        await this.setState({
            isUploading: true
        })
        const formData = new FormData()
        // console.log("Selected File");
        // console.log(this.state.selectedFile);
        formData.append('csvFile', this.state.selectedFile)
        await this.props.importOrderBatch(formData);

        // Clear selected file
        this.setState({
            selectedFile: null
        })
        document.getElementById("filePicker").value = ""
    }

    mapErrMessToParagraph = (errMessArr) => {
        return errMessArr.map((errMessObj) => {
            return <p style={{ color: "red" }}>{errMessObj}</p>
        })
    }

    async componentDidMount() {

        const account = this.props.loginAccount.account;
        this.setState({
            account: account
        });
    }

    render() {
        if (this.state.account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first.</h1>
                    </div>
                </div>
            );
        } else if (this.state.account.roleId !== ROLE_OFFICE_MANAGER
            && this.state.account.roleId !== ROLE_OFFICE_EMPLOYEE) {
            return (
                <UnauthorizedAccessDisplay />
            );
        } else {
            if (this.props.importOrderBatchs.errMess !== null
                || this.props.importOrderBatchs.successMessage !== null) {
                // console.log("Import Order Batchs");
                // console.log(this.props.importOrderBatchs.errMess);
                if (this.state.isUploading) {
                    this.setState({
                        isUploading: false
                    })
                }
            }

            return (
                <div className="container" style={{ position: "relative" }}>
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem>
                                <Link to={ROUTE_HOME}>Home</Link>
                            </BreadcrumbItem>
                            <BreadcrumbItem>Order Management</BreadcrumbItem>
                            <BreadcrumbItem><Link to={ROUTE_ADD_NEW_ORDER}>Add New Order</Link></BreadcrumbItem>
                            <BreadcrumbItem active>Import Order Batch</BreadcrumbItem>
                        </Breadcrumb>
                    </div>
                    <div className="row">
                        <div className="col-12" style={{ display: "flex", justifyContent: "space-between" }}>
                            <h3>Import Order Batch</h3>
                            <hr />
                        </div>
                        <div className="col-12">
                            <div>
                                <center>
                                    <Label className="text-danger">
                                        {"<!!!>"} Caution: Before uploading, you need to <a
                                            href={`${baseUrl}order/sampleFileForImportOrderByBatch`} target={"_blank"}>
                                            download standard CSV file</a> from server. This is required by company policy.
                                    </Label>
                                    <Label className="text-danger">In case you don't know the address codes (cityProvinceId, districtId, wardCommuneId), you can <Link to={ROUTE_ADDRESS_ID_LOOKUP} target={"_blank"}>lookup here.</Link></Label>
                                </center>
                            </div>
                            <div className="d-flex justify-content-center"
                                style={{
                                    marginBottom: "20px"
                                }}>
                                <div className="col-md-6" style={{ padding: "0" }}>
                                    <div style={{ marginBottom: "10px" }}>
                                        <input id={"filePicker"} type="file" name="file" onChange={this.onChangeHandler} />
                                    </div>
                                    <div>
                                        <button type="button" className="btn" style={{ backgroundColor: "#ffb74d" }}
                                            onClick={this.onClickHandler}>Upload
                                        </button>
                                    </div>
                                </div>

                                <div className="col-md-6" style={{ padding: "0" }}>
                                    <Label style={{ display: "block" }}>
                                        Message(s)
                                    </Label>

                                    {!this.state.isUploading ?
                                        <div
                                            disabled={true}
                                            style={{
                                                height: '250px',
                                                width: '100%',
                                                overflowY: 'scroll',
                                                backgroundColor: "#eef1f4",
                                                padding: "10px"
                                            }}>
                                            {this.props.importOrderBatchs.successMessage !== null ?
                                                <p style={{ color: "#4caf50" }}>
                                                    {this.props.importOrderBatchs.successMessage}
                                                </p> :
                                                (this.props.importOrderBatchs.errMess !== null ?
                                                    this.mapErrMessToParagraph(this.props.importOrderBatchs.errMess) : "")
                                            }
                                        </div> : <Loading />
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        importOrderBatchs: state.importOrderBatch
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        importOrderBatch: (formData) => {
            dispatch(importOrderBatch(formData));
        },
        clearImportOrderBatch: () => {
            dispatch(clearImportOrderBatch())
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ImportOrderBatch);
