import React, { Component } from 'react';
import { Button, Label, Col, Row, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Form, Control, Errors, actions } from 'react-redux-form';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { postNewOrder, fetchCityProvinceIdList, fetchDistrictIdList, fetchWardCommuneIdList, fetchApproverList, fetchAssigneeList } from '../../redux/actions/ActionCreators';
import { required, validCompanyName, validCustomerName, validEmail, validPhoneNumber } from '../../shared/fieldValidation';
import {
    ROUTE_HOME,
    ROUTE_ORDER_DETAILS,
    ROUTE_ADD_NEW_ORDER,
    VALIDATION_MSG_INVALID_NAME,
    VALIDATION_MSG_INVALID_COMPANY_NAME,
    VALIDATION_MSG_REQUIRED,
    VALIDATION_MSG_INVALID_EMAIL,
    VALIDATION_MSG_INVALID_PHONE_NUMBER,
    SUCCESS_MESSAGE_ADD_NEW_ORDER,
    ROLE_OFFICE_EMPLOYEE,
    ROLE_OFFICE_MANAGER
} from '../../shared/constants';
import UnauthorizedAccessDisplay from '../common/UnauthorizedAccessDisplay';
import FormLabel from '../common/FormLabel';
import { Loading } from '../common/LoadingComponent';

class AddNewOrderForm extends Component {
    constructor(props) {
        super(props);
        const addresses = this.props.addresses;
        const orderApproverAndAssigneeInfos = this.props.orderApproverAndAssigneeInfos;

        if (addresses.cityProvinceList.length === 0 && !addresses.isLoaded) {
            this.props.fetchCityProvinceIdList();
            this.props.fetchDistrictIdList();
            this.props.fetchWardCommuneIdList();
        }

        if (orderApproverAndAssigneeInfos.approverList.length === 0 && !orderApproverAndAssigneeInfos.isApproverListLoaded) {
            this.props.fetchApproverList();
        }

        if (orderApproverAndAssigneeInfos.assigneeList.length === 0 && !orderApproverAndAssigneeInfos.isAssigneeListLoaded) {
            this.props.fetchAssigneeList();
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            selectedCityProvinceId: "",
            selectedDistrictId: "",
            selectedWardCommuneId: "",
        };
    }

    handleSubmit = async (values) => {
        await this.props.postNewOrder(values);
        await setTimeout(() => {
            this.props.resetNewOrderForm()
        }, 3000);
    }

    resetForm = () => {
        this.props.resetNewOrderForm();
    }

    mapCityProvinceToOption = () => {
        return this.props.addresses.cityProvinceList.map((cityProvinceObj) => (
            <option
                key={cityProvinceObj.cityProvinceId}
                value={cityProvinceObj.cityProvinceId}
            >
                {cityProvinceObj.name}
            </option>
        ));
    };

    mapDistrictToOption = () => {
        const districtList = this.props.addresses.districtList;
        let provinceId = "";

        provinceId = this.state.selectedCityProvinceId;

        if (provinceId != null && provinceId !== '') {
            return districtList
                .filter(dist => dist.cityProvinceId === parseInt(provinceId))
                .map((districtObj) => (
                    <option
                        key={districtObj.districtId}
                        value={districtObj.districtId}>
                        {districtObj.name}
                    </option>
                ));
        }
    }

    mapWardCommuneToOption = () => {
        const wardCommuneList = this.props.addresses.wardCommuneList;
        let districtId = "";

        districtId = this.state.selectedDistrictId;

        if (districtId != null && districtId !== '') {
            return wardCommuneList
                .filter(ward => ward.districtId === parseInt(districtId))
                .map((wardCommuneObj) => (
                    <option
                        key={wardCommuneObj.wardCommuneId}
                        value={wardCommuneObj.wardCommuneId}
                    >
                        {wardCommuneObj.name}
                    </option>
                ));
        }
    }

    mapApproverListToOption = (list) => {
        return list.map((item) => (
            <option
                key={item.userName}
                value={item.userName}
            >
                {item.userName}
            </option>
        ));
    };

    mapAssigneeListToOption = (list) => {
        return list.map((item) => (
            <option
                key={item.userName}
                value={item.userName}
            >
                {item.userName}
            </option>
        ));
    };

    handleCityProvinceChange = (event) => {
        this.setState({
            selectedCityProvinceId: event.target.value,
            selectedDistrictId: '',
            selectedWardCommuneId: '',
        });
    }

    handleDistrictChange = (event) => {
        this.setState({
            selectedDistrictId: event.target.value,
            selectedWardCommuneId: '',
        });
    }

    handleWardCommuneChange = (event) => {
        this.setState({
            selectedWardCommuneId: event.target.value
        });
    }

    isSaveButtonEnabled() {
        return (
            this.state.selectedCityProvinceId !== ''
            && this.state.selectedDistrictId !== ''
            && this.state.selectedWardCommuneId !== ''
        );
    }

    render() {
        const account = this.props.loginAccount.account;
        if (account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first</h1>
                    </div>
                </div>
            );
        } else if (roleAccessible(account.roleId)) {
            const addedSuccessStatus = this.props.addedSuccessStatus;
            const errMess = this.props.errMess;
            const addedOrderId = this.props.addedOrderId;
            const isAddOrderLoading = this.props.isAddOrderLoading;

            const redirectLink = addedOrderId != null ? ROUTE_ORDER_DETAILS + '/' + addedOrderId : '#';
            const successMessage = addedSuccessStatus ?
                <h5 className="text-success">{SUCCESS_MESSAGE_ADD_NEW_ORDER} Click <Link to={redirectLink}>here</Link> to view order details or add items to this order.</h5>
                : null;
            return (
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                            <BreadcrumbItem>Order Management</BreadcrumbItem>
                            <BreadcrumbItem><Link to={ROUTE_ADD_NEW_ORDER}>Add New Order</Link></BreadcrumbItem>
                            <BreadcrumbItem active>Add New Order Form</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>Add New Order Form</h3>
                            <hr />
                        </div>
                    </div>
                    {isAddOrderLoading ? <Loading /> :
                        <div className="row row-content">
                            <div className="col-12">
                                {successMessage}
                                <h5 className="text-danger">{errMess != null ? errMess : ''}</h5>
                            </div>
                            <div className="col-12 col-md-9 col-xl-8">
                                <Form model="addOrder"
                                    onSubmit={(values) => this.handleSubmit(values)}
                                >
                                    <Row>
                                        <Label><strong>Customer's Information</strong></Label>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="customerName" md={3}>
                                            <FormLabel text="Name" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text model=".customerName" id="customerName" name="customerName"
                                                className="form-control" validators={{ required, validCustomerName }} />
                                            <Errors className="text-danger" model=".customerName" show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED,
                                                    validCustomerName: VALIDATION_MSG_INVALID_NAME
                                                }} />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="customerCompanyName" md={3}>
                                            <FormLabel text="Company name" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text model=".customerCompanyName" id="customerCompanyName" name="customerCompanyName"
                                                className="form-control" validators={{ required, validCompanyName }} />
                                            <Errors className="text-danger" model=".customerCompanyName" show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED,
                                                    validCompanyName: VALIDATION_MSG_INVALID_COMPANY_NAME
                                                }} />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="customerEmail" md={3}>
                                            <FormLabel text="Email" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text model=".customerEmail" id="customerEmail" name="customerEmail"
                                                className="form-control" validators={{ required, validEmail }} />
                                            <Errors className="text-danger" model=".customerEmail" show="touched"
                                                messages={{
                                                    // required: VALIDATION_MSG_REQUIRED,
                                                    validEmail: VALIDATION_MSG_INVALID_EMAIL
                                                }} />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="customerPhoneNumber" md={3}>
                                            <FormLabel text="Phone number" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text model=".customerPhoneNumber" id="customerPhoneNumber" name="customerPhoneNumber"
                                                className="form-control" validators={{ required, validPhoneNumber }} />
                                            <Errors className="text-danger" model=".customerPhoneNumber" show="touched"
                                                messages={{
                                                    // required: VALIDATION_MSG_REQUIRED,
                                                    validPhoneNumber: VALIDATION_MSG_INVALID_PHONE_NUMBER
                                                }} />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="customerCityProvinceId" md={3}>
                                            <FormLabel text="City/Province" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select
                                                model=".customerCityProvinceId"
                                                id="customerCityProvinceId"
                                                name="customerCityProvinceId"
                                                className="form-control"
                                                onChange={this.handleCityProvinceChange}
                                            >
                                                <option key='' value=''>
                                                    -City/Province-
                                        </option>
                                                {this.mapCityProvinceToOption()}
                                            </Control.select>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="customerDistrictId" md={3}>
                                            <FormLabel text="District" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select
                                                model=".customerDistrictId"
                                                id="customerDistrictId"
                                                name="customerDistrictId"
                                                className="form-control"
                                                onChange={this.handleDistrictChange}
                                            >
                                                <option key='' value=''>
                                                    -District-
                                        </option>
                                                {this.mapDistrictToOption()}
                                            </Control.select>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="customerWardCommuneId" md={3}>
                                            <FormLabel text="Ward/Commune" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select
                                                model=".customerWardCommuneId"
                                                id="customerWardCommuneId"
                                                name="customerWardCommuneId"
                                                className="form-control"
                                                onChange={this.handleWardCommuneChange}
                                            >
                                                <option key='' value=''>
                                                    -Ward/Commune-
                                        </option>
                                                {this.mapWardCommuneToOption()}
                                            </Control.select>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="customerAddress" md={3}>
                                            <FormLabel text="Detailed address" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text model=".customerAddress" id="customerAddress" name="customerAddress"
                                                className="form-control" validators={{ required }} />
                                            <Errors className="text-danger" model=".customerAddress" show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }} />
                                        </Col>
                                    </Row>

                                    <hr />
                                    <Row>
                                        <Label><strong>Order's Approver and Assignee</strong></Label>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="approver" md={3}>
                                            <FormLabel text="Approver" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select model=".approver" name="approver" id="approver"
                                                className="form-control" validators={{ required }}>
                                                <option key='' value=''>
                                                    -Approver-
                                        </option>
                                                {this.mapApproverListToOption(this.props.orderApproverAndAssigneeInfos.approverList)}
                                            </Control.select>
                                            <Errors className="text-danger" model=".approver" show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }} />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="assignee" md={3}>
                                            <FormLabel text="Assignee" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select model=".assignee" name="assignee" id="assignee"
                                                className="form-control" validators={{ required }}>
                                                <option key='' value=''>
                                                    -Assignee-
                                        </option>
                                                {this.mapAssigneeListToOption(this.props.orderApproverAndAssigneeInfos.assigneeList)}
                                            </Control.select>
                                            <Errors className="text-danger" model=".assignee" show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }} />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Col>
                                            <Button disabled={!this.isSaveButtonEnabled()}
                                                color="warning"
                                                type="submit"
                                                className="submitButton"
                                            >
                                                Add Order
                                        </Button>

                                    &nbsp;
                                    <Button color="light"
                                                className="backButton"
                                                onClick={this.resetForm}
                                            >
                                                Reset
                                    </Button>
                                        </Col>
                                    </Row>
                                </Form>
                            </div>
                        </div>
                    }
                </div>
            );
        } else {
            return (
                <UnauthorizedAccessDisplay />
            );
        }
    }
}

const roleAccessible = (roleId) => {
    return roleId === ROLE_OFFICE_MANAGER || roleId === ROLE_OFFICE_EMPLOYEE;
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        isAddOrderLoading: state.orderDetail.isAddOrderLoading,
        addedSuccessStatus: state.orderDetail.addedSuccessStatus,
        errMess: state.orderDetail.errMess,
        addedOrderId: state.orderDetail.addedOrderId,
        addresses: state.addresses,
        orderApproverAndAssigneeInfos: state.orderApproverAndAssigneeInfos
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        fetchCityProvinceIdList: () => { dispatch(fetchCityProvinceIdList()) },
        fetchDistrictIdList: () => { dispatch(fetchDistrictIdList()) },
        fetchWardCommuneIdList: () => { dispatch(fetchWardCommuneIdList()) },
        fetchApproverList: () => { dispatch(fetchApproverList()) },
        fetchAssigneeList: () => { dispatch(fetchAssigneeList()) },
        postNewOrder: (values) => { dispatch(postNewOrder(values)) },
        resetNewOrderForm: () => { dispatch(actions.reset('addOrder')) }
    });
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddNewOrderForm));