import React, {Component} from 'react';
import {Breadcrumb, BreadcrumbItem, Button} from 'reactstrap';
import {Link, withRouter} from 'react-router-dom';
import OrderListSearchForm from './ViewOrderListSearchFormComponent';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import {connect} from 'react-redux';
import {
    ROLE_OFFICE_MANAGER,
    ROLE_OFFICE_EMPLOYEE,
    ROUTE_ORDER_DETAILS,
    ROUTE_VIEW_ORDER_LIST,
    ROLE_FACTORY_MANAGER,
    ORDER_STATUS_REJECTED,
    SUCCESS_MESSAGE_DELETE_GENERAL,
    ROUTE_VIEW_CUTTING_INSTRUCTION,
    ROUTE_HOME,
    ORDER_STATUS_PROCESSING, ROUTE_VIEW_USER_INFO, ERROR_PAGE_NOT_FOUND
} from '../../shared/constants';
import {getValueOf, dateDisplayVietnamese, formatterVND} from '../../shared/utils';
import {StyledTableCell} from '../common/StyledTableCell';
import TablePaging from '../common/TablePaging';
import OrderStatusDisplay from '../common/OrderStatusDisplay';
import UnauthorizedAccessDisplay from '../common/UnauthorizedAccessDisplay';
import {
    fetchOrder,
    exportOrderBatch,
    deleteOrders,
} from '../../redux/actions/ActionCreators';
import {Loading} from '../common/LoadingComponent';
import {confirmAlert} from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import ErrorDisplay from '../common/ErrorDisplay';

const useStyles = makeStyles({
    table: {
        minWidth: 600,
    }
});

const headCells = [
    {id: 'id', numeric: true, disablePadding: false, label: 'ID'},
    {id: 'customerName', numeric: false, disablePadding: false, label: 'Customer'},
    {id: 'creator', numeric: false, disablePadding: false, label: 'Creator'},
    {id: 'createdDate', numeric: false, disablePadding: false, label: 'Created Date'},
    {id: 'approver', numeric: false, disablePadding: false, label: 'Approver'},
    {id: 'assignee', numeric: false, disablePadding: false, label: 'Assignee'},
    {id: 'totalPrice', numeric: false, disablePadding: false, label: 'Total Price'},
    {id: 'status', numeric: false, disablePadding: false, label: 'Status'}
];

function OrderListTableHead(props) {
    const {order, orderBy} = props;
    return (
        <TableHead>
            <TableRow>
                <StyledTableCell padding="checkbox">
                    {/* <Checkbox
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={rowCount > 0 && numSelected === rowCount}
                        onChange={onSelectAllClick}
                        inputProps={{ 'aria-label': 'select all' }}
                    /> */}
                </StyledTableCell>
                {headCells.map((headCell) => (
                    <StyledTableCell
                        key={headCell.id}
                        align={headCell.numeric ? 'right' : 'left'}
                        padding={headCell.disablePadding ? 'none' : 'default'}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        {headCell.label}
                    </StyledTableCell>
                ))}
                <StyledTableCell></StyledTableCell>
                <StyledTableCell></StyledTableCell>
            </TableRow>
        </TableHead>
    );
}

OrderListTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
    // onSelectAllClick: PropTypes.func.isRequired,
    rowCount: PropTypes.number.isRequired
};

function OrderListTable(props) {
    const classes = useStyles();
    const [selected, setSelected] = React.useState([]);
    const account = props.account;
    let rows = props.orderList;

    const handleClick = (event, row) => {
        const id = row.orderId;
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }
        setSelected(newSelected);
    };

    const isSelected = (orderId) => selected.indexOf(orderId) !== -1;

    const handleExportOrderBatch = function () {
        if (selected.length > 0) {
            confirmAlert({
                title: 'Confirm to submit',
                message: `Are you sure to export these orders (ID: ${selected.toString()})? This will refresh your browser to take effects.`,
                buttons: [
                    {
                        label: 'Yes',
                        onClick: async () => {
                            // alert("Export Order Batch");
                            await props.exportOrderBatch(selected.toString());
                            setSelected([]);
                        }
                    },
                    {
                        label: 'No'
                    }
                ]
            })
        } else {
            confirmAlert({
                title: 'Information',
                message: `Please select at least 1 order.`,
                buttons: [
                    {
                        label: 'OK',
                    }
                ]
            })
        }
    }

    const handleDelete = (orderId) => {
        confirmAlert({
            title: 'Confirm',
            message: `Are you sure to delete this order (order ID: ${orderId})? This will refresh your browser to take effects.`,
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => {
                        props.deleteOrders(orderId);
                    }
                },
                {
                    label: 'No'
                }
            ]
        });
    }

    const getLastTableColumn = (roleId, orderId, orderStatusId) => {
        if (roleId == null || orderId == null || orderStatusId == null) {
            return <StyledTableCell width="5%"></StyledTableCell>;
        }

        if (roleId === ROLE_FACTORY_MANAGER && orderStatusId === ORDER_STATUS_PROCESSING) {
            return <StyledTableCell width="10%"><Link
                to={`${ROUTE_VIEW_CUTTING_INSTRUCTION}?orderId=${orderId}`}>Cutting
                Instructions</Link></StyledTableCell>;
        } else if (roleId === ROLE_OFFICE_MANAGER && orderStatusId === ORDER_STATUS_REJECTED) {
            return (
                <StyledTableCell width="5%">
                    <Link to='#' onClick={() => handleDelete(orderId)}>Delete</Link>
                </StyledTableCell>
            );
        } else {
            return <StyledTableCell width="5%"></StyledTableCell>;
        }
    }

    return (
        <React.Fragment>
            <div className="col-12">
                <h5>Number of entries: {props.totalEntries}</h5>
            </div>
            <div className="col-12">
                <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="customized table">
                        <OrderListTableHead
                            classes={classes}
                            numSelected={selected.length}
                            // onSelectAllClick={handleSelectAllClick}
                            rowCount={rows.length}
                        />
                        <TableBody>
                            {rows.map((row) => {
                                    const isItemSelected = isSelected(row.orderId);
                                    return (
                                        <TableRow key={row.orderId}
                                                  onClick={(event) => handleClick(event, row)}
                                                  role="checkbox"
                                                  aria-checked={isItemSelected}
                                                  selected={isItemSelected}
                                            // style={index % 2 ? {background: "#f2f2f2"} : {background: "white"}}
                                        >
                                            {(account.roleId === ROLE_OFFICE_EMPLOYEE
                                                || account.roleId === ROLE_OFFICE_MANAGER) ?
                                                <StyledTableCell padding="checkbox">
                                                    <Checkbox
                                                        checked={isItemSelected}
                                                    />
                                                </StyledTableCell> :
                                                <StyledTableCell></StyledTableCell>
                                            }
                                            <StyledTableCell width="5%"
                                                             align="right">{getValueOf(row.orderId)}</StyledTableCell>
                                            <StyledTableCell width="20%">{getValueOf(row.customerName)}</StyledTableCell>
                                            <StyledTableCell width="10%">
                                                {getValueOf(row.creator) === account.userName ?
                                                    <span>You</span>
                                                    : <Link
                                                        to={`${ROUTE_VIEW_USER_INFO}?userName=${getValueOf(row.creator)}`}>
                                                        {getValueOf(row.creator)}
                                                    </Link>}

                                            </StyledTableCell>
                                            <StyledTableCell
                                                width="10%">{getValueOf(dateDisplayVietnamese(row.createdDate))}</StyledTableCell>
                                            <StyledTableCell width="10%">
                                                {getValueOf(row.approver) === account.userName ?
                                                    <span>You</span>
                                                    : <Link
                                                        to={`${ROUTE_VIEW_USER_INFO}?userName=${getValueOf(row.approver)}`}>
                                                        {getValueOf(row.approver)}
                                                    </Link>}
                                            </StyledTableCell>
                                            <StyledTableCell width="10%">
                                                {getValueOf(row.assignee) === account.userName ?
                                                    <span>You</span>
                                                    : <Link
                                                        to={`${ROUTE_VIEW_USER_INFO}?userName=${getValueOf(row.assignee)}`}>
                                                        {getValueOf(row.assignee)}
                                                    </Link>}
                                            </StyledTableCell>
                                            <StyledTableCell
                                                width="10%">{getValueOf(formatterVND.format(row.totalPrice))}</StyledTableCell>
                                            <StyledTableCell width="10%"><OrderStatusDisplay
                                                status={getValueOf(row.orderStatus)}/></StyledTableCell>
                                            <StyledTableCell width="5%">
                                                <Link to={`${ROUTE_ORDER_DETAILS}/${row.orderId}`}>Details</Link>
                                            </StyledTableCell>
                                            {getLastTableColumn(account.roleId, row.orderId, row.orderStatusId)}
                                        </TableRow>
                                    )
                                }
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
                <br/>
                <TablePaging numberOfPages={props.numberOfPages}
                             currentPage={props.currentPage}
                             linkTo={ROUTE_VIEW_ORDER_LIST}/>
            </div>
            {(props.totalEntries > 0) && (account.roleId === ROLE_OFFICE_EMPLOYEE
                || account.roleId === ROLE_OFFICE_MANAGER) &&
            <div className="col-12 col-sm-6 col-lg-4">

                {/*<Button color="warning" onClick={handleMarkDone}>Mark selected order(s) as Completed</Button>*/}
                <Button style={{marginBottom: "40px"}} color="warning" onClick={handleExportOrderBatch}>Export Order(s)
                    to CSV</Button>
            </div>
            }
        </React.Fragment>
    );
}

class ViewOrderList extends Component {
    componentDidMount() {
        const account = this.props.loginAccount.account;
        if (null != account && roleAccessible(account.roleId)) {
            const orderList = this.props.orders.orders;
            const currentPageNumber = this.props.orders.currentPageNumber;
            const numberOfPages = this.props.orders.numberOfPages;
            let pageNumber = this.props.pageNumber;
            const searchData = this.props.orders.searchData;
            const errMess = this.props.orders.errMess;
            if (errMess != null) {
                return;
            } else if (null == orderList || (pageNumber !== currentPageNumber && pageNumber <= numberOfPages)) {
                if (searchData != null) {
                    this.props.fetchOrderSearch(pageNumber, searchData);
                } else {
                    this.props.fetchOrder(pageNumber);
                }

            }
        }
    }

    render() {
        const account = this.props.loginAccount.account;
        if (account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first</h1>
                    </div>
                </div>
            );
        } else if (roleAccessible(account.roleId)) {
            const orderList = this.props.orders.orders;
            const numberOfPages = this.props.orders.numberOfPages;
            const currentPageNumber = this.props.orders.currentPageNumber;
            const totalEntries = this.props.orders.totalEntries;
            const deleteSuccessStatus = this.props.orders.deleteSuccessStatus;
            const errMess = this.props.orders.errMess;
            const isLoading = this.props.orders.isLoading;

            if (currentPageNumber > numberOfPages && numberOfPages > 0) {
                return (
                    <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
                )
            }

            return (
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                            <BreadcrumbItem>Order Management</BreadcrumbItem>
                            <BreadcrumbItem active>View Order List</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>View Order List</h3>
                            <hr/>
                        </div>
                    </div>
                    {isLoading ?
                        <Loading/> :
                        <div className="row">
                            <div className="col-12">
                                <h4>Search</h4>
                                <div className="col-12 col-md-10 col-lg-9 col-xl-8 mb-3">
                                    <OrderListSearchForm fetchOrderSearch={this.props.fetchOrderSearch}/>
                                </div>
                                <h5 className="text-success">{deleteSuccessStatus ? SUCCESS_MESSAGE_DELETE_GENERAL : ''}</h5>
                                <h5 className="text-danger">{errMess != null ? errMess : ''}</h5>

                                <OrderListTable orderList={(orderList == null) ? [] : orderList}
                                                numberOfPages={numberOfPages}
                                                currentPage={currentPageNumber}
                                                totalEntries={totalEntries}
                                                account={account}
                                                postMarkDoneOrder={this.props.postMarkDoneOrder}
                                                exportOrderBatch={this.props.exportOrderBatch}
                                                deleteOrders={this.props.deleteOrders}
                                />
                            </div>
                        </div>
                    }
                </div>
            )
        } else {
            return (
                <UnauthorizedAccessDisplay/>
            );
        }
    }
}

const roleAccessible = (roleId) => {
    return roleId === ROLE_OFFICE_MANAGER || roleId === ROLE_OFFICE_EMPLOYEE || roleId === ROLE_FACTORY_MANAGER;
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        exportOrderBatchs: state.exportOrderBatch,
        orders: state.orders,
        addresses: state.addresses
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        fetchOrder: (pageNumber) => {
            dispatch(fetchOrder(pageNumber))
        },
        fetchOrderSearch: (pageNumber, searchData) => {
            dispatch(fetchOrder(pageNumber, 'POST', searchData))
        },
        exportOrderBatch: (arrOfOrderIds) => {
            dispatch(exportOrderBatch(arrOfOrderIds));
        },
        deleteOrders: (orderId) => {
            dispatch(deleteOrders(orderId));
        }
    });
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ViewOrderList));