import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, Col } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import { ORDER_STATUS_PENDING, ORDER_STATUS_APPROVED, ORDER_STATUS_PROCESSING, ORDER_STATUS_COMPLETED, ORDER_STATUS_REJECTED } from '../../shared/constants';
import { connect } from 'react-redux';
import { InitialSearchOrderList } from '../../redux/reducers/forms';

class OrderListSearchForm extends Component {
    constructor(props) {
        super(props);
        this.handleSearch = this.handleSearch.bind(this);
        this.toggleSearchByDate = this.toggleSearchByDate.bind(this);

        const searchData = (this.props.orders.searchData != null) ? this.props.orders.searchData : InitialSearchOrderList;
        // console.log(searchData.searchByDate);
        this.state = {
            searchByDate: (searchData.searchByDate === 1)
        }
    }

    toggleSearchByDate() {
        this.setState({
            searchByDate: !this.state.searchByDate
        });
    }

    handleSearch(event) {
        let searchData = {
            orderId: this.orderId.value,
            customerName: this.customerName.value,
            customerPhoneNumber: this.customerPhoneNumber.value,
            customerEmail: this.customerEmail.value,
            customerCompanyName: this.customerCompanyName.value,
            searchByDate: this.searchByDate.checked ? 1 : 0
        }
        if (this.searchByDate.checked) {
            searchData.fromDate = this.fromDate.value;
            searchData.toDate = this.toDate.value;
        }
        if (this.status.value !== 'All') {
            searchData.orderStatusId = this.status.value;
        }

        this.props.fetchOrderSearch(1, searchData);
        event.preventDefault();
    }
    render() {
        const searchData = (this.props.orders.searchData != null) ? this.props.orders.searchData : InitialSearchOrderList;
        return (
            <Form
                innerRef={form => this.form = form}
                onSubmit={this.handleSearch}
            >
                <FormGroup row>
                    <Label sm={3} htmlFor="orderId">Order ID</Label>
                    <Col sm={9}>
                        <Input type="text" id="orderId" name="orderId"
                            pattern='[0-9]+' title="Order ID should only contain digits 0-9 only"
                            placeholder='Ex: 3, 12, 0242,...'
                            defaultValue={searchData.orderId}
                            innerRef={(input) => this.orderId = input} />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={3} htmlFor="customerName">Customer's name</Label>
                    <Col sm={9}>
                        <Input type="text" id="customerName" name="customerName"
                            defaultValue={searchData.customerName}
                            innerRef={(input) => this.customerName = input} />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={3} htmlFor="customerPhoneNumber">Customer's phone number</Label>
                    <Col sm={9}>
                        <Input type="text" id="customerPhoneNumber" name="customerPhoneNumber"
                            pattern='[0-9]+' title="Phone number should only contain digits 0-9 only"
                            placeholder='Ex: 0343, 123, 0942,...'
                            defaultValue={searchData.customerPhoneNumber}
                            innerRef={(input) => this.customerPhoneNumber = input} />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={3} htmlFor="customerEmail">Customer's email</Label>
                    <Col sm={9}>
                        <Input type="text" id="customerEmail" name="customerEmail"
                            defaultValue={searchData.customerEmail}
                            innerRef={(input) => this.customerEmail = input} />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={3} htmlFor="customerCompanyName">Customer's company name</Label>
                    <Col sm={9}>
                        <Input type="text" id="customerCompanyName" name="customerCompanyName"
                            defaultValue={searchData.customerCompanyName}
                            innerRef={(input) => this.customerCompanyName = input} />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="status" sm={3}>Status</Label>
                    <Col sm={6} lg={4}>
                        <Input type="select" name="status" id="status"
                            defaultValue={(searchData.orderStatusId != null) ? searchData.orderStatusId : 'All'}
                            innerRef={(input) => this.status = input}>
                            <option value="All">All</option>
                            <option value={ORDER_STATUS_PENDING}>Pending</option>
                            <option value={ORDER_STATUS_APPROVED}>Approved</option>
                            <option value={ORDER_STATUS_PROCESSING}>Processing</option>
                            <option value={ORDER_STATUS_COMPLETED}>Completed</option>
                            <option value={ORDER_STATUS_REJECTED}>Rejected</option>
                        </Input>
                    </Col>
                </FormGroup>
                <FormGroup check>
                    <Label check>
                        <Input type="checkbox" name="searchByDate" id="searchByDate"
                            defaultChecked={this.state.searchByDate}
                            innerRef={(input) => this.searchByDate = input}
                            onClick={this.toggleSearchByDate} />
                        <b>Search by created date</b>
                    </Label>
                </FormGroup>

                {this.state.searchByDate &&
                    <FormGroup row>
                        <Label className="offset-sm-3" sm={2} htmlFor="fromDate">From:</Label>
                        <Col sm={7}>
                            <Input type="date" id="fromDate" name="fromDate"
                                required={true}
                                defaultValue={searchData.fromDate}
                                innerRef={(input) => this.fromDate = input}
                            />
                        </Col>
                        <Label className="offset-sm-3" sm={2} htmlFor="toDate">To:</Label>
                        <Col sm={7}>
                            <Input type="date" id="toDate" name="toDate"
                                required={true}
                                defaultValue={searchData.toDate}
                                innerRef={(input) => this.toDate = input}
                            />
                        </Col>
                    </FormGroup>
                }

                <br />

                <FormGroup row>
                    <Col xs={4} sm={2}>
                        <Button
                            type="submit"
                            color="warning">Search</Button>

                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        orders: state.orders
    };
}


export default withRouter(connect(mapStateToProps)(OrderListSearchForm));