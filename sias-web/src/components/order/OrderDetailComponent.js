import React, { useState } from 'react';
import { Breadcrumb, BreadcrumbItem, Button, Toast, ToastBody, ToastHeader } from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
    createInstruction,
    fetchCuttingInstructionByOrderId,
    fetchOrderDetail,
    fetchOrderItems,
    postMarkDoneOrder,
    putModifyOrderStatus
} from '../../redux/actions/ActionCreators';
import {
    ROLE_OFFICE_MANAGER,
    ROLE_OFFICE_EMPLOYEE,
    ORDER_STATUS_PENDING,
    ROLE_FACTORY_MANAGER,
    ORDER_STATUS_PROCESSING,
    ORDER_STATUS_APPROVED,
    SUCCESS_MESSAGE_SUBMIT_GENERAL,
    ROUTE_HOME,
    ROUTE_VIEW_CUTTING_INSTRUCTION,
    ERROR_PAGE_NOT_FOUND
} from '../../shared/constants';
import { Loading } from '../common/LoadingComponent';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import ModifyOrderDetail from './ModifyOrderDetailComponent';
import ModifyOrderItems from './ModifyOrderItemsComponent';
import OrderDetailCard from './OrderDetailCardComponent';
import OrderItemsTable from './OrderItemsTableComponent';
import { useEffect } from 'react';
import UnauthorizedAccessDisplay from '../common/UnauthorizedAccessDisplay';
import ErrorDisplay from '../common/ErrorDisplay';

function OrderDetail(props) {
    const VIEWING = 0;
    const EDITING_INFO = 1;
    const EDITING_ITEMS = 2;
    const [editing, setEditing] = useState(VIEWING);
    const [isToastOpen, setIsToastOpen] = useState(false);
    const [orderState, setOrderState] = useState(null);
    const [isCreateBtnClicked, setIsCreateBtnClicked] = useState(false);

    const account = props.loginAccount.account;

    const handleSubmit = async () => {
        if (orderState !== null) {
            confirmAlert({
                title: 'Confirm',
                message: `Are you sure to create instruction for
                                                    this order (Order ID: ${props.orderId})?
                                                    This will refresh your browser to take effects.`,
                buttons: [
                    {
                        label: 'Yes',
                        onClick: async () => {
                            alert("Creating Instruction");
                            await props.createInstruction(props.orderId);
                            setIsCreateBtnClicked(true);
                        }
                    },
                    {
                        label: 'No'
                    }
                ]
            });
        }
    }

    const mapErrMessToParagraph = (errMessArr) => {
        return errMessArr.map((errMessObj) => {
            return <p>{errMessObj}</p>
        })
    }

    const backToView = () => {
        setEditing(VIEWING);
    }

    const handleMarkDone = () => {
        confirmAlert({
            title: 'Confirm',
            message: `Are you sure to mark this order as Completed? This will refresh your browser to take effects.`,
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => {
                        props.postMarkDoneOrder(props.orderId);
                    }
                },
                {
                    label: 'No'
                }
            ]
        });
    }

    const handleMarkProcessing = () => {
        confirmAlert({
            title: 'Confirm',
            message: `Are you sure to mark this order as Processing? This will refresh your browser to take effects.`,
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => {
                        props.putMarkProcessingOrder(props.orderId);
                    }
                },
                {
                    label: 'No'
                }
            ]
        });
    }

    useEffect(() => {
        const orderId = props.orderId;
        const currentOrderId = props.currentlyViewedOrder.currentOrderId;
        const errMess = props.orderDetail.errMess;

        const fetchOrderData = async () => {
            await props.fetchOrderDetail(orderId);
            await props.fetchOrderItems(orderId);
        }

        const fetchCuttingInstruction = async () => {
            await props.fetchCuttingInstructionByOrderId(orderId);
        }

        if (errMess == null && currentOrderId !== orderId) {
            fetchOrderData();
        }

        if (orderId !== null && props.loginAccount.account.roleId === ROLE_FACTORY_MANAGER) {
            fetchCuttingInstruction();
        }
    }, []);

    if (account == null) {
        return (
            <div className="container">
                <div className="row row-content">
                    <h1>You must login first</h1>
                </div>
            </div>
        );
    } else if (roleAccessible(account.roleId)) {
        const errMess = props.orderDetail.errMess;
        const orderDetailErrMess = props.orderDetail.orderDetailErrMess;
        const order = props.orderDetail.order;
        const items = props.orderDetail.items;
        const successStatus = props.orderDetail.successStatus;
        // const isLoading = props.orderDetail.isLoading;

        if (order !== null && orderState === null) {
            // console.log("fetchCuttingInstruction")
            setOrderState(order);
        }

        let cuttingInstruction = props.fetchCuttingInstructions;

        const toggleCreateCuttingInstructionBtn = (
            cuttingInstruction.cuttingInstruction === null
            && order !== null && order !== undefined
            && order.orderStatusId === ORDER_STATUS_PROCESSING
            && items !== null && items !== undefined
            && items.length > 0
        )

        const breadCrumb = (
            <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                    <BreadcrumbItem>Order Management</BreadcrumbItem>
                    <BreadcrumbItem><Link to="/vieworderlist">View Order List</Link></BreadcrumbItem>
                    <BreadcrumbItem active>Order Details</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>Order Details</h3>
                    <hr />
                </div>
            </div>
        );

        if (order == null) {
            if (orderDetailErrMess == null) {
                return (
                    <div className="container">
                        {breadCrumb}
                        <div className="row row-content">
                            <Loading />
                        </div>
                    </div>
                );
            } else {
                return <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
            }
        }

        const content = (value) => {
            switch (value) {
                case EDITING_INFO:
                    return (
                        <div className="col-12 col-md-9 col-xl-8">
                            <ModifyOrderDetail account={account} order={order}
                                isStatusEditable={(items != null && items.length > 0)}
                                backToView={backToView} />
                        </div>
                    );
                case EDITING_ITEMS:
                    return (
                        <div className="col-12 col-xl-10">
                            <ModifyOrderItems account={account} orderId={order.orderId} items={items}
                                backToView={backToView} />
                        </div>
                    );
                default:
                    return (
                        <React.Fragment>
                            <div className="col-12 col-md-9 col-xl-4">
                                <h4>Order Information
                                    {isOrderEditable(order, account) &&
                                        <Button color="link" onClick={() => setEditing(EDITING_INFO)}><i
                                            className="fa fa-edit" /></Button>
                                    }
                                </h4>
                                <OrderDetailCard order={order} account={account} />
                            </div>
                            <div className="col-12 col-xl-8">
                                <div style={{ display: "flex", justifyContent: "space-between", marginBottom: "10px" }}>
                                    <h4>
                                        Order Items
                                        {isOrderEditable(order, account) &&
                                            <Button color="link" onClick={() => setEditing(EDITING_ITEMS)}><i
                                                className="fa fa-edit" /></Button>
                                        }
                                    </h4>
                                </div>

                                <Toast
                                    isOpen={isToastOpen}
                                    style={{
                                        // position: "absolute",
                                        // bottom: 20,
                                        // left: "-11%",
                                        boxShadow: "0 0.05rem 0.75rem #303841",
                                        position: "fixed",
                                        bottom: "10px",
                                        right: "10px",
                                        backgroundColor: "white",
                                        zIndex: "1"
                                    }}
                                >
                                    <ToastHeader
                                        toggle={() => {
                                            setIsToastOpen(!isToastOpen);
                                        }}
                                    >
                                        Information
                                    </ToastHeader>
                                    <ToastBody>
                                        {props.createCuttingInstructions.successStatus
                                            ? "Cutting instruction for this order is being processed! Please wait!"
                                            :
                                            props.createCuttingInstructions.errMess !== null ?
                                                mapErrMessToParagraph(props.createCuttingInstructions.errMess) : ""}
                                    </ToastBody>
                                </Toast>

                                <OrderItemsTable items={items} />
                                {props.createCuttingInstructions.errMess !== null ?
                                    <div style={{ marginTop: 20, color: "red" }}>
                                        <p>{props.createCuttingInstructions.errMess}</p>
                                    </div> : ""
                                }
                                <div className="mt-3 row">
                                    <div className="col-6 d-flex justify-content-start">
                                        {isCompleteButtonActive(order.orderStatusId, account.roleId, items)
                                            && cuttingInstruction.cuttingInstruction !== null &&
                                            < Button
                                                color="success"
                                                onClick={handleMarkDone}>
                                                Complete this order
                                        </Button>
                                        }
                                        {isProcessingButtonActive(order.orderStatusId, account.roleId, items) &&
                                            <Button
                                                color="warning"
                                                onClick={handleMarkProcessing}>
                                                Mark order as Processing
                                        </Button>
                                        }
                                    </div>
                                    <div className="col-6 d-flex justify-content-end">
                                        {account.roleId === ROLE_FACTORY_MANAGER ?
                                            toggleCreateCuttingInstructionBtn
                                                ?
                                                <Button
                                                    type="submit"
                                                    color="secondary"
                                                    className="submitButton"
                                                    onClick={
                                                        handleSubmit
                                                    }
                                                >
                                                    Create Cutting Instruction
                                                </Button> : order !== null
                                                    && order.orderStatusId === ORDER_STATUS_PROCESSING && items !== null && items.length > 0 ?
                                                    <Button color="info" tag={Link}
                                                        to={`${ROUTE_VIEW_CUTTING_INSTRUCTION}?orderId=${order.orderId}`}
                                                        target={"_blank"}>View Cutting Instruction</Button>
                                                    : null
                                            : null
                                        }
                                    </div>
                                </div>
                            </div>
                        </React.Fragment>
                    );
            }
        }

        // console.log("props.createCuttingInstructions.successStatus");
        // console.log(props.createCuttingInstructions.successStatus);

        // console.log("isCreateBtnClicked");
        // console.log(isCreateBtnClicked);

        if (props.createCuttingInstructions.successStatus && isCreateBtnClicked) {
            setIsCreateBtnClicked(false);
            setIsToastOpen(true);
            window.location.reload();
        }

        if (props.createCuttingInstructions.errMess !== null && isCreateBtnClicked) {
            setIsCreateBtnClicked(false);
            setIsToastOpen(true);
            // window.location.reload();
        }

        if (isCreateBtnClicked) {
            return <Loading message={"Be patient! Please wait until the browser refreshes itself..."} />
        }

        return (
            <div className="container">
                {breadCrumb}

                <div className="row row-content">
                    <div className="col-12">
                        <h5 className="text-success">{successStatus ? SUCCESS_MESSAGE_SUBMIT_GENERAL : ''}</h5>
                        <h5 className="text-danger">{errMess != null ? errMess : ''}</h5>
                    </div>
                    {content(editing)}
                </div>
            </div>
        );
    } else {
        return (
            <UnauthorizedAccessDisplay />
        );
    }
}

const isCompleteButtonActive = (orderStatusId, roleId, items) => {
    return (roleId === ROLE_FACTORY_MANAGER)
        && (orderStatusId === ORDER_STATUS_PROCESSING)
        && (items != null && items.length > 0);
}

const isProcessingButtonActive = (orderStatusId, roleId, items) => {
    return (roleId === ROLE_FACTORY_MANAGER)
        && (orderStatusId === ORDER_STATUS_APPROVED)
        && (items != null && items.length > 0);
}

const isOrderEditable = (order, account) => {
    return (account.roleId === ROLE_OFFICE_MANAGER || account.roleId === ROLE_OFFICE_EMPLOYEE)
        && (order.orderStatusId === ORDER_STATUS_PENDING)
        && (account.userName === order.creator || account.userName === order.updater || account.userName === order.approver);
}

const roleAccessible = (roleId) => {
    return roleId === ROLE_OFFICE_MANAGER || roleId === ROLE_OFFICE_EMPLOYEE || roleId === ROLE_FACTORY_MANAGER;
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        orders: state.orders,
        orderDetail: state.orderDetail,
        currentlyViewedOrder: state.currentlyViewedOrder,
        fetchCuttingInstructions: state.fetchCuttingInstruction,
        createCuttingInstructions: state.createCuttingInstruction,
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        fetchOrderDetail: (orderId) => {
            dispatch(fetchOrderDetail(orderId))
        },
        fetchOrderItems: (orderId) => {
            dispatch(fetchOrderItems(orderId))
        },
        postMarkDoneOrder: (orderId) => {
            dispatch(postMarkDoneOrder(orderId))
        },
        putMarkProcessingOrder: (orderId) => {
            dispatch(putModifyOrderStatus(orderId, ORDER_STATUS_PROCESSING))
        },
        fetchCuttingInstructionByOrderId: (orderId) => {
            dispatch(fetchCuttingInstructionByOrderId(orderId))
        },
        createInstruction: (orderId) => {
            dispatch(createInstruction(orderId))
        }
    });
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetail);