import React from 'react';
import {Card, CardBody, CardHeader} from 'reactstrap';
import {getValueOf, dateDisplayVietnamese, formatterVND} from '../../shared/utils';
import OrderStatusDisplay from '../common/OrderStatusDisplay';
import {ROUTE_VIEW_USER_INFO} from "../../shared/constants";
import {Link} from "react-router-dom";

const ColoredLine = ({color}) => (
    <hr
        style={{
            color: color,
            backgroundColor: color
        }}
    />
);

function OrderDetailCard(props) {
    const order = props.order;
    const account = props.account;

    if (order == null) return (<div></div>);
    else {
        return (
            <Card>
                <CardHeader className="bg-warning text-black"><b>Order Details</b></CardHeader>
                <CardBody>
                    <p><b><u>Customer Information</u></b></p>
                    <dl className="row p-1">
                        <dt className="col-5">Name</dt>
                        <dd className="col-7">{getValueOf(order.customerName)}</dd>
                        <dt className="col-5">Company</dt>
                        <dd className="col-7">{getValueOf(order.customerCompanyName)}</dd>
                        <dt className="col-5">Email</dt>
                        <dd className="col-7">{getValueOf(order.customerEmail)}</dd>
                        <dt className="col-5">Phone Number</dt>
                        <dd className="col-7">{getValueOf(order.customerPhoneNumber)}</dd>
                        <dt className="col-5">Address</dt>
                        <dd className="col-7">{getValueOf(order.customerAddress)}</dd>
                        <dt className="col-5"></dt>
                        <dd className="col-7">{getValueOf(order.customerWardCommune + ', ' + order.customerDistrict + ', ' + order.customerCityProvince)}</dd>
                    </dl>
                    <ColoredLine color="black"/>
                    <p><b><u>Order Information</u></b></p>
                    <dl className="row p-1">
                        <dt className="col-5">Order ID</dt>
                        <dd className="col-7">{getValueOf(order.orderId)}</dd>
                        <dt className="col-5">Status</dt>
                        <dd className="col-7"><OrderStatusDisplay status={getValueOf(order.orderStatus)}/></dd>
                        <dt className="col-5">Total Price</dt>
                        <dd className="col-7">{getValueOf(formatterVND.format(order.totalPrice))}</dd>
                        <dt className="col-5">Creator</dt>
                        <dd className="col-7">
                            {getValueOf(order.creator) === account.userName ?
                                <span>You</span>
                                : getValueOf(order.creator) !== 'N/A' ? <Link
                                    to={`${ROUTE_VIEW_USER_INFO}?userName=${getValueOf(order.creator)}`}>
                                    {getValueOf(order.creator)}
                                </Link> : 'N/A'}
                        </dd>
                        <dt className="col-5">Created Date</dt>
                        <dd className="col-7">{getValueOf(dateDisplayVietnamese(order.createdDate))}</dd>
                        <dt className="col-5">Updater</dt>
                        <dd className="col-7">
                            {getValueOf(order.updater) === account.userName ?
                                <span>You</span>
                                : getValueOf(order.updater) !== 'N/A' ? <Link
                                    to={`${ROUTE_VIEW_USER_INFO}?userName=${getValueOf(order.updater)}`}>
                                    {getValueOf(order.updater)}
                                </Link> : 'N/A'}
                        </dd>
                        <dt className="col-5">Updated Date</dt>
                        <dd className="col-7">{getValueOf(dateDisplayVietnamese(order.updatedDate))}</dd>
                        <dt className="col-5">Approver</dt>
                        <dd className="col-7">
                            {getValueOf(order.approver) === account.userName ?
                                <span>You</span>
                                : getValueOf(order.approver) !== 'N/A' ? <Link
                                    to={`${ROUTE_VIEW_USER_INFO}?userName=${getValueOf(order.approver)}`}>
                                    {getValueOf(order.approver)}
                                </Link> : 'N/A'}
                        </dd>
                        <dt className="col-5">Assignee</dt>
                        <dd className="col-7">
                            {getValueOf(order.assignee) === account.userName ?
                                <span>You</span>
                                : getValueOf(order.assignee) !== 'N/A' ? <Link
                                    to={`${ROUTE_VIEW_USER_INFO}?userName=${getValueOf(order.assignee)}`}>
                                    {getValueOf(order.assignee)}
                                </Link> : 'N/A'}
                        </dd>
                    </dl>
                </CardBody>
            </Card>
        );
    }
}

export default OrderDetailCard;