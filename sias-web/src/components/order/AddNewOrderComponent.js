import React from 'react';
import { Button, Col, Row, Breadcrumb, BreadcrumbItem, Card, CardText, CardTitle } from 'reactstrap';
import { Link, withRouter } from 'react-router-dom';
import { ROUTE_ADD_NEW_ORDER_FORM, ROUTE_IMPORT_ORDER_BATCH, ROLE_OFFICE_MANAGER, ROUTE_HOME, ROLE_OFFICE_EMPLOYEE } from '../../shared/constants';
import { connect } from 'react-redux';
import UnauthorizedAccessDisplay from '../common/UnauthorizedAccessDisplay';

function AddNewOrder(props) {
    const account = props.loginAccount.account;
    if (account == null) {
        return (
            <div className="container">
                <div className="row row-content">
                    <h1>You must login first</h1>
                </div>
            </div>
        );
    } else if (roleAccessible(account.roleId)) {
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                        <BreadcrumbItem>Order Management</BreadcrumbItem>
                        <BreadcrumbItem active>Add New Order</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>Add New Order</h3>
                        <hr />
                    </div>
                </div>
                <div className="row row-content d-flex justify-content-center">
                    <Row>
                        <Col sm="6">
                            <Card body>
                                <CardTitle tag="h5">Add new order by using form</CardTitle>
                                <CardText>Quickly add a new order with an easy-to-use form.</CardText>
                                <Button color='warning' tag={Link} to={ROUTE_ADD_NEW_ORDER_FORM}>Use Form</Button>
                            </Card>
                        </Col>
                        <Col sm="6">
                            <Card body>
                                <CardTitle tag="h5">Add new order by uploading a CSV File</CardTitle>
                                <CardText>Add multiple orders simultaneously by using a standard CSV File.</CardText>
                                <Button color='warning' tag={Link} to={ROUTE_IMPORT_ORDER_BATCH}>Use CSV File</Button>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    } else {
        return (
            <UnauthorizedAccessDisplay />
        );
    }
}

const roleAccessible = (roleId) => {
    return roleId === ROLE_OFFICE_MANAGER || roleId === ROLE_OFFICE_EMPLOYEE;
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount
    };
}

export default withRouter(connect(mapStateToProps)(AddNewOrder));