import React from 'react';
import { devInfos } from '../../shared/devInfos';

function Footer(props) {
    return (
        <div className="footer">
            <div className="container">
                <div className="row justify-content-between">
                    <div className="d-none d-lg-block col-lg-2">
                        <a href="/">
                            <img src="/assets/images/logo_footer.png" height="78" width="100"
                                alt="Steel Industry Assistant System" />
                        </a>
                    </div>
                    <div className="col-12 col-sm-6 col-lg-5 justify-content-center align-self-center">
                        {devInfos.address}
                    </div>
                    <div className="col-12 col-sm-6 col-lg-5 justify-content-center align-self-center">
                        <address>
                            <div className="row justify-content-center">
                                <div className="col-12 col-xl-5">
                                    <span className="fa fa-phone fa-lg"></span> {devInfos.phoneNumber}
                                </div>
                                <div className="col-12 col-xl-7">
                                    <span className="fa fa-envelope fa-lg"></span> {devInfos.email}
                                </div>
                            </div>
                        </address>
                    </div>

                </div>
                <div className="row justify-content-center">
                    <div className="col-auto">
                        © Copyright 2020 Steel Industry Assistant System
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer;