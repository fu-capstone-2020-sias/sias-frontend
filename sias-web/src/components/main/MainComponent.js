import React, { Component } from 'react';
import Header from '../header/HeaderComponent';
import Footer from '../footer/FooterComponent';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import SwitchRoutes from '../common/SwitchRoutes';


class Main extends Component {
    render() {
        return (
            <div className="App">
                <Header />
                <SwitchRoutes account={this.props.loginAccount.account} />
                <Footer />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount
    };
}

export default withRouter(connect(mapStateToProps)(Main));