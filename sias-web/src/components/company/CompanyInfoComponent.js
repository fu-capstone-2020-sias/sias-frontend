import React, {Component} from 'react';
import {connect} from 'react-redux';
import {ROLE_ADMIN} from '../../shared/constants'
import {actions} from 'react-redux-form';
import {fetchCompanyInfo, modifyCompanyInfo} from '../../redux/ActionCreators';
import ViewCompanyInfo from "./ViewCompanyInfoComponent";
import ModifyCompanyInfo from "./ModifyCompanyInfoComponent";

class CompanyInfo extends Component {
    componentDidMount() {
        const account = this.props.loginAccount.account;
        if (null != account) {
            this.props.fetchCompanyInfo(); // No need to pass ID because server get Company Info ID of user on server
        }
    }

    render() {
        const account = this.props.loginAccount.account;
        if (account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first.</h1>
                    </div>
                </div>
            );
        } else if (account.roleId === ROLE_ADMIN) {
            return <ModifyCompanyInfo
                loginAccount={account}
                resetCompanyInfoForm={this.props.resetCompanyInfoForm}
                modifyCompanyInfo={this.props.modifyCompanyInfo}
            />
        } else {
            let companyInfo = this.props.companyInfo;
            return (
                <ViewCompanyInfo
                    companyInfo={companyInfo}
                />
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        companyInfo: state.companyInfo
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCompanyInfo: () => {
            dispatch(fetchCompanyInfo())
        },
        modifyCompanyInfo: (companyInfo) => {
            dispatch(modifyCompanyInfo(companyInfo))
        }, // receive object
        resetCompanyInfoForm: () => {
            dispatch(actions.reset('companyInfo'))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CompanyInfo);
