import React, { Component } from "react";
import {
    Breadcrumb,
    BreadcrumbItem,
    Button,
    Label,
    Col,
    Row,
} from "reactstrap";
import { Toast, ToastBody, ToastHeader } from "reactstrap";
import { Form, Control, Errors } from "react-redux-form";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { fetchCompanyInfo, modifyCompanyInfo } from "../../redux/actions/ActionCreators";
import {
    ROLE_ADMIN,
    ROUTE_HOME,
    VALIDATION_MSG_INVALID_COMPANY_NAME,
    VALIDATION_MSG_INVALID_EMAIL,
    VALIDATION_MSG_REQUIRED,
    VALIDATION_MSG_INVALID_PHONE_NUMBER,
    VALIDATION_MSG_INVALID_WEBSITE
} from "../../shared/constants";
import { actions } from "react-redux-form";
import {
    fetchCityProvinceIdList,
    fetchDistrictIdList,
    fetchWardCommuneIdList,
} from "../../shared/utils";
import { Loading } from "../common/LoadingComponent";
import UnauthorizedAccessDisplay from "../common/UnauthorizedAccessDisplay";
import FormLabel from "../common/FormLabel";
import { required, validCompanyName, validPhoneNumber, validEmail, validWebsite } from "../../shared/fieldValidation"


class ModifyCompanyInfo extends Component {
    handleSubmit = async (values) => {
        await this.props.modifyCompanyInfo(
            values.name,
            this.state.selectedCityProvinceId,
            this.state.selectedDistrictId,
            this.state.selectedWardCommuneId,
            values.address,
            values.phoneNumber,
            values.email,
            values.notes,
            values.companyWebsite
        );
        // this.props.resetCompanyInfoForm();
        this.setState({
            isToastOpen: true
        })
        setTimeout(() => {
            this.setState({
                isToastOpen: false
            })
            window.location.reload();
        }, 3000)
    };

    fetchCityProvinceIdList = async () => {
        let response = await fetchCityProvinceIdList();
        // console.log("Response:");
        // console.log(response.data.rows);
        this.setState({
            cityProvinceIdList: response.data.rows,
        });
    };

    fetchDistrictIdList = async (cityProvinceId) => {
        let response = await fetchDistrictIdList(cityProvinceId);
        // console.log("Response:");
        // console.log(response.data.rows);
        this.setState({
            districtIdList: response.data.rows,
        });
    };

    fetchWardCommuneIdList = async (districtId) => {
        let response = await fetchWardCommuneIdList(districtId);
        // console.log("Response:");
        // console.log(response.data.rows);
        this.setState({
            wardCommuneIdList: response.data.rows,
        });
    };

    mapErrMessToParagraph = (errMessArr) => {
        return errMessArr.map((errMessObj) => {
            return <p>{errMessObj}</p>
        })
    }

    mapCityProvinceToOption = () => {
        return this.state.cityProvinceIdList.map((cityProvinceObj) => (
            <option
                key={cityProvinceObj.cityProvinceId}
                value={cityProvinceObj.cityProvinceId}
            >
                {cityProvinceObj.name}
            </option>
        ));
    };

    mapDistrictToOption = () => {
        return this.state.districtIdList.map((districtObj) => (
            <option key={districtObj.districtId} value={districtObj.districtId}
            >
                {districtObj.name}
            </option>
        ));
    };

    mapWardCommuneToOption = () => {
        return this.state.wardCommuneIdList.map((wardCommuneObj) => (
            <option
                key={wardCommuneObj.wardCommuneId}
                value={wardCommuneObj.wardCommuneId}
            >
                {wardCommuneObj.name}
            </option>
        ));
    };

    state = {
        firstLoadCityProvince: false,
        cityProvinceIdList: [],
        districtIdList: [],
        wardCommuneIdList: [],
        selectedCityProvinceId: "",
        selectedDistrictId: "",
        selectedWardCommuneId: "",
        isToastOpen: false,
    };

    componentDidMount() {
        const account = this.props.loginAccount.account;
        this.props.resetCompanyInfoForm();
        this.setState({
            account: account,
        });
        if (null != account) {
            this.props.fetchCompanyInfo(); // No need to pass ID because server get Company Info ID of user on server
        }

        // console.log("componentDidMount");

    }

    render() {
        if (this.state.account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first.</h1>
                    </div>
                </div>
            );
        } else if (this.state.account.roleId !== ROLE_ADMIN) {
            return (
                <UnauthorizedAccessDisplay />
            );
        } else {
            if (this.props.companyInfos.companyInfo != null) {
                // console.log("Check company info");
                // console.log(this.props.companyInfos.companyInfo);
                if (this.state.firstLoadCityProvince === false) {
                    this.fetchCityProvinceIdList();
                    this.fetchDistrictIdList(this.props.companyInfos.companyInfo.cityProvinceId); // Default HCM
                    this.fetchWardCommuneIdList(this.props.companyInfos.companyInfo.districtId); // Default Quận 1
                    this.setState({
                        firstLoadCityProvince: true,
                        selectedCityProvinceId: this.props.companyInfos.companyInfo.cityProvinceId,
                        selectedDistrictId: this.props.companyInfos.companyInfo.districtId,
                        selectedWardCommuneId: this.props.companyInfos.companyInfo.wardCommuneId,
                    });
                }

                return (
                    <div className="container" style={{ position: "relative" }}>
                        <div className="row">
                            <Breadcrumb>
                                <BreadcrumbItem>
                                    <Link to={ROUTE_HOME}>Home</Link>
                                </BreadcrumbItem>
                                <BreadcrumbItem>Company Information</BreadcrumbItem>
                                <BreadcrumbItem active>Modify Company Information</BreadcrumbItem>
                            </Breadcrumb>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <h3>Modify Company Information</h3>
                                <hr />
                                <h5 className="text-success">
                                    {/* {successStatus ? "Your feedback was sent successfully" : ""} */}
                                </h5>
                            </div>
                            <div className="col-12 col-md-10 offset-md-1 col-xl-8 offset-xl-2 mb-5">
                                <Form
                                    model="companyInfo"
                                    onSubmit={(values) => this.handleSubmit(values)}
                                >
                                    <Row className="form-group">
                                        <Label htmlFor="name" md={3}>
                                            <FormLabel text="Company Name" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text
                                                model=".name"
                                                id="name"
                                                name="name"
                                                className="form-control"
                                                validators={{ required, validCompanyName }}
                                                defaultValue={this.props.companyInfos.companyInfo.name}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".name"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".name"
                                                show="touched"
                                                messages={{
                                                    validCompanyName:
                                                        VALIDATION_MSG_INVALID_COMPANY_NAME
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="cityProvinceId" md={3}>
                                            <FormLabel text="City/Province" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select
                                                className="form-control"
                                                defaultValue={this.props.companyInfos.companyInfo.cityProvinceId}
                                                model=".cityProvinceId"
                                                id=".cityProvinceId"
                                                onClick={async (event) => {
                                                    if (
                                                        event.target.value !==
                                                        this.state.selectedCityProvinceId
                                                    ) {
                                                        // console.log("Check event mapCityProvinceToOption");
                                                        // console.log(event.target.value);
                                                        this.setState({
                                                            selectedCityProvinceId: event.target.value,
                                                        });

                                                        await this.fetchDistrictIdList(event.target.value);

                                                        this.setState({
                                                            selectedDistrictId: this.state.districtIdList[0]
                                                                .districtId,
                                                        });

                                                        await this.fetchWardCommuneIdList(
                                                            this.state.districtIdList[0].districtId // Default
                                                        );

                                                        this.setState({
                                                            selectedWardCommuneId: this.state
                                                                .wardCommuneIdList[0].wardCommuneId,
                                                        });
                                                    }
                                                }}
                                            >
                                                {this.state.cityProvinceIdList.length !== 0 ? (
                                                    this.mapCityProvinceToOption()
                                                ) : (
                                                        <option key={"1"} value={"1"}>
                                                            {"Loading..."}
                                                        </option>
                                                    )}
                                            </Control.select>
                                            <Errors
                                                className="text-danger"
                                                model=".cityProvinceId"
                                                show="touched"
                                                messages={{
                                                    required: "This is a required field.",
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="district" md={3}>
                                            <FormLabel text="District" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select
                                                className="form-control"
                                                defaultValue={this.props.companyInfos.companyInfo.districtId}
                                                model=".districtId"
                                                id=".districtId"
                                                onClick={async (event) => {
                                                    if (
                                                        event.target.value !== this.state.selectedDistrictId
                                                    ) {
                                                        // console.log("Check event mapDistrictToOption");
                                                        // console.log(event.target.value);
                                                        this.setState({
                                                            selectedDistrictId: event.target.value,
                                                        });

                                                        await this.fetchWardCommuneIdList(event.target.value);

                                                        this.setState({
                                                            selectedWardCommuneId: this.state
                                                                .wardCommuneIdList[0].wardCommuneId,
                                                        });
                                                    }
                                                }}
                                            >
                                                {/* Using method to fetch */}
                                                {this.state.districtIdList.length !== 0 ? (
                                                    this.mapDistrictToOption()
                                                ) : (
                                                        <option key={"1"} value={"1"}>
                                                            {"Loading..."}
                                                        </option>
                                                    )}
                                            </Control.select>
                                            <Errors
                                                className="text-danger"
                                                model=".districtId"
                                                show="touched"
                                                messages={{
                                                    required: "This is a required field.",
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="wardCommuneId" md={3}>
                                            <FormLabel text="Ward/Commune" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select
                                                className="form-control"
                                                defaultValue={this.props.companyInfos.companyInfo.wardCommuneId}
                                                model=".wardCommuneId"
                                                id=".wardCommuneId"
                                                onClick={async (event) => {
                                                    if (
                                                        event.target.value !==
                                                        this.state.selectedWardCommuneId
                                                    ) {
                                                        // console.log("Check event mapWardCommuneToOption");
                                                        // console.log(event.target.value);
                                                        this.setState({
                                                            selectedWardCommuneId: event.target.value,
                                                        });
                                                    }
                                                }}
                                            >
                                                {/* Using method to fetch */}
                                                {this.state.wardCommuneIdList.length !== 0 ? (
                                                    this.mapWardCommuneToOption()
                                                ) : (
                                                        <option key={"1"} value={"1"}>
                                                            {"Loading..."}
                                                        </option>
                                                    )}
                                            </Control.select>
                                            <Errors
                                                className="text-danger"
                                                model=".wardCommuneId"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="address" md={3}>
                                            <FormLabel text="Address" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text
                                                model=".address"
                                                id="address"
                                                name="address"
                                                className="form-control"
                                                validators={{ required }}
                                                defaultValue={this.props.companyInfos.companyInfo.address}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".address"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="phoneNumber" md={3}>
                                            <FormLabel text="Phone Number" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text
                                                model=".phoneNumber"
                                                id="phoneNumber"
                                                name="phoneNumber"
                                                className="form-control"
                                                validators={{ required, validPhoneNumber }}
                                                defaultValue={this.props.companyInfos.companyInfo.phoneNumber}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".phoneNumber"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".phoneNumber"
                                                show="touched"
                                                messages={{
                                                    validPhoneNumber: VALIDATION_MSG_INVALID_PHONE_NUMBER
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="email" md={3}>
                                            <FormLabel text="Email" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text
                                                model=".email"
                                                id="email"
                                                name="email"
                                                className="form-control"
                                                validators={{ required, validEmail }}
                                                defaultValue={this.props.companyInfos.companyInfo.email}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".email"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".email"
                                                show="touched"
                                                messages={{
                                                    validEmail: VALIDATION_MSG_INVALID_EMAIL
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="notes" md={3}>
                                            <FormLabel text="Description" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text
                                                model=".notes"
                                                id="notes"
                                                name="notes"
                                                className="form-control"
                                                validators={{ required }}
                                                defaultValue={this.props.companyInfos.companyInfo.notes}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".notes"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="companyWebsite" md={3}>
                                            <FormLabel text="Website" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text
                                                model=".companyWebsite"
                                                id="companyWebsite"
                                                name="companyWebsite"
                                                className="form-control"
                                                validators={{ required, validWebsite }}
                                                defaultValue={this.props.companyInfos.companyInfo.companyWebsite}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".companyWebsite"
                                                show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".companyWebsite"
                                                show="touched"
                                                messages={{
                                                    validWebsite: VALIDATION_MSG_INVALID_WEBSITE
                                                }}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Col md={3}>
                                            <Button
                                                type="submit"
                                                color="warning"
                                                className="submitButton"
                                            >
                                                Submit
                                            </Button>
                                        </Col>
                                    </Row>
                                </Form>
                                {/*{this.props.companyInfos.successStatus && this.props.companyInfos.successMessage ?*/}
                                <Toast
                                    isOpen={this.state.isToastOpen}
                                    style={{
                                        // position: "absolute",
                                        // bottom: 20,
                                        // left: "-11%",
                                        boxShadow: "0 0.05rem 0.75rem #303841",
                                        position: "fixed",
                                        bottom: "10px",
                                        right: "10px",
                                        backgroundColor: "white",
                                        zIndex: "1"
                                    }}
                                >
                                    <ToastHeader
                                        toggle={() => {
                                            this.setState({
                                                isToastOpen: !this.state.isToastOpen,
                                            });
                                        }}
                                    >
                                        Information
                                    </ToastHeader>
                                    <ToastBody>
                                        {this.props.companyInfos.successStatus
                                            ? this.props.companyInfos.successMessage
                                            :
                                            this.props.companyInfos.errMess !== null ?
                                                this.mapErrMessToParagraph(this.props.companyInfos.errMess) : ""}
                                    </ToastBody>
                                </Toast>
                                {/*: ""*/}
                                {/*}*/}
                            </div>
                        </div>
                    </div>
                );
            } else {
                return (
                    <div className={"container"}>
                        <Loading />
                    </div>
                )
            }
        }
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        companyInfos: state.companyInfos,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCompanyInfo: () => {
            dispatch(fetchCompanyInfo());
        },
        modifyCompanyInfo: async (
            name,
            cityProvinceId,
            districtId,
            wardCommuneId,
            address,
            phoneNumber,
            email,
            notes,
            companyWebsite
        ) => {
            dispatch(
                modifyCompanyInfo(
                    name,
                    cityProvinceId,
                    districtId,
                    wardCommuneId,
                    address,
                    phoneNumber,
                    email,
                    notes,
                    companyWebsite
                )
            );
        },
        resetCompanyInfoForm: () => {
            dispatch(actions.reset("companyInfo"));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModifyCompanyInfo);
