import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchCompanyInfo } from "../../redux/actions/ActionCreators";
import { Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader } from "reactstrap";
import { Link } from "react-router-dom";
import { Loading } from "../common/LoadingComponent";
import { ROUTE_HOME } from "../../shared/constants";
import { getValueOf } from "../../shared/utils";

class ViewCompanyInfo extends Component {
    componentDidMount() {
        const account = this.props.loginAccount.account;
        if (account != null) {
            this.props.fetchCompanyInfo();
        }
    }

    getLink = (website) => {
        if (website == null) {
            return <span>{getValueOf(website)}</span>
        } else {
            let url = 'https://';
            if (website.startsWith('www')) {
                url += website;
            } else {
                url = url + 'www.' + website;
            }
            return <a href={url}>{getValueOf(website)}</a>
        }
    }

    render() {
        if (this.props.loginAccount.account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first.</h1>
                    </div>
                </div>
            );
        } else {
            const info = this.props.companyInfos.companyInfo;
            return (
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem>
                                <Link to={ROUTE_HOME}>Home</Link>
                            </BreadcrumbItem>
                            <BreadcrumbItem>Company Information</BreadcrumbItem>
                            <BreadcrumbItem active>View Company Information</BreadcrumbItem>
                        </Breadcrumb>
                    </div>
                    <div>
                        <h3>Company Information</h3>
                        <hr />
                    </div>
                    { info != null ?
                        (<div className="row row-content">
                            <div className="col-12 col-md-6">
                                <Card>
                                    <CardHeader className="bg-warning text-black"><b>Company Information</b></CardHeader>
                                    <CardBody>
                                        <dl className="row p-1">
                                            <dt className="col-5">Company Name</dt>
                                            <dd className="col-7">{getValueOf(info.name)}</dd>
                                            <dt className="col-5">Phone Number</dt>
                                            <dd className="col-7">{getValueOf(info.phoneNumber)}</dd>
                                            <dt className="col-5">Email</dt>
                                            <dd className="col-7">{getValueOf(info.email)}</dd>
                                            <dt className="col-5">Address</dt>
                                            <dd className="col-7">{getValueOf(`${info.address}, ${info.wardCommune}, ${info.district}, ${info.cityProvince}`)}</dd>
                                            <dt className="col-5">Website</dt>
                                            <dd className="col-7">{this.getLink(info.companyWebsite)}</dd>
                                        </dl>
                                    </CardBody>
                                </Card>
                            </div>
                            <div className="col-12 col-md-8 mt-5">
                                <Card>
                                    <CardBody className="bg-faded">
                                        <blockquote className="blockquote">
                                            <p className="mb-1 mt-1">
                                                {' '}
                                                {getValueOf(info.notes)}
                                                {' '}</p>                                            
                                        </blockquote>
                                    </CardBody>
                                </Card>
                            </div>
                        </div>)
                        : 
                        <Loading />
                    }

                </div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        companyInfos: state.companyInfos,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCompanyInfo: () => {
            dispatch(fetchCompanyInfo());
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewCompanyInfo);
