import React, {useState} from 'react';
import {Breadcrumb, BreadcrumbItem, Button} from 'reactstrap';
import {Link, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {useEffect} from 'react';
import {fetchWarehouseDetail} from '../../redux/actions/ActionCreators';
import {
    ROLE_OFFICE_MANAGER,
    ROLE_FACTORY_MANAGER,
    SUCCESS_MESSAGE_SUBMIT_GENERAL,
    ROUTE_HOME,
    ROUTE_VIEW_WAREHOUSE_LIST,
    ROUTE_VIEW_STOCK_PRODUCT_LIST,
    ERROR_PAGE_NOT_FOUND
} from '../../shared/constants';
import {Loading} from '../common/LoadingComponent';
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import WarehouseDetailCard from './WarehouseDetailCardComponent';
import ModifyWarehouseDetail from './ModifyWarehouseDetailComponent';
import UnauthorizedAccessDisplay from '../common/UnauthorizedAccessDisplay';
import ErrorDisplay from '../common/ErrorDisplay';

function WarehouseDetail(props) {
    const [isEditing, setEditing] = useState(false);
    const account = props.loginAccount.account;

    const toggleEdit = () => {
        setEditing(!isEditing);
    }

    useEffect(() => {
        const warehouseId = props.warehouseId;
        const currentWarehouseId = props.currentlyViewedWarehouse.currentWarehouseId;
        const fetchWarehouseData = async () => {
            await props.fetchWarehouseDetail(warehouseId);
        }

        if (currentWarehouseId !== warehouseId) {
            fetchWarehouseData();
        }

    }, []);

    if (account == null) {
        return (
            <div className="container">
                <div className="row row-content">
                    <h1>You must login first</h1>
                </div>
            </div>
        );
    } else if (roleAccessible(account.roleId)) {
        const isLoading = props.warehouseDetail.isLoading;
        const errMess = props.warehouseDetail.errMess;
        const warehouse = props.warehouseDetail.warehouse;
        const successStatus = props.warehouseDetail.successStatus;

        const breadCrumb = (
            <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                    <BreadcrumbItem>Stock Management</BreadcrumbItem>
                    <BreadcrumbItem><Link to={ROUTE_VIEW_WAREHOUSE_LIST}>View Warehouse List</Link></BreadcrumbItem>
                    <BreadcrumbItem active>Warehouse Details</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>Warehouse Details</h3>
                    <hr/>
                </div>
            </div>
        );

        if (isLoading) {
            return (
                <div className="container">
                    {breadCrumb}
                    <div className="row row-content">
                        <Loading/>
                    </div>
                </div>
            );
        } else if (errMess != null) {
            return <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
        }
        const content = (value) => {
            if (value) {
                return (
                    <div className="col-12 col-md-9 col-xl-8">
                        <ModifyWarehouseDetail account={account} warehouse={warehouse} toggleEdit={toggleEdit}/>
                    </div>
                );
            } else {
                return (
                    <React.Fragment>
                        <div className="col-12 d-flex justify-content-center">
                            <h5 className="text-danger">
                                To view stock products of this warehouse, please go to <Link
                                to={ROUTE_VIEW_STOCK_PRODUCT_LIST} target='_blank'>View Stock Product List</Link> page.
                            </h5>
                        </div>
                        <div className="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                            <WarehouseDetailCard warehouse={warehouse} account={account}/>
                            <div className="mt-3">
                                {isWarehouseEditable(account, warehouse) &&
                                <Button color="warning" onClick={toggleEdit}>Edit</Button>
                                }
                            </div>
                        </div>
                    </React.Fragment>
                );
            }
        }

        return (
            <div className="container">
                {breadCrumb}

                <div className="row row-content">
                    <div className="col-12">
                        <h5 className="text-success">{successStatus ? SUCCESS_MESSAGE_SUBMIT_GENERAL : ''}</h5>
                        <h5 className="text-danger">{errMess != null ? errMess : ''}</h5>
                    </div>
                    {content(isEditing)}
                </div>
            </div>
        );
    } else {
        return (
            <UnauthorizedAccessDisplay/>
        );
    }
}

const isWarehouseEditable = (account, warehouse) => {
    return (account.roleId === ROLE_OFFICE_MANAGER || account.roleId === ROLE_FACTORY_MANAGER) && (account.userName === warehouse.headOfWarehouse);
}

const roleAccessible = (roleId) => {
    return roleId === ROLE_OFFICE_MANAGER || roleId === ROLE_FACTORY_MANAGER;
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        warehouses: state.warehouses,
        warehouseDetail: state.warehouseDetail,
        currentlyViewedWarehouse: state.currentlyViewedWarehouse
    };
}


const mapDispatchToProps = (dispatch) => {
    return ({
        fetchWarehouseDetail: (warehouseId) => {
            dispatch(fetchWarehouseDetail(warehouseId))
        }
    });
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WarehouseDetail));