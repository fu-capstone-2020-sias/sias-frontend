import React, { Component } from 'react';
import { Button, Label, Col, Row, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Form, Control, Errors, actions } from 'react-redux-form';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {
    postNewWarehouse, fetchCityProvinceIdList, fetchDistrictIdList, fetchWardCommuneIdList,
    fetchWarehouseManagerIdList, fetchDepartmentIdList
} from '../../redux/actions/ActionCreators';
import { required, validWarehouseName } from '../../shared/fieldValidation';
import { ROUTE_HOME, VALIDATION_MSG_INVALID_WAREHOUSE_NAME, VALIDATION_MSG_REQUIRED, SUCCESS_MESSAGE_ADD_NEW_WAREHOUSE, ROUTE_WAREHOUSE_DETAILS, ROLE_OFFICE_MANAGER } from '../../shared/constants';
import FormLabel from '../common/FormLabel';
import UnauthorizedAccessDisplay from '../common/UnauthorizedAccessDisplay';
import { Loading } from '../common/LoadingComponent';

class AddNewWarehouse extends Component {
    constructor(props) {
        super(props);
        const addresses = this.props.addresses;
        if (addresses.cityProvinceList.length === 0 && !addresses.isLoaded) {
            this.props.fetchCityProvinceIdList();
            this.props.fetchDistrictIdList();
            this.props.fetchWardCommuneIdList();
        }
        this.props.fetchDepartmentIdList();
        this.props.fetchWarehouseManagerIdList();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            selectedCityProvinceId: "",
            selectedDistrictId: "",
            selectedWardCommuneId: "",
            selectedDepartmentId: "",
            selectedManagerId: "",
        };
    }

    handleSubmit = async (values) => {
        await this.props.postNewWarehouse(values);
        await setTimeout(() => {
            this.props.resetNewWarehouseForm()
        }, 3000);
    }

    resetForm = () => {
        this.props.resetNewWarehouseForm();
        this.setState({
            selectedCityProvinceId: '',
            selectedDistrictId: '',
            selectedWardCommuneId: '',
        });
    }

    mapCityProvinceToOption = () => {
        return this.props.addresses.cityProvinceList.map((cityProvinceObj) => (
            <option
                key={cityProvinceObj.cityProvinceId}
                value={cityProvinceObj.cityProvinceId}
            >
                {cityProvinceObj.name}
            </option>
        ));
    };

    mapDistrictToOption = () => {
        const districtList = this.props.addresses.districtList;
        let provinceId = "";

        provinceId = this.state.selectedCityProvinceId;

        if (provinceId != null && provinceId !== '') {
            return districtList
                .filter(dist => dist.cityProvinceId === parseInt(provinceId))
                .map((districtObj) => (
                    <option
                        key={districtObj.districtId}
                        value={districtObj.districtId}>
                        {districtObj.name}
                    </option>
                ));
        }
    }

    mapWardCommuneToOption = () => {
        const wardCommuneList = this.props.addresses.wardCommuneList;
        let districtId = "";

        districtId = this.state.selectedDistrictId;

        if (districtId != null && districtId !== '') {
            return wardCommuneList
                .filter(ward => ward.districtId === parseInt(districtId))
                .map((wardCommuneObj) => (
                    <option
                        key={wardCommuneObj.wardCommuneId}
                        value={wardCommuneObj.wardCommuneId}>
                        {wardCommuneObj.name}
                    </option>
                ));
        }
    }

    addDepartmentOption = () => {
        const departmentList = this.props.addresses.departmentList;
        return departmentList.map((departmentObj) =>
            (<option
                key={departmentObj.departmentId}
                value={departmentObj.departmentId}>
                {departmentObj.name}
            </option>));
    }

    addManagerOption = () => {
        const managerList = this.props.addresses.managerList;

        return managerList.map((managerObj) =>
            (<option
                key={managerObj.userName}
                value={managerObj.userName}>
                {managerObj.userName}
            </option>));
    }

    handleCityProvinceChange = (event) => {
        this.setState({
            selectedCityProvinceId: event.target.value,
            selectedDistrictId: '',
            selectedWardCommuneId: '',
        });
    }

    handleDistrictChange = (event) => {
        this.setState({
            selectedDistrictId: event.target.value,
            selectedWardCommuneId: '',
        });
    }

    handleWardCommuneChange = (event) => {
        this.setState({
            selectedWardCommuneId: event.target.value
        });
    }

    handleDepartmentChange = (event) => {
        this.setState({
            selectedDepartmentId: event.target.value
        });
    }

    handleManagerChange = (event) => {
        this.setState({
            selectedManagerId: event.target.value
        });
    }

    isSaveButtonEnabled() {
        return (
            this.state.selectedCityProvinceId !== ''
            && this.state.selectedDistrictId !== ''
            && this.state.selectedWardCommuneId !== ''
            && this.state.selectedManagerId !== ''
            && this.state.selectedDepartmentId !== ''
        );
    }


    render() {
        const account = this.props.loginAccount.account;
        if (account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first</h1>
                    </div>
                </div>
            );
        } else if (roleAccessible(account.roleId)) {
            const addedSuccessStatus = this.props.addedSuccessStatus;
            const errMess = this.props.errMess;
            const addedWarehouseId = this.props.addedWarehouseId;
            const isAddWarehouseLoading = this.props.isAddWarehouseLoading;

            const redirectLink = addedWarehouseId != null ? ROUTE_WAREHOUSE_DETAILS + '/' + addedWarehouseId : '#';
            const successMessage = addedSuccessStatus ?
                <h5 className="text-success">{SUCCESS_MESSAGE_ADD_NEW_WAREHOUSE} Click <Link to={redirectLink}>here</Link> to view warehouse details.</h5>
                : null;
            return (
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                            <BreadcrumbItem>Stock Management</BreadcrumbItem>
                            <BreadcrumbItem active>Add New Warehouse</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>Add New Warehouse</h3>
                            <hr />
                        </div>
                    </div>
                    {isAddWarehouseLoading ? <Loading /> :
                        <div className="row row-content">
                            <div className="col-12">
                                {successMessage}
                                <h5 className="text-danger">{errMess != null ? errMess : ''}</h5>
                            </div>
                            <div className="col-12 col-md-9 col-xl-8">
                                <Form model="addWarehouse"
                                    onSubmit={(values) => this.handleSubmit(values)}
                                >
                                    <Row>
                                        <Label><strong>Warehouse's Information</strong></Label>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="warehouseName" md={3}>
                                            <FormLabel text="Warehouse name" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text model=".warehouseName" id="warehouseName" name="warehouseName"
                                                className="form-control" validators={{ required, validWarehouseName }} />
                                            <Errors className="text-danger" model=".warehouseName" show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED,
                                                    validWarehouseName: VALIDATION_MSG_INVALID_WAREHOUSE_NAME
                                                }} />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="cityProvinceId" md={3}>
                                            <FormLabel text="City/Province" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select
                                                model=".cityProvinceId"
                                                id="cityProvinceId"
                                                name="cityProvinceId"
                                                className="form-control"
                                                onChange={this.handleCityProvinceChange}
                                            >
                                                <option key='' value=''>
                                                    -City/Province-
                                        </option>
                                                {this.mapCityProvinceToOption()}
                                            </Control.select>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="districtId" md={3}>
                                            <FormLabel text="District" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select
                                                model=".districtId"
                                                id="districtId"
                                                name="districtId"
                                                className="form-control"
                                                onChange={this.handleDistrictChange}
                                            >
                                                <option key='' value=''>
                                                    -District-
                                        </option>
                                                {this.mapDistrictToOption()}
                                            </Control.select>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="communeId" md={3}>
                                            <FormLabel text="Ward/Commune" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select
                                                model=".communeId"
                                                id="communeId"
                                                name="communeId"
                                                className="form-control"
                                                onChange={this.handleWardCommuneChange}
                                            >
                                                <option key='' value=''>
                                                    -Ward/Commune-
                                        </option>
                                                {this.mapWardCommuneToOption()}
                                            </Control.select>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="address" md={3}>
                                            <FormLabel text="Detailed Address" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text model=".address" id="address" name="address"
                                                className="form-control" validators={{ required }} />
                                            <Errors className="text-danger" model=".address" show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }} />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="department" md={3}>
                                            <FormLabel text="Department" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select
                                                model=".department"
                                                id="department"
                                                name="department"
                                                className="form-control"
                                                onChange={this.handleDepartmentChange}
                                            >
                                                <option key='' value=''>
                                                    -Department-
                                        </option>
                                                {this.addDepartmentOption()}
                                            </Control.select>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="warehouseManager" md={3}>
                                            <FormLabel text="Warehouse Manager" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select
                                                model=".warehouseManager"
                                                id="warehouseManager"
                                                name="warehouseManager"
                                                className="form-control"
                                                onChange={this.handleManagerChange}
                                            >
                                                <option key='' value=''>
                                                    -Warehouse Manager-
                                        </option>
                                                {this.addManagerOption()}
                                            </Control.select>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Col>
                                            <Button disabled={!this.isSaveButtonEnabled()}
                                                color="warning"
                                                type="submit"
                                                className="submitButton"
                                            >
                                                Add Warehouse
                                        </Button>
                                    &nbsp;
                                    <Button color="light"
                                                className="backButton"
                                                onClick={this.resetForm}
                                            >
                                                Reset
                                    </Button>
                                        </Col>
                                    </Row>
                                </Form>
                            </div>
                        </div>
                    }
                </div>
            );
        } else {
            return (
                <UnauthorizedAccessDisplay />
            );
        }
    }
}

const roleAccessible = (roleId) => {
    return roleId === ROLE_OFFICE_MANAGER;
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        isAddWarehouseLoading: state.warehouseDetail.isAddWarehouseLoading,
        addedSuccessStatus: state.warehouseDetail.addedSuccessStatus,
        errMess: state.warehouseDetail.errMess,
        addedWarehouseId: state.warehouseDetail.addedWarehouseId,
        addresses: state.addresses
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        fetchCityProvinceIdList: () => { dispatch(fetchCityProvinceIdList()) },
        fetchDistrictIdList: () => { dispatch(fetchDistrictIdList()) },
        fetchWardCommuneIdList: () => { dispatch(fetchWardCommuneIdList()) },
        fetchDepartmentIdList: () => { dispatch(fetchDepartmentIdList()) },
        fetchWarehouseManagerIdList: () => { dispatch(fetchWarehouseManagerIdList()) },
        postNewWarehouse: (values) => { dispatch(postNewWarehouse(values)) },
        resetNewWarehouseForm: () => { dispatch(actions.reset('addWarehouse')) }
    });
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddNewWarehouse));