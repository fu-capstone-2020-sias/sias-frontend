import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { connect } from 'react-redux';
import { Loading } from '../common/LoadingComponent';
import {
    ROUTE_HOME,
    ROLE_OFFICE_MANAGER,
    ROLE_FACTORY_MANAGER,
    ROUTE_VIEW_WAREHOUSE_LIST,
    ROUTE_WAREHOUSE_DETAILS,
    ROUTE_VIEW_USER_INFO,
    SUCCESS_MESSAGE_DELETE_GENERAL,
    ERROR_PAGE_NOT_FOUND
} from '../../shared/constants';
import { getValueOf } from '../../shared/utils';
import { StyledTableCell } from '../common/StyledTableCell';
import TablePaging from '../common/TablePaging';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import { fetchWarehouse, deleteWarehouse } from '../../redux/actions/ActionCreators';
import UnauthorizedAccessDisplay from '../common/UnauthorizedAccessDisplay';
import ErrorDisplay from '../common/ErrorDisplay';

const useStyles = makeStyles({
    table: {
        minWidth: 600,
    }
});

const headCells = [
    { id: 'warehouseId', numeric: true, disablePadding: false, label: 'ID' },
    { id: 'name', numeric: false, disablePadding: false, label: 'Name' },
    { id: 'address', numeric: false, disablePadding: false, label: 'Address' },
    { id: 'cityProvince', numeric: false, disablePadding: false, label: 'City/Province' },
    { id: 'department', numeric: false, disablePadding: false, label: 'Department' },
    { id: 'headOfWarehouse', numeric: false, disablePadding: false, label: 'Manager' },
];

function WarehouseProductListTableHead() {
    return (
        <TableHead>
            <TableRow>
                {headCells.map((headCell) => (
                    <StyledTableCell
                        key={headCell.id}
                        align={headCell.numeric ? 'right' : 'left'}
                        padding={headCell.disablePadding ? 'none' : 'default'}
                    >
                        {headCell.label}
                    </StyledTableCell>
                ))}
                <StyledTableCell></StyledTableCell>
                {/* <StyledTableCell></StyledTableCell> */}
            </TableRow>
        </TableHead>
    );
}

function WarehouseProductListTable(props) {
    const classes = useStyles();
    const [selected, setSelected] = React.useState([]);
    let rows = props.warehousesList;
    const account = props.account;

    const handleClick = (event, row) => {
        const id = row.warehouseId;
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }
        setSelected(newSelected);
    };

    const isSelected = (warehouseId) => selected.indexOf(warehouseId) !== -1;

    const handleDelete = (warehouseId, warehouseName) => {
        confirmAlert({
            title: 'Confirm',
            message: `Are you sure to delete this warehouse? "${warehouseName}" (ID: ${warehouseId})? This will refresh your browser to take effects.`,
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => {
                        props.deleteWarehouse(warehouseId);
                    }
                },
                {
                    label: 'No'
                }
            ]
        });
    }

    const getLastTableColumn = (account, row) => {
        if (account.roleId === ROLE_FACTORY_MANAGER || account.roleId === ROLE_OFFICE_MANAGER) {
            return (
                <StyledTableCell width="10%"><Link to={`${ROUTE_WAREHOUSE_DETAILS}/${row.warehouseId}`}>Details</Link>
                    {isWarehouseDeletable(account, row) &&
                        <span>&nbsp;|&nbsp;<Link to='#'
                            onClick={() => handleDelete(row.warehouseId, row.name)}>Delete</Link></span>
                    }
                </StyledTableCell>
            );
        } else {
            return <StyledTableCell width="5%"></StyledTableCell>;
        }
    }

    const getAddress = (row) => {
        return getValueOf(`${row.address}, ${row.wardcommune.name}, ${row.district.name}`);
    }

    return (
        <React.Fragment>
            <div className="col-12">
                <h5>Number of entries: {props.totalEntries}</h5>
            </div>
            <div className="col-12">
                <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="customized table">
                        <WarehouseProductListTableHead
                            classes={classes}
                            numSelected={selected.length}
                            rowCount={rows.length}
                        />
                        <TableBody>
                            {rows.map((row) => {
                                const isItemSelected = isSelected(row.stockProductId);
                                return (
                                    <TableRow key={row.stockProductId}
                                        onClick={(event) => handleClick(event, row)}
                                        role="checkbox"
                                        aria-checked={isItemSelected}
                                        selected={isItemSelected}
                                    >
                                        <StyledTableCell width="5%"
                                            align="right">{getValueOf(row.warehouseId)}</StyledTableCell>
                                        <StyledTableCell width="20%">{getValueOf(row.name)}</StyledTableCell>
                                        <StyledTableCell width="25%">{getAddress(row)}</StyledTableCell>
                                        <StyledTableCell
                                            width="15%">{getValueOf(row.cityprovince.name)}</StyledTableCell>
                                        <StyledTableCell width="15%">{getValueOf(row.department.name)}</StyledTableCell>
                                        <StyledTableCell width="15%">
                                            {getValueOf(row.headOfWarehouse) === account.userName ?
                                                <span>You</span>
                                                : <Link
                                                    to={`${ROUTE_VIEW_USER_INFO}?userName=${getValueOf(row.headOfWarehouse)}`}>
                                                    {getValueOf(row.headOfWarehouse)}
                                                </Link>}
                                        </StyledTableCell>
                                        {
                                            getLastTableColumn(account, row)
                                        }
                                    </TableRow>
                                )
                            }
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
                <br />
                <TablePaging numberOfPages={props.numberOfPages}
                    currentPage={props.currentPage}
                    linkTo={ROUTE_VIEW_WAREHOUSE_LIST} />
            </div>
        </React.Fragment>
    );
}

WarehouseProductListTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
    // onSelectAllClick: PropTypes.func.isRequired,
    rowCount: PropTypes.number.isRequired
};

class ViewWarehouseList extends Component {
    componentDidMount() {
        const account = this.props.loginAccount.account;
        if (null != account && roleAccessible(account.roleId)) {
            const warehousesList = this.props.warehouses.warehouses;
            const currentPageNumber = this.props.warehouses.currentPageNumber;
            const numberOfPages = this.props.warehouses.numberOfPages;
            let pageNumber = this.props.pageNumber;
            // const searchData = this.props.stockProducts.searchData;
            const errMess = this.props.warehouses.errMess;
            if (errMess != null) {
                return;
            } else if (null == warehousesList || (pageNumber !== currentPageNumber && pageNumber <= numberOfPages)) {
                this.props.fetchWarehouse(pageNumber);
            }
        }
    }

    render() {
        const account = this.props.loginAccount.account;
        if (account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first</h1>
                    </div>
                </div>
            );
        } else if (roleAccessible(account.roleId)) {
            const warehousesList = this.props.warehouses.warehouses;
            const totalEntries = this.props.warehouses.totalEntries;
            const numberOfPages = this.props.warehouses.numberOfPages;
            const currentPageNumber = this.props.warehouses.currentPageNumber;
            const deleteSuccessStatus = this.props.warehouses.deleteSuccessStatus;
            const errMess = this.props.warehouses.errMess;
            const isLoading = this.props.warehouses.isLoading;

            if (currentPageNumber > numberOfPages) {
                return (
                    <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
                )
            }

            return (
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                            <BreadcrumbItem>Stock Management</BreadcrumbItem>
                            <BreadcrumbItem active>View Warehouse List</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>View Warehouse List</h3>
                            <hr />
                        </div>
                    </div>
                    {isLoading ?
                        <Loading /> :
                        <div className="row row-content">
                            <div className="col-12">
                                <h5 className="text-success">{deleteSuccessStatus ? SUCCESS_MESSAGE_DELETE_GENERAL : ''}</h5>
                                <h5 className="text-danger">{errMess != null ? errMess : ''}</h5>
                                <WarehouseProductListTable
                                    warehousesList={(warehousesList == null) ? [] : warehousesList}
                                    numberOfPages={numberOfPages}
                                    currentPage={currentPageNumber}
                                    totalEntries={totalEntries}
                                    account={account}
                                    deleteWarehouse={this.props.deleteWarehouse}
                                />
                            </div>
                        </div>
                    }
                </div>
            );

        } else {
            return (
                <UnauthorizedAccessDisplay />
            );
        }
    }
}

const roleAccessible = (roleId) => {
    return roleId === ROLE_OFFICE_MANAGER || roleId === ROLE_FACTORY_MANAGER;
}

const isWarehouseDeletable = (account, warehouse) => {
    return (account.userName === warehouse.headOfWarehouse);
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        warehouses: state.warehouses,
        addresses: state.addresses
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        fetchWarehouse: (pageNumber) => {
            dispatch(fetchWarehouse(pageNumber))
        },
        deleteWarehouse: (warehouseId) => {
            dispatch(deleteWarehouse(warehouseId))
        }
    });
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewWarehouseList);