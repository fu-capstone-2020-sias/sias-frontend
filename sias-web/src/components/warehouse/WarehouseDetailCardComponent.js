import React from 'react';
import { Card, CardBody, CardHeader } from 'reactstrap';
import { getValueOf, dateDisplayVietnamese } from '../../shared/utils';
import {Link} from "react-router-dom";
import {ROUTE_VIEW_USER_INFO} from "../../shared/constants";

const ColoredLine = ({ color }) => (
    <hr
        style={{
            color: color,
            backgroundColor: color
        }}
    />
);

function WarehouseDetailCard(props) {
    const warehouse = props.warehouse;
    const account = props.account;

    if (warehouse == null) return (<div></div>);
    else {
        return (
            <Card>
                <CardHeader className="bg-warning text-black"><b>Warehouse Details</b></CardHeader>
                <CardBody>
                    <dl className="row p-1">
                        <dt className="col-6">Name</dt>
                        <dd className="col-6">{getValueOf(warehouse.name)}</dd>
                        <dt className="col-6">Warehouse ID</dt>
                        <dd className="col-6">{getValueOf(warehouse.warehouseId)}</dd>
                        <dt className="col-6">Department</dt>
                        <dd className="col-6">{getValueOf(warehouse.department.name)}</dd>
                        {/*2-line address*/}
                        <dt className="col-6">Address</dt>
                        <dd className="col-6">{getValueOf(warehouse.address)}</dd>
                        <dt className="col-6"></dt>
                        <dd className="col-6">{getValueOf(`${warehouse.wardcommune.name}, ${warehouse.district.name}, ${warehouse.cityprovince.name}`)}</dd>
                        {/*2-line manager info*/}
                        <dt className="col-6">Warehouse Manager</dt>
                        <dd className="col-6">{getValueOf(warehouse.headOfWarehouse)}</dd>
                        <dt className="col-6"></dt>
                        <dd className="col-6">{getValueOf(`(${warehouse.head_Of_Warehouse.firstName} ${warehouse.head_Of_Warehouse.lastName})`)}</dd>
                    </dl>
                    <ColoredLine color="black" />
                    <dl className="row p-1">
                        <dt className="col-6">Creator</dt>
                        <dd className="col-6">
                            {getValueOf(warehouse.creator) === account.userName ?
                                <span>You</span>
                                : getValueOf(warehouse.creator) !== 'N/A' ? <Link
                                    to={`${ROUTE_VIEW_USER_INFO}?userName=${getValueOf(warehouse.creator)}`}>
                                    {getValueOf(warehouse.creator)}
                                </Link> : 'N/A'}
                        </dd>
                        <dt className="col-6">Created Date</dt>
                        <dd className="col-6">{getValueOf(dateDisplayVietnamese(warehouse.createdDate))}</dd>
                        <dt className="col-6">Updater</dt>
                        <dd className="col-6">
                            {getValueOf(warehouse.updater) === account.userName ?
                                <span>You</span>
                                : getValueOf(warehouse.updater) !== 'N/A' ? <Link
                                    to={`${ROUTE_VIEW_USER_INFO}?userName=${getValueOf(warehouse.updater)}`}>
                                    {getValueOf(warehouse.updater)}
                                </Link> : 'N/A'}
                        </dd>
                        <dt className="col-6">Updated Date</dt>
                        <dd className="col-6">{getValueOf(dateDisplayVietnamese(warehouse.updatedDate))}</dd>
                    </dl>
                </CardBody>
            </Card>
        );
    }
}

export default WarehouseDetailCard;