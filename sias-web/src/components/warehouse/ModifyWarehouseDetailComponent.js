import React, { Component } from 'react';
import { Button, Label, Col, Row } from 'reactstrap';
import { Form, Control, Errors, actions } from 'react-redux-form';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {
    VALIDATION_MSG_INVALID_NAME,
    VALIDATION_MSG_REQUIRED
} from '../../shared/constants';
import {
    putModifyWarehouseDetail,
    fetchCityProvinceIdList,
    fetchDistrictIdList,
    fetchWardCommuneIdList,
    fetchDepartmentIdList,
    fetchWarehouseManagerIdList
} from '../../redux/actions/ActionCreators';
import {
    required,
    validWarehouseName
} from '../../shared/fieldValidation';
import FormLabel from '../common/FormLabel';

class ModifyWarehouseDetail extends Component {
    constructor(props) {
        super(props);
        const addresses = this.props.addresses;
        if (addresses.cityProvinceList.length === 0 && !addresses.isLoaded) {
            this.props.fetchCityProvinceIdList();
            this.props.fetchDistrictIdList();
            this.props.fetchWardCommuneIdList();
        }
        this.props.fetchDepartmentIdList();
        this.props.fetchWarehouseManagerIdList();

        this.toggleEditAddress = this.toggleEditAddress.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            selectedCityProvinceId: "",
            selectedDistrictId: "",
            selectedWardCommuneId: "",
            selectedDepartmentId: "",
            selectedManagerId: "",
            editAddress: false,
            firstLoad: true
        };
    }

    handleSubmit = (values) => {
        this.props.putModifyWarehouseDetail(values);
    }

    toggleEditAddress() {
        this.setState({
            editAddress: !this.state.editAddress
        });
    }

    mapCityProvinceToOption = () => {
        return this.props.addresses.cityProvinceList.map((cityProvinceObj) => (
            <option
                key={cityProvinceObj.cityProvinceId}
                value={cityProvinceObj.cityProvinceId}
            >
                {cityProvinceObj.name}
            </option>
        ));
    };

    mapDistrictToOption = () => {
        const districtList = this.props.addresses.districtList;
        let provinceId = "";

        provinceId = this.state.selectedCityProvinceId;

        if (provinceId != null && provinceId !== '') {
            return districtList
                .filter(dist => dist.cityProvinceId === parseInt(provinceId))
                .map((districtObj) => (
                    <option
                        key={districtObj.districtId}
                        value={districtObj.districtId}>
                        {districtObj.name}
                    </option>
                ));
        }
    }

    mapWardCommuneToOption = () => {
        const wardCommuneList = this.props.addresses.wardCommuneList;
        let districtId = "";

        districtId = this.state.selectedDistrictId;

        if (districtId != null && districtId !== '') {
            return wardCommuneList
                .filter(ward => ward.districtId === parseInt(districtId))
                .map((wardCommuneObj) => (
                    <option
                        key={wardCommuneObj.wardCommuneId}
                        value={wardCommuneObj.wardCommuneId}
                    >
                        {wardCommuneObj.name}
                    </option>
                ));
        }
    }

    addDepartmentOption = () => {
        const departmentList = this.props.addresses.departmentList;
        return departmentList.map((departmentObj) =>
            (<option
                key={departmentObj.departmentId}
                value={departmentObj.departmentId}>
                {departmentObj.name}
            </option>));
    }

    addManagerOption = () => {
        const managerList = this.props.addresses.managerList;

        return managerList.map((managerObj) =>
            (<option
                key={managerObj.userName}
                value={managerObj.userName}>
                {managerObj.userName}
            </option>));
    }

    handleCityProvinceChange = (event) => {
        this.setState({
            selectedCityProvinceId: event.target.value,
            selectedDistrictId: '',
            selectedWardCommuneId: '',
        });
    }

    handleDistrictChange = (event) => {
        this.setState({
            selectedDistrictId: event.target.value,
            selectedWardCommuneId: '',
        });
    }

    handleWardCommuneChange = (event) => {
        this.setState({
            selectedWardCommuneId: event.target.value
        });
    }

    handleDepartmentChange = (event) => {
        this.setState({
            selectedDepartmentId: event.target.value
        });
    }

    handleManagerChange = (event) => {
        this.setState({
            selectedManagerId: event.target.value
        });
    }

    isSaveButtonEnabled() {
        return (
            !this.state.editAddress
            || (this.state.selectedCityProvinceId !== ''
                && this.state.selectedDistrictId !== ''
                && this.state.selectedWardCommuneId !== '')
        );
    }

    render() {
        let warehouse = this.props.warehouse;

        if (warehouse == null) {
            return (<div></div>);
        }
        if (this.state.firstLoad) {
            this.props.setDefaultEditWarehouseDetail(warehouse);
            this.setState({
                firstLoad: false
            })
        }

        return (
            <Form model="editWarehouseDetail"
                onSubmit={(values) => this.handleSubmit(values)}
            >
                <Row className="form-group">
                    <Label htmlFor="warehouseId" md={3}>Warehouse Id</Label>
                    <Col md={9}>
                        <Control.text model=".warehouseId" id="warehouseId" name="warehouseId"
                            className="form-control"
                            value={warehouse.warehouseId}
                            disabled />

                    </Col>
                </Row>

                <Row className="form-group">
                    <Label htmlFor="name" md={3}>
                        <FormLabel text="Warehouse name" required='true' />
                    </Label>
                    <Col md={9}>
                        <Control.text model=".name" id="name" name="name"
                            className="form-control" validators={{ required, validWarehouseName }} />
                        <Errors className="text-danger" model=".name" show="touched"
                            messages={{
                                required: VALIDATION_MSG_REQUIRED,
                                validWarehouseName: VALIDATION_MSG_INVALID_NAME
                            }} />
                    </Col>
                </Row>

                <Row className="form-group">
                    <Label htmlFor="parentDepartmentId" md={3}>
                        <FormLabel text="Department" required='true' />
                    </Label>
                    <Col md={9}>
                        <Control.select
                            model=".parentDepartmentId"
                            id="parentDepartmentId"
                            name="parentDepartmentId"
                            className="form-control"
                            validators={{ required }}
                            onChange={this.handleDepartmentChange}
                        >
                            <option key='' value=''>
                                -Department-
                                        </option>
                            {this.addDepartmentOption()}
                        </Control.select>
                        <Errors className="text-danger" model=".parentDepartmentId" show="touched"
                            messages={{
                                required: VALIDATION_MSG_REQUIRED
                            }} />
                    </Col>
                </Row>

                <Row className="form-group">
                    <Label htmlFor="headOfWarehouse" md={3}>
                        <FormLabel text="Warehouse Manager" required='true' />
                    </Label>
                    <Col md={9}>
                        <Control.select
                            model=".headOfWarehouse"
                            id="headOfWarehouse"
                            name="headOfWarehouse"
                            className="form-control"
                            validators={{ required }}
                            onChange={this.handleManagerChange}
                        >
                            <option key='' value=''>
                                -Warehouse Manager-
                                        </option>
                            {this.addManagerOption()}
                        </Control.select>
                        <Errors className="text-danger" model=".headOfWarehouse" show="touched"
                            messages={{
                                required: VALIDATION_MSG_REQUIRED
                            }} />
                    </Col>
                </Row>

                <Row className="form-group">
                    <Col md={9}>
                        <div className="form-check">
                            <Label check>
                                <Control.checkbox model=".editAddress" name="editAddress" id="editAddress"
                                    className="form-check-input"
                                    defaultChecked={false}
                                    onClick={this.toggleEditAddress} />
                                <strong>Edit address? &nbsp;</strong>
                            </Label>
                        </div>
                    </Col>
                </Row>
                {!this.state.editAddress &&
                    <React.Fragment>
                        <Row className="form-group">
                            <Label htmlFor="cityprovince.name" md={3}>City/Province</Label>
                            <Col md={9}>
                                <Control.text model=".cityprovince.name" id="cityprovince.name" name="cityprovince.name"
                                    className="form-control"
                                    value={warehouse.cityprovince.name}
                                    disabled />

                            </Col>
                        </Row>
                        <Row className="form-group">
                            <Label htmlFor="district.name" md={3}>District</Label>
                            <Col md={9}>
                                <Control.text model=".district.name" id="district.name" name="district.name"
                                    className="form-control"
                                    value={warehouse.district.name}
                                    disabled />

                            </Col>
                        </Row>
                        <Row className="form-group">
                            <Label htmlFor="wardcommune.name" md={3}>Ward/Commune</Label>
                            <Col md={9}>
                                <Control.text model=".wardcommune.name" id="wardcommune.name" name="wardcommune.name"
                                    className="form-control"
                                    value={warehouse.wardcommune.name}
                                    disabled />

                            </Col>
                        </Row>
                        <Row className="form-group">
                            <Label htmlFor="address" md={3}>Address</Label>
                            <Col md={9}>
                                <Control.text model=".address" id="address" name="address"
                                    className="form-control"
                                    value={warehouse.address}
                                    disabled />
                            </Col>
                        </Row>
                    </React.Fragment>
                }

                {this.state.editAddress &&
                    <React.Fragment>
                        <Row className="form-group">
                            <Label htmlFor="newWarehouseCityProvinceId" md={3}>
                                <FormLabel text="City/Province" required='true' />
                            </Label>
                            <Col md={9}>
                                <Control.select
                                    model=".newWarehouseCityProvinceId"
                                    id="newWarehouseCityProvinceId"
                                    name="newWarehouseCityProvinceId"
                                    className="form-control"
                                    validators={{ required }}
                                    onChange={this.handleCityProvinceChange}
                                >
                                    <option key='' value=''>
                                        -City/Province-
                                    </option>
                                    {this.mapCityProvinceToOption()}
                                </Control.select>
                                <Errors className="text-danger" model=".newWarehouseCityProvinceId" show="touched"
                                    messages={{
                                        required: VALIDATION_MSG_REQUIRED
                                    }} />
                            </Col>
                        </Row>

                        <Row className="form-group">
                            <Label htmlFor="newWarehouseDistrictId" md={3}>
                                <FormLabel text="District" required='true' />
                            </Label>
                            <Col md={9}>
                                <Control.select
                                    model=".newWarehouseDistrictId"
                                    id="newWarehouseDistrictId"
                                    name="newWarehouseDistrictId"
                                    className="form-control"
                                    validators={{ required }}
                                    onChange={this.handleDistrictChange}
                                >
                                    <option key='' value=''>
                                        -District-
                                    </option>
                                    {this.mapDistrictToOption()}
                                </Control.select>
                                <Errors className="text-danger" model=".newWarehouseDistrictId" show="touched"
                                    messages={{
                                        required: VALIDATION_MSG_REQUIRED
                                    }} />
                            </Col>
                        </Row>

                        <Row className="form-group">
                            <Label htmlFor="newWarehouseWardCommuneId" md={3}>
                                <FormLabel text="Ward/Commune" required='true' />
                            </Label>
                            <Col md={9}>
                                <Control.select
                                    model=".newWarehouseWardCommuneId"
                                    id="newWarehouseWardCommuneId"
                                    name="newWarehouseWardCommuneId"
                                    className="form-control"
                                    validators={{ required }}
                                    onChange={this.handleWardCommuneChange}
                                >
                                    <option key='' value=''>
                                        -Ward/Commune-
                                    </option>
                                    {this.mapWardCommuneToOption()}
                                </Control.select>
                                <Errors className="text-danger" model=".newWarehouseWardCommuneId" show="touched"
                                    messages={{
                                        required: VALIDATION_MSG_REQUIRED
                                    }} />
                            </Col>
                        </Row>

                        <Row className="form-group">
                            <Label htmlFor="newWarehouseAddress" md={3}>
                                <FormLabel text="Address" required='true' />
                            </Label>
                            <Col md={9}>
                                <Control.text model=".newWarehouseAddress" id="newWarehouseAddress" name="newWarehouseAddress"
                                    className="form-control" validators={{ required }} />
                                <Errors className="text-danger" model=".newWarehouseAddress" show="touched"
                                    messages={{
                                        required: VALIDATION_MSG_REQUIRED
                                    }} />
                            </Col>
                        </Row>
                    </React.Fragment>
                }



                <Row className="form-group">
                    <Col>
                        <Button disabled={!this.isSaveButtonEnabled()}
                            color="warning"
                            type="submit"
                            className="submitButton"
                        >
                            Save
                            </Button>

                        &nbsp;
                        <Button color="light"
                            className="backButton"
                            onClick={this.props.toggleEdit}
                        >
                            Back
                        </Button>
                    </Col>
                </Row>
            </Form>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        successStatus: state.warehouseDetail.successStatus,
        addresses: state.addresses
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        fetchCityProvinceIdList: () => { dispatch(fetchCityProvinceIdList()) },
        fetchDistrictIdList: () => { dispatch(fetchDistrictIdList()) },
        fetchWardCommuneIdList: () => { dispatch(fetchWardCommuneIdList()) },
        fetchDepartmentIdList: () => { dispatch(fetchDepartmentIdList()) },
        fetchWarehouseManagerIdList: () => { dispatch(fetchWarehouseManagerIdList()) },
        setDefaultEditWarehouseDetail: (values) => { dispatch(actions.merge("editWarehouseDetail", values)) },
        putModifyWarehouseDetail: (values) => { dispatch(putModifyWarehouseDetail(values)) }
    });
}



export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ModifyWarehouseDetail));