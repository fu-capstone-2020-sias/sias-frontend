import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, Col } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import { STOCK_PRODUCT_STATUS_BAD, STOCK_PRODUCT_STATUS_BAD_TEXT, STOCK_PRODUCT_STATUS_MEDIUM_TEXT, STOCK_PRODUCT_STATUS_GOOD_TEXT, STOCK_PRODUCT_STATUS_GOOD, STOCK_PRODUCT_STATUS_MEDIUM, STOCK_PRODUCT_PRICE_MIN, STOCK_PRODUCT_PRICE_MAX, STOCK_PRODUCT_LENGTH_MIN, STOCK_PRODUCT_LENGTH_MAX } from '../../shared/constants';
import { connect } from 'react-redux';
import { InitialSearchStockProductList } from '../../redux/reducers/forms';

class StockProductListSearchForm extends Component {
    constructor(props) {
        super(props);        
        this.handleSearch = this.handleSearch.bind(this);
        this.toggleSearchByDate = this.toggleSearchByDate.bind(this);

        const searchData = (this.props.stockProducts.searchData != null) ? this.props.stockProducts.searchData : InitialSearchStockProductList;
        this.state = {
            searchByDate: (searchData.searchByDate === 1)
        }
    }

    mapSteelTypeToOption = () => {
        return this.props.steelInfos.steelTypeList.map((steelTypeObj) => (
            <option
                key={steelTypeObj.steeltypeId}
                value={steelTypeObj.steeltypeId}
            >
                {steelTypeObj.name}
            </option>
        ));
    };

    mapWarehouseToOption = () => {
        return this.props.warehouseSelections.selectionList.map((warehouseObj) => (
            <option
                key={warehouseObj.warehouseId}
                value={warehouseObj.warehouseId}
            >
                {warehouseObj.name}
            </option>
        ));
    };

    toggleSearchByDate() {
        this.setState({
            searchByDate: !this.state.searchByDate
        });
    }

    handleSearch(event) {
        const fromPrice = this.fromPrice.value === '' ? STOCK_PRODUCT_PRICE_MIN : this.fromPrice.value;
        const toPrice = this.toPrice.value === '' ? STOCK_PRODUCT_PRICE_MAX : this.toPrice.value;
        const fromLength = this.fromLength.value === '' ? STOCK_PRODUCT_LENGTH_MIN : this.fromLength.value;
        const toLength = this.toLength.value === '' ? STOCK_PRODUCT_LENGTH_MAX : this.toLength.value;
        let searchData = {
            stockProductId: this.stockProductId.value,
            sku: this.sku.value,
            barcode: this.barcode.value,
            name: this.productName.value,
            fromPrice: fromPrice,
            toPrice: toPrice,
            fromLength: fromLength,
            toLength: toLength,
            searchByDate: this.searchByDate.checked ? 1 : 0
        }
        if (this.searchByDate.checked) {
            searchData.fromDate = this.fromDate.value;
            searchData.toDate = this.toDate.value;
        }
        if (this.status.value !== 'All') {
            searchData.statusId = this.status.value;
        }
        if (this.steelTypeId.value !== 'All') {
            searchData.type = this.steelTypeId.value;
        }
        if (this.warehouseId.value !== 'All') {
            searchData.warehouseId = this.warehouseId.value;
        }

        this.props.fetchStockProductSearch(1, searchData);
        event.preventDefault();
    }

    render() {
        const searchData = (this.props.stockProducts.searchData != null) ? this.props.stockProducts.searchData : InitialSearchStockProductList;
        return (
            <Form
                innerRef={form => this.form = form}
                onSubmit={this.handleSearch}
            >
                <FormGroup row>
                    <Label sm={3} htmlFor="stockProductId">Stock product ID</Label>
                    <Col sm={9}>
                        <Input type="text" id="stockProductId" name="stockProductId"
                            pattern='[0-9]+' title="Stock product ID should only contain digits 0-9 only"
                            placeholder='Ex: 3, 12, 0242,...'
                            defaultValue={searchData.stockProductId}
                            innerRef={(input) => this.stockProductId = input} />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label sm={3} htmlFor="sku">SKU</Label>
                    <Col sm={9}>
                        <Input type="text" id="sku" name="sku"
                            defaultValue={searchData.sku}
                            innerRef={(input) => this.sku = input} />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label sm={3} htmlFor="barcode">Bar code</Label>
                    <Col sm={9}>
                        <Input type="text" id="barcode" name="barcode"
                            pattern='[0-9]+' title="Bar code should only contain digits 0-9 only"
                            placeholder='Ex: 3, 12, 0242,...'
                            defaultValue={searchData.barcode}
                            innerRef={(input) => this.barcode = input} />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label sm={3} htmlFor="productName">Product name</Label>
                    <Col sm={9}>
                        <Input type="text" id="productName" name="productName"
                            defaultValue={searchData.productName}
                            innerRef={(input) => this.productName = input} />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label sm={3}>Price (VND):</Label>
                    <Label sm={3} md={1} htmlFor="fromPrice">From</Label>
                    <Col sm={6} md={3}>
                        <Input type="number" min={STOCK_PRODUCT_PRICE_MIN} max={STOCK_PRODUCT_PRICE_MAX}
                            id="fromPrice" name="fromPrice"
                            title="Price should be between 0 and 9,999,999,999"
                            // placeholder='Ex: 3, 12, 0242,...'
                            defaultValue={searchData.fromPrice}
                            innerRef={(input) => this.fromPrice = input} />
                    </Col>
                    <Label sm={3} md={1} className="offset-sm-3 offset-md-0" htmlFor="toPrice">to</Label>
                    <Col sm={6} md={3}>
                        <Input type="number" min={STOCK_PRODUCT_PRICE_MIN} max={STOCK_PRODUCT_PRICE_MAX}
                            id="toPrice" name="toPrice"
                            title="Price should be between 0 and 9,999,999,999"
                            // placeholder='Ex: 3, 12, 0242,...'
                            defaultValue={searchData.toPrice}
                            innerRef={(input) => this.toPrice = input} />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label sm={3}>Length (mm):</Label>
                    <Label sm={3} md={1} htmlFor="fromLength">From</Label>
                    <Col sm={6} md={3}>
                        <Input type="number" min={STOCK_PRODUCT_LENGTH_MIN} max={STOCK_PRODUCT_LENGTH_MAX}
                            id="fromLength" name="fromLength"
                            title="Length should be between 0 and 99,999"
                            // placeholder='Ex: 3, 12, 0242,...'
                            defaultValue={searchData.fromLength}
                            innerRef={(input) => this.fromLength = input} />
                    </Col>
                    <Label sm={3} md={1} className="offset-sm-3 offset-md-0" htmlFor="toLength">to</Label>
                    <Col sm={6} md={3}>
                        <Input type="number" min={STOCK_PRODUCT_LENGTH_MIN} max={STOCK_PRODUCT_LENGTH_MAX}
                            id="toLength" name="toLength"
                            title="Length should be between 0 and 99,999"
                            // placeholder='Ex: 3, 12, 0242,...'
                            defaultValue={searchData.toLength}
                            innerRef={(input) => this.toLength = input} />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="warehouse" sm={3}>Warehouse</Label>
                    <Col sm={6} lg={4}>
                        <Input type="select" name="warehouse" id="warehouse"
                            defaultValue={(searchData.warehouseId != null) ? searchData.warehouseId : 'All'}
                            innerRef={(input) => this.warehouseId = input}>
                            <option value="All">All</option>
                            {this.mapWarehouseToOption()}
                        </Input>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="steelType" sm={3}>Type</Label>
                    <Col sm={6} lg={4}>
                        <Input type="select" name="steelType" id="steelType"
                            defaultValue={(searchData.steelTypeId != null) ? searchData.steelTypeId : 'All'}
                            innerRef={(input) => this.steelTypeId = input}>
                            <option value="All">All</option>
                            {this.mapSteelTypeToOption()}
                        </Input>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="status" sm={3}>Status</Label>
                    <Col sm={6} lg={4}>
                        <Input type="select" name="status" id="status"
                            defaultValue={(searchData.statusId != null) ? searchData.statusId : 'All'}
                            innerRef={(input) => this.status = input}>
                            <option value="All">All</option>
                            <option value={STOCK_PRODUCT_STATUS_GOOD}>{STOCK_PRODUCT_STATUS_GOOD_TEXT}</option>
                            <option value={STOCK_PRODUCT_STATUS_MEDIUM}>{STOCK_PRODUCT_STATUS_MEDIUM_TEXT}</option>
                            <option value={STOCK_PRODUCT_STATUS_BAD}>{STOCK_PRODUCT_STATUS_BAD_TEXT}</option>
                        </Input>
                    </Col>
                </FormGroup>

                <FormGroup check>
                    <Label check>
                        <Input type="checkbox" name="searchByDate" id="searchByDate"
                            defaultChecked={this.state.searchByDate}
                            innerRef={(input) => this.searchByDate = input}
                            onClick={this.toggleSearchByDate} />
                        <b>Search by manufactured date</b>
                    </Label>
                </FormGroup>

                {this.state.searchByDate &&
                    <FormGroup row>
                        <Label className="offset-sm-3" sm={2} htmlFor="fromDate">From:</Label>
                        <Col sm={7}>
                            <Input type="date" id="fromDate" name="fromDate"
                                required={true}
                                defaultValue={searchData.fromDate}
                                innerRef={(input) => this.fromDate = input}
                            />
                        </Col>
                        <Label className="offset-sm-3" sm={2} htmlFor="toDate">To:</Label>
                        <Col sm={7}>
                            <Input type="date" id="toDate" name="toDate"
                                required={true}
                                defaultValue={searchData.toDate}
                                innerRef={(input) => this.toDate = input}
                            />
                        </Col>
                    </FormGroup>
                }
                {!this.state.searchByDate && <br />}
                <FormGroup row>
                    <Col xs={4} sm={2}>
                        <Button
                            type="submit"
                            color="warning">Search</Button>

                    </Col>
                </FormGroup>
            </Form>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        stockProducts: state.stockProducts,
        steelInfos: state.steelInfos,
        warehouseSelections: state.warehouseSelections
    };
}

export default withRouter(connect(mapStateToProps)(StockProductListSearchForm));