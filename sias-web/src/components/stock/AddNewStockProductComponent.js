import React from 'react';
import { Button, Col, Row, Breadcrumb, BreadcrumbItem, Card, CardText, CardTitle } from 'reactstrap';
import { Link, withRouter } from 'react-router-dom';
import { ROUTE_HOME, ROUTE_ADD_NEW_STOCK_PRODUCT_FORM, ROUTE_IMPORT_STOCK_PRODUCT_BATCH, ROLE_OFFICE_MANAGER, ROLE_FACTORY_MANAGER, ROUTE_UPLOAD_STOCK_PRODUCT_IMAGE } from '../../shared/constants';
import { connect } from 'react-redux';
import UnauthorizedAccessDisplay from '../common/UnauthorizedAccessDisplay';

function AddNewStockProduct(props) {
    const account = props.loginAccount.account;
    if (account == null) {
        return (
            <div className="container">
                <div className="row row-content">
                    <h1>You must login first</h1>
                </div>
            </div>
        );
    } else if (roleAccessible(account.roleId)) {
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                        <BreadcrumbItem>Stock Management</BreadcrumbItem>
                        <BreadcrumbItem active>Add New Stock Product</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>Add New Stock Product</h3>
                        <hr />
                    </div>
                </div>
                <div className="row row-content d-flex justify-content-center">
                    <Row>
                        <Col sm="4">
                            <Card body>
                                <CardTitle tag="h5">Add stock product by using a standard form</CardTitle>
                                <CardText>Quickly add a new stock product with an easy-to-use form.</CardText>
                                <Button color='warning' tag={Link} to={ROUTE_ADD_NEW_STOCK_PRODUCT_FORM}>Use Form</Button>
                            </Card>
                        </Col>
                        <Col sm="4">
                            <Card body>
                                <CardTitle tag="h5">Add stock product by uploading a CSV File</CardTitle>
                                <CardText>Add multiple stock products simultaneously by using a standard CSV File.</CardText>
                                <Button color='warning' tag={Link} to={ROUTE_IMPORT_STOCK_PRODUCT_BATCH}>Use CSV File</Button>
                            </Card>
                        </Col>
                        <Col sm="4">
                            <Card body>
                                <CardTitle tag="h5">Add stock product by uploading images (OCR)</CardTitle>
                                <CardText>Quickly add a new stock product with just a scanned image of product's label.</CardText>
                                <Button color='warning' tag={Link} to={ROUTE_UPLOAD_STOCK_PRODUCT_IMAGE}>Upload Images</Button>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    } else {
        return (
            <UnauthorizedAccessDisplay />
        );
    }
}

const roleAccessible = (roleId) => {
    return roleId === ROLE_OFFICE_MANAGER || roleId === ROLE_FACTORY_MANAGER;
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount
    };
}

export default withRouter(connect(mapStateToProps)(AddNewStockProduct));