import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Button } from 'reactstrap';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { connect } from 'react-redux';
import { Loading } from '../common/LoadingComponent';
import {
    ROLE_OFFICE_MANAGER,
    ROLE_FACTORY_MANAGER,
    SUCCESS_MESSAGE_DELETE_GENERAL,
    ROUTE_VIEW_STOCK_PRODUCT_LIST,
    ROUTE_STOCK_PRODUCT_DETAILS,
    ROUTE_HOME,
    ROUTE_WAREHOUSE_DETAILS,
    ERROR_PAGE_NOT_FOUND
} from '../../shared/constants';
import { getValueOf, dateDisplayVietnamese, formatterVND, formatterLengthUnits } from '../../shared/utils';
import { StyledTableCell } from '../common/StyledTableCell';
import TablePaging from '../common/TablePaging';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import {
    fetchStockProduct,
    deleteStockProducts,
    fetchSteelTypeList,
    exportStockBatch,
    fetchWarehouseSelectionList
} from '../../redux/actions/ActionCreators';
import StockProductStatusDisplay from '../common/StockProductStatusDisplay';
import StockProductListSearchForm from './ViewStockProductListSearchForm';
import Checkbox from "@material-ui/core/Checkbox";
import UnauthorizedAccessDisplay from '../common/UnauthorizedAccessDisplay';
import ErrorDisplay from '../common/ErrorDisplay';

const useStyles = makeStyles({
    table: {
        minWidth: 600,
    }
});

const headCells = [
    { id: 'id', numeric: true, disablePadding: false, label: 'ID' },
    { id: 'name', numeric: false, disablePadding: false, label: 'Name' },
    { id: 'sku', numeric: false, disablePadding: false, label: 'SKU' },
    { id: 'barcode', numeric: false, disablePadding: false, label: 'Bar code' },
    { id: 'type', numeric: false, disablePadding: false, label: 'Type' },
    { id: 'length', numeric: false, disablePadding: false, label: 'Length' },
    { id: 'manufacturedDate', numeric: false, disablePadding: false, label: 'Manufactured Date' },
    { id: 'warehouse', numeric: false, disablePadding: false, label: 'Warehouse' },
    { id: 'status', numeric: false, disablePadding: false, label: 'Status' },
    { id: 'price', numeric: false, disablePadding: false, label: 'Price' }
];

function StockProductListTableHead(props) {
    return (
        <TableHead>
            <TableRow>
                <StyledTableCell padding="checkbox">
                    {/* <Checkbox
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={rowCount > 0 && numSelected === rowCount}
                        onChange={onSelectAllClick}
                        inputProps={{ 'aria-label': 'select all' }}
                    /> */}
                </StyledTableCell>
                {headCells.map((headCell) => (
                    <StyledTableCell
                        key={headCell.id}
                        align={headCell.numeric ? 'right' : 'left'}
                        padding={headCell.disablePadding ? 'none' : 'default'}
                    >
                        {headCell.label}
                    </StyledTableCell>
                ))}
                <StyledTableCell></StyledTableCell>
                {roleDeleteable(props.account.roleId) &&
                    <StyledTableCell></StyledTableCell>
                }
            </TableRow>
        </TableHead>
    );
}

function StockProductListTable(props) {
    const classes = useStyles();
    const [selected, setSelected] = React.useState([]);
    let rows = props.stockProductList;
    const account = props.account;

    const handleClick = (event, row) => {
        const id = row.stockProductId;
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }
        setSelected(newSelected);
    };

    const isSelected = (stockProductId) => selected.indexOf(stockProductId) !== -1;

    const handleDelete = (stockProductId, stockProductName) => {
        confirmAlert({
            title: 'Confirm',
            message: `Are you sure to delete this stock product "${stockProductName}" (ID: ${stockProductId})? This will refresh your browser to take effects.`,
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => {
                        props.deleteStockProducts(stockProductId);
                    }
                },
                {
                    label: 'No'
                }
            ]
        });
    }

    const handleExportStockBatch = function () {
        if (selected.length > 0) {
            confirmAlert({
                title: 'Confirm to submit',
                message: `Are you sure to export these stock products (ID: ${selected.toString()})? This will refresh your browser to take effects.`,
                buttons: [
                    {
                        label: 'Yes',
                        onClick: async () => {
                            // alert("Export Stock Batch");
                            await props.exportStockBatch(selected.toString());
                            setSelected([]);
                        }
                    },
                    {
                        label: 'No'
                    }
                ]
            })
        } else {
            confirmAlert({
                title: 'Information',
                message: `Please select at least 1 stock product.`,
                buttons: [
                    {
                        label: 'OK',
                    }
                ]
            })
        }
    }

    const getWarehouseLink = (row) => {
        if (row.warehouse != null && row.warehouse.warehouseId != null) {
            return <Link to={`${ROUTE_WAREHOUSE_DETAILS}/${row.warehouse.warehouseId}`} target='_blank'>{getValueOf(row.warehouse.name)}</Link>
        }
        return getValueOf(row.warehouse.name);
    }

    const getLastTableColumn = (account, row) => {
        const roleId = account.roleId;
        const stockProductId = row.stockProductId;
        const stockProductName = row.name;
        const headOfWarehouse = row.warehouse.headOfWarehouse;
        if (roleDeleteable(roleId)) {
            if (account.userName === headOfWarehouse) {
                return (
                    <StyledTableCell width="5%">
                        <Link to='#' onClick={() => handleDelete(stockProductId, stockProductName)}>Delete</Link>
                    </StyledTableCell>
                );
            } else {
                return <StyledTableCell width="5%"></StyledTableCell>;
            }
        } else {
            return null;
        }
    }

    return (
        <React.Fragment>
            <div className="col-12">
                <h5>Number of entries: {props.totalEntries}</h5>
            </div>
            <div className="col-12">
                <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="customized table">
                        <StockProductListTableHead
                            classes={classes}
                            numSelected={selected.length}
                            rowCount={rows.length}
                            account={account}
                        />
                        <TableBody>
                            {rows.map((row) => {
                                const isItemSelected = isSelected(row.stockProductId);
                                return (
                                    <TableRow key={row.stockProductId}
                                        onClick={(event) => handleClick(event, row)}
                                        role="checkbox"
                                        aria-checked={isItemSelected}
                                        selected={isItemSelected}
                                    >
                                        {(account.roleId === ROLE_FACTORY_MANAGER
                                            || account.roleId === ROLE_OFFICE_MANAGER) ?
                                            <StyledTableCell padding="checkbox">
                                                <Checkbox
                                                    checked={isItemSelected}
                                                />
                                            </StyledTableCell> :
                                            <StyledTableCell></StyledTableCell>
                                        }
                                        <StyledTableCell width="5%"
                                            align="right">{getValueOf(row.stockProductId)}</StyledTableCell>
                                        <StyledTableCell width="15%">{getValueOf(row.name)}</StyledTableCell>
                                        <StyledTableCell width="10%">{getValueOf(row.sku)}</StyledTableCell>
                                        <StyledTableCell width="10%">{getValueOf(row.barcode)}</StyledTableCell>
                                        <StyledTableCell width="5%">{getValueOf(row.steeltype.name)}</StyledTableCell>
                                        <StyledTableCell
                                            width="10%">{getValueOf(formatterLengthUnits.format(row.length))}</StyledTableCell>
                                        <StyledTableCell
                                            width="10%">{getValueOf(dateDisplayVietnamese(row.manufacturedDate))}</StyledTableCell>
                                        <StyledTableCell width="10%">{getWarehouseLink(row)}</StyledTableCell>
                                        <StyledTableCell width="10%"><StockProductStatusDisplay
                                            status={getValueOf(row.stockproductstatus.name)} /></StyledTableCell>
                                        <StyledTableCell
                                            width="10%">{getValueOf(formatterVND.format(row.price))}</StyledTableCell>
                                        <StyledTableCell width="5%">
                                            <Link
                                                to={`${ROUTE_STOCK_PRODUCT_DETAILS}/${row.stockProductId}`}>Details</Link>
                                        </StyledTableCell>
                                        {getLastTableColumn(account, row)}
                                    </TableRow>
                                )
                            }
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
                <br />
                <TablePaging numberOfPages={props.numberOfPages}
                    currentPage={props.currentPage}
                    linkTo={ROUTE_VIEW_STOCK_PRODUCT_LIST} />
            </div>
            {(props.totalEntries > 0) && (account.roleId === ROLE_FACTORY_MANAGER
                || account.roleId === ROLE_OFFICE_MANAGER) &&
                <div className="col-12 col-sm-6 col-lg-4">
                    <Button style={{ marginBottom: "40px" }} color="warning" onClick={handleExportStockBatch}>Export Stock Product(s) to CSV</Button>
                </div>
            }
        </React.Fragment>
    );
}

StockProductListTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
    // onSelectAllClick: PropTypes.func.isRequired,
    rowCount: PropTypes.number.isRequired
};

class ViewStockProductList extends Component {
    constructor(props) {
        super(props);
        const steelInfos = this.props.steelInfos;
        const warehouseSelections = this.props.warehouseSelections;
        if (steelInfos.steelTypeList.length === 0 && !steelInfos.isLoaded) {
            this.props.fetchSteelTypeList();
        }
        if (warehouseSelections.selectionList.length === 0 && !warehouseSelections.isSteelTypeLoaded) {
            this.props.fetchWarehouseSelectionList();
        }
    }

    componentDidMount() {
        const account = this.props.loginAccount.account;
        if (null != account && roleAccessible(account.roleId)) {
            const stockProductList = this.props.stockProducts.stockProducts;
            const currentPageNumber = this.props.stockProducts.currentPageNumber;
            const numberOfPages = this.props.stockProducts.numberOfPages;
            let pageNumber = this.props.pageNumber;
            const searchData = this.props.stockProducts.searchData;
            const errMess = this.props.stockProducts.errMess;
            if (errMess != null) {
                return;
            } else if (null == stockProductList || (pageNumber !== currentPageNumber && pageNumber <= numberOfPages)) {
                if (searchData != null) {
                    this.props.fetchStockProductSearch(pageNumber, searchData);
                } else {
                    this.props.fetchStockProduct(pageNumber);
                }

            }
        }
    }

    render() {
        const account = this.props.loginAccount.account;
        if (account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first</h1>
                    </div>
                </div>
            );
        } else if (roleAccessible(account.roleId)) {
            const stockProductList = this.props.stockProducts.stockProducts;
            const totalEntries = this.props.stockProducts.totalEntries;
            const numberOfPages = this.props.stockProducts.numberOfPages;
            const currentPageNumber = this.props.stockProducts.currentPageNumber;

            // console.log(currentPageNumber + '/' + numberOfPages);

            const deleteSuccessStatus = this.props.stockProducts.deleteSuccessStatus;
            const errMess = this.props.stockProducts.errMess;
            const isLoading = this.props.stockProducts.isLoading;

            if (currentPageNumber > numberOfPages && numberOfPages > 0) {
                return (
                    <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
                )
            }

            return (
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                            <BreadcrumbItem>Stock Management</BreadcrumbItem>
                            <BreadcrumbItem active>View Stock Product List</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>View Stock Product List</h3>
                            <hr />
                        </div>
                    </div>
                    {isLoading ?
                        <Loading /> :
                        <div className="row">
                            <div className="col-12">
                                <h4>Search</h4>
                            </div>
                            <div className="col-12 col-md-10 col-lg-9 col-xl-8">
                                <StockProductListSearchForm fetchStockProductSearch={this.props.fetchStockProductSearch} />
                            </div>
                            <div className="col-12 mt-3">
                                <h5 className="text-success">{deleteSuccessStatus ? SUCCESS_MESSAGE_DELETE_GENERAL : ''}</h5>
                                <h5 className="text-danger">{errMess != null ? errMess : ''}</h5>
                                <div className="row">
                                    <StockProductListTable
                                        stockProductList={(stockProductList == null) ? [] : stockProductList}
                                        numberOfPages={numberOfPages}
                                        currentPage={currentPageNumber}
                                        totalEntries={totalEntries}
                                        account={account}
                                        deleteStockProducts={this.props.deleteStockProducts}
                                        exportStockBatch={this.props.exportStockBatch}
                                    />
                                </div>
                            </div>
                        </div>
                    }
                </div>
            );
        } else {
            return (
                <UnauthorizedAccessDisplay />
            );
        }
    }
}

const roleAccessible = (roleId) => {
    return roleId === ROLE_OFFICE_MANAGER || roleId === ROLE_FACTORY_MANAGER;
}

const roleDeleteable = (roleId) => {
    return roleId === ROLE_OFFICE_MANAGER;
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        stockProducts: state.stockProducts,
        addresses: state.addresses,
        steelInfos: state.steelInfos,
        warehouseSelections: state.warehouseSelections,
        exportStockBatchs: state.exportStockBatch,
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        fetchStockProduct: (pageNumber) => {
            dispatch(fetchStockProduct(pageNumber))
        },
        fetchStockProductSearch: (pageNumber, searchData) => {
            dispatch(fetchStockProduct(pageNumber, searchData))
        },
        deleteStockProducts: (stockProductId) => {
            dispatch(deleteStockProducts(stockProductId))
        },
        fetchSteelTypeList: () => {
            dispatch(fetchSteelTypeList())
        },
        fetchWarehouseSelectionList: () => {
            dispatch(fetchWarehouseSelectionList())
        },
        exportStockBatch: (idList) => {
            dispatch(exportStockBatch(idList));
        }
    });
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ViewStockProductList));