import React from 'react';
import {Card, CardBody, CardHeader} from 'reactstrap';
import {getValueOf, dateDisplayVietnamese, formatterVND, formatterLengthUnits} from '../../shared/utils';
import StockProductStatusDisplay from '../common/StockProductStatusDisplay';
import {ROUTE_VIEW_USER_INFO, ROUTE_WAREHOUSE_DETAILS} from '../../shared/constants';
import {Link, withRouter} from 'react-router-dom';

const ColoredLine = ({color}) => (
    <hr
        style={{
            color: color,
            backgroundColor: color
        }}
    />
);

function StockProductDetailCard(props) {
    const stockProduct = props.stockProduct;
    const account = props.account;

    if (stockProduct == null) return (<div></div>);
    else {
        return (
            <Card>
                <CardHeader className="bg-warning text-black"><b>Stock Product Details</b></CardHeader>
                <CardBody>
                    <dl className="row p-1">
                        <dt className="col-6">Name</dt>
                        <dd className="col-6">{getValueOf(stockProduct.name)}</dd>
                        <dt className="col-6">Stock Product ID</dt>
                        <dd className="col-6">{getValueOf(stockProduct.stockProductId)}</dd>
                        <dt className="col-6">SKU</dt>
                        <dd className="col-6">{getValueOf(stockProduct.sku)}</dd>
                        <dt className="col-6">Bar Code</dt>
                        <dd className="col-6">{getValueOf(stockProduct.barcode)}</dd>
                        <dt className="col-6">Steel Type</dt>
                        <dd className="col-6">{getValueOf(stockProduct.steeltype.name)}</dd>
                        <dt className="col-6">Stock Product Group</dt>
                        <dd className="col-6">{getValueOf(stockProduct.stockproductgroup.name)}</dd>
                        <dt className="col-6">Standard</dt>
                        <dd className="col-6">{getValueOf(stockProduct.stockproductstandard.name)}</dd>
                        <dt className="col-6">Status</dt>
                        <dd className="col-6"><StockProductStatusDisplay
                            status={getValueOf(stockProduct.stockproductstatus.name)}/></dd>
                        <dt className="col-6">Price</dt>
                        <dd className="col-6">{getValueOf(formatterVND.format(stockProduct.price))}</dd>
                        <dt className="col-6">Length</dt>
                        <dd className="col-6">{getValueOf(formatterLengthUnits.format(stockProduct.length))}</dd>
                        <dt className="col-6">Manufactured Date</dt>
                        <dd className="col-6">{getValueOf(dateDisplayVietnamese(stockProduct.manufacturedDate))}</dd>
                        <dt className="col-6">Warehouse</dt>
                        <dd className="col-6">{getWarehouseLink(stockProduct)}</dd>
                    </dl>
                    <ColoredLine color="black"/>
                    <dl className="row p-1">
                        <dt className="col-6">Creator</dt>
                        <dd className="col-6">
                            {getValueOf(stockProduct.creator) === account.userName ?
                                <span>You</span>
                                : getValueOf(stockProduct.creator) !== 'N/A' ? <Link
                                    to={`${ROUTE_VIEW_USER_INFO}?userName=${getValueOf(stockProduct.creator)}`}>
                                    {getValueOf(stockProduct.creator)}
                                </Link> : 'N/A'}
                        </dd>
                        <dt className="col-6">Created Date</dt>
                        <dd className="col-6">{getValueOf(dateDisplayVietnamese(stockProduct.createdDate))}</dd>
                        <dt className="col-6">Updater</dt>
                        <dd className="col-6">
                            {getValueOf(stockProduct.updater) === account.userName ?
                                <span>You</span>
                                : getValueOf(stockProduct.updater) !== 'N/A' ? <Link
                                    to={`${ROUTE_VIEW_USER_INFO}?userName=${getValueOf(stockProduct.updater)}`}>
                                    {getValueOf(stockProduct.updater)}
                                </Link> : 'N/A'}
                        </dd>
                        <dt className="col-6">Updated Date</dt>
                        <dd className="col-6">{getValueOf(dateDisplayVietnamese(stockProduct.updatedDate))}</dd>
                    </dl>
                </CardBody>
            </Card>
        );
    }
}

const getWarehouseLink = (row) => {
    if (row.warehouse != null && row.warehouse.warehouseId != null) {
        return <Link to={`${ROUTE_WAREHOUSE_DETAILS}/${row.warehouse.warehouseId}`}
                     target='_blank'>{getValueOf(row.warehouse.name)}</Link>
    }
    return getValueOf(row.warehouse.name);
}

export default withRouter(StockProductDetailCard);