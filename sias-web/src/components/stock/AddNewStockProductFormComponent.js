import React, { Component } from 'react';
import { Button, Label, Col, Row, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Form, Control, Errors, actions } from 'react-redux-form';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { ROUTE_HOME, VALIDATION_MSG_REQUIRED, STOCK_PRODUCT_STATUS_GOOD, STOCK_PRODUCT_STATUS_MEDIUM, STOCK_PRODUCT_STATUS_BAD, VALIDATION_MSG_INVALID_BAR_CODE, VALIDATION_MSG_INVALID_ITEM_PRICE, VALIDATION_MSG_INVALID_ITEM_LENGTH, SUCCESS_MESSAGE_ADD_NEW_STOCK_PRODUCT, ROUTE_STOCK_PRODUCT_DETAILS, VALIDATION_MSG_INVALID_SKU, VALIDATION_MSG_MAX_LENGTH, VALIDATION_MSG_EXACT_LENGTH, VALIDATION_MSG_INVALID_NAME, ROUTE_ADD_NEW_STOCK_PRODUCT, ROLE_FACTORY_MANAGER, ROLE_OFFICE_MANAGER } from '../../shared/constants';
import { required, requiredNumber, validBarCode, validPrice, validLength, validSKU, maxLength, validProductName, exactLength, inFuture } from '../../shared/fieldValidation';
import { getYearList, getMonthList, getDayList, mapParamsToObject } from '../../shared/utils';
import { postNewStockProduct, fetchSteelTypeList, fetchStockGroupList, fetchStockProductStandardList, fetchWarehouseSelectionList } from '../../redux/actions/ActionCreators';
import UnauthorizedAccessDisplay from '../common/UnauthorizedAccessDisplay';
import FormLabel from '../common/FormLabel';
import { Loading } from '../common/LoadingComponent';

class AddNewStockProductForm extends Component {
    constructor(props) {
        super(props);

        const steelInfos = this.props.steelInfos;
        const warehouseSelections = this.props.warehouseSelections;

        if (steelInfos.steelTypeList.length === 0 && !steelInfos.isSteelTypeLoaded) {
            this.props.fetchSteelTypeList();
        }
        if (steelInfos.stockGroupList.length === 0 && !steelInfos.isStockGroupLoaded) {
            this.props.fetchStockGroupList();
        }
        if (steelInfos.stockProductStandardList.length === 0 && !steelInfos.isStockProductStandardLoaded) {
            this.props.fetchStockProductStandardList();
        }
        if (warehouseSelections.selectionList.length === 0 && !warehouseSelections.isSteelTypeLoaded) {
            this.props.fetchWarehouseSelectionList();
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            selectedYear: "",
            selectedMonth: "",
            selectedDay: "",
        };
    }

    componentDidMount() {
        const search = window.location.search;
        const params = (new URLSearchParams(search)).toString();

        if (params != null && params !== '') {
            const paramsObject = mapParamsToObject(params);

            this.props.setDefaultInfoFromImageUpload(paramsObject);
        }

    }

    handleSubmit = async (values) => {
        const manufacturedDate = new Date(values.manufacturedYear, values.manufacturedMonth, values.manufacturedDay);
        if (parseInt(this.state.selectedMonth) !== manufacturedDate.getMonth()) {
            alert('Please fill in valid date!');
        } else if (inFuture(manufacturedDate)) {
            alert('Manufactured date cannot be in future!');
        }
        else {
            // alert(JSON.stringify(values));
            await this.props.postNewStockProduct(values);
            await setTimeout(() => {
                this.resetForm()
            }, 3000);
        }

    }

    resetForm = () => {
        this.props.resetNewStockProductForm();
    }

    mapYearListToOption = () => {
        return getYearList().map((year) => (
            <option
                key={year}
                value={year}
            >
                {year}
            </option>
        ));
    };

    mapMonthListToOption = () => {
        return getMonthList().map((month) => (
            <option
                key={month}
                value={month - 1}
            >
                {month}
            </option>
        ));
    };

    mapDayListToOption = () => {
        return getDayList(this.state.selectedMonth, this.state.selectedYear).map((day) => (
            <option
                key={day}
                value={day}
            >
                {day}
            </option>
        ));
    };

    mapSteelTypeListToOption = (list) => {
        return list.map((item) => (
            <option
                key={item.steeltypeId}
                value={item.steeltypeId}
            >
                {item.name}
            </option>
        ));
    };

    mapStockGroupListToOption = (list) => {
        return list.map((item) => (
            <option
                key={item.stockProductGroupId}
                value={item.stockProductGroupId}
            >
                {item.name}
            </option>
        ));
    };

    mapStockProductStandardListToOption = (list) => {
        return list.map((item) => (
            <option
                key={item.stockproductstandardId}
                value={item.stockproductstandardId}
            >
                {item.name}
            </option>
        ));
    };

    mapWarehouseToOption = () => {
        return this.props.warehouseSelections.selectionList.map((warehouseObj) => (
            <option
                key={warehouseObj.warehouseId}
                value={warehouseObj.warehouseId}
            >
                {warehouseObj.name}
            </option>
        ));
    };

    handleYearChange = (event) => {
        this.setState({
            selectedYear: event.target.value
        });
    }

    handleMonthChange = (event) => {
        this.setState({
            selectedMonth: event.target.value
        });
    }

    handleDayChange = (event) => {
        this.setState({
            selectedDay: event.target.value
        });
    }

    isSaveButtonEnabled() {
        return (
            this.state.selectedYear !== ''
            && this.state.selectedMonth !== ''
            && this.state.selectedDay !== ''
        );
    }

    render() {
        const account = this.props.loginAccount.account;
        if (account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first</h1>
                    </div>
                </div>
            );
        } else if (roleAccessible(account.roleId)) {
            const addedSuccessStatus = this.props.addedSuccessStatus;
            const errMess = this.props.errMess;
            const addedStockProductId = this.props.addedStockProductId;
            const isAddStockProductLoading = this.props.isAddStockProductLoading;

            const redirectLink = addedStockProductId != null ? ROUTE_STOCK_PRODUCT_DETAILS + '/' + addedStockProductId : '#';
            const successMessage = addedSuccessStatus ?
                <h5 className="text-success">{SUCCESS_MESSAGE_ADD_NEW_STOCK_PRODUCT} Click <Link to={redirectLink}>here</Link> to view stock product details.</h5>
                : null;

            return (
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                            <BreadcrumbItem>Stock Management</BreadcrumbItem>
                            <BreadcrumbItem><Link to={ROUTE_ADD_NEW_STOCK_PRODUCT}>Add New Stock Product</Link></BreadcrumbItem>
                            <BreadcrumbItem active>Add New Stock Product Form</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>Add New Stock Product</h3>
                            <hr />
                        </div>
                    </div>
                    {isAddStockProductLoading ? <Loading /> :
                        <div className="row row-content">
                            <div className="col-12">
                                {successMessage}
                                <h5 className="text-danger">{errMess != null ? errMess : ''}</h5>
                            </div>
                            <div className="col-12 col-md-9 col-xl-8">
                                <Form model="addNewStockProduct"
                                    onSubmit={(values) => this.handleSubmit(values)}
                                >
                                    <Row className="form-group">
                                        <Label htmlFor="sku" md={3}>
                                            <FormLabel text="SKU" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text model=".sku" id="sku" name="sku"
                                                className="form-control" validators={{ required, validSKU, maxLength: maxLength(20) }} />
                                            <Errors className="text-danger" model=".sku" show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }} />
                                            <Errors className="text-danger" model=".sku" show="touched"
                                                messages={{
                                                    validSKU: VALIDATION_MSG_INVALID_SKU,
                                                }} />
                                            <Errors className="text-danger" model=".sku" show="touched"
                                                messages={{
                                                    maxLength: VALIDATION_MSG_MAX_LENGTH(20)
                                                }} />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="barcode" md={3}>
                                            <FormLabel text="Bar code" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text model=".barcode" id="barcode" name="barcode"
                                                className="form-control" validators={{ required, validBarCode, exactLength: exactLength(12) }} />
                                            <Errors className="text-danger" model=".barcode" show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }} />
                                            <Errors className="text-danger" model=".barcode" show="touched"
                                                messages={{
                                                    validBarCode: VALIDATION_MSG_INVALID_BAR_CODE
                                                }} />
                                            <Errors className="text-danger" model=".barcode" show="touched"
                                                messages={{
                                                    exactLength: VALIDATION_MSG_EXACT_LENGTH(12)
                                                }} />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="name" md={3}>
                                            <FormLabel text="Product name" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text model=".name" id="name" name="name"
                                                className="form-control" validators={{ required, validProductName }} />
                                            <Errors className="text-danger" model=".name" show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }} />
                                            <Errors className="text-danger" model=".name" show="touched"
                                                messages={{
                                                    validProductName: VALIDATION_MSG_INVALID_NAME
                                                }} />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="price" md={3}>
                                            <FormLabel text="Price" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text model=".price" id="price" name="price"
                                                className="form-control" validators={{ requiredNumber, validPrice }} />
                                            <Errors className="text-danger" model=".price" show="touched"
                                                messages={{
                                                    requiredNumber: VALIDATION_MSG_REQUIRED,
                                                    validPrice: VALIDATION_MSG_INVALID_ITEM_PRICE
                                                }} />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="length" md={3}>
                                            <FormLabel text="Length" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.text model=".length" id="length" name="length"
                                                className="form-control" validators={{ requiredNumber, validLength }} />
                                            <Errors className="text-danger" model=".length" show="touched"
                                                messages={{
                                                    requiredNumber: VALIDATION_MSG_REQUIRED,
                                                    validLength: VALIDATION_MSG_INVALID_ITEM_LENGTH
                                                }} />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label sm={3}>
                                            <FormLabel text="Manufactured Date" required='true' />
                                        </Label>
                                        <Col sm={3} lg={2}>
                                            <Control.select model=".manufacturedDay"
                                                name="manufacturedDay"
                                                id="manufacturedDay"
                                                className="form-control"
                                                onChange={this.handleDayChange}
                                                defaultValue={''}
                                            >
                                                <option key='' value=''>
                                                    -DD-
                                        </option>
                                                {this.mapDayListToOption()}
                                            </Control.select>
                                        </Col>

                                        <Col sm={3} lg={2}>
                                            <Control.select model=".manufacturedMonth"
                                                name="manufacturedMonth"
                                                id="manufacturedMonth"
                                                className="form-control"
                                                onChange={this.handleMonthChange}
                                                defaultValue={''}
                                            >
                                                <option key='' value=''>
                                                    -MM-
                                        </option>
                                                {this.mapMonthListToOption()}
                                            </Control.select>
                                        </Col>

                                        <Col sm={3} lg={2}>
                                            <Control.select model=".manufacturedYear"
                                                name="manufacturedYear"
                                                id="manufacturedYear"
                                                className="form-control"
                                                onChange={this.handleYearChange}
                                                defaultValue={''}
                                            >
                                                <option key='' value=''>
                                                    -YYYY-
                                        </option>
                                                {this.mapYearListToOption()}
                                            </Control.select>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="statusId" md={3}>
                                            <FormLabel text="Status" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select model=".statusId" name="statusId" id="statusId"
                                                className="form-control">
                                                <option key={STOCK_PRODUCT_STATUS_GOOD} value={STOCK_PRODUCT_STATUS_GOOD}>Good</option>
                                                <option key={STOCK_PRODUCT_STATUS_MEDIUM} value={STOCK_PRODUCT_STATUS_MEDIUM}>Medium</option>
                                                <option key={STOCK_PRODUCT_STATUS_BAD} value={STOCK_PRODUCT_STATUS_BAD}>Bad</option>
                                            </Control.select>
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="type" md={3}>
                                            <FormLabel text="Type" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select model=".type" name="type" id="type"
                                                className="form-control" validators={{ required }}>
                                                <option key='' value=''>
                                                    -Type-
                                        </option>
                                                {this.mapSteelTypeListToOption(this.props.steelInfos.steelTypeList)}
                                            </Control.select>
                                            <Errors className="text-danger" model=".type" show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }} />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="stockGroupId" md={3}>
                                            <FormLabel text="Stock Group" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select model=".stockGroupId" name="stockGroupId" id="stockGroupId"
                                                className="form-control" validators={{ required }}>
                                                <option key='' value=''>
                                                    -Stock Group-
                                        </option>
                                                {this.mapStockGroupListToOption(this.props.steelInfos.stockGroupList)}
                                            </Control.select>
                                            <Errors className="text-danger" model=".stockGroupId" show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }} />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="standard" md={3}>
                                            <FormLabel text="Standard" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select model=".standard" name="standard" id="standard"
                                                className="form-control" validators={{ required }}>
                                                <option key='' value=''>
                                                    -Standard-
                                        </option>
                                                {this.mapStockProductStandardListToOption(this.props.steelInfos.stockProductStandardList)}
                                            </Control.select>
                                            <Errors className="text-danger" model=".standard" show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }} />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Label htmlFor="warehouseId" md={3}>
                                            <FormLabel text="Warehouse" required='true' />
                                        </Label>
                                        <Col md={9}>
                                            <Control.select model=".warehouseId" name="warehouseId" id="warehouseId"
                                                className="form-control" validators={{ required }}>
                                                <option key='' value=''>
                                                    -Warehouse-
                                        </option>
                                                {this.mapWarehouseToOption(this.props.warehouseSelections.selectionList)}
                                            </Control.select>
                                            <Errors className="text-danger" model=".warehouseId" show="touched"
                                                messages={{
                                                    required: VALIDATION_MSG_REQUIRED
                                                }} />
                                        </Col>
                                    </Row>

                                    <Row className="form-group">
                                        <Col>
                                            <Button disabled={!this.isSaveButtonEnabled()}
                                                color="warning"
                                                type="submit"
                                                className="submitButton"
                                            >
                                                Add Product
                                    </Button>

                                    &nbsp;
                                    <Button color="light"
                                                className="backButton"
                                                onClick={this.resetForm}
                                            >
                                                Reset
                                    </Button>
                                        </Col>
                                    </Row>
                                </Form>
                            </div>
                        </div>
                    }
                </div>
            );
        } else {
            return (
                <UnauthorizedAccessDisplay />
            );
        }
    }
}

const roleAccessible = (roleId) => {
    return roleId === ROLE_OFFICE_MANAGER || roleId === ROLE_FACTORY_MANAGER;
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        isAddStockProductLoading: state.stockProductDetail.isAddStockProductLoading,
        addedSuccessStatus: state.stockProductDetail.addedSuccessStatus,
        errMess: state.stockProductDetail.errMess,
        addedStockProductId: state.stockProductDetail.addedStockProductId,
        steelInfos: state.steelInfos,
        warehouseSelections: state.warehouseSelections
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        resetNewStockProductForm: () => { dispatch(actions.reset('addNewStockProduct')) },
        setDefaultInfoFromImageUpload: (values) => { dispatch(actions.merge("addNewStockProduct", values)) },
        postNewStockProduct: (values) => { dispatch(postNewStockProduct(values)) },
        fetchSteelTypeList: () => { dispatch(fetchSteelTypeList()) },
        fetchStockGroupList: () => { dispatch(fetchStockGroupList()) },
        fetchStockProductStandardList: () => { dispatch(fetchStockProductStandardList()) },
        fetchWarehouseSelectionList: () => { dispatch(fetchWarehouseSelectionList()) }
    });
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddNewStockProductForm));