import React, { useState } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Button, Label, Col, Row, Breadcrumb, BreadcrumbItem, Form, Input } from 'reactstrap';
import { ROUTE_HOME, ROUTE_ADD_NEW_STOCK_PRODUCT, ROLE_FACTORY_MANAGER, ROLE_OFFICE_MANAGER, ROUTE_ADD_NEW_STOCK_PRODUCT_FORM, LABEL_IMAGE_TYPE_NSMP, LABEL_IMAGE_TYPE_EN, LABEL_IMAGE_TYPE_BCP, LABEL_IMAGE_TYPE_NIKKEN, LABEL_IMAGE_TYPE_NIKKEN_DOC } from '../../shared/constants';
import UnauthorizedAccessDisplay from '../common/UnauthorizedAccessDisplay';
import { confirmAlert } from "react-confirm-alert";
import { uploadingImage, uploadImage } from '../../redux/actions/ActionCreators';
import qs from 'qs';
import { Uploading } from '../common/LoadingComponent';

function UploaderContent(props) {
    const [selectedFile, setSelectedFile] = useState(null);
    const [labelType, setLabelType] = useState(LABEL_IMAGE_TYPE_NSMP);

    const onLabelTypeChange = (event) => {
        setLabelType(event.target.value);
    }

    const onChangeHandler = (event) => {
        setSelectedFile(event.target.files[0] !== undefined ? event.target.files[0] : null)
    }

    const uploadButtonClicked = async () => {
        if (selectedFile == null) {
            confirmAlert({
                title: 'Information',
                message: `Please choose 1 file to upload.`,
                buttons: [
                    {
                        label: 'Yes',
                    }
                ]
            });
            return;
        }
        await props.uploadingImage();
        const formData = new FormData();
        formData.append('image', selectedFile);
        formData.append('option', labelType);
        await props.uploadImage(formData);
    }
    return (
        <Form>
            <Row className="form-group">
                <Label xs={3} lg={2} for="labelType">Label Type</Label>
                <Col xs={6} lg={4}>
                    <Input id="labelType" type="select"
                        name="labelType"
                        onChange={onLabelTypeChange}
                    >
                        <option key={LABEL_IMAGE_TYPE_NSMP} value={LABEL_IMAGE_TYPE_NSMP}>NSMP</option>
                        <option key={LABEL_IMAGE_TYPE_EN} value={LABEL_IMAGE_TYPE_EN}>EN</option>
                        <option key={LABEL_IMAGE_TYPE_BCP} value={LABEL_IMAGE_TYPE_BCP}>BCP</option>
                        <option key={LABEL_IMAGE_TYPE_NIKKEN} value={LABEL_IMAGE_TYPE_NIKKEN}>NIKKEN</option>
                        <option key={LABEL_IMAGE_TYPE_NIKKEN_DOC} value={LABEL_IMAGE_TYPE_NIKKEN_DOC}>NIKKEN-DOC</option>
                    </Input>
                </Col>
            </Row>
            <Row className="form-group">
                <Label xs={3} lg={2} for="imageFile">Image File</Label>
                <Col xs={9} lg={6}>
                    <Input id="imageFile" type="file" name="file"
                        onChange={onChangeHandler}
                        accept="image/*"
                    />
                </Col>
            </Row>
            <Row>
                <Col xs={3}>
                    <Button type="button" color="warning" onClick={uploadButtonClicked}>Upload</Button>
                </Col>
            </Row>
        </Form>
    )
}

function UploadStockProductImage(props) {
    const account = props.loginAccount.account;
    if (account == null) {
        return (
            <div className="container">
                <div className="row row-content">
                    <h1>You must login first</h1>
                </div>
            </div>
        );
    } else if (roleAccessible(account.roleId)) {
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                        <BreadcrumbItem>Stock Management</BreadcrumbItem>
                        <BreadcrumbItem><Link to={ROUTE_ADD_NEW_STOCK_PRODUCT}>Add New Stock Product</Link></BreadcrumbItem>
                        <BreadcrumbItem active>Add stock product by uploading images (OCR)</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>Add stock product by uploading images (OCR)</h3>
                        <hr />
                    </div>
                </div>
                <div className="row row-content">
                    <div className="col-12">
                        {upperContent(props.imageUpload)}
                        <div className="col-12">
                            <UploaderContent uploadImage={props.uploadImage} uploadingImage={props.uploadingImage} />
                        </div>
                    </div>
                </div>
            </div>
        );
    } else {
        return (
            <UnauthorizedAccessDisplay />
        );
    }
}

const upperContent = (imageUpload) => {
    if (imageUpload != null) {
        const errMess = imageUpload.errMess;
        const successStatus = imageUpload.successStatus;
        const dataToInsert = imageUpload.dataToInsert;
        if (errMess != null) {
            return <h5 className="text-danger">{errMess}</h5>
        } else if (successStatus) {
            return (
                <div>
                    <h5 className="text-success">
                        {'Image uploaded successfully. Click '}
                        <Link to={`${ROUTE_ADD_NEW_STOCK_PRODUCT_FORM}?${mapObjectToParams(dataToInsert)}`}>here</Link>
                        {' to complete adding new stock product.'}
                    </h5>
                </div>
            )
        } else if (imageUpload.isUploading) {
            return <Uploading />
        }
    }
    return <div></div>;
}

const mapObjectToParams = (dataToInsert) => {
    if (dataToInsert !== undefined && dataToInsert != null) {
        if (dataToInsert.type != null) {
            const values = {
                ...dataToInsert,
                type: dataToInsert.type.steeltypeId
            }
            return qs.stringify(values);
        } else {
            return qs.stringify(dataToInsert);
        }
    }
    return '';
}

const roleAccessible = (roleId) => {
    return roleId === ROLE_OFFICE_MANAGER || roleId === ROLE_FACTORY_MANAGER;
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        imageUpload: state.imageUpload
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        uploadImage: (formData) => {
            dispatch(uploadImage(formData));
        },
        uploadingImage: () => {
            dispatch(uploadingImage())
        }
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UploadStockProductImage));