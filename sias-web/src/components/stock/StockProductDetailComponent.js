import React, { useState } from 'react';
import { Breadcrumb, BreadcrumbItem, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { useEffect } from 'react';
import { fetchStockProductDetail } from '../../redux/actions/ActionCreators';
import {
    ROLE_OFFICE_MANAGER,
    ROUTE_HOME,
    ROLE_FACTORY_MANAGER,
    SUCCESS_MESSAGE_SUBMIT_GENERAL,
    ERROR_PAGE_NOT_FOUND
} from '../../shared/constants';
import { Loading } from '../common/LoadingComponent';
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import StockProductDetailCard from './StockProductDetailCardComponent';
import ModifyStockProductDetail from './ModifyStockProductDetailComponent';
import UnauthorizedAccessDisplay from '../common/UnauthorizedAccessDisplay';
import ErrorDisplay from '../common/ErrorDisplay';

function StockProductDetail(props) {
    const [isEditing, setEditing] = useState(false);
    const account = props.loginAccount.account;

    const toggleEdit = () => {
        setEditing(!isEditing);
    }

    useEffect(() => {
        const stockProductId = props.stockProductId;
        const currentStockProductId = props.currentlyViewedStockProduct.currentStockProductId;
        const errMess = props.stockProductDetail.errMess;

        const fetchStockProductData = async () => {
            await props.fetchStockProductDetail(stockProductId);
        }

        if (errMess == null && currentStockProductId !== stockProductId) {
            fetchStockProductData();
        }

    }, []);

    if (account == null) {
        return (
            <div className="container">
                <div className="row row-content">
                    <h1>You must login first</h1>
                </div>
            </div>
        );
    } else if (roleAccessible(account.roleId)) {
        const errMess = props.stockProductDetail.errMess;
        const stockProduct = props.stockProductDetail.stockProduct;
        const successStatus = props.stockProductDetail.successStatus;

        const breadCrumb = (
            <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to={ROUTE_HOME}>Home</Link></BreadcrumbItem>
                    <BreadcrumbItem>Stock Management</BreadcrumbItem>
                    <BreadcrumbItem><Link to="/viewstockproductlist">View Stock Products</Link></BreadcrumbItem>
                    <BreadcrumbItem active>Stock Product Details</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>Stock Product Details</h3>
                    <hr />
                </div>
            </div>
        );

        if (stockProduct == null) {
            if (errMess == null) {
                return (
                    <div className="container">
                        {breadCrumb}
                        <div className="row row-content">
                            <Loading />
                        </div>
                    </div>
                );
            } else {
                return <ErrorDisplay errorType={ERROR_PAGE_NOT_FOUND} />
            }
        }
        const content = (value) => {
            if (value) {
                return (
                    <div className="col-12 col-md-9 col-xl-8">
                        <ModifyStockProductDetail account={account} stockProduct={stockProduct} toggleEdit={toggleEdit} />
                    </div>
                );
            } else {
                return (
                    <React.Fragment>
                        <div className="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                            <StockProductDetailCard stockProduct={stockProduct} account={account} />
                            <div className="mt-3">
                                {isStockProductEditable(account.roleId) &&
                                    <Button color="warning" onClick={toggleEdit}>Edit</Button>
                                }
                            </div>
                        </div>
                    </React.Fragment>
                );
            }
        }

        return (
            <div className="container">
                {breadCrumb}

                <div className="row row-content">
                    <div className="col-12">
                        <h5 className="text-success">{successStatus ? SUCCESS_MESSAGE_SUBMIT_GENERAL : ''}</h5>
                        <h5 className="text-danger">{errMess != null ? errMess : ''}</h5>
                    </div>
                    {content(isEditing)}
                </div>
            </div>
        );
    } else {
        return (
            <UnauthorizedAccessDisplay />
        );
    }
}

const isStockProductEditable = (roleId) => {
    return roleId === ROLE_OFFICE_MANAGER || roleId === ROLE_FACTORY_MANAGER;
}

const roleAccessible = (roleId) => {
    return roleId === ROLE_OFFICE_MANAGER || roleId === ROLE_FACTORY_MANAGER;
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        stockProducts: state.stockProducts,
        stockProductDetail: state.stockProductDetail,
        currentlyViewedStockProduct: state.currentlyViewedStockProduct
    };
}


const mapDispatchToProps = (dispatch) => {
    return ({
        fetchStockProductDetail: (stockProductId) => {
            dispatch(fetchStockProductDetail(stockProductId))
        }
    });
}

export default connect(mapStateToProps, mapDispatchToProps)(StockProductDetail);