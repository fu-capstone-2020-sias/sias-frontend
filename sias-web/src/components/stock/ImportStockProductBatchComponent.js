import React, { Component } from "react";
import {
    Breadcrumb,
    BreadcrumbItem,
    Label,
    Button,
    Form,
} from "reactstrap";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import {
    ROLE_FACTORY_MANAGER,
    ROLE_OFFICE_MANAGER,
    ROUTE_ADD_NEW_STOCK_PRODUCT,
    ROUTE_HOME,
    ROUTE_STEEL_INFOS_LOOKUP
} from "../../shared/constants";
import { Loading } from "../common/LoadingComponent";
import { baseUrl } from "../../shared/baseUrl";
import { clearStockProductBatch, importStockProductBatch } from "../../redux/actions/ActionCreators";
import { confirmAlert } from "react-confirm-alert";
import UnauthorizedAccessDisplay from "../common/UnauthorizedAccessDisplay";

class ImportStockProductBatch extends Component {
    state = {
        account: null,
        selectedFile: null,
        isUploading: false
    }

    onChangeHandler = (event) => {
        // console.log("onChangeHandler");
        // console.log(event.target.files[0])
        this.setState({
            selectedFile: event.target.files[0] !== undefined ? event.target.files[0] : null,
        })
    }

    onClickHandler = async () => {
        if (this.state.selectedFile == null) {
            confirmAlert({
                title: 'Information',
                message: `Please choose 1 file to upload.`,
                buttons: [
                    {
                        label: 'Yes',
                    }
                ]
            });
            return;
        }

        await this.props.clearStockProductBatch();
        await this.setState({
            isUploading: true
        })
        const formData = new FormData()
        // console.log("Selected File");
        // console.log(this.state.selectedFile);
        formData.append('csvFile', this.state.selectedFile)
        await this.props.importStockProductBatch(formData);

        // Clear selected file
        this.setState({
            selectedFile: null
        })
        document.getElementById("filePicker").value = ""
    }

    mapErrMessToParagraph = (errMessArr) => {
        let tempArr = [];
        // console.log("errMessArr");
        // console.log(errMessArr);

        if (errMessArr[0] != null && errMessArr[0] === "File data must be csv file") {
            tempArr.push(<p style={{ color: "red" }}>{errMessArr}</p>)
            return tempArr;
        }

        if (errMessArr[0] != null && typeof errMessArr[0] !== 'object') {
            // console.log("errMess[0] check");
            // console.log(errMessArr[0]);
            // console.log("errMessArr[0].indexOf(\"File Header\")");
            // console.log(errMessArr[0].indexOf("File Header"));
        }

        if (errMessArr[0] != null && typeof errMessArr[0] !== 'object' && errMessArr[0].indexOf("File Header") !== -1) {
            for (let i = 0; i < errMessArr.length; i++) {
                tempArr.push(<p style={{ color: "red" }}>{errMessArr[i]}</p>)
            }

            return tempArr;
        }


        // console.log("errMessArr mapErrMessToPara");
        for (let j = 0; j < errMessArr.length; j++) {
            for (let property in errMessArr[j]) {
                if (property === "row") {
                    break;
                }
                let rowNumber = errMessArr[j]["row"];
                // console.log("property errMess");
                // console.log(property);
                // console.log(errMessArr[0][property]);
                if (Array.isArray(errMessArr[0][property])) {
                    for (let i = 0; i < errMessArr[0][property].length; i++) {
                        tempArr.push(<p style={{ color: "red" }}>Row {rowNumber}: {errMessArr[0][property][i]}</p>)
                    }
                } else
                    tempArr.push(<p style={{ color: "red" }}>Row {rowNumber}: {errMessArr[0][property]}</p>)
            }
        }
        return tempArr;
    }

    async componentDidMount() {

        const account = this.props.loginAccount.account;
        this.setState({
            account: account
        });
    }

    render() {
        if (this.state.account == null) {
            return (
                <div className="container">
                    <div className="row row-content">
                        <h1>You must login first.</h1>
                    </div>
                </div>
            );
        } else if (this.state.account.roleId !== ROLE_OFFICE_MANAGER
            && this.state.account.roleId !== ROLE_FACTORY_MANAGER) {
            return (
                <UnauthorizedAccessDisplay />
            );
        } else {
            if (this.props.importStockProductBatchs.errMess !== null
                || this.props.importStockProductBatchs.successMessage !== null) {
                // console.log("Import Order Batchs");
                // console.log(this.props.importStockProductBatchs.errMess);
                if (this.state.isUploading) {
                    this.setState({
                        isUploading: false
                    })
                }
            }

            return (
                <div className="container" style={{ position: "relative" }}>
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem>
                                <Link to={ROUTE_HOME}>Home</Link>
                            </BreadcrumbItem>
                            <BreadcrumbItem>Stock Management</BreadcrumbItem>
                            <BreadcrumbItem><Link to={ROUTE_ADD_NEW_STOCK_PRODUCT}>Add New Stock
                                Product</Link></BreadcrumbItem>
                            <BreadcrumbItem active>Import Stock Product Batch</BreadcrumbItem>
                        </Breadcrumb>
                    </div>
                    <div className="row">
                        <div className="col-12" style={{ display: "flex", justifyContent: "space-between" }}>
                            <h3>Import Stock Product Batch</h3>
                        </div>
                        <div className="col-12">
                            <hr />
                        </div>
                        <div className="col-12">
                            <div style={{ marginBottom: "20px" }}>
                                <center>
                                    <Form action={`${baseUrl}stockProduct/template`} method="post">
                                        <Label className="text-danger">
                                            {"<!!!>"} Caution: Before uploading, you need to
                                            </Label>
                                        <Button className='ml-1 mr-1'
                                            style={{ fontSize: 14, padding: 0, lineHeight: 1 }}
                                            color="link" key={Date.now()} type="submit">
                                            download standard CSV file
                                            </Button>
                                        <Label className="text-danger">
                                            from server. This is required by company policy.
                                        </Label>
                                    </Form>
                                    <Label className="text-danger">In case you don't know the steel information codes (steel type ID, standard ID, warehouse ID, etc.), you can <Link to={ROUTE_STEEL_INFOS_LOOKUP} target={"_blank"}>lookup here.</Link></Label>
                                </center>
                            </div>
                            <div className="d-flex justify-content-center"
                                style={{
                                    marginBottom: "20px"
                                }}>
                                <div className="col-md-6 p-0">
                                    <div style={{ marginBottom: "10px" }}>
                                        <input id={"filePicker"} type="file" name="file"
                                            onChange={this.onChangeHandler} />
                                    </div>
                                    <div>
                                        <button type="button" className="btn" style={{ backgroundColor: "#ffb74d" }}
                                            onClick={this.onClickHandler}>Upload
                                        </button>
                                    </div>
                                </div>

                                <div className="col-md-6 p-0">
                                    <Label style={{ display: "block" }}>
                                        Message(s)
                                    </Label>

                                    {!this.state.isUploading ?
                                        <div
                                            disabled={true}
                                            style={{
                                                height: '250px',
                                                width: '100%',
                                                overflowY: 'scroll',
                                                backgroundColor: "#eef1f4",
                                                padding: "10px"
                                            }}>
                                            {this.props.importStockProductBatchs.successMessage !== null ?
                                                <p style={{ color: "#4caf50" }}>
                                                    {this.props.importStockProductBatchs.successMessage}
                                                </p> :
                                                (this.props.importStockProductBatchs.errMess !== null ?
                                                    this.mapErrMessToParagraph(this.props.importStockProductBatchs.errMess) : "")
                                            }
                                        </div> : <Loading />
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div >
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        loginAccount: state.loginAccount,
        importStockProductBatchs: state.importStockProductBatch
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        importStockProductBatch: (formData) => {
            dispatch(importStockProductBatch(formData));
        },
        clearStockProductBatch: () => {
            dispatch(clearStockProductBatch())
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ImportStockProductBatch);
