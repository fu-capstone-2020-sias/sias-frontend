import React, { Component } from 'react';
import { Button, Label, Col, Row } from 'reactstrap';
import { Form, Control, Errors, actions } from 'react-redux-form';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {
    VALIDATION_MSG_REQUIRED,
    STOCK_PRODUCT_STATUS_GOOD,
    STOCK_PRODUCT_STATUS_MEDIUM,
    STOCK_PRODUCT_STATUS_BAD,
    VALIDATION_MSG_INVALID_BAR_CODE,
    VALIDATION_MSG_INVALID_ITEM_PRICE,
    VALIDATION_MSG_INVALID_ITEM_LENGTH,
    VALIDATION_MSG_INVALID_SKU,
    VALIDATION_MSG_EXACT_LENGTH,
    VALIDATION_MSG_MAX_LENGTH,
    VALIDATION_MSG_INVALID_PRODUCT_NAME
} from '../../shared/constants';
import {
    required,
    requiredNumber,
    validProductName,
    validBarCode,
    validPrice,
    validLength,
    validSKU,
    exactLength,
    maxLength,
    inFuture
} from '../../shared/fieldValidation';
import { getYearList, getMonthList, getDayList } from '../../shared/utils';
import { putModifyStockProductDetail, fetchSteelTypeList, fetchStockGroupList, fetchStockProductStandardList, fetchWarehouseSelectionList } from '../../redux/actions/ActionCreators';
import FormLabel from '../common/FormLabel';

class ModifyStockProductDetail extends Component {
    constructor(props) {
        super(props);

        const steelInfos = this.props.steelInfos;
        const warehouseSelections = this.props.warehouseSelections;

        if (steelInfos.steelTypeList.length === 0 && !steelInfos.isSteelTypeLoaded) {
            this.props.fetchSteelTypeList();
        }
        if (steelInfos.stockGroupList.length === 0 && !steelInfos.isStockGroupLoaded) {
            this.props.fetchStockGroupList();
        }
        if (steelInfos.stockProductStandardList.length === 0 && !steelInfos.isStockProductStandardLoaded) {
            this.props.fetchStockProductStandardList();
        }
        if (warehouseSelections.selectionList.length === 0 && !warehouseSelections.isSteelTypeLoaded) {
            this.props.fetchWarehouseSelectionList();
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            selectedYear: "",
            selectedMonth: "",
            selectedDay: "",
            firstLoad: true,
        };
    }

    componentDidMount() {
        const manufacturedDate = new Date(this.props.stockProduct.manufacturedDate);
        this.setState({
            selectedYear: manufacturedDate.getFullYear(),
            selectedMonth: manufacturedDate.getMonth(),
            selectedDay: manufacturedDate.getDate()
        });
    }

    handleSubmit = (values) => {
        const manufacturedDate = new Date(values.manufacturedYear, values.manufacturedMonth, values.manufacturedDay);
        if (parseInt(this.state.selectedMonth) !== manufacturedDate.getMonth()) {
            alert('Please fill in valid date!');
        } else if (inFuture(manufacturedDate)) {
            alert('Manufactured date cannot be in future!');
        } else {
            this.props.putModifyStockProductDetail(values);
        }
    }

    mapYearListToOption = () => {
        return getYearList().map((year) => (
            <option
                key={year}
                value={year}
            >
                {year}
            </option>
        ));
    };

    mapMonthListToOption = () => {
        return getMonthList().map((month) => (
            <option
                key={month}
                value={month - 1}
            >
                {month}
            </option>
        ));
    };

    mapDayListToOption = () => {
        return getDayList(this.state.selectedMonth, this.state.selectedYear).map((day) => (
            <option
                key={day}
                value={day}
            >
                {day}
            </option>
        ));
    };

    mapSteelTypeListToOption = (list) => {
        return list.map((item) => (
            <option
                key={item.steeltypeId}
                value={item.steeltypeId}
            >
                {item.name}
            </option>
        ));
    };

    mapStockGroupListToOption = (list) => {
        return list.map((item) => (
            <option
                key={item.stockProductGroupId}
                value={item.stockProductGroupId}
            >
                {item.name}
            </option>
        ));
    };

    mapStockProductStandardListToOption = (list) => {
        return list.map((item) => (
            <option
                key={item.stockproductstandardId}
                value={item.stockproductstandardId}
            >
                {item.name}
            </option>
        ));
    };

    mapWarehouseToOption = () => {
        return this.props.warehouseSelections.selectionList.map((warehouseObj) => (
            <option
                key={warehouseObj.warehouseId}
                value={warehouseObj.warehouseId}
            >
                {warehouseObj.name}
            </option>
        ));
    };

    handleYearChange = (event) => {
        this.setState({
            selectedYear: event.target.value
        });
    }

    handleMonthChange = (event) => {
        this.setState({
            selectedMonth: event.target.value
        });
    }

    handleDayChange = (event) => {
        this.setState({
            selectedDay: event.target.value
        });
    }

    isSaveButtonEnabled() {
        return (
            this.state.selectedYear !== ''
            && this.state.selectedMonth !== ''
            && this.state.selectedDay !== ''
        );
    }

    render() {
        let stockProduct = this.props.stockProduct;

        if (stockProduct == null) {
            return (<div></div>);
        }

        if (this.state.firstLoad) {
            this.props.setDefaultEditStockProductDetail(stockProduct);
            this.setState({
                firstLoad: false
            })
        }

        return (
            <Form model="editStockProductDetail"
                onSubmit={(values) => this.handleSubmit(values)}
            >
                <Row className="form-group">
                    <Label htmlFor="stockProductId" md={3}>Stock Product ID</Label>
                    <Col md={9}>
                        <Control.text model=".stockProductId" id="stockProductId" name="stockProductId"
                            className="form-control"
                            value={stockProduct.stockProductId}
                            disabled />

                    </Col>
                </Row>

                <Row className="form-group">
                    <Label htmlFor="sku" md={3}>
                        <FormLabel text="SKU" required='true' />
                    </Label>
                    <Col md={9}>
                        <Control.text model=".sku" id="sku" name="sku"
                            className="form-control" validators={{ required, validSKU, maxLength: maxLength(20) }} />
                        <Errors className="text-danger" model=".sku" show="touched"
                            messages={{
                                required: VALIDATION_MSG_REQUIRED
                            }} />
                        <Errors className="text-danger" model=".sku" show="touched"
                            messages={{
                                validSKU: VALIDATION_MSG_INVALID_SKU
                            }} />
                        <Errors className="text-danger" model=".sku" show="touched"
                            messages={{
                                maxLength: VALIDATION_MSG_MAX_LENGTH(20)
                            }} />
                    </Col>
                </Row>

                <Row className="form-group">
                    <Label htmlFor="barcode" md={3}>
                        <FormLabel text="Bar code" required='true' />
                    </Label>
                    <Col md={9}>
                        <Control.text model=".barcode" id="barcode" name="barcode"
                            className="form-control" validators={{ required, validBarCode, exactLength: exactLength(12) }} />
                        <Errors className="text-danger" model=".barcode" show="touched"
                            messages={{
                                required: VALIDATION_MSG_REQUIRED
                            }} />
                        <Errors className="text-danger" model=".barcode" show="touched"
                            messages={{
                                validBarCode: VALIDATION_MSG_INVALID_BAR_CODE
                            }} />
                        <Errors className="text-danger" model=".barcode" show="touched"
                            messages={{
                                exactLength: VALIDATION_MSG_EXACT_LENGTH(12)
                            }} />
                    </Col>
                </Row>

                <Row className="form-group">
                    <Label htmlFor="name" md={3}>
                        <FormLabel text="Product name" required='true' />
                    </Label>
                    <Col md={9}>
                        <Control.text model=".name" id="name" name="name"
                            className="form-control" validators={{ required, validProductName }} />
                        <Errors className="text-danger" model=".name" show="touched"
                            messages={{
                                required: VALIDATION_MSG_REQUIRED
                            }} />
                        <Errors className="text-danger" model=".name" show="touched"
                            messages={{
                                validProductName: VALIDATION_MSG_INVALID_PRODUCT_NAME
                            }} />
                    </Col>
                </Row>

                <Row className="form-group">
                    <Label htmlFor="price" md={3}>
                        <FormLabel text="Price" required='true' />
                    </Label>
                    <Col md={9}>
                        <Control.text model=".price" id="price" name="price"
                            className="form-control" validators={{ requiredNumber, validPrice }} />
                        <Errors className="text-danger" model=".price" show="touched"
                            messages={{
                                requiredNumber: VALIDATION_MSG_REQUIRED,
                                validPrice: VALIDATION_MSG_INVALID_ITEM_PRICE
                            }} />
                    </Col>
                </Row>

                <Row className="form-group">
                    <Label htmlFor="length" md={3}>
                        <FormLabel text="Length" required='true' />
                    </Label>
                    <Col md={9}>
                        <Control.text model=".length" id="length" name="length"
                            className="form-control" validators={{ requiredNumber, validLength }} />
                        <Errors className="text-danger" model=".length" show="touched"
                            messages={{
                                requiredNumber: VALIDATION_MSG_REQUIRED,
                                validLength: VALIDATION_MSG_INVALID_ITEM_LENGTH
                            }} />
                    </Col>
                </Row>

                <Row className="form-group">
                    <Label sm={3}>
                        <FormLabel text="Manufactured Date" required='true' />
                    </Label>
                    <Col sm={3} lg={2}>
                        <Control.select model=".manufacturedDay"
                            name="manufacturedDay"
                            id="manufacturedDay"
                            className="form-control"
                            onChange={this.handleDayChange}
                            defaultValue={new Date(stockProduct.manufacturedDate).getDate()}
                        >
                            <option key='' value=''>
                                -DD-
                            </option>
                            {this.mapDayListToOption()}
                        </Control.select>
                    </Col>

                    <Col sm={3} lg={2}>
                        <Control.select model=".manufacturedMonth"
                            name="manufacturedMonth"
                            id="manufacturedMonth"
                            className="form-control"
                            onChange={this.handleMonthChange}
                            defaultValue={new Date(stockProduct.manufacturedDate).getMonth()}
                        >
                            <option key='' value=''>
                                -MM-
                            </option>
                            {this.mapMonthListToOption()}
                        </Control.select>
                    </Col>

                    <Col sm={3} lg={2}>
                        <Control.select model=".manufacturedYear"
                            name="manufacturedYear"
                            id="manufacturedYear"
                            className="form-control"
                            onChange={this.handleYearChange}
                            defaultValue={new Date(stockProduct.manufacturedDate).getFullYear()}
                        >
                            <option key='' value=''>
                                -YYYY-
                            </option>
                            {this.mapYearListToOption()}
                        </Control.select>
                    </Col>
                </Row>

                <Row className="form-group">
                    <Label htmlFor="statusId" md={3}>
                        <FormLabel text="Status" required='true' />
                    </Label>
                    <Col md={9}>
                        <Control.select model=".statusId" name="statusId" id="statusId"
                            className="form-control">
                            <option key={STOCK_PRODUCT_STATUS_GOOD} value={STOCK_PRODUCT_STATUS_GOOD}>Good</option>
                            <option key={STOCK_PRODUCT_STATUS_MEDIUM} value={STOCK_PRODUCT_STATUS_MEDIUM}>Medium</option>
                            <option key={STOCK_PRODUCT_STATUS_BAD} value={STOCK_PRODUCT_STATUS_BAD}>Bad</option>
                        </Control.select>
                    </Col>
                </Row>

                <Row className="form-group">
                    <Label htmlFor="type" md={3}>
                        <FormLabel text="Type" required='true' />
                    </Label>
                    <Col md={9}>
                        <Control.select model=".type" name="type" id="type"
                            className="form-control">
                            {this.mapSteelTypeListToOption(this.props.steelInfos.steelTypeList)}
                        </Control.select>
                    </Col>
                </Row>

                <Row className="form-group">
                    <Label htmlFor="stockGroupId" md={3}>
                        <FormLabel text="Stock Group" required='true' />
                    </Label>
                    <Col md={9}>
                        <Control.select model=".stockGroupId" name="stockGroupId" id="stockGroupId"
                            className="form-control">
                            {this.mapStockGroupListToOption(this.props.steelInfos.stockGroupList)}
                        </Control.select>
                    </Col>
                </Row>

                <Row className="form-group">
                    <Label htmlFor="standard" md={3}>
                        <FormLabel text="Standard" required='true' />
                    </Label>
                    <Col md={9}>
                        <Control.select model=".standard" name="standard" id="standard"
                            className="form-control">
                            {this.mapStockProductStandardListToOption(this.props.steelInfos.stockProductStandardList)}
                        </Control.select>
                    </Col>
                </Row>

                <Row className="form-group">
                    <Label htmlFor="warehouseId" md={3}>
                        <FormLabel text="Warehouse" required='true' />
                    </Label>
                    <Col md={9}>
                        <Control.select model=".warehouseId" name="warehouseId" id="warehouseId"
                            className="form-control">
                            {this.mapWarehouseToOption(this.props.warehouseSelections.selectionList)}
                        </Control.select>
                    </Col>
                </Row>

                <Row className="form-group">
                    <Col>
                        <Button disabled={!this.isSaveButtonEnabled()}
                            color="warning"
                            type="submit"
                            className="submitButton"
                        >
                            Save
                            </Button>

                        &nbsp;
                        <Button color="light"
                            className="backButton"
                            onClick={this.props.toggleEdit}
                        >
                            Back
                        </Button>
                    </Col>
                </Row>
            </Form>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        successStatus: state.stockProductDetail.successStatus,
        steelInfos: state.steelInfos,
        warehouseSelections: state.warehouseSelections
    };
}

const mapDispatchToProps = (dispatch) => {
    return ({
        setDefaultEditStockProductDetail: (values) => { dispatch(actions.merge("editStockProductDetail", values)) },
        putModifyStockProductDetail: (values) => { dispatch(putModifyStockProductDetail(values)) },
        fetchSteelTypeList: () => { dispatch(fetchSteelTypeList()) },
        fetchStockGroupList: () => { dispatch(fetchStockGroupList()) },
        fetchStockProductStandardList: () => { dispatch(fetchStockProductStandardList()) },
        fetchWarehouseSelectionList: () => { dispatch(fetchWarehouseSelectionList()) }
    });
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ModifyStockProductDetail));